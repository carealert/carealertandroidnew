package com.sensorcall.carealertandroid_new;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.ui.lovedones.EditLovedOnesActivity;
import com.sensorcall.carealertandroid_new.ui.lovedones.LovedOne;
import com.sensorcall.carealertandroid_new.util.RetrofitInstance;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Selectwho extends AppCompatActivity {
    TextView skip;
    EditText name;
    Button continue_button;
    ProgressDialog loaddialog, progressDialog;
    private LovedOne lovedOne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectwho);
        name = (EditText) findViewById(R.id.who_input);
        continue_button = (Button) findViewById(R.id.selectwho_continue);


        loaddialog = new ProgressDialog(this);
        loaddialog = showLoadingDialog(this);
        loaddialog.dismiss();


        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                continue_button.setEnabled(false);
                String name_string = name.getText().toString();
                SharedPreferences CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
                SharedPreferences.Editor CareAlert_SharedData_Editor = CareAlert_SharedData.edit();
                CareAlert_SharedData_Editor.putString("Name", name_string);
                CareAlert_SharedData_Editor.commit();
                checkNetConnectivity();
                // openselectionpage();
            }
        });


        lovedOne = getIntent().getParcelableExtra(Constants.LOVED_ONE);

        if (lovedOne != null) {
            name.setText(lovedOne.getProfileName());
        }
    }

    void checkNetConnectivity() {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet", "devic123");
            if (lovedOne != null) {
                updateLovedOne();
            } else {
                openselectionpage();
            }
        } else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateLovedOne() {
        lovedOne.setProfileName(name.getText().toString());
        loaddialog.show();
        Call<ResponseBody> call = RetrofitInstance.getRetrofitInstance(getApplicationContext()).updateLovedOnes(lovedOne);

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(Selectwho.this, R.string.update_failed, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    showSuccessDialog();
                }
                loaddialog.dismiss();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Selectwho.this, "Failed", Toast.LENGTH_SHORT).show();
                loaddialog.dismiss();

            }
        });

    }

    private void showSuccessDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.loved_one_updated)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    Intent editLovedOnesActivity = new Intent(this, EditLovedOnesActivity.class);
                    editLovedOnesActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);;
                    startActivity(editLovedOnesActivity);
                })
                .setCancelable(false)
                .show();
    }

    public void showNoInternetConnectionDialog() {
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void openselectionpage() {
        Intent selectionpage = new Intent(this, Selection.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(selectionpage);

    }

    public void opennameloactionpage() {
        Intent namelocationpage = new Intent(this, Namelocation.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(namelocationpage);

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}