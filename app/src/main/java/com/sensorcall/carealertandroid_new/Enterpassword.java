package com.sensorcall.carealertandroid_new;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.biometrics.BiometricManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Enterpassword extends AppCompatActivity {

    boolean reset;
    Retrofit retrofit;
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    String Password;
    boolean condition1_value=false,condition2_value=false,condition3_value=false;
    Button continue_button;
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    ProgressDialog loaddialog,progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterpassword);
        continue_button= (Button) findViewById(R.id.enterpassword_continue);

        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        CareAlert_SharedData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor=CareAlert_SharedData.edit();
        checkNetConnectivity();

/*
        final TextView condition1 =(TextView) findViewById(R.id.condition1);
        final TextView condition2 =(TextView) findViewById(R.id.condition2);
        final TextView condition3 =(TextView) findViewById(R.id.condition3);
        final EditText password_input= (EditText) findViewById(R.id.password_input1);

        password_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                char ch;
                int UC=0,SC=0;
                condition2.setBackgroundResource(R.drawable.checkbox_rectangle);
                condition3.setBackgroundResource(R.drawable.checkbox_rectangle);
                condition1.setBackgroundResource(R.drawable.checkbox_rectangle);
                condition1_value=false;
                condition2_value=false;
                condition2_value=false;
                Password=password_input.getText().toString();
                if (Password.length()>8){
                    condition1.setBackgroundResource(R.drawable.checkbox_positive);
                    condition1_value=true;
                }
                else{
                    condition1_value=false;
                }
                if(Password.length()==0){
                    condition1_value=false;
                    condition2_value=false;
                    condition2_value=false;
                }


                for(int i=0;i<Password.length();i++){
                    ch=Password.charAt(i);
                    if(Character.isUpperCase(ch)){
                        UC=UC+1;

                    }
                    if (!(Character.isLetterOrDigit(ch))){
                        SC=SC+1;
                    }
                }
                if(SC>0){
                    condition2.setBackgroundResource(R.drawable.checkbox_positive);
                    condition2_value=true;
                }else{
                    condition2_value=false;
                }

                if(UC>0){
                    condition3.setBackgroundResource(R.drawable.checkbox_positive);
                    condition3_value=true;
                }
                else{
                    condition3_value=false;
                }

            }
        });

        retrofit= new Retrofit.Builder()
                .baseUrl("https://carealertdev.azurewebsites.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);

        reset=CareAlert_SharedData.getBoolean("Reset",false);
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                continue_button.setEnabled(false);
                loaddialog.show();
                Log.d("Condtions", "onClick: 1"+condition1_value+" 2 "+ condition2_value+" 3 "+condition3_value);
                if (reset){
                    if (condition1_value && condition2_value && condition3_value) {
                        String phone_number = CareAlert_SharedData.getString("Phone_Number","");
                        Integer Otp = CareAlert_SharedData.getInt("Otp",0000);

                        Resetpasswordapicall(phone_number, Otp, Password);
                    }
                    else{
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Enterpassword.this, "Password doesn't meet all requirements", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if (condition1_value && condition2_value && condition3_value) {
                        CareAlert_SharedData_Editor.putString("Password",Password);
                        CareAlert_SharedData_Editor.commit();
                        Biometric_Check();
                        //Check for Biometric
                    }
                    else{
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Enterpassword.this, "Password doesn't meet all requirements", Toast.LENGTH_SHORT).show();

                    }
                }

            }
        });
            */
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            enterPassNet();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void enterPassNet(){
        final TextView condition1 =(TextView) findViewById(R.id.condition1);
        final TextView condition2 =(TextView) findViewById(R.id.condition2);
        final TextView condition3 =(TextView) findViewById(R.id.condition3);
        final EditText password_input= (EditText) findViewById(R.id.password_input1);

        password_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                char ch;
                int UC=0,SC=0;
                condition2.setBackgroundResource(R.drawable.checkbox_rectangle);
                condition3.setBackgroundResource(R.drawable.checkbox_rectangle);
                condition1.setBackgroundResource(R.drawable.checkbox_rectangle);
                condition1_value=false;
                condition2_value=false;
                condition2_value=false;
                Password=password_input.getText().toString();
                if (Password.length()>8){
                    condition1.setBackgroundResource(R.drawable.checkbox_positive);
                    condition1_value=true;
                }
                else{
                    condition1_value=false;
                }
                if(Password.length()==0){
                    condition1_value=false;
                    condition2_value=false;
                    condition2_value=false;
                }


                for(int i=0;i<Password.length();i++){
                    ch=Password.charAt(i);
                    if(Character.isUpperCase(ch)){
                        UC=UC+1;

                    }
                    if (!(Character.isLetterOrDigit(ch))){
                        SC=SC+1;
                    }
                }
                if(SC>0){
                    condition2.setBackgroundResource(R.drawable.checkbox_positive);
                    condition2_value=true;
                }else{
                    condition2_value=false;
                }

                if(UC>0){
                    condition3.setBackgroundResource(R.drawable.checkbox_positive);
                    condition3_value=true;
                }
                else{
                    condition3_value=false;
                }

            }
        });

        retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);

        reset=CareAlert_SharedData.getBoolean("Reset",false);
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                continue_button.setEnabled(false);
                loaddialog.show();
                Log.d("Condtions", "onClick: 1"+condition1_value+" 2 "+ condition2_value+" 3 "+condition3_value);
                if (reset){
                    if (condition1_value && condition2_value && condition3_value) {
                        String phone_number = CareAlert_SharedData.getString("Phone_Number","");
                        Integer Otp = CareAlert_SharedData.getInt("Otp",0000);

                        Resetpasswordapicall(phone_number, Otp, Password);
                    }
                    else{
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Enterpassword.this, "Password doesn't meet all requirements", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if (condition1_value && condition2_value && condition3_value) {
                        CareAlert_SharedData_Editor.putString("Password",Password);
                        CareAlert_SharedData_Editor.commit();
                        Biometric_Check();
                        //Check for Biometric
                    }
                    else{
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Enterpassword.this, "Password doesn't meet all requirements", Toast.LENGTH_SHORT).show();

                    }
                }

            }
        });
    }


    public void Resetpasswordapicall(String Number, int OTP, final String password){
        Call<Post> call= jsonPlaceHolderApi.createpostresetpass(Number,OTP,password);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (response.isSuccessful()) {
                    CareAlert_SharedData_Editor.putString("Password",password);
                    CareAlert_SharedData_Editor.putBoolean("Reset",false);
                    CareAlert_SharedData_Editor.commit();
                    EnterLoginPage();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String Error = jObjError.getString("message");
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Enterpassword.this, Error, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Enterpassword.this, "Unknown Error", Toast.LENGTH_SHORT).show();

                    }
                }
            }


            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                continue_button.setEnabled(true);
                loaddialog.dismiss();
                Toast.makeText(Enterpassword.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void EnterLoginPage(){
        Intent openLoginpage= new Intent(this,LoginScreen.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        CareAlert_SharedData_Editor.putBoolean("Reset",true);
        CareAlert_SharedData_Editor.commit();
        startActivity(openLoginpage);


    }
    public void EnterFingerprintPage(){
        Intent openFingerprintpage= new Intent(this,Fingerprint.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(openFingerprintpage);
    }
    public void EnterSelectionpage(){
        Intent openSelectionpage=new Intent(this,Deviceaddsteps.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(openSelectionpage);
    }

    public void openemailpage(){
        Intent openemail=new Intent(this,Enteremail.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(openemail);

    }
    public void Biometric_Check(){
        if(BiometricManager.BIOMETRIC_SUCCESS==0){
            EnterFingerprintPage();
        }
        else{
            updateuser("false");
        }
    }

    void updateuser(String fingerprint){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        String password=CareAlert_SharedData.getString("Password","");
        String email=CareAlert_SharedData.getString("Email","");
        String authToken = CareAlert_SharedData.getString("Auth_Token","");
        JsonObject body= new JsonObject();
        body.addProperty("Email",email);
        body.addProperty("Password",password);
        body.addProperty("FingerPrint",fingerprint);

        Call<Post> call = jsonPlaceHolderApi.updateuser(authToken,body);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(response.isSuccessful()){
                    CareAlert_SharedData_Editor.remove("Password");
                    CareAlert_SharedData_Editor.remove("Email");
                    CareAlert_SharedData_Editor.putBoolean("SignupProcess",true);
                    CareAlert_SharedData_Editor.commit();
                    EnterSelectionpage();

                }
                else{
                    continue_button.setEnabled(true);
                    loaddialog.dismiss();
                    Toast.makeText(Enterpassword.this, "Update User Failed", Toast.LENGTH_SHORT).show();



                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                continue_button.setEnabled(true);
                loaddialog.dismiss();
                Toast.makeText(Enterpassword.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}