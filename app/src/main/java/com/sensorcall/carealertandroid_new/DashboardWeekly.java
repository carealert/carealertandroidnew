package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.CustomBottomNavigationView1;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.GetDevicePojo;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.VoiceCallingScreen;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DashboardWeekly extends AppCompatActivity {

    LineChart Actchart,Comchart,Envchart;
    LineDataSet lineDataSet,lineDataSet1,lineDataSet2;
    LineData lineData,lineData2,lineData1;
    int locationId;
    String Auth_Token;
    ProgressBar inscreenloader;
    Retrofit retrofit;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    JsonPlaceHolder jsonPlaceHolderApi1;
    TextView DateText;
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    Float[] ActData,EnvData,ComData;
    String[] DayData= new String[7];
    private List<GetDevicePojo> pojos;
    ProgressDialog loader, progressDialog;
    Button DeviceButton[];
    Boolean fnccheck[];
    Drawable drawable,drawable1;
    boolean LoadingData=false;
    ArrayList activityentries=new ArrayList<>(),comentries=new ArrayList<>(),enventries=new ArrayList<>();
    ArrayList<String> xEntrys=new ArrayList<String>();
    JsonPlaceHolder jsonplaceholder;
    ImageView ActivityImage,ComfortImage,EnvironmentImage;
    TextView LocationName;
    TextView Message;
    String care_receiver_type, loc_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_weekly);
        DateText =(TextView) findViewById(R.id.wellness_subtitle2);
        Actchart=(LineChart) findViewById(R.id.activity_main_linechart);
        Comchart= (LineChart) findViewById(R.id.comfortchart);
        Envchart= (LineChart) findViewById(R.id.envchart);
        inscreenloader= (ProgressBar) findViewById(R.id.progressBar1);
        Message= (TextView) findViewById(R.id.wellness_subtitle);
        LocationName = (TextView) findViewById(R.id.location_name);
        CareAlert_SharedData= getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor= CareAlert_SharedData.edit();
        Auth_Token=CareAlert_SharedData.getString("Auth_Token","");


        loader= new ProgressDialog(this);
        loader=showLoadingDialog(this);

        inscreenloader.setVisibility(View.VISIBLE);

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.MINUTES)
                .connectTimeout(10, TimeUnit.MINUTES)
                .build();

        retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        jsonPlaceHolderApi1=retrofit.create(JsonPlaceHolder.class);
        jsonplaceholder = retrofit.create(JsonPlaceHolder.class);

        LoadArchiveData("DashBoard");
        //getDefaultLocationId();
        checkNetConnectivity();
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            getDefaultLocationId();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    private void getDefaultLocationId() {
        loader.dismiss();
        Log.d("default location", "into the location id after net connection");
        Call<List<GetLocation>> call = jsonplaceholder.getDefaultLocation("Bearer " + Auth_Token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if (response.isSuccessful()) {
                    Log.i("Succees", "onResponse: Yes");
                    List<GetLocation> get_list = response.body();
                    Log.d("Location Count", "onResponse: "+get_list.size());
                    if (get_list.isEmpty()) {
                        Toast.makeText(DashboardWeekly.this, "Default location is not set for this account", Toast.LENGTH_SHORT).show();
                        //loaddialog.dismiss();
                        inscreenloader.setVisibility(View.GONE);
                    } else {
                        for (int i = 0; i < get_list.size(); i++) {
                            if (get_list.get(i).isDefaultLocationId()) {
                                locationId = get_list.get(i).getLocationId();
                                loc_name = get_list.get(i).getLocationName();
                                LocationName.setText(loc_name);
                                Log.d("Location Name", "onResponse: "+loc_name);
                                Log.d("Default Location Id is:", "onResponse: "+locationId);
                                List<LovedOnes> Lovedonelist= get_list.get(i).getLovedOne();
                                Log.d("LoveOneList is", "onResponse: "+Lovedonelist.size());
                                for(int j=0;j<Lovedonelist.size();j++) {
                                    if(Lovedonelist.get(j).isPrimary()){
                                        care_receiver_type = Lovedonelist.get(j).getProfileName();
                                        Message.setText("How is " + care_receiver_type + " doing?");

                                        break;
                                    }
                                }
                                getPojo();
                                getDashboardData(false,locationId,"DashBoard");
                                break;
                            }

                            //Log.i("Location Mil Gayi", "Abc " + Integer.toString(locationId));

                        }
                    }
                    if(locationId==0){
                        Toast.makeText(DashboardWeekly.this, "No Location Set", Toast.LENGTH_SHORT).show();
                        inscreenloader.setVisibility(View.GONE);
                        //loaddialog.dismiss();
                    }
                    else{
                        Log.i("Default Location is:", "onResponse: "+ locationId);
                    }

                } else {
                    Toast.makeText(DashboardWeekly.this, "LocationId Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.d("res error", "resuis: " + response.errorBody().toString());
                    inscreenloader.setVisibility(View.GONE);
                    //loaddialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                Toast.makeText(DashboardWeekly.this, "No Default location set", Toast.LENGTH_SHORT).show();
                Log.d("failure12345", "error in fail");
                //loaddialog.dismiss();
                inscreenloader.setVisibility(View.GONE);
            }
        });
    }

/*
    private void getDefaultLocationId(){
        loader.show();
        Log.d("default location","into the location id after net connection");
        String Auth_token=CareAlert_SharedData.getString("Auth_Token","");
        Call<List<GetLocation>> call = jsonPlaceHolderApi1.getDefaultLocation("Bearer "+Auth_token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if(response.isSuccessful())
                {
                    Log.i("Succees", "onResponse: Yes");
                    List<GetLocation> get_list = response.body();
                    if(get_list.isEmpty()) {
                        Toast.makeText(DashboardWeekly.this, "Default location is not set for this account", Toast.LENGTH_SHORT).show();
                        loader.dismiss();
                    }
                    else{
                        for(int i=0;i<get_list.size();i++){
                            if(get_list.get(i).isDefaultLocationId()) {
                                locationId = get_list.get(i).getLocationId();
                                Log.i("Location Mil Gayi", "Abc " + Integer.toString(locationId));
                                getPojo();
                                getDashboardData(false,locationId,"DashBoard");
                            }
                        }
                    }
                }
                else{
                    Toast.makeText(DashboardWeekly.this, "LocationId Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.d("res error","resuis: "+response.errorBody().toString());
                    loader.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                Toast.makeText(DashboardWeekly.this, "No Default location set", Toast.LENGTH_SHORT).show();
                Log.d("failure12345","error in fail");
                loader.dismiss();
            }
        });
    }
*/

    void getPojo(){
        loader.show();
        //   final String auth = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIrOTE4Mjc1MjQ1MTM5IiwianRpIjoiYWIzMjc2OTktNDAzZi00YjBlLWE5NzgtODBlNmIyZTg4YjcxIiwiaWF0IjoxNjAxMjg3OTk3LCJyb2xlIjoiYXBpX2FjY2VzcyIsImlkIjoiMjZmMDVkOGItNDIyYS00Y2Y1LTk2NDUtMDQ0NzJmYzY4NThjIiwibmJmIjoxNjAxMjg3OTk2LCJleHAiOjE2MDE4OTI3OTYsImlzcyI6IndlYkFwaSIsImF1ZCI6Imh0dHA6Ly9jYXJlYWxlcnRkZXYuYXp1cmV3ZWJzaXRlcy5uZXQvIn0.5Dkswm5E78AESwzYrPIxlPvLK7IGU8fJV8-DQqqDMmM";
        String Auth_token=CareAlert_SharedData.getString("Auth_Token","");
        Log.d("Auth_token","Check---: "+Auth_token);
        Call<List<GetDevicePojo>> call = jsonPlaceHolderApi1.getPojo(locationId,"Bearer "+Auth_token);
        Log.d("location123","locationgetpojo: "+locationId);
        call.enqueue(new Callback<List<GetDevicePojo>>() {

            @Override  @RequiresApi(api = Build.VERSION_CODES.N)
            public void onResponse(Call<List<GetDevicePojo>> call, Response<List<GetDevicePojo>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(DashboardWeekly.this, "Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    loader.dismiss();
                    return;
                }
                else {
                    pojos = response.body();
                    pojos.sort(Comparator.comparing(GetDevicePojo::getDeviceProfileName));
                    Log.d("Response_","pojos=="+ pojos.toString());
                    setUpUI();
                }
            }
            @Override
            public void onFailure(Call<List<GetDevicePojo>> call, Throwable t) {
                Toast.makeText(DashboardWeekly.this, "Failed", Toast.LENGTH_SHORT).show();
                loader.dismiss();
            }
        });
    }
    @SuppressLint("ResourceType")
    private void setUpUI() {
        Resources res = getResources();
        drawable= ResourcesCompat.getDrawable(res, R.drawable.add_more, null);
        drawable1 = ResourcesCompat.getDrawable(res, R.drawable.add_more1, null);

        LinearLayout layout= (LinearLayout) findViewById(R.id.DeviceButtons);
        if(pojos.size()>0) {
            DeviceButton= new Button[pojos.size()];
            fnccheck= new Boolean[pojos.size()];
            for(int i=0;i<pojos.size();i++){
                fnccheck[i]=false;
            }
            for (int i = 0; i < pojos.size(); i++) {

                    DeviceButton[i]= new Button(this);
                    LinearLayout.LayoutParams layoutParams =new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    DeviceButton[i].setLayoutParams(layoutParams);
                    layoutParams.setMargins(10,5,10,5);
                    DeviceButton[i].setHeight(20);
                    DeviceButton[i].setText(pojos.get(i).getDeviceProfileName());
                    DeviceButton[i].setTextColor(Color.parseColor("#71BBE2"));
                    DeviceButton[i].setBackgroundDrawable(drawable1);
                    DeviceButton[i].setTextSize(8);
                    DeviceButton[i].setAllCaps(false);
                    DeviceButton[i].setId(pojos.get(i).getDeviceId());
                    DeviceButton[i].setWidth(100);
                    DeviceButton[i].setHeight(10);
                    DeviceButton[i].setPadding(2,2,2,2);  // Text cut button
                    layout.addView(DeviceButton[i]);

                    String DeviceName=pojos.get(i).getDeviceProfileName();
                    int Deviceid=pojos.get(i).getDeviceId();
                    Button btn= DeviceButton[i];
                    int currentbutton=i;
                    //    int deviceId = pojos.get(i).getDeviceId();
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            btn_call.setEnabled(true);

                            if (LoadingData) {
                                Toast.makeText(DashboardWeekly.this, "Loading Data from Server\nPlease Try Again Later", Toast.LENGTH_SHORT).show();
                            } else {
                                inscreenloader.setVisibility(View.VISIBLE);
                                if (fnccheck[currentbutton]) {
                                    btn.setTextColor(Color.parseColor("#71BBE2"));
                                    btn.setBackgroundDrawable(drawable1);
                                    LoadArchiveData("DashBoard");
                                    getDashboardData(false, locationId, "DashBoard");
                                    fnccheck[currentbutton] = false;

                                } else {
                                    btn.setTextColor(Color.parseColor("#ffffff"));
                                    btn.setBackgroundDrawable(drawable);
                                    LoadArchiveData(DeviceName);
                                    getDashboardData(true, Deviceid, DeviceName);
                                    fnccheck[currentbutton] = true;
                                    for(int i=0;i<pojos.size();i++){
                                        if(i==currentbutton)
                                            continue;
                                        fnccheck[i]=false;
                                    }
                                    UncheckButton(currentbutton);
                                }
                            }
                        }
                    });

            }
        }
        else{
            Toast.makeText(DashboardWeekly.this, "Devices has not installed at this location", Toast.LENGTH_SHORT).show();
        }

        loader.dismiss();
    }

    public void UncheckButton(int currbtn){
        for(int i=0;i<pojos.size();i++){
            if(i==currbtn)
                continue;
            DeviceButton[i].setBackground(drawable1);
            DeviceButton[i].setTextColor(Color.parseColor("#71BBE2"));
        }
    }


    public void LoadArchiveData(String Name){
        activityentries=new ArrayList<>();
        comentries=new ArrayList<>();
        enventries=new ArrayList<>();
        String OldSpan= CareAlert_SharedData.getString("DateWeekly"+Name,"-");
        DateText.setText(OldSpan);
        ActData= getCachedData("Activity_Trend"+Name);
        EnvData=    getCachedData("Env_Trend"+Name);
        ComData=    getCachedData("Com_Trend"+Name);
        String str = CareAlert_SharedData.getString("Day_Trend"+Name, null);
        if(str!=null) {
            String str1[] = str.split(",");
            // at i=0 it is space so start from 1
            for (int i = 1; i < str1.length; i++) {
                DayData[i - 1] = str1[i];
            }
        }
        if(!(ActData==null)){
            for(int i=0;i<7;i++){
                activityentries.add(new Entry(i,ActData[i]));
                comentries.add(new Entry(i,ComData[i]));
                enventries.add(new Entry(i,EnvData[i]));
                xEntrys.add(DayData[i]);
            }
        }
        setupLinecharts();
    }
    public void getDashboardData(Boolean Device, int Id, String Name) {
        LoadingData=true;
        //int currentTimestamp = (int) System.currentTimeMillis()/1000;
        long Timestamp= System.currentTimeMillis();
        int currentTimestamp =(int)(Timestamp/1000);
        int TillTime = currentTimestamp -(86400*7);
        Call<List<Post>> call;
        if(!Device) {
            call = jsonPlaceHolderApi.getdashboardtrends("Bearer " + Auth_Token, Id, null, currentTimestamp, TillTime);
        }
        else{
            call = jsonPlaceHolderApi.getdashboardtrends("Bearer " + Auth_Token, null, Id, currentTimestamp, TillTime);

        }
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    SimpleDateFormat sdf=new SimpleDateFormat("MMM dd, yyyy");
                    String currentDateandTime = sdf.format(new Date());
                    try {
                        Date cdate=sdf.parse(currentDateandTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar now2= Calendar.getInstance();
                    now2.add(Calendar.DATE, -7);
                    //String beforedate=now2.get(Calendar.DATE)+"/"+(now2.get(Calendar.MONTH) + 1)+"/"+now2.get(Calendar.YEAR);
                    String beforedate= sdf.format(now2.getTime());
                    DateText.setText(beforedate+" to "+currentDateandTime);
                    xEntrys= new ArrayList<>();
                    activityentries= new ArrayList();
                    comentries=new ArrayList();
                    enventries= new ArrayList();
                    ActData=new Float[7];
                    ComData= new Float[7];
                    EnvData = new Float[7];
                    DayData = new String[7];
                    Log.i("Dashboard Response", "onResponse: "+ response.body().toString());
                    List<Post> DashboardData=response.body();
                    for(int i=0;i<DashboardData.size();i++){
                        activityentries.add(new Entry(i,DashboardData.get(i).getActivity()));
                        comentries.add(new Entry(i,DashboardData.get(i).getComfort()));
                        enventries.add(new Entry(i,DashboardData.get(i).getEnvironment()));
                        ActData[i]=DashboardData.get(i).getActivity();
                        ComData[i]=DashboardData.get(i).getComfort();
                        EnvData[i]=DashboardData.get(i).getEnvironment();
                        Date date1= null;
                        try {
                            date1 = new SimpleDateFormat("dd/MM/yyyy").parse(DashboardData.get(i).getDate());
                        } catch (ParseException e) {
                            Log.i("Failed TO Parse Date", "onResponse: SHIT");
                        }
                        DateFormat format2=new SimpleDateFormat("EEEE");
                        String finalDay=format2.format(date1);
                        xEntrys.add(Character.toString(finalDay.charAt(0)));
                        DayData[i]=Character.toString(finalDay.charAt(0));
                        putCachedData("Activity_Trend"+Name,ActData);
                        putCachedData("Env_Trend"+Name,EnvData);
                        putCachedData("Com_Trend"+Name,ComData);
                    }
                    String str = " ";
                    for(int i=0;i<DayData.length;i++){
                        str = str + ", "+ DayData[i];
                    }
                    CareAlert_SharedData_Editor.putString("Day_Trend"+Name,str);
                    CareAlert_SharedData_Editor.putString("DateWeekly"+Name,beforedate+" to "+currentDateandTime);
                    CareAlert_SharedData_Editor.commit();
                    setupLinecharts();
                    inscreenloader.setVisibility(View.GONE);
                    LoadingData=false;
                    //loaddialog.dismiss();
                }
                else{
                    Toast.makeText(DashboardWeekly.this, "Could Not get DashBoard Info", Toast.LENGTH_SHORT).show();

                    inscreenloader.setVisibility(View.GONE);
                    LoadingData=false;
                    //loaddialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Toast.makeText(DashboardWeekly.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                inscreenloader.setVisibility(View.GONE);
                LoadingData=false;
                //loaddialog.dismiss();
            }
        });

    }



    public void setupLinecharts(){
        Actchart.getLegend().setEnabled(false);
        Actchart.getDescription().setEnabled(false);
        Actchart.getLegend().setEnabled(false);
        Actchart.getAxisLeft().setDrawGridLines(false);
        Actchart.getAxisLeft().setDrawLabels(false);
        Actchart.getXAxis().setDrawGridLines(false);
        Actchart.setExtraLeftOffset(10);
        Actchart.setExtraRightOffset(10);
        Actchart.getAxisRight().setDrawGridLines(false);
        Actchart.getAxisRight().setDrawLabels(false);

        Comchart.getLegend().setEnabled(false);
        Comchart.getDescription().setEnabled(false);
        Comchart.getLegend().setEnabled(false);
        Comchart.getAxisLeft().setDrawGridLines(false);
        Comchart.getAxisLeft().setDrawLabels(false);
        Comchart.getXAxis().setDrawGridLines(false);
        Comchart.setExtraLeftOffset(10);
        Comchart.setExtraRightOffset(10);
        Comchart.getAxisRight().setDrawGridLines(false);
        Comchart.getAxisRight().setDrawLabels(false);

        Envchart.getLegend().setEnabled(false);
        Envchart.getDescription().setEnabled(false);
        Envchart.getLegend().setEnabled(false);
        Envchart.getAxisLeft().setDrawGridLines(false);
        Envchart.getAxisLeft().setDrawLabels(false);
        Envchart.getXAxis().setDrawGridLines(false);
        Envchart.setExtraLeftOffset(10);
        Envchart.setExtraRightOffset(10);
        Envchart.getAxisRight().setDrawGridLines(false);
        Envchart.getAxisRight().setDrawLabels(false);




        lineDataSet = new LineDataSet(activityentries, "");
        lineDataSet.setDrawFilled(true);
        lineDataSet.setDrawValues(false);
        Drawable gradient= ContextCompat.getDrawable(this,R.drawable.linechartgradient);
        lineDataSet.setFillDrawable(gradient);
        XAxis xAxis = Actchart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xEntrys.get((int) value);

            }
        });
        lineData = new LineData(lineDataSet);
        Actchart.setData(lineData);
        lineDataSet.setColor(Color.BLUE);
        lineDataSet.setValueTextColor(Color.BLACK);
        lineDataSet.setDrawCircles(false);

        lineDataSet1 = new LineDataSet(comentries, "");
        lineDataSet1.setDrawFilled(true);
        lineDataSet1.setDrawValues(false);
        lineDataSet1.setFillDrawable(gradient);
        XAxis xAxis1 = Comchart.getXAxis();
        xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis1.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xEntrys.get((int) value);

            }
        });
        lineData1 = new LineData(lineDataSet1);
        Comchart.setData(lineData1);
        lineDataSet1.setColor(Color.BLUE);
        lineDataSet1.setValueTextColor(Color.BLACK);
        lineDataSet1.setDrawCircles(false);

        lineDataSet2 = new LineDataSet(enventries, "");
        lineDataSet2.setDrawFilled(true);
        lineDataSet2.setDrawValues(false);
        lineDataSet2.setFillDrawable(gradient);
        XAxis xAxis2 = Envchart.getXAxis();
        xAxis2.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis2.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xEntrys.get((int) value);

            }
        });
        lineData2 = new LineData(lineDataSet2);
        Envchart.setData(lineData2);
        lineDataSet2.setColor(Color.BLUE);
        lineDataSet2.setValueTextColor(Color.BLACK);
        lineDataSet2.setDrawCircles(false);
        Actchart.invalidate();
        Actchart.refreshDrawableState();
        Envchart.invalidate();
        Envchart.refreshDrawableState();
        Comchart.invalidate();
        Comchart.refreshDrawableState();
    }

    public Float[] getCachedData(String Name){
        String str = CareAlert_SharedData.getString(Name, null);
        if(str!=null){
            String str1[] = str.split(",");
            Float arr[] = new Float[str1.length-1];
            // at i=0 it is space so start from 1
            for(int i=1;i<str1.length;i++){
                arr[i-1]=Float.parseFloat(str1[i]);
            }
            return arr;
        }
        return null;
    }

    public void putCachedData(String Name, Float[] Values){
        String str = " ";
        for(int i=0;i<Values.length;i++){
            str = str + ", "+ String.valueOf(Values[i]);
        }
        CareAlert_SharedData_Editor.putString(Name,str);
        CareAlert_SharedData_Editor.commit();
    }
    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

}