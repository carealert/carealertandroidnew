package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Enterotp extends AppCompatActivity {
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    TextView resentotp_msg;
    Retrofit retrofit;
    boolean reset;
    SharedPreferences CareAlert_StoredData;
    SharedPreferences.Editor CareAlert_StoredData_Editor;
    ProgressDialog loaddialog,progressDialog;
    Button continue_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterotp);

        CareAlert_StoredData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_StoredData_Editor = CareAlert_StoredData.edit();

        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();

        checkNetConnectivity();
/*
        final EditText otp1,otp2,otp3,otp4;
        final int[] Send_otp = new int[1];
        final String phone_number = CareAlert_StoredData.getString("Phone_Number","Number is blank");
        reset=CareAlert_StoredData.getBoolean("Reset",false);

        continue_button = (Button)findViewById(R.id.enterotp_continue);

        resentotp_msg=(TextView) findViewById(R.id.retry_msg);
        retrofit= new Retrofit.Builder()
                .baseUrl("https://carealertdev.azurewebsites.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        ResetCounter(phone_number);

        otp1= (EditText) findViewById(R.id.otp_digit_1);
        otp2= (EditText) findViewById(R.id.otp_digit_2);
        otp3= (EditText) findViewById(R.id.otp_digit_3);
        otp4= (EditText) findViewById(R.id.otp_digit_4);



        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length()==1){
                    otp2.requestFocus();
                    Send_otp[0] =Integer.valueOf(otp1.getText().toString());
                }

            }
        });
        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length()==1){
                    otp3.requestFocus();
                    Send_otp[0] =(Send_otp[0]*10)+Integer.valueOf(otp2.getText().toString());
                }

            }
        });
        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length()==1){
                    otp4.requestFocus();
                    Send_otp[0] =(Send_otp[0]*10)+Integer.valueOf(otp3.getText().toString());
                }
            }
        });
        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length()==1) {
                    continue_button.setVisibility(View.VISIBLE);
                    Send_otp[0] =(Send_otp[0]*10)+Integer.valueOf(otp4.getText().toString());
                }
            }
        });
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                Call<Post> call= jsonPlaceHolderApi.createpostotp(phone_number,Send_otp[0],reset);
                call.enqueue(new Callback<Post>() {
                    @Override
                    public void onResponse(Call<Post> call, Response<Post> response) {
                        if(response.isSuccessful()){

                            if (reset){
                                //forward OTP to Password Page
                                openEnterPasswordPage(Send_otp[0]);
                            }
                            else {
                                //Need to Extract Auth Token
                                String Auth_Token = response.body().getAuthToken();
                                FirebaseMessaging.getInstance().getToken()
                                        .addOnCompleteListener(new OnCompleteListener<String>() {
                                            @Override
                                            public void onComplete(@NonNull Task<String> task) {
                                                if (!task.isSuccessful()) {
                                                    Log.w("HI", "Fetching FCM registration token failed", task.getException());
                                                    return;
                                                }

                                                // Get new FCM registration token
                                                String token = task.getResult();

                                                // Log and toast
                                                String msg = token.toString();
                                                Log.d("Tiken is",msg);
                                                JsonObject Body=new JsonObject();
                                                Log.d("HandleKey", "onResponse: "+msg);
                                                Body.addProperty("CMSHandle",msg);
                                                Body.addProperty("Platform","fcm");
                                                Call<GetLocation> callnot=jsonPlaceHolderApi.PushNotificationRegistration("Bearer "+Auth_Token,Body);
                                                callnot.enqueue(new Callback<GetLocation>() {
                                                    @Override
                                                    public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                                                        if(response.isSuccessful()) {
                                                            Log.d("Notificationreg", "onResponse: " + true);
                                                        }
                                                        else{
                                                            try {
                                                                Log.d("Notificationreg", "onResponse: "+response.code()+response.errorBody().toString());
                                                                Log.d("Notificationreg", "onResponse: " +response.errorBody().string());
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<GetLocation> call, Throwable t) {
                                                        Log.d("Notificationreg", "onResponse: Failed" + false);

                                                    }
                                                });

                                            }


                                        });

                                openEnterEmailPage(Auth_Token);
                            }
                        }
                        else{
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                String Error = jObjError.getString("message");
                                otp1.setText("");
                                otp2.setText("");
                                otp3.setText("");
                                otp4.setText("");
                                otp1.requestFocus();
                                continue_button.setVisibility(View.INVISIBLE);
                                continue_button.setEnabled(true);
                                loaddialog.dismiss();
                                Toast.makeText(Enterotp.this, Error, Toast.LENGTH_SHORT).show();

                            }catch(Exception e){
                                otp1.setText("");
                                otp2.setText("");
                                otp3.setText("");
                                otp4.setText("");
                                otp1.requestFocus();
                                continue_button.setVisibility(View.INVISIBLE);
                                continue_button.setEnabled(true);
                                loaddialog.dismiss();
                                Toast.makeText(Enterotp.this, "Unknown Error", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Post> call, Throwable t) {
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Enterotp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
*/
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            enterOtpNet();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }



    public void enterOtpNet(){
        final EditText otp1,otp2,otp3,otp4;
        final int[] Send_otp = new int[4];
        final String phone_number = CareAlert_StoredData.getString("Phone_Number","Number is blank");
        reset=CareAlert_StoredData.getBoolean("Reset",false);

        continue_button = (Button)findViewById(R.id.enterotp_continue);

        resentotp_msg=(TextView) findViewById(R.id.retry_msg);
        retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        ResetCounter(phone_number);

        otp1= (EditText) findViewById(R.id.otp_digit_1);
        otp2= (EditText) findViewById(R.id.otp_digit_2);
        otp3= (EditText) findViewById(R.id.otp_digit_3);
        otp4= (EditText) findViewById(R.id.otp_digit_4);



        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length()==1){
                    otp2.requestFocus();
                    Send_otp[0] =Integer.valueOf(otp1.getText().toString());
                }
                else{
                    Send_otp[0] =0;
                }

            }
        });
        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length()==1){
                    otp3.requestFocus();
                    Send_otp[1] =Integer.valueOf(otp2.getText().toString());
                }
                else{
                    Send_otp[1] =0;
                }

            }
        });
        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length()==1){
                    otp4.requestFocus();
                    Send_otp[2] =Integer.valueOf(otp3.getText().toString());
                }
                else{
                    Send_otp[2] =0;
                }

            }
        });
        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length()==1 ) {
                    continue_button.setVisibility(View.VISIBLE);
                    Send_otp[3] =Integer.valueOf(otp4.getText().toString());
                }
                else{
                    continue_button.setVisibility(View.INVISIBLE);
                    Send_otp[3] =0;
                }

            }
        });
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                int otp= Send_otp[0]*1000+Send_otp[1]*100+Send_otp[2]*10+Send_otp[3];
                Call<Post> call= jsonPlaceHolderApi.createpostotp(phone_number,otp,reset);
                call.enqueue(new Callback<Post>() {
                    @Override
                    public void onResponse(Call<Post> call, Response<Post> response) {
                        if(response.isSuccessful()){

                            if (reset){
                                //forward OTP to Password Page
                                openEnterPasswordPage(otp);
                            }
                            else {
                                //Need to Extract Auth Token
                                String Auth_Token = response.body().getAuthToken();
                                FirebaseMessaging.getInstance().getToken()
                                        .addOnCompleteListener(new OnCompleteListener<String>() {
                                            @Override
                                            public void onComplete(@NonNull Task<String> task) {
                                                if (!task.isSuccessful()) {
                                                    Log.w("HI", "Fetching FCM registration token failed", task.getException());
                                                    return;
                                                }

                                                // Get new FCM registration token
                                                String token = task.getResult();

                                                // Log and toast
                                                String msg = token.toString();
                                                Log.d("Tiken is",msg);
                                                JsonObject Body=new JsonObject();
                                                Log.d("HandleKey", "onResponse: "+msg);
                                                Body.addProperty("CMSHandle",msg);
                                                Body.addProperty("Platform","fcm");
                                                Call<GetLocation> callnot=jsonPlaceHolderApi.PushNotificationRegistration("Bearer "+Auth_Token,Body);
                                                callnot.enqueue(new Callback<GetLocation>() {
                                                    @Override
                                                    public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                                                        if(response.isSuccessful()) {
                                                            Log.d("Notificationreg", "onResponse: " + true);
                                                        }
                                                        else{
                                                            try {
                                                                Log.d("Notificationreg", "onResponse: "+response.code()+response.errorBody().toString());
                                                                Log.d("Notificationreg", "onResponse: " +response.errorBody().string());
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<GetLocation> call, Throwable t) {
                                                        Log.d("Notificationreg", "onResponse: Failed" + false);

                                                    }
                                                });

                                            }


                                        });

                                openEnterEmailPage(Auth_Token);
                            }
                        }
                        else{
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                String Error = jObjError.getString("message");
                                otp1.setText("");
                                otp2.setText("");
                                otp3.setText("");
                                otp4.setText("");
                                otp1.requestFocus();
                                continue_button.setVisibility(View.INVISIBLE);
                                continue_button.setEnabled(true);
                                loaddialog.dismiss();
                                Toast.makeText(Enterotp.this, Error, Toast.LENGTH_SHORT).show();

                            }catch(Exception e){
                                otp1.setText("");
                                otp2.setText("");
                                otp3.setText("");
                                otp4.setText("");
                                otp1.requestFocus();
                                continue_button.setVisibility(View.INVISIBLE);
                                continue_button.setEnabled(true);
                                loaddialog.dismiss();
                                Toast.makeText(Enterotp.this, "Unknown Error", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Post> call, Throwable t) {
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Enterotp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }

    public void openEnterEmailPage(String Auth_token){
        CareAlert_StoredData_Editor.putString("Auth_Token",Auth_token);
        CareAlert_StoredData_Editor.commit();
        Intent EnterEmailPage=new Intent(this,Enteremail.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(EnterEmailPage);
    }
    public void openEnterPasswordPage(int OTP){
        CareAlert_StoredData_Editor.putInt("Otp",OTP);
        CareAlert_StoredData_Editor.commit();
        Intent EnterPasswordPage=new Intent(this,Enterpassword.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(EnterPasswordPage);
    }
    public void openEnterPhonePage(){
        Intent EnterPhonePage=new Intent(this,EnterPhone.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(EnterPhonePage);

    }

    public void ResetCounter(final String phone_number){
        final String mainstring="Didn't receive? ";
        new CountDownTimer(30000,1000){

            @Override
            public void onTick(long l) {
                String remaniningvalue;
                if (l/1000<10){
                    remaniningvalue="0"+String.valueOf(l/1000);
                }
                else{
                    remaniningvalue=String.valueOf(l/1000);
                }
                resentotp_msg.setText(Html.fromHtml(mainstring+"<font color=#6F33FF>Resend in 00:"+remaniningvalue+"</font>"));
            }

            @Override
            public void onFinish() {
                String msg="Resend Otp";
                SpannableString span_resendotp_msg=new SpannableString(msg);
                ClickableSpan clickspan= new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View view) {
                        Toast.makeText(Enterotp.this, "Otp Sent", Toast.LENGTH_SHORT).show();
                        ResetCounter(phone_number);
                        SendOtpAgain(phone_number);
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLUE);
                        ds.setUnderlineText(false);
                    }
                };
                span_resendotp_msg.setSpan(clickspan,0,10, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                resentotp_msg.setText(span_resendotp_msg);
                resentotp_msg.setMovementMethod(LinkMovementMethod.getInstance());


            }
        }.start();
    }

    public void SendOtpAgain(String phone_number){
        if(reset){
            sendresetotp(phone_number);
        }
        else{
            sendfirsttimeotp(phone_number);
        }

    }
    public void sendfirsttimeotp(final String phone_number){
        Call<Post> call= jsonPlaceHolderApi.createpost(phone_number);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                if (response.isSuccessful()) {
                    Toast.makeText(Enterotp.this, "OTP Resent", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String Error = jObjError.getString("message");
                        Toast.makeText(Enterotp.this, Error, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(Enterotp.this, "Unknown Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {;
                Toast.makeText(Enterotp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void sendresetotp(final String phone_number){
        Call<Post> call= jsonPlaceHolderApi.createpostresetpasswordreq(phone_number);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                if (response.isSuccessful()) {
                    Toast.makeText(Enterotp.this, "OTP Resent", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String Error = jObjError.getString("message");
                        Toast.makeText(Enterotp.this, Error, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        loaddialog.dismiss();
                        Toast.makeText(Enterotp.this, "Unknown Error", Toast.LENGTH_SHORT).show();

                    }
                }


            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Toast.makeText(Enterotp.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}