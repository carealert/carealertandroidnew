package com.sensorcall.carealertandroid_new;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;

import java.io.InputStream;
import java.util.ArrayList;

import android.bluetooth.BluetoothAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.model.DeviceRegistrationStatus;
import com.sensorcall.carealertandroid_new.util.AppUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Bluetoothdevice extends AppCompatActivity {
    private ListView listView;
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    private final static int REQUEST_ENABLE_BT = 1;
    String WifiDatastring;
    JSONObject WifiDataJson;
    boolean ReconfigureBT = false;
    public final static int MESSAGE_READ = 2; // used in bluetooth handler to identify message update
    private final static int CONNECTING_STATUS = 3;
    Button continue_button;
    public static int REQUEST_ACCESS_COARSE_LOCATION = 1;
    private ArrayList<String> mDeviceList = new ArrayList<String>();
    private ArrayList<String> mMACList = new ArrayList<String>();
    private BluetoothAdapter mBluetoothAdapter;
    TextView text;

    ProgressDialog loaddialog, progressDialog;
    private int selected_pos = -1, devicefound = -1;
    int REQUEST_ENABLE_BLUETOOTH = 11;
    InputStream WifiData;
    private static final UUID Service_UUID = UUID.fromString("dcfed825-7ad0-4d01-8a38-4f432e2df258");
    private static final UUID MY_UUID = UUID.fromString("dcfed826-7ad0-4d01-8a38-4f432e2df258");
    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();
    MyAdapter arrayAdapter;
    //private Handler mHandler; // Our main handler that will receive callback notifications

    BluetoothSocket bTSocket = null;

    BluetoothDevice device;
    private Handler mHandler;
    private Runnable runnable;

    private int charatecsticIndex = 0;
    private String wifiList = "";
    private String deviceStatus = "";
    private List<BluetoothGattService> service;
    BluetoothGatt mBluetoothgatt;
    private String deviceId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetoothdevice);
        listView = (ListView) findViewById(R.id.list);
        text = findViewById(R.id.text);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://sensorscall.com/store/"));
                startActivity(intent);
            }
        });
        loaddialog = new ProgressDialog(this);
        loaddialog = showLoadingDialog(this);
        loaddialog.dismiss();
        continue_button = (Button) findViewById(R.id.bluetooth_continue);
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
        CareAlert_SharedData_Editor = CareAlert_SharedData.edit();
        ReconfigureBT = CareAlert_SharedData.getBoolean("ReconfigureBT", false);
        if (ReconfigureBT) {
            deviceId = getIntent().getStringExtra(Constants.DEVICEID);
            Toast.makeText(this, "Make Sure the Device is in Bluetooth mode to Reconfigure", Toast.LENGTH_SHORT).show();

        }

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            discover();
        } else {
            Bluetoothon();
        }


        findViewById(R.id.btn_scan_bluethooth).setOnClickListener(v -> {
            discover();
        });


        checkNetConnectivity();
    }

    void checkNetConnectivity() {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet", "devic123");
            bluetoothContinueNet();
        } else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog() {
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    public void bluetoothContinueNet() {
        continue_button.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
            @Override
            public void onClick(View view) {
                showLoading(false);
                if (selected_pos < 0) {
                    Toast.makeText(Bluetoothdevice.this, "No Device Selected, Please Select a Device", Toast.LENGTH_SHORT).show();
                } else {
                    loaddialog.show();
                    if (mBluetoothAdapter.isDiscovering()) {
                        mBluetoothAdapter.cancelDiscovery();
                    }
                    Toast.makeText(Bluetoothdevice.this, "Connecting to" + mDeviceList.get(selected_pos), Toast.LENGTH_SHORT).show();

                    try {
                        connect();
                    } catch (Exception ex) {
                        Toast.makeText(Bluetoothdevice.this, "Failed to Connect", Toast.LENGTH_SHORT).show();
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                    }

                }
            }
        });
    }

    private void Bluetoothon() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mReceiver, filter);
    }


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.i("BT check", "DiscoveryStarted");
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (bluetoothState) {
                    case BluetoothAdapter.STATE_ON:
                        Toast.makeText(Bluetoothdevice.this, "Bluetooth Turned On", Toast.LENGTH_SHORT).show();
                        discover();
                        break;
                }
            }

        }
    };


    private void discover() {
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
        Log.i("Discover Function", "DiscoveryStarted");
        mBluetoothAdapter.startDiscovery();
        registerReceiver(DeviceReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        startTimer();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mBluetoothAdapter.cancelDiscovery();
    }

    private void startTimer() {
        showLoading(true);
        mHandler = new Handler(Looper.getMainLooper());
        runnable = new Runnable() {
            @Override
            public void run() {
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }
                showLoading(false);

            }
        };
        mHandler.postDelayed(runnable, 30000);
    }

    private void showLoading(boolean show) {
        findViewById(R.id.progress_bluetooth_search).setVisibility(show ? View.VISIBLE : View.GONE);
        findViewById(R.id.btn_scan_bluethooth).setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void connect() {

        device = mBTDevices.get(selected_pos);
        mBluetoothgatt = device.connectGatt(this, false, gattCallback);
    }

    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.i("onConnectionStateChange", "Status: " + status);
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i("gattCallback", "STATE_CONNECTED");
                //gatt.requestMtu(512);
                gatt.discoverServices();
            }
            if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                loaddialog.dismiss();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            service = gatt.getServices();

            Log.i("onServicesDiscovered", service.toString());
            charatecsticIndex = 1;
            gatt.readCharacteristic(service.get(2).getCharacteristics().get(charatecsticIndex));
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic
                                                 characteristic, int status) {

            Log.i("onCharacteristicRead", characteristic.toString());
            final byte[] data = characteristic.getValue();

            Log.v("DATA Length is:", "data.length: " + data.length);

            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data) {
                    stringBuilder.append(String.format("%02X ", byteChar));

                    Log.v("stringformat", String.format("%02X ", byteChar));
                }
                Log.v("I Dont Know", new String(data) + "\n" + stringBuilder.toString());
                String PassData = new String(data);
                if (charatecsticIndex == 1) {
                    charatecsticIndex = 0;
                    wifiList = PassData;
                    mBluetoothgatt.readCharacteristic(service.get(2).getCharacteristics().get(charatecsticIndex));


                } else if (charatecsticIndex == 0) {
                    deviceStatus = PassData;
                    charatecsticIndex = 3;
                    gatt.disconnect();
                    callCheckDeviceApi();

                }

            } else {
                Toast.makeText(Bluetoothdevice.this, "Failed to Connect to Device", Toast.LENGTH_SHORT).show();
                gatt.disconnect();
                continue_button.setEnabled(true);
                loaddialog.dismiss();

            }

        }
    };



    private void callCheckDeviceApi() {
        String deviceId = "";

        try {
            JSONObject datajson = new JSONObject(deviceStatus);
            deviceId = datajson.getString("deviceId");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if(this.ReconfigureBT){
            if(this.deviceId.equals(deviceId)){
                openwifipage(wifiList);
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Bluetoothdevice.this, R.string.please_select_same_device_which_you_need_to_configure, Toast.LENGTH_SHORT).show();
                    }
                });
                loaddialog.dismiss();
            }
        } else {

            JsonPlaceHolder jsonPlaceHolderApi1;
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.MINUTES)
                    .connectTimeout(10, TimeUnit.MINUTES)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            jsonPlaceHolderApi1 = retrofit.create(JsonPlaceHolder.class);

            Call<DeviceRegistrationStatus> call = jsonPlaceHolderApi1.getDeviceRegistrationStatus(deviceId);

            call.enqueue(new Callback<DeviceRegistrationStatus>() {

                @Override
                @RequiresApi(api = Build.VERSION_CODES.N)
                public void onResponse(Call<DeviceRegistrationStatus> call, Response<DeviceRegistrationStatus> response) {
                    if (!response.isSuccessful()) {
                        Toast.makeText(Bluetoothdevice.this, "Response is unsuccessful", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        if (response.body().isDeviceAvailable) {
                            openwifipage(wifiList);
                        } else {
                            Toast.makeText(Bluetoothdevice.this, R.string.error_device_aleready_registered, Toast.LENGTH_LONG).show();
                        }

                        loaddialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<DeviceRegistrationStatus> call, Throwable t) {
                    Toast.makeText(Bluetoothdevice.this, "Failed", Toast.LENGTH_SHORT).show();

                }
            });
        }
    }


    protected void onDestroy() {
        super.onDestroy();
        cancelCallbacks();
    }

    private void cancelCallbacks() {
        unregisterReceiver(DeviceReceiver);
        mHandler.removeCallbacks(runnable);
    }


    private final BroadcastReceiver DeviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("BR", "Checking I am In");
            final String recaction = intent.getAction();
            Log.i("Broadcast Reviever", "Waiting for Device Found");
            if (recaction.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice DeviceFound = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (DeviceFound.getName() != null && !(AppUtil.pairedDevice != null && DeviceFound.getAddress().equals(AppUtil.pairedDevice.getAddress()))) {
                    if (DeviceFound.getName().toLowerCase().contains("carealert") && (!mMACList.contains(DeviceFound.getAddress()))) {
                        mDeviceList.add(DeviceFound.getName());
                        mMACList.add(DeviceFound.getAddress());
                        Log.i("Device Details", "Name: " + DeviceFound.getName() + " MAC: " + DeviceFound.getAddress());
                        arrayAdapter = new MyAdapter(mDeviceList);
                        mBTDevices.add(DeviceFound);
                        listView.setAdapter(arrayAdapter);
                    }
                }
            }
        }
    };


    class MyAdapter extends BaseAdapter {

        private ArrayList<String> arrayList;

        MyAdapter(ArrayList<String> arrayList) {
            this.arrayList = arrayList;
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return arrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(Bluetoothdevice.this, R.layout.row, null);
            }
            final LinearLayout ll_parent = (LinearLayout) view.findViewById(R.id.ll_parent);
            final RadioButton radioButton = (RadioButton) view.findViewById(R.id.radio);
            radioButton.setText(arrayList.get(position));
            radioButton.setChecked(position == selected_pos);
            radioButton.setTag(position);
            ll_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selected_pos = (Integer) v.findViewById(R.id.radio).getTag();
                    notifyDataSetChanged();
                }
            });

            return view;
        }
    }


    public void openwifipage(String Data) {
        cancelCallbacks();
        Log.i("Data is", "openwifipage: " + Data);
        Intent wifipage = new Intent(this, Wifipage.class);
        wifipage.putExtra("CareAlert_Device", (Parcelable) mBTDevices.get(selected_pos));
        wifipage.putExtra("WifiData", Data);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(wifipage);

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @Override
    public void onBackPressed() {
        if (ReconfigureBT) {
            CareAlert_SharedData_Editor.putBoolean("ReconfigureBT", false);
            CareAlert_SharedData_Editor.commit();
            Intent manageDevice = new Intent(Bluetoothdevice.this, ManageDevices.class);
            manageDevice.putExtra("DefaultLocationId", CareAlert_SharedData.getInt("LocationId", -1));
            startActivity(manageDevice);
        } else {
            Intent namedevice = new Intent(Bluetoothdevice.this, Namedeviceroom.class);
            startActivity(namedevice);
        }
    }
}
