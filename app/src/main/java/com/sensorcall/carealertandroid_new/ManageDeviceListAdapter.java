package com.sensorcall.carealertandroid_new;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;


public class ManageDeviceListAdapter  extends ArrayAdapter<GetDeviceByLocationId>{
    private Context mcontext;
    private List<GetDeviceByLocationId> values;
    Drawable Healthy;
    Drawable unhealthy;
    Drawable conn;
    Drawable disconn;



    private static class ViewHolder {
        TextView deviceprofile;
        TextView deviceRelatediD;
        TextView deviceNumber;
        ImageView Health, Connection;
        TextView currentVersionNumber;
    }
    public ManageDeviceListAdapter(List<GetDeviceByLocationId> values, Context context,Drawable Healthy, Drawable Unhealthy, Drawable Connected, Drawable DisConnected) {
        super(context, R.layout.manage_device_listview, values);

        this.mcontext = context;
        this.values = values;
        this.Healthy=Healthy;
        this.unhealthy= Unhealthy;
        this.conn= Connected;
        this.disconn= DisConnected;

    }


    @Override
    @NonNull
    public View getView(int position, View convertView, ViewGroup parent)
    {
        GetDeviceByLocationId device= getItem(position);
//        String deviceProfileName = getItem(position).getDeviceProfileName();
//        int healthStatus = getItem(position).getHealthStatus();
//        String deviceNumber = getItem(position).getDeviceNumber();
//        String deviceRelatedId = getItem(position).getDeviceRelatedId();
        int healthstatus =getItem(position).getHealthStatus();
        int Devicestatus= getItem(position).getDeviceStatus();

//        int locId= getItem(position).getLocationId();
//        int deviceId=getItem(position).getDeviceId();
        final View result;


        ViewHolder viewHolder; // view lookup cache stored in tag



        if (convertView == null) {


            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.manage_device_listview, parent, false);
            viewHolder.deviceprofile = (TextView) convertView.findViewById(R.id.txt1);
            viewHolder.deviceNumber = (TextView) convertView.findViewById(R.id.txt2);
            viewHolder.Health = (ImageView) convertView.findViewById(R.id.Health);
            viewHolder.Connection = (ImageView) convertView.findViewById(R.id.Connection);
            viewHolder.currentVersionNumber= (TextView) convertView.findViewById(R.id.txt3);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        viewHolder.deviceprofile.setText(device.getDeviceProfileName());
        viewHolder.deviceNumber.setText(device.getDeviceNumber());
        viewHolder.currentVersionNumber.setText("Firmware version: "+device.getCurrentVersionNumber());
        if(healthstatus==2)  {
            viewHolder.Health.setImageDrawable(Healthy);
        }
        else{
            viewHolder.Health.setImageDrawable(unhealthy);
        }
        if(Devicestatus==3){
            viewHolder.Connection.setImageDrawable(conn);
        }
        else{
            viewHolder.Connection.setImageDrawable(disconn);
        }



        return convertView;

    }


}
