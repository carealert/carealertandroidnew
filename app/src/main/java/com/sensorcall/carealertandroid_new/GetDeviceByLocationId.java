package com.sensorcall.carealertandroid_new;

public class GetDeviceByLocationId
{
    int DeviceId;
    String DeviceNumber, DeviceRelatedId, DeviceProfileName,CurrentVersionNumber;
    int DeviceStatus, LocationId;

    public String getCurrentVersionNumber() {
        return CurrentVersionNumber;
    }

    public void setCurrentVersionNumber(String currentVersionNumber) {
        CurrentVersionNumber = currentVersionNumber;
    }

    public int getVolume() {
        return Volume;
    }

    int HealthStatus;
    boolean IsOn;
    int Volume;
    public GetDeviceByLocationId(int Deviceid, String deviceNumber, String deviceRelatedId, String deviceProfileName, int deviceStatus, int healthStatus, boolean isOn, int locationId,String currentVersionNumber) {

        DeviceId= Deviceid;
        DeviceNumber = deviceNumber;
        DeviceRelatedId = deviceRelatedId;
        DeviceProfileName = deviceProfileName;
        DeviceStatus = deviceStatus;
        HealthStatus = healthStatus;
        IsOn = isOn;
        LocationId = locationId;
        CurrentVersionNumber=currentVersionNumber;
    }

    public GetDeviceByLocationId(String deviceProfileName) {
        DeviceProfileName = deviceProfileName;
    }

    public int getDeviceId() {
        return DeviceId;
    }

    public String getDeviceNumber() {
        return DeviceNumber;
    }

    public String getDeviceRelatedId() {
        return DeviceRelatedId;
    }

    public String getDeviceProfileName() {
        return DeviceProfileName;
    }

    public int getDeviceStatus() {
        return DeviceStatus;
    }

    public int getHealthStatus() {
        return HealthStatus;
    }

    public boolean getIsOn() {
        return IsOn;
    }

    public int getLocationId() {
        return LocationId;
    }

    public void setDeviceProfileName(String deviceProfileName) {
        DeviceProfileName = deviceProfileName;
    }
}
