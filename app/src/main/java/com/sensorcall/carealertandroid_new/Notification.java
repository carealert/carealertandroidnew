package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.CustomBottomNavigationView1;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sensorcall.carealertandroid_new.ui.notifications.ReadActivityMapActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Notification extends AppCompatActivity {
    CustomBottomNavigationView1 curvedBottomNavigationView;

    LinearLayout Notification_HL;
    LinearLayout Notification_VL;
    int locationId;
    String Auth_token;
    ProgressDialog loader, progressDialog;
    Drawable ConfidenceStrong, ConfidenceWeak;
    Drawable pres, cooking, work, showering, sleeping;
    Drawable motionImage, presenceImage, modelImage;
    String care_receiver_type = "";
    private JsonPlaceHolder jsonPlaceHolderApi;
    SharedPreferences CareAlert_SharedData;
    TextView Name;
    private GetLocation lastActivityLocation;
    private LabeledSwitch inferenceSwitch;
    private List<GetLocation> mActivityList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        FloatingActionButton floatingActionButton = findViewById(R.id.call_fab);
        curvedBottomNavigationView = findViewById(R.id.bottom_navigation);
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
        Name = findViewById(R.id.text_time);
        loader = new ProgressDialog(Notification.this);
        loader = showLoadingDialog(this);

        TextView textView = findViewById(R.id.text_read_activity_map);
        SpannableString content = new SpannableString(textView.getText());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textView.setText(content);
        ConfidenceStrong = getResources().getDrawable(getResources().getIdentifier("@drawable/ic_active", null, getPackageName()));
        ConfidenceWeak = getResources().getDrawable(getResources().getIdentifier("@drawable/ic_warning", null, getPackageName()));
        pres = getResources().getDrawable(getResources().getIdentifier("@drawable/presence", null, getPackageName()));
        cooking = getResources().getDrawable(getResources().getIdentifier("@drawable/cooking", null, getPackageName()));
        showering = getResources().getDrawable(getResources().getIdentifier("@drawable/shower", null, getPackageName()));
        work = getResources().getDrawable(getResources().getIdentifier("@drawable/work", null, getPackageName()));
        sleeping = getResources().getDrawable(getResources().getIdentifier("@drawable/sleeping", null, getPackageName()));
        //  motionImage = getResources().getDrawable(getResources().getIdentifier("@drawable/motion", null,getPackageName()));
        motionImage = getResources().getDrawable(getResources().getIdentifier("@drawable/transfer_within_a_station", null, getPackageName()));

        presenceImage = getResources().getDrawable(getResources().getIdentifier("@drawable/person_pin_circle", null, getPackageName()));
        modelImage = getResources().getDrawable(getResources().getIdentifier("@drawable/volume_up_solid", null, getPackageName()));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolder.class);
        Notification_VL = findViewById(R.id.VerticalLL);
        inferenceSwitch = findViewById(R.id.switch_inference);
        Auth_token = CareAlert_SharedData.getString("Auth_Token", "");
        //getDefaultLocationId();
        checkNetConnectivity();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent voice_calling_screen = new Intent(Notification.this, SelectToCall.class);
                startActivity(voice_calling_screen);
            }
        });
        bottomNavigation();


        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent read_map_screen = new Intent(Notification.this, ReadActivityMapActivity.class);
                read_map_screen.putExtra(ReadActivityMapActivity.ARG_LOCATION, lastActivityLocation);
                startActivity(read_map_screen);
                //overridePendingTransition(0, 0);

            }
        });


        inferenceSwitch.setOnToggledListener(new OnToggledListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                showData(mActivityList, isOn);
            }
        });
    }

    void checkNetConnectivity() {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet", "devic123");
            getDefaultLocationId();
        } else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog() {
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    private void getDefaultLocationId() {
        loader.show();
        Log.d("default location", "into the location id after net connection");
        Call<List<GetLocation>> call = jsonPlaceHolderApi.getDefaultLocation("Bearer " + Auth_token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if (response.isSuccessful()) {
                    Log.i("Succees", "onResponse: Yes");
                    List<GetLocation> get_list = response.body();
                    if (get_list.isEmpty()) {
                        Toast.makeText(Notification.this, "Default location is not set for this account", Toast.LENGTH_SHORT).show();
                        loader.dismiss();
                    } else {
                        for (int i = 0; i < get_list.size(); i++) {
                            if (get_list.get(i).isDefaultLocationId()) {
                                Name.setText(get_list.get(i).getLocationName());
                                locationId = get_list.get(i).getLocationId();
                                Log.i("Location Mil Ga yi", "Abc " + Integer.toString(locationId));
                                SetupLL();
                                break;
                            }
                        }
                    }
                } else {
                    Toast.makeText(Notification.this, "LocationId Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.d("res error", "resuis: " + response.errorBody().toString());
                    loader.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                Toast.makeText(Notification.this, "No Default location set", Toast.LENGTH_SHORT).show();
                Log.d("failure12345", "error in fail");
                loader.dismiss();
            }
        });
    }

    public void SetupLL() {
        Log.d("Entered Setup LL", "SetupLL: ");
        if (locationId == 0) {
            Toast.makeText(this, "No Deafault Location Found", Toast.LENGTH_SHORT).show();
        } else {
            Log.d("AuthToken is ", "SetupLL: " + Auth_token);
            Call<List<GetLocation>> call = jsonPlaceHolderApi.getActivity("Bearer " + Auth_token, locationId);
            call.enqueue(new Callback<List<GetLocation>>() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                    if (response.isSuccessful()) {
                        mActivityList = response.body();

                        Log.d("Activity List is ", "onResponse: " + mActivityList.toString());
                        if (mActivityList.size() == 0) {
                            Toast.makeText(Notification.this, "No Activity in Last 48 hours", Toast.LENGTH_SHORT).show();
                        } else {
                            showData(mActivityList, false);
                        }
                    } else {
                        Log.d("Failed Response", "onResponse: " + response.code());

                        Toast.makeText(Notification.this, "Failed to get Activity Data", Toast.LENGTH_SHORT).show();
                    }
                    loader.dismiss();
                }

                @Override
                public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                    Toast.makeText(Notification.this, "Failed to get Activity Data\n" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    loader.dismiss();
                }
            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showData(List<GetLocation> ActivityList, boolean isChecked) {
        LinearLayout last_activity_layout = findViewById(R.id.last_activity);
        LinearLayout today_activity_layout = findViewById(R.id.today_activity);
        LinearLayout yesterday_activity_layout = findViewById(R.id.yesterday_activity);


        if (last_activity_layout.getChildCount() > 1) {
            last_activity_layout.removeViewAt(1);
        }
        today_activity_layout.removeAllViews();
        yesterday_activity_layout.removeAllViews();


        int test = last_activity_layout.getBottom();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int height = dm.heightPixels;
        ImageView img = findViewById(R.id.icon);
        TextView text_call = findViewById(R.id.text_info_call);
        TextView read_activity_map = findViewById(R.id.text_read_activity_map);

        int total_height = img.getMeasuredHeight() + text_call.getMeasuredHeight() + read_activity_map.getMeasuredHeight();//+text_time.getMeasuredHeight();//+text_Activity_history.getMeasuredHeight();//+last_activity_layout.getMeasuredHeight();//+today_activity_text1.getHeight()+today_activity_text2.getHeight();
        int partition = (int) (pxToDp(height - total_height) / 2);

        ScrollView scrollView = (ScrollView) findViewById(R.id.today_activity_scroll);
        //RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (partition+30)*2);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, partition);

        params.setMargins(0, 0, 0, 0);
        scrollView.setLayoutParams(params);

        ScrollView scrollView1 = (ScrollView) findViewById(R.id.yesterday_activity_scroll);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, partition);
        scrollView1.setLayoutParams(params1);

        for (int i = 0; i < ActivityList.size(); i++) {
            GetLocation Activity = ActivityList.get(i);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            View HL = LayoutInflater.from(getApplicationContext()).inflate(R.layout.activity_not_layout,
                    findViewById(R.id.Notification_LLN));
            layoutParams.setMargins(0, 0, 0, 5);
            HL.setLayoutParams(layoutParams);
            HL.setId(i);
            TextView TextName = HL.findViewById(R.id.Activity_DeviceName);
            TextName.setText(textBreak(Activity.getDeviceName()));
            TextName.setAutoSizeTextTypeUniformWithConfiguration(1, 15, 1, TypedValue.COMPLEX_UNIT_DIP);

            TextView ActivityName = HL.findViewById(R.id.Activity_Name);

            int actId = Activity.getActivityId();
            if ((actId >= 0 && actId <= 2) || (actId >= 11 && actId <= 12) || actId == 22) {
                ActivityName.setText("");
                ActivityName.setVisibility(View.INVISIBLE);
            } else {
                ActivityName.setText(Activity.getActivity());
                ActivityName.setAutoSizeTextTypeUniformWithConfiguration(1, 20, 1, TypedValue.COMPLEX_UNIT_DIP);
            }

            ImageView Confimg = HL.findViewById(R.id.ActivityConf);
            //    Confimg.setImageDrawable(ConfidenceStrong);
            ImageView motion_image = HL.findViewById(R.id.Activity_Motion);
            //motion_image.setImageDrawable(motionImage);
            ImageView presence_image = HL.findViewById(R.id.Activity_Presence);
            // presence_image.setImageDrawable(ConfidenceStrong);
            ImageView activity_model = HL.findViewById(R.id.activity_model);
            //activity_model.setImageDrawable(modelImage);

            // confidence check : if confidencevalue is greater than 50, confidence is "STRONG", else week

            int Confvalue = Activity.getConfidence();
            if (Confvalue > 7) {
                Confimg.setImageResource(R.drawable.ic_active);
            } else {
                Confimg.setImageResource(R.drawable.ic_warning);
            }


            if (Confvalue > 50) {
                activity_model.setImageResource(R.drawable.ic_sound_up);
            } else {
                activity_model.setImageResource(R.drawable.ic_sound_up_unslected);
            }

                            /*Motion check : When presence is 1 or ActivityId is 1 to 10 highlight/activate (green)
                             the motion icon else do not highlight it (greyed)*/
            int presenceValue = Activity.getPresence();
            if (presenceValue == 1 || (actId >= 1 && actId <= 10)) {
                motion_image.setImageResource(R.drawable.ic_motion);
            } else {
                motion_image.setImageResource(R.drawable.ic_motion_unselected);
            }

                            /*Presence - When ActivityId is 11, 13, 14, 15, or 16 highlight presence icon (green)
                            else do not highlight it (greyed)*/
            // presence_image.setImageDrawable(presenceImage);
            if (actId == 11 || (actId >= 13 && actId <= 16)) {
                presence_image.setImageResource(R.drawable.ic_presense);
            } else {
                presence_image.setImageResource(R.drawable.ic_presense_unselecetd);
            }


            LocalDateTime datetime1 = LocalDateTime.now();
            String strDateFormat = "E, MMM dd yyyy";
            DateTimeFormatter format = DateTimeFormatter.ofPattern(strDateFormat);
            String formatDateTime = datetime1.format(format);
            LocalDateTime datetime2 = datetime1.minusDays(1);
            String yesterdayDateTime = datetime2.format(format);
            //  String strDateFormat = "E, MMM dd yyyy hh:mm a"; //Date format is Specified
            SimpleDateFormat formatter = new SimpleDateFormat(strDateFormat);
            String strActivityFormat = "E, MMM dd yyyy hh:mm a"; //Date format is Specified
            SimpleDateFormat formatterActivity = new SimpleDateFormat(strActivityFormat);
            Date dt_1 = null;
            Date dt_2 = null;
            Date yesterday = null;
            Date activity_date = null;
            try {
                dt_1 = formatter.parse(formatDateTime);
                dt_2 = formatter.parse(Activity.getTime());
                yesterday = formatter.parse(yesterdayDateTime);
                activity_date = formatterActivity.parse(Activity.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String strDateFormat1 = "h:mm a"; //Date format is Specified
            SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat1);
            String strDate = objSDF.format(activity_date);
            TextView dateTime = HL.findViewById(R.id.activity_DateTime);
            dateTime.setText(strDate);

            if (isChecked) {
                if (Confvalue > 7) {
                    if (dt_1.compareTo(dt_2) == 0) {
                        System.out.println("Both are same dates");

                        if (i == 0) {
                            lastActivityLocation = ActivityList.get(i);
                            last_activity_layout.addView(HL);
                        } else {
                            today_activity_layout.addView(HL);
                        }
                    } else if (yesterday.compareTo(dt_2) == 0) {
                        HL.setBackgroundColor(Color.parseColor("#E6F6FF"));
                        yesterday_activity_layout.addView(HL);
                    }
                }
            } else {
                if (dt_1.compareTo(dt_2) == 0) {
                    System.out.println("Both are same dates");

                    if (i == 0) {
                        lastActivityLocation = ActivityList.get(i);
                        last_activity_layout.addView(HL);
                    } else {
                        today_activity_layout.addView(HL);
                    }
                } else if (yesterday.compareTo(dt_2) == 0) {
                    HL.setBackgroundColor(Color.parseColor("#E6F6FF"));
                    yesterday_activity_layout.addView(HL);
                }
            }

        }
    }

    public void bottomNavigation() {

        Menu menu = curvedBottomNavigationView.getMenu();
        //   MenuItem menuitem= menu.getItem(3);
        MenuItem menuitem = menu.getItem(0);
        menuitem.setChecked(true);

        curvedBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.navigation_dashboard:
                        //Toast.makeText(Notification.this, "Dashboard clicked", Toast.LENGTH_SHORT).show();
                        Intent dashBoard = new Intent(Notification.this, Dashboard.class);
                        startActivity(dashBoard);
                        Notification.this.finish();
                        break;

                    case R.id.navigation_Reminders:
                        Intent reminder = new Intent(Notification.this, RemindersViewActivity.class);
                        //Toast.makeText(Notification.this, "Reminders clicked", Toast.LENGTH_SHORT).show();
                        startActivity(reminder);
                        break;

                    case R.id.navigation_notifications:
                        // Toast.makeText(Notification.this, "Notifications click", Toast.LENGTH_SHORT).show();
                        Intent notification = new Intent(Notification.this, Notification.class);
                        startActivity(notification);
                        break;

                    case R.id.navigation_settings:
                        //Toast.makeText(Notification.this, "Settings clicked", Toast.LENGTH_SHORT).show();
                        Intent settings = new Intent(Notification.this, Settings.class);
                        startActivity(settings);
                        break;
                }
                return true;
            }
        });
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static String textBreak(String splitingText) {
        String str = splitingText;
        String[] splited = str.split("\\s+");
        String[] line = new String[splited.length];

        //   System.out.println(splited.length);
        int i = 0;
        int line_number = 0;
        while (i < splited.length) {
            if (i == 0) {
                line[line_number] = splited[i];
            } else {

                if ((line[line_number].length() + splited[i].length()) < 15) {
                    line[line_number] = line[line_number] + " " + splited[i];
                } else {
                    line_number = line_number + 1;
                    if (line[line_number] == null) {
                        line[line_number] = splited[i];
                    } else {
                        line[line_number] = line[line_number] + " " + splited[i];
                    }

                }
            }

            i = i + 1;
        }

        String final_text = "";
        for (int j = 0; j < line.length; j++) {
            if (line[j] != null) {
                if (j == 0) {
                    final_text = line[j];
                } else {
                    final_text = final_text + "\n" + line[j];
                }
            }

        }
        System.out.println(final_text);
        return final_text;
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

}