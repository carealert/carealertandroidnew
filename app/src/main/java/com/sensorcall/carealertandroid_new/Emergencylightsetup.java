package com.sensorcall.carealertandroid_new;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.CustomBottomNavigationView1;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Emergencylightsetup extends AppCompatActivity  implements NetworkStateReceiver.NetworkStateReceiverListener {
    private NetworkStateReceiver networkStateReceiver;      // Receiver that detects network state changes
    Switch emergency_switch, motion_switch, night_switch;
    JsonPlaceHolderApi get_api;
    ProgressDialog loader, progressDialog;
    private ConnectionDetector detector;
    AlertDialog.Builder builder;
    SharedPreferences CareAlert_SharedData;
    TextView loc_name;
    String loc;
    Retrofit retrofit;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    JsonPlaceHolder jsonplaceholder;
    String Auth_Token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergencylightsetup);
        startNetworkBroadcastReceiver(this);
        motion_switch = findViewById(R.id.motion_switch);
        night_switch = findViewById(R.id.night_switch);
        emergency_switch = findViewById(R.id.emergency_switch);
        loc_name = (TextView) findViewById(R.id.location_name);
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
//        loader=new ProgressDialog(this);
//        loader.setMessage("Loading...");
//        loader.setCanceledOnTouchOutside(false);
//        loader.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        loader.setContentView(R.layout.progress_dialog);

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Auth_Token = CareAlert_SharedData.getString("Auth_Token", "");
        jsonplaceholder = retrofit.create(JsonPlaceHolder.class);

        getDefaultLocationId();

        loader = new ProgressDialog(this);
        loader = showLoadingDialog(this);

    }


    public void alert() {
        builder = new AlertDialog.Builder(this);
        builder.setMessage("Cannot load the contents. Make sure your phone has an internet connection " +
                "and try again.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }


    @IntRange(from = 0, to = 3)
    public static int getConnectionType(Context context) {
        int result = 0; // Returns connection type. 0: none; 1: mobile data; 2: wifi
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm != null) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = 2;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = 1;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)) {
                        result = 3;
                    }
                }
            }
        } else {
            if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null) {
                    // connected to the internet
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                        result = 2;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        result = 1;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_VPN) {
                        result = 3;
                    }
                }
            }
        }
        return result;
    }


    @Override
    protected void onPause() {
        /***/
        unregisterNetworkBroadcastReceiver(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        /***/
        registerNetworkBroadcastReceiver(this);
        super.onResume();
    }

    @Override
    public void networkAvailable() {
        Log.i("Availability", "networkAvailable()");
        //Proceed with online actions in activity (e.g. hide offline UI from user, start services, etc...)
        Log.d("Internet", "connected to net");


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        get_api = retrofit.create(JsonPlaceHolderApi.class);
        loader.show();
        final String Auth = CareAlert_SharedData.getString("Auth_Token", "");
        Call<List<GetLocation>> call = get_api.getLocation("Bearer " + Auth);
        call.enqueue(new Callback<List<GetLocation>>() {

            int locid = -1;
            int emergency_switch_var = 0;
            int night_switch_var = 0;
            int motion_switch_var = 0;

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {

                if (response == null) {
                    Log.d("IsNull", "True");
                } else {
                    Log.d("IsNull", "False");
                }

                if (response.isSuccessful()) {


                    List<GetLocation> get_list = response.body();

                    for (int i = 0; i < get_list.size(); i++) {
                        if (get_list.get(i).isDefaultLocationId() == true) {

                            if (get_list.get(i).isEmergencyLightEnabled() == true) {
                                emergency_switch.setChecked(true);
                                emergency_switch_var = 1;
                            } else
                                emergency_switch.setChecked(false);

                            if (get_list.get(i).isOnMotionDetected() == true) {
                                motion_switch.setChecked(true);
                                motion_switch_var = 1;
                            } else motion_switch.setChecked(false);


                            if (get_list.get(i).isNightLightEnabled() == true) {
                                night_switch.setChecked(true);
                                night_switch_var = 1;
                            } else
                                night_switch.setChecked(false);

                            locid = get_list.get(i).getLocationId();

                            break;
                        }
                        //loader.dismiss();


                    }
                    loader.dismiss();

                    int finalLocid = locid;
                    int finalA = emergency_switch_var;
                    int finalB = night_switch_var;
                    int finalC = motion_switch_var;


                    emergency_switch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int x = getConnectionType(getApplicationContext());
                            if (x != 0) {
                                boolean bool = false;
                                if (emergency_switch.isChecked()) {
                                    emergency_switch.setTextOn("ON");
                                    bool = true;
                                } else
                                    emergency_switch.setTextOff("OFF");

                                JsonObject body1 = new JsonObject();
                                body1.addProperty("LocationId", finalLocid);
                                body1.addProperty("IsEmergencyLightEnabled", bool);

                                Call<GetLocation> call1 = get_api.updateLocation("Bearer " + Auth, body1);
                                call1.enqueue(new Callback<GetLocation>() {
                                    @Override
                                    public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                                        if (response.isSuccessful()) {
                                            Toast.makeText(Emergencylightsetup.this, "Updated Emergency Settings ", Toast.LENGTH_SHORT).show();

                                        } else {
                                            try {
                                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                                //Toast.makeText(getApplicationContext(), jObjError.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                                                String s = jObjError.getJSONObject("error").getString("message");
                                                Log.d("Error", s);
                                            } catch (Exception e) {
                                                // Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                Log.d("Error Handler", e.getMessage());
                                            }
                                        }
                                    }


                                    @Override
                                    public void onFailure(Call<GetLocation> call, Throwable t) {
                                        loader.dismiss();
                                        alert();
                                        Log.d("Failure", "Fails to update Emergency Settings");

                                        //Toast.makeText(Emergencylightsetup.this, "Fails to update Emergency settings ", Toast.LENGTH_LONG).show();

                                    }
                                });

                            } else {
                                alert();
                                if (finalA == 1) {
                                    emergency_switch.setChecked(true);
                                    emergency_switch.setTextOn("ON");
                                } else {
                                    emergency_switch.setChecked(false);
                                    emergency_switch.setTextOff("OFF");
                                }
                            }
                        }
                    });

                    int finalLocid1 = locid;
                    night_switch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int x = getConnectionType(getApplicationContext());
                            if (x != 0) {

                                boolean bool = false;
                                if (night_switch.isChecked()) {
                                    night_switch.setTextOn("ON");
                                    bool = true;
                                } else
                                    night_switch.setTextOff("OFF");


                                JsonObject body1 = new JsonObject();
                                body1.addProperty("LocationId", finalLocid1);
                                body1.addProperty("IsNightLightEnabled", bool);

                                Call<GetLocation> call1 = get_api.updateLocation("Bearer " + Auth, body1);
                                call1.enqueue(new Callback<GetLocation>() {
                                    @Override
                                    public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                                        if (response.isSuccessful()) {
                                            Toast.makeText(Emergencylightsetup.this, "Updated NightLight Settings ", Toast.LENGTH_SHORT).show();

                                        } else {

                                            //  Toast.makeText(getApplicationContext(), response.code(), Toast.LENGTH_LONG).show();
                                            try {
                                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                                String s = jObjError.getJSONObject("error").getString("message");
                                                Log.d("Error", s);
                                                // Toast.makeText(getApplicationContext(), jObjError.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                                            } catch (Exception e) {
                                                Log.d("Error Handler", e.getMessage());
                                                // Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }


                                    @Override
                                    public void onFailure(Call<GetLocation> call, Throwable t) {
                                        loader.dismiss();
                                        alert();

                                    }
                                });

                            } else {
                                alert();
                                if (finalB == 1) {
                                    night_switch.setChecked(true);
                                    night_switch.setTextOn("ON");
                                } else {
                                    night_switch.setChecked(false);
                                    night_switch.setTextOff("OFF");
                                }
                                Toast.makeText(Emergencylightsetup.this, "Fails to update NightLight settings ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });


                    int finalLocid2 = locid;
                    motion_switch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            int x = getConnectionType(getApplicationContext());
                            if (x != 0) {

                                boolean bool = false;
                                if (motion_switch.isChecked()) {
                                    motion_switch.setTextOn("ON");
                                    bool = true;
                                } else
                                    motion_switch.setTextOff("OFF");

                                JsonObject body1 = new JsonObject();
                                body1.addProperty("LocationId", finalLocid2);
                                body1.addProperty("OnMotionDetected", bool);

                                Call<GetLocation> call1 = get_api.updateLocation("Bearer " + Auth, body1);
                                call1.enqueue(new Callback<GetLocation>() {
                                    @Override
                                    public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                                        if (response.isSuccessful()) {
                                            Toast.makeText(Emergencylightsetup.this, "Updated Motions Settings ", Toast.LENGTH_SHORT).show();

                                        } else {

                                            //  Toast.makeText(getApplicationContext(), response.code(), Toast.LENGTH_LONG).show();
                                            try {
                                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                                //Toast.makeText(getApplicationContext(), jObjError.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                                                String s = jObjError.getJSONObject("error").getString("message");
                                                Log.d("Error", s);
                                            } catch (Exception e) {
                                                //Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                Log.d("Error Handler", e.getMessage());
                                            }
                                        }
                                    }


                                    @Override
                                    public void onFailure(Call<GetLocation> call, Throwable t) {
                                        loader.dismiss();
                                        alert();
                                        Toast.makeText(Emergencylightsetup.this, "Fails to update Motions settings ", Toast.LENGTH_LONG).show();

                                    }
                                });

                            } else {
                                alert();
                                if (finalC == 1) {
                                    motion_switch.setChecked(true);
                                    motion_switch.setTextOn("ON");
                                } else {
                                    motion_switch.setChecked(false);
                                    motion_switch.setTextOff("OFF");
                                }
                            }
                        }
                    });
                }
            }


            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                loader.dismiss();
                Log.d("Failure", "No Internet");
                //Toast.makeText(Emergencylightsetup.this, "In OnFail Method in mergency Setup", Toast.LENGTH_LONG).show();
                //  alert();

            }


        });
    }


    @Override
    public void networkUnavailable() {
        Log.i("unavailability", "networkUnavailable()");
        Log.d("No internet", "No Internet");
        loader.dismiss();


    }


    public void startNetworkBroadcastReceiver(Context currentContext) {
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener((NetworkStateReceiver.NetworkStateReceiverListener) currentContext);
        registerNetworkBroadcastReceiver(currentContext);
    }

    /**
     * Register the NetworkStateReceiver with your activity
     *
     * @param currentContext
     */
    public void registerNetworkBroadcastReceiver(Context currentContext) {
        currentContext.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     * Unregister the NetworkStateReceiver with your activity
     *
     * @param currentContext
     */
    public void unregisterNetworkBroadcastReceiver(Context currentContext) {
        currentContext.unregisterReceiver(networkStateReceiver);
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    private void getDefaultLocationId() {
        Log.d("default location", "into the location id after net connection");
        Call<List<GetLocation>> call = jsonplaceholder.getDefaultLocation("Bearer " + Auth_Token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if (response.isSuccessful()) {
                    Log.i("Succees", "onResponse: Yes");
                    List<GetLocation> get_list = response.body();
                    Log.d("Location Count", "onResponse: " + get_list.size());
                    if (get_list.isEmpty()) {
                        Toast.makeText(Emergencylightsetup.this, "Default location is not set for this account", Toast.LENGTH_SHORT).show();
                        //loaddialog.dismiss();
                    } else {
                        for (int i = 0; i < get_list.size(); i++) {
                            if (get_list.get(i).isDefaultLocationId()) {
                                loc = get_list.get(i).getLocationName();
                                loc_name.setText(loc);
                                Log.d("Location Name", "onResponse: " + loc);
                            }

                            //Log.i("Location Mil Gayi", "Abc " + Integer.toString(locationId));

                        }
                    }
                } else {
                    Toast.makeText(Emergencylightsetup.this, "LocationId Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.d("res error", "resuis: " + response.errorBody().toString());

                    //loaddialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                Toast.makeText(Emergencylightsetup.this, "No location set", Toast.LENGTH_SHORT).show();
                Log.d("failure12345", "error in fail");
            }
        });
    }
}