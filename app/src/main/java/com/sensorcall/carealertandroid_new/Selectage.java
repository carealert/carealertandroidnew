package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sensorcall.carealertandroid_new.ui.lovedones.LovedOne;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Selectage extends AppCompatActivity {

    Button continue_button;
    SeekBar age_bar;
    TextView age_class, age_disp;//,skip;
    ProgressDialog loaddialog, progressDialog;
    int age = 0;
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    private LovedOne lovedOne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectage);
        continue_button = (Button) findViewById(R.id.selectage_continue);
        age_bar = (SeekBar) findViewById(R.id.seek_bar);
        age_class = (TextView) findViewById(R.id.age_class);
        age_disp = (TextView) findViewById(R.id.age_disp);
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
        CareAlert_SharedData_Editor = CareAlert_SharedData.edit();

        loaddialog = new ProgressDialog(this);
        loaddialog = showLoadingDialog(this);
        loaddialog.dismiss();
        age_class.setText("");
        age_disp.setText("");

        lovedOne = getIntent().getParcelableExtra(Constants.LOVED_ONE);

        if (lovedOne != null) {
            age_bar.setProgress(lovedOne.getAge());
            showProgressType(lovedOne.getAge());
            age = lovedOne.getAge();
        }

        checkNetConnectivity();

/*
        age_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i==1){
                    age_class.setText("New Born");
                    age_disp.setText("You are caring for a Baby  who is "+ Integer.toString(i)+" year old.");
                }
                if (i>1 && i<=12){
                    age_class.setText("Baby");
                    age_disp.setText("You are caring for a Baby  who is "+ Integer.toString(i)+" years old.");
                }
                if (i>12 && i<20){
                    age_class.setText("Teenager");
                    age_disp.setText("You are caring for a Teenager  who is "+ Integer.toString(i)+" years old.");

                }
                if (i>=20 && i<60){
                    age_class.setText("Middle Aged");
                    age_disp.setText("You are caring for a Middle Aged Person  who is "+ Integer.toString(i)+" years old.");
                }
                if(i>=60){
                    age_class.setText("Elder Person");
                    age_disp.setText("You are caring for an Elder Person  who is "+ Integer.toString(i)+" years old.");
                }
                age=i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
/*
        String skipstring="Skip";
        SpannableString skip_span=new SpannableString(skipstring);
        ClickableSpan clickspan= new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                age=0;
                createlovedones();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        skip_span.setSpan(clickspan,0,4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //skip.setText(skip_span);

        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                createlovedones();
            }
        });
        */
    }

    void checkNetConnectivity() {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet", "devic123");
            setUpUI();
        } else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog() {
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    public void setUpUI() {
        age_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                showProgressType(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
/*
        String skipstring="Skip";
        SpannableString skip_span=new SpannableString(skipstring);
        ClickableSpan clickspan= new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                age=0;
                createlovedones();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        skip_span.setSpan(clickspan,0,4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //skip.setText(skip_span);
*/
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (age > 0) {
                    loaddialog.show();
                    continue_button.setEnabled(false);
                    //createlocation();
                    if (lovedOne != null) {
                        openSelectCaringPerson();
                    } else {
                        openquess();
                    }
                } else {
                    Toast.makeText(Selectage.this, R.string.please_select_age, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void openSelectCaringPerson() {
        Intent selectWho = new Intent(this, Selectwho.class);
        selectWho.putExtra(Constants.LOVED_ONE, lovedOne);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(selectWho);
    }


    private void showProgressType(int i) {
        if (i == 1) {
            age_class.setText("New Born");
            age_disp.setText("You are caring for a New Born who is " + Integer.toString(i) + " year old.");
        }
        if (i > 1 && i <= 12) {
            age_class.setText("Baby");
            age_disp.setText("You are caring for a Baby who is " + Integer.toString(i) + " years old.");
        }
        if (i > 12 && i < 20) {
            age_class.setText("Teenager");
            age_disp.setText("You are caring for a Teenager  who is " + Integer.toString(i) + " years old.");

        }
        if (i >= 20 && i < 60) {
            age_class.setText("Middle Aged");
            age_disp.setText("You are caring for a Middle Aged Person  who is " + Integer.toString(i) + " years old.");
        }
        if (i >= 60) {
            age_class.setText("Elder Person");
            age_disp.setText("You are caring for an Elder Person  who is " + Integer.toString(i) + " years old.");
        }
        age = i;
        CareAlert_SharedData_Editor.putInt("age", age);
        CareAlert_SharedData_Editor.commit();
        if (lovedOne != null) {
            lovedOne.setAge(age);
        }
    }

    public void openquess() {
        Intent devivesetup = new Intent(this, questionnaire.class);
        devivesetup.putExtra(Constants.LOVED_ONE, lovedOne);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(devivesetup);
    }


    public void openselectionpage() {
        Intent selectionpage = new Intent(this, Selection.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(selectionpage);
    }

   /* public void createlocation(){
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        String Authtoken= CareAlert_SharedData.getString("Auth_Token","");
        JsonObject body=new JsonObject();
        String LocationName= CareAlert_SharedData.getString("LocationNickName","");
        String location= CareAlert_SharedData.getString("GeoLocationData","Empty found");
        String Name=CareAlert_SharedData.getString("Name","");
        String ImageName= CareAlert_SharedData.getString("Img_Name","");
        body.addProperty("LocationName",LocationName);
        body.addProperty("LocationAddress",location);
        body.addProperty("GeoLocation",CareAlert_SharedData.getString("Latitude","")+","+CareAlert_SharedData.getString("Longitude",""));
        if(!CareAlert_SharedData.getBoolean("AddedviaSignupProcess",true))
            body.addProperty("IsDefaultLocationId",false);
        Call<Post> call= jsonPlaceHolderApi.createlocation("Bearer "+Authtoken,body);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(response.isSuccessful()){
                    if(!CareAlert_SharedData.getBoolean("AddedviaSignupProcess",true)){
                        CareAlert_SharedData_Editor.putInt("SecondaryLocation",response.body().getLocationId());
                        CareAlert_SharedData_Editor.commit();

                    }
                    else {
                        int Locationid = response.body().getLocationId();
                        CareAlert_SharedData_Editor.putInt("LocationId", Locationid);
                        CareAlert_SharedData_Editor.commit();

                    }
                    JsonObject LovedonesBody= new JsonObject();
                    LovedonesBody.addProperty("LovedOneProfilePicture",ImageName);
                    LovedonesBody.addProperty("Age",age);
                    LovedonesBody.addProperty("ProfileName",Name);
                    LovedonesBody.addProperty("LocationId",response.body().getLocationId());
                    Call LovedOneCreate= jsonPlaceHolderApi.createlovedones("Bearer "+Authtoken,LovedonesBody);
                    LovedOneCreate.enqueue(new Callback() {
                        @Override
                        public void onResponse(Call call, Response response) {
                            if(response.isSuccessful()){
                                loaddialog.dismiss();
                                openquess();
                            }
                            else{
                                try {
                                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                                    String Error = jObjError.getString("message");
                                    continue_button.setEnabled(true);
                                    loaddialog.dismiss();
                                    Toast.makeText(Selectage.this, Error, Toast.LENGTH_SHORT).show();

                                } catch (Exception e) {
                                    continue_button.setEnabled(true);
                                    loaddialog.dismiss();
                                    Toast.makeText(Selectage.this, "Unknown Error", Toast.LENGTH_SHORT).show();

                                }

                            }
                        }

                        @Override
                        public void onFailure(Call call, Throwable t) {
                            continue_button.setEnabled(true);
                            loaddialog.dismiss();
                            Toast.makeText(Selectage.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String Error = jObjError.getString("message");
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Selectage.this, Error, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                        Toast.makeText(Selectage.this, "Unknown Error", Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                continue_button.setEnabled(true);
                loaddialog.dismiss();
                Toast.makeText(Selectage.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }*/
//    public void createlovedones(){
//        Retrofit retrofit= new Retrofit.Builder()
//                .baseUrl("https://carealertdev.azurewebsites.net/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        JsonPlaceHolderApi jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
//        String Auth_Token= CareAlert_SharedData.getString("Auth_Token","");
//        String Name=CareAlert_SharedData.getString("Name","");
//        String ImageName= CareAlert_SharedData.getString("Img_Name","");
//        int Location_Id=0;
//        if(CareAlert_SharedData.getBoolean("AddedviaSignupProcess",true)) {
//            Location_Id = CareAlert_SharedData.getInt("LocationId", 0);
//        }else{
//            Location_Id = CareAlert_SharedData.getInt("SecondaryLocation", 0);
//        }
//        JsonObject body= new JsonObject();
//        body.addProperty("LovedOneProfilePicture",ImageName);
//        body.addProperty("LocationId",Location_Id);
//        body.addProperty("Age",age);
//        body.addProperty("ProfileName",Name);
//        Call<Post> call=jsonPlaceHolderApi.createlovedones("Bearer "+Auth_Token,body);
//        call.enqueue(new Callback<Post>() {
//            @Override
//            public void onResponse(Call<Post> call, Response<Post> response) {
//                if(response.isSuccessful()){
//                    if(CareAlert_SharedData.getBoolean("AddedviaSignupProcess",true)) {
//                        opendevicesetup();
//                    }
//                    else{
//                        Intent addlocation= new Intent(Selectage.this,AddLocation.class);
//                        addlocation.putExtra("DefaultLocationId",CareAlert_SharedData.getInt("LocationId",0));
//                        addlocation.putExtra("locname",CareAlert_SharedData.getString("locname",""));
//                        addlocation.putExtra("locadd",CareAlert_SharedData.getString("locadd",""));
//                        continue_button.setEnabled(true);
//                        loaddialog.dismiss();
//                        startActivity(addlocation);
//                    }
//                }
//                else{
//                    try{
//                        JSONObject jObjError = new JSONObject(response.errorBody().string());
//                        String Error = jObjError.getString("message");
//                        continue_button.setEnabled(true);
//                        loaddialog.dismiss();
//                        Toast.makeText(Selectage.this,Error, Toast.LENGTH_SHORT).show();
//                    }catch (Exception e){
//                        continue_button.setEnabled(true);
//                        loaddialog.dismiss();
//                        Toast.makeText(Selectage.this,"Unkown Error Occured", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Post> call, Throwable t) {
//                continue_button.setEnabled(true);
//                loaddialog.dismiss();
//                Toast.makeText(Selectage.this,t.getMessage(), Toast.LENGTH_SHORT).show();
//
//
//            }
//        });
//    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }


}