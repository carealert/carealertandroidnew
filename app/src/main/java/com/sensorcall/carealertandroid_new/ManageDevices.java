package com.sensorcall.carealertandroid_new;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.CustomBottomNavigationView1;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ManageDevices extends AppCompatActivity   implements NetworkStateReceiver.NetworkStateReceiverListener {
    private NetworkStateReceiver networkStateReceiver;
    Button add_device, edit_device, back;
    //Button turnoff;

    Drawable GoodHealth,BadHealth,Connected,Disconnected;
    AlertDialog.Builder builder;
    TextView txt_result, text;
    JsonPlaceHolderApi getdevice_api;
    SharedPreferences CareAlert_ShareData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    int default_id_initial=-1;
    ListView list;
    TextView update;
    String Auth;
    Boolean update_bool=false;
    List<GetDeviceByLocationId>value;
    ProgressDialog loader, progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_devices);
        Intent i=getIntent();
        GoodHealth=getResources().getDrawable(getResources().getIdentifier("@drawable/ic_active", null, getPackageName()));
        BadHealth=getResources().getDrawable(getResources().getIdentifier("@drawable/ic_warning", null, getPackageName()));
        Connected=getResources().getDrawable(getResources().getIdentifier("@drawable/ic_wifi", null, getPackageName()));
        Disconnected=getResources().getDrawable(getResources().getIdentifier("@drawable/ic_wifi_not", null, getPackageName()));

        startNetworkBroadcastReceiver(this);
        CareAlert_ShareData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor=CareAlert_ShareData.edit();
        default_id_initial= i.getIntExtra("DefaultLocationId", 0);
        CareAlert_SharedData_Editor.putInt("LocationId",default_id_initial);
        Auth=CareAlert_ShareData.getString("Auth_Token","");
        CareAlert_SharedData_Editor.commit();
        add_device=(Button) findViewById(R.id.mainbutton);
        FloatingActionButton floatingActionButton = findViewById(R.id.call_fab);
        loader = new ProgressDialog(this);
        loader=showLoadingDialog(this);

        update=findViewById(R.id.update);

        text= findViewById(R.id.text);
        list = findViewById(R.id.list_device);
        edit_device=findViewById(R.id.edit_btn);
        //turnoff=findViewById(R.id.turn_off_btn);

        add_device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(default_id_initial==0){
                    Toast.makeText(ManageDevices.this, "No Default Location Set, Cannot Add Device", Toast.LENGTH_SHORT).show();
                }
                else {
                    CareAlert_SharedData_Editor.putBoolean("AddedviaSignupProcess", false);
                    CareAlert_SharedData_Editor.putBoolean("ReconfigureBT", false);
                    CareAlert_SharedData_Editor.commit();
                    Log.d("Default Location id is", "onClick: " + default_id_initial);
                    Intent openNameDeviceRoom = new Intent(ManageDevices.this, Namedeviceroom.class);
                    startActivity(openNameDeviceRoom);
                }
            }
        });


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loader.show();
                Call<GetLocation> call1= getdevice_api.Firmwarerunning("Bearer "+Auth, default_id_initial);
                call1.enqueue(new Callback<GetLocation>() {
                    @Override
                    public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                        if(response.isSuccessful()){
                            Call<Void> call2=getdevice_api.updatedeviceByLocationId(default_id_initial,"Bearer "+Auth);

                            call2.enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Call<Void> call, Response<Void> response) {
                                    if(response.isSuccessful()){
                                        Toast.makeText(ManageDevices.this, "FirmWareUpdate Sent to Devices", Toast.LENGTH_SHORT).show();
                                        loader.dismiss();
                                    }
                                    else{
                                        Toast.makeText(ManageDevices.this, "Failed to Push Firmware to device", Toast.LENGTH_SHORT).show();
                                        loader.dismiss();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Void> call, Throwable t) {
                                    Toast.makeText(ManageDevices.this, "Failed to Push Firmware to device", Toast.LENGTH_SHORT).show();
                                    loader.dismiss();
                                }
                            });
                        }
                        else{
                            Toast.makeText(ManageDevices.this, "Firmware Update has been already sent to the Devices, Please retry after some time.", Toast.LENGTH_SHORT).show();
                            try {
                                Log.d("Update Response", "onResponse: "+response.code()+response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            loader.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetLocation> call, Throwable t) {
                        Toast.makeText(ManageDevices.this, "Failed to Push Firmware to device", Toast.LENGTH_SHORT).show();
                        loader.dismiss();
                    }
                });


            }
        });
    }

    public void alert() {
        builder = new AlertDialog.Builder(this);
        builder.setMessage("Cannot load the contents. Make sure your phone has an internet connection " +
                "and try again.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void alert1() {
        builder = new AlertDialog.Builder(this);
        builder.setMessage("Cannot load the contents. Make sure your phone has an internet connection " +
                "and try again.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void startNetworkBroadcastReceiver(Context currentContext) {
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener((NetworkStateReceiver.NetworkStateReceiverListener) currentContext);
        registerNetworkBroadcastReceiver(currentContext);
    }

    /**
     * Register the NetworkStateReceiver with your activity
     * @param currentContext
     */
    public void registerNetworkBroadcastReceiver(Context currentContext) {
        currentContext.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     Unregister the NetworkStateReceiver with your activity
     * @param currentContext
     */
    public void unregisterNetworkBroadcastReceiver(Context currentContext) {
        currentContext.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkUnavailable() {
        Log.i("unavailability", "networkUnavailable()");
        //Proceed with offline actions in activity (e.g. sInform user they are offline, stop services, etc...)


        Log.d("No internet", "No Internet");
    }

    public void networkAvailable()
    {
        Log.i("Availability", "networkAvailable()");
        //Proceed with online actions in activity (e.g. hide offline UI from user, start services, etc...)

        loader.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getdevice_api = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<GetDeviceByLocationId>> call = getdevice_api.getDevice(default_id_initial,"Bearer "+Auth);
        call.enqueue(new Callback<List<GetDeviceByLocationId>>() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onResponse(Call<List<GetDeviceByLocationId>> call, Response<List<GetDeviceByLocationId>> response) {
                if (response == null)
                {
                    Log.d("IsNull", "True");
                } else {
                    Log.d("IsNull", "False");
                }

                if(response.isSuccessful())
                {
                    List<GetDeviceByLocationId> getDevice = response.body();
                    ManageDeviceListAdapter adapter = new ManageDeviceListAdapter(getDevice, getApplicationContext(),GoodHealth,BadHealth,Connected,Disconnected);

                    for(int i=0;i< getDevice.size();i++)
                    {
                        Log.d("Device Status is ", "onResponse: "+getDevice.get(i).getDeviceStatus());
                        if(getDevice.get(i).getHealthStatus()==3)
                        {
                            update_bool=true;
                        }

                    }
                    Log.d("Update Value is", "onResponse: "+update_bool);
                    if(update_bool){
                        update.setVisibility(View.VISIBLE);
                    }
                    else{
                        update.setVisibility(View.INVISIBLE);
                    }
                    if(getDevice.size()==0)
                    {
                        text.setText("Need a CareAlert Device? Click here to Buy one.");
                        text.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("https://sensorscall.com/store/"));
                                startActivity(intent);
                            }
                        });
                        update.setVisibility(View.GONE);
                    }
                    list.setAdapter(adapter);

                    loader.dismiss();
                    list.setOnTouchListener(new ListView.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            int action = motionEvent.getAction();
                            switch (action) {
                                case MotionEvent.ACTION_DOWN:
                                    // Disallow ScrollView to intercept touch events.
                                    view.getParent().requestDisallowInterceptTouchEvent(true);
                                    break;

                                case MotionEvent.ACTION_UP:
                                    // Allow ScrollView to intercept touch events.
                                    view.getParent().requestDisallowInterceptTouchEvent(false);
                                    break;
                            }
                            // Handle ListView touch events.
                            view.onTouchEvent(motionEvent);
                            return true;
                        }
                    });




                    list.setOnItemClickListener(new AdapterView.OnItemClickListener()
                    {
                        BottomSheetDialog bottomsheet;
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
                        {
                            bottomsheet = new BottomSheetDialog(ManageDevices.this, R.style.BottomSheetDialogTheme);


                            View bottomSheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.bottomsheet_manage_device,
                                    findViewById(R.id.bottom_sheet_container));

                            bottomsheet.setContentView(bottomSheetView);

                            bottomsheet.setCanceledOnTouchOutside(false);
                            TextView txt11= (TextView)bottomsheet.findViewById(R.id.Room);
                            TextView txt2=(TextView)bottomsheet.findViewById(R.id.Room_add);
                            String str1=getDevice.get(i).getDeviceProfileName();
                            String str2=getDevice.get(i).getDeviceNumber();
                            boolean isOn= getDevice.get(i).getIsOn();
                            txt11.setVisibility(View.VISIBLE);
                            txt2.setVisibility(View.VISIBLE);
                            if(str1!=null) txt11.setText(str1);
                            if(str2!=null) txt2.setText(str2);
                            bottomsheet.show();
                            Button Reconfigure= bottomSheetView.findViewById(R.id.reconfig_btn);
                            Button edit_device= bottomsheet.findViewById(R.id.edit_btn);
                            Button dismiss=bottomsheet.findViewById(R.id.dismiss_btn);
                            Button delete=bottomsheet.findViewById(R.id.delete_btn);
//                            Button turnoff=bottomsheet.findViewById(R.id.turn_off_btn);
//                            if(!isOn)
//                                turnoff.setText("Turn On");

                            Reconfigure.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CareAlert_SharedData_Editor.putBoolean("ReconfigureBT",true);
                                    CareAlert_SharedData_Editor.commit();
                                    Intent openBTDevice= new Intent(ManageDevices.this,Bluetoothdevice.class);
                                    openBTDevice.putExtra(Constants.DEVICEID,getDevice.get(i).getDeviceNumber());
                                    startActivity(openBTDevice);
                                }
                            });

//                            turnoff.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    JsonObject body = new JsonObject();
//                                    body.addProperty("DeviceId", getDevice.get(i).getDeviceId());
//                                    body.addProperty("LocationId", getDevice.get(i).getLocationId());
//                                    if(isOn)
//                                        body.addProperty("IsOn", false);
//                                    else
//                                        body.addProperty("IsOn", true);
//                                    Call<GetDeviceByLocationId> call2=getdevice_api.updateDevice("Bearer "+Auth, body);
//                                    call2.enqueue(new Callback<GetDeviceByLocationId>() {
//                                        @SuppressLint("ClickableViewAccessibility")
//                                        @Override
//                                        public void onResponse(Call<GetDeviceByLocationId> call, Response<GetDeviceByLocationId> response)
//                                        {
//                                            if (response == null)
//                                            {
//                                                Log.d("IsNullUpdate", "True");
//                                            } else
//                                            {
//                                                Log.d("Response", String.valueOf(response.code()));
//                                            }
//
//                                            if(response.isSuccessful())
//                                            {
//                                                bottomsheet.dismiss();
//                                                if(!isOn)
//                                                Toast.makeText(getApplicationContext(),"Device TurnedOn", Toast.LENGTH_SHORT).show();
//                                                else
//                                                    Toast.makeText(getApplicationContext(),"Device TurnedOff", Toast.LENGTH_SHORT).show();
//
//                                                refresh();
//                                            }
//                                        }
//
//                                        @Override
//                                        public void onFailure(Call<GetDeviceByLocationId> call, Throwable t)
//                                        {
//                                            alert();
//
//                                        }
//                                    });
//                                }
//                            });

                            delete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                Call<Post> calldel=getdevice_api.deleteDevice("Bearer " + Auth,getDevice.get(i).getDeviceId());
                                calldel.enqueue(new Callback<Post>() {
                                    @Override
                                    public void onResponse(Call<Post> call, Response<Post> response) {
                                        if(response.isSuccessful()){
                                            Toast.makeText(ManageDevices.this, "Device Deleted Successfully", Toast.LENGTH_SHORT).show();
                                            refresh();
                                        }
                                        else{
                                            Toast.makeText(ManageDevices.this, "Failed to Delete Device", Toast.LENGTH_SHORT).show();
                                            bottomsheet.dismiss();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Post> call, Throwable t) {

                                        Toast.makeText(ManageDevices.this, "Failed to Delete Device", Toast.LENGTH_SHORT).show();
                                        bottomsheet.dismiss();
                                    }
                                });
                                }
                            });


                            edit_device.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    loader.show();
                                    bottomsheet.setContentView(R.layout.bottomsheet_edit_device_btn);
                                    EditText edit_devicename= bottomsheet.findViewById(R.id.edit_devicename);
                                    Spinner roomname_input= bottomsheet.findViewById(R.id.room_input);
                                    SeekBar Volume= bottomsheet.findViewById(R.id.seek_bar);
                                    final int[] Relatedidindex = {0};
                                    TextView Volumedes= bottomsheet.findViewById(R.id.vol);
                                    Call<JsonArray> call1= getdevice_api.GetRoomType("Bearer "+Auth);
                                   call1.enqueue(new Callback<JsonArray>() {
                                       @Override
                                       public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                                           if(response.isSuccessful()){
                                               JsonArray Body= response.body();
                                               ArrayList<String> RoomTypes= new ArrayList<>();
                                               for(int j=0;j<Body.size();j++){
                                                   RoomTypes.add(Body.get(j).toString().replace("\"",""));
                                                   Log.d("Input", "onResponse: "+Body.get(j).toString().toLowerCase().replace("\"",""));
                                                   Log.d("Input", "onResponse: Name "+getDevice.get(i).getDeviceRelatedId().toLowerCase());
                                                    if(Body.get(j).toString().toLowerCase().replace("\"","").equals(getDevice.get(i).getDeviceRelatedId().toLowerCase())){
                                                        Log.d("Entered", "onResponse: ");
                                                        Relatedidindex[0] =j;
                                                   }
                                               }
                                               ArrayAdapter<String> adapter = new ArrayAdapter<String>(ManageDevices.this, android.R.layout.simple_list_item_1);
                                               adapter.addAll(RoomTypes);
                                               adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                               roomname_input.setAdapter(adapter);
                                               roomname_input.setSelection(Relatedidindex[0]);
                                               Log.d("Posistion is", "onClick: "+Relatedidindex[0]);
                                               loader.dismiss();
                                           }
                                           else{
                                               Toast.makeText(ManageDevices.this, "Failed to Fetch Room Types Retrying-> not success", Toast.LENGTH_SHORT).show();
                                               //Intent Refresh= new Intent(Namedeviceroom.this,Namedeviceroom.class);
                                               //startActivity(Refresh);
                                               loader.dismiss();
                                               bottomsheet.dismiss();
                                           }
                                       }

                                       @Override
                                       public void onFailure(Call<JsonArray> call, Throwable t) {
                                           Toast.makeText(ManageDevices.this, "Failed to Fetch Room Types Retrying-> not success", Toast.LENGTH_SHORT).show();
                                           //Intent Refresh= new Intent(Namedeviceroom.this,Namedeviceroom.class);
                                           //startActivity(Refresh);
                                           loader.dismiss();
                                           bottomsheet.dismiss();
                                       }
                                   });
                                    edit_devicename.setText(getDevice.get(i).getDeviceProfileName());


                                    Volume.setProgress(getDevice.get(i).getVolume()-7);
                                    Volumedes.setText(String.valueOf(getDevice.get(i).getVolume()-7));
                                    Volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                        @Override
                                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                            Volumedes.setText(String.valueOf(i));
                                        }

                                        @Override
                                        public void onStartTrackingTouch(SeekBar seekBar) {

                                        }

                                        @Override
                                        public void onStopTrackingTouch(SeekBar seekBar) {

                                        }
                                    });
                                    Button cont=bottomsheet.findViewById(R.id.conti_btn);



                                    cont.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            String name = edit_devicename.getText().toString();
                                            if(name.length()==0){
                                                Toast.makeText(ManageDevices.this, "Device Name cannot be empty", Toast.LENGTH_SHORT).show();

                                            }
                                            else if(Volume.getProgress()==0){
                                                Toast.makeText(ManageDevices.this, "Cannot Set the Device Volume to 0", Toast.LENGTH_SHORT).show();

                                            }
                                            else if(Volume.getProgress()==getDevice.get(i).getVolume() && name==getDevice.get(i).getDeviceProfileName() && roomname_input.getSelectedItem().toString().toLowerCase()==getDevice.get(i).getDeviceRelatedId().toLowerCase() ){
                                                bottomsheet.dismiss();
                                            }
                                            else {
                                                loader.show();
                                            Log.i("New Device Name is ", "onClick: " + name);
                                            Log.i("New Device Name id id", "onClick: " + getDevice.get(i).getDeviceId());
                                            JsonObject body = new JsonObject();
                                            body.addProperty("DeviceId", getDevice.get(i).getDeviceId());
                                            body.addProperty("DeviceProfileName", name);
                                            body.addProperty("DeviceRelatedId",roomname_input.getSelectedItem().toString().toLowerCase());
                                            body.addProperty("Volume",Volume.getProgress()+7);
                                            Log.i("Json Body is ", "onClick: " + body.toString());
                                            Call<GetDeviceByLocationId> call1 = getdevice_api.updateDevice("Bearer " + Auth, body);
                                            getDevice.get(i).setDeviceProfileName(name);
                                            bottomsheet.dismiss();
                                            call1.enqueue(new Callback<GetDeviceByLocationId>() {
                                                @Override
                                                public void onResponse(Call<GetDeviceByLocationId> call, Response<GetDeviceByLocationId> response) {

                                                    if (response.isSuccessful()) {
                                                        Toast.makeText(ManageDevices.this, "Device Updated ", Toast.LENGTH_SHORT).show();
                                                        refresh();
                                                    } else {
                                                        Toast.makeText(ManageDevices.this, "Device Update Failed", Toast.LENGTH_SHORT).show();
                                                        try {
                                                            Log.d("Device Update", "onResponse: "+response.code()+response.errorBody().string());
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                        loader.dismiss();
                                                    }
//
                                                }

                                                @Override
                                                public void onFailure(Call<GetDeviceByLocationId> call, Throwable t) {
                                                    loader.dismiss();
                                                    alert();
                                                    //Toast.makeText(ManageDevices.this, "Fails to updat location", Toast.LENGTH_SHORT).show();
                                                }


                                            });
                                        }
                                        }
                                    });
                                }
                            });

                            dismiss.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    bottomsheet.dismiss();
                                }
                            });




                        }

                    });
                }
                else
                {
                    Toast.makeText(ManageDevices.this, "Unrecognized error", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<List<GetDeviceByLocationId>> call, Throwable t) {
                loader.dismiss();
                alert1();
                Log.d("failure", "Fails to inflate the list");
                //Toast.makeText(ManageDevices.this, "Null", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onPause() {
        /***/
        unregisterNetworkBroadcastReceiver(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        /***/
        registerNetworkBroadcastReceiver(this);
        super.onResume();
    }


    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public void refresh(){
        Intent refresh=new Intent(ManageDevices.this,ManageDevices.class);

        refresh.putExtra("DefaultLocationId",default_id_initial);
        startActivity(refresh);

    }

    public void onBackPressed() {
        Intent opensettings= new Intent(ManageDevices.this,Settings.class);
        startActivity(opensettings);
        return;
    }


}





