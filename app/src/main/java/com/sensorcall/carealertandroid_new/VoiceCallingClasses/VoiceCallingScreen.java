package com.sensorcall.carealertandroid_new.VoiceCallingClasses;



import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.Dashboard;
import com.sensorcall.carealertandroid_new.HelperClass.LiveChat;
import com.sensorcall.carealertandroid_new.Notification;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.STUN.app_client.AppClient;
import com.sensorcall.carealertandroid_new.Settings;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class VoiceCallingScreen extends AppCompatActivity {

    private static final int REQUEST_PERMISSION = 123;
    private static LiveChat chatClient;
    private AppClient appClient;
    private static boolean isCalling;
    private Button btn_end_call, btn_mike;
    String care_receiver, device_name, device_number;
    int locationId;
    private TextView txt_call_mom, txt_info_call, txt_date_time, txt_info_room, txt_call_status,txt_mike;
    String current_date_time = new SimpleDateFormat("MMM dd, yyyy, hh:mm aa", Locale.getDefault()).format(new Date());
    SharedPreferences CareAlert_SharedData;
    String Auth_token;
    private static Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voicecallingscreen);

        Intent selectToCall = getIntent();
        care_receiver = selectToCall.getStringExtra("txt_call_mom");
        device_name = selectToCall.getStringExtra("current_location");
        device_number = selectToCall.getStringExtra("device_number");

        //     locationId = selectToCall.getIntExtra("LocationId",0);

        Log.d("xyzdeviceNUmber","abc: "+device_number);
        Log.d("xyzdeviceName","abc: "+device_name);
        mContext=this;
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData",0);
        Auth_token=CareAlert_SharedData.getString("Auth_Token","");
        Log.d("abcd","checkAuthToken: "+Auth_token);
//        appClient.authKey(Auth_token);

        //Attached view to button
        txt_info_call = (TextView) findViewById(R.id.text_info_call);
        txt_call_mom = (TextView) findViewById(R.id.title_call_mom);
        txt_info_room = (TextView) findViewById(R.id.text_info_room);
        txt_call_status = (TextView) findViewById(R.id.text_call_stauts);
        txt_mike = (TextView) findViewById(R.id.text_mike);
        txt_mike.setVisibility(View.INVISIBLE);
        btn_mike = (Button) findViewById(R.id.button_mike);
        btn_mike.setVisibility(View.INVISIBLE);
        txt_call_status.setText("Connecting...");
        txt_info_room.setText(device_name);
        txt_call_mom.setText("Call "+care_receiver);
        txt_info_call.setText("Place A Voice call to your "+care_receiver);
//        btn_end_call = (Button) findViewById(R.id.button_end_call);
        txt_date_time = (TextView) findViewById(R.id.text_time);
        txt_date_time.setText(current_date_time);

        FloatingActionButton floatingActionButton = findViewById(R.id.end_fab);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        checkNetConnectivity();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionButton.setEnabled(false);
                cancelCall();
            }
        });
        bottomNavigation();
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                        REQUEST_PERMISSION);
                initiateCall();
                //    Log.d("xyz","not_initatecall:");
            } else {
                initiateCall();
                //   Log.d("xyz","initatecall:");
            }
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    private void initiateCall() {
        Thread callThread = new Thread(new Runnable() {
            @Override
            public void run() {
                isCalling = true;
                try {
                    Log.e("AUth going in ", ""+Auth_token );
                    chatClient = new LiveChat(device_number, device_name, VoiceCallingScreen.this,Auth_token);
                    boolean initRes = chatClient.chatInit();
                    Log.d("return value","Check: initRes:  "+initRes);   //showing false
                    if (initRes) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                txt_call_status.setText("Connected");
                                txt_call_status.setTextColor(Color.parseColor("#539B81"));
                                btn_mike.setVisibility(View.VISIBLE);
                                btn_mike.setSoundEffectsEnabled(false);
                                btn_mike.setBackgroundResource(R.drawable.ic_mic);
                                txt_mike.setVisibility(View.VISIBLE);
                                txt_mike.setText("Press and Hold to Talk");
                                btn_mike.setOnTouchListener(new View.OnTouchListener() {
                                    @Override
                                    public boolean onTouch(View v, MotionEvent event) {
                                        switch (event.getAction()) {
                                            case MotionEvent.ACTION_DOWN:
                                                if (NetConnection.checkConnection(VoiceCallingScreen.this) == true) {
                                                    chatClient.startTalk();
                                                    btn_mike.setSoundEffectsEnabled(false);
                                                    btn_mike.setBackgroundResource(R.drawable.ic_mic_green);
                                                    txt_mike.setText("Talk");
                                                }else{
                                                    Toast.makeText(VoiceCallingScreen.this, "Please Check internet connection", Toast.LENGTH_SHORT).show();
                                                    cancelCall();
                                                }
                                                break;
                                            case MotionEvent.ACTION_UP:
                                                chatClient.stopTalk();
                                                btn_mike.setBackgroundResource(R.drawable.ic_mic);
                                                txt_mike.setText("Press and Hold to Talk");
                                                break;
                                        }
                                        return false;
                                    }
                                });
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                cancelCall();
                            }
                        });
                    }
                } catch (Exception e) {
                    // Log.d("xyz","initatecall:Catch");
                    e.printStackTrace();
                }
            }
        });
        callThread.start();
    }
//        Log.d("devicenumber","checkD " + device_number);
//        Log.d("portnumber","checkP " + port_number);


    public void cancelCall(){
        btn_mike.setVisibility(View.INVISIBLE);
        txt_mike.setVisibility(View.INVISIBLE);
        txt_call_status.setText("Disconnecting...");
        txt_call_status.setTextColor(Color.parseColor("#FF3F35"));

        Thread cancelCallThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (chatClient != null) {
                    chatClient.chatTerminate();
                }
                isCalling = false;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent selectTocall = new Intent(VoiceCallingScreen.this, SelectToCall.class);
                        startActivity(selectTocall);
                        finish();
                    }
                });
            }
        });
        cancelCallThread.start();
    }

    public static  void FailedCall(){
        Toast.makeText(mContext, "Call Failed from Device Side, Call Disconnected", Toast.LENGTH_SHORT).show();
        if (chatClient != null) {
            chatClient.chatTerminate();
        }
        isCalling = false;
        Intent SelecttoCallAct= new Intent(mContext,SelectToCall.class);
        mContext.startActivity(SelecttoCallAct);
    }


    //Bottom Navigation
    public void bottomNavigation(){

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        Menu menu = navigation.getMenu();
        MenuItem menuitem = menu.getItem(4);
        menu.getItem(0).setEnabled(false);
        menu.getItem(1).setEnabled(false);
        menu.getItem(3).setEnabled(false);
        menu.getItem(4).setEnabled(false);
        menuitem.setChecked(true);
        menuitem.setEnabled(false);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.navigation_dashboard:
                       // Toast.makeText(VoiceCallingScreen.this, "Dashboard clicked", Toast.LENGTH_SHORT).show();
                        Intent dashBoard = new Intent(VoiceCallingScreen.this, Dashboard.class);
                        startActivity(dashBoard);
                        break;

                    case R.id.navigation_Reminders:
                        Intent reminder = new Intent(VoiceCallingScreen.this, RemindersViewActivity.class);
                     //   Toast.makeText(VoiceCallingScreen.this, "Reminders clicked", Toast.LENGTH_SHORT).show();
                        startActivity(reminder);
                        break;

                    case R.id.navigation_notifications:
                      //  Toast.makeText(VoiceCallingScreen.this, "Notifications click", Toast.LENGTH_SHORT).show();
                        Intent notification = new Intent(VoiceCallingScreen.this, Notification.class);
                        startActivity(notification);
                        break;

                    case R.id.navigation_settings:
                      //  Toast.makeText(VoiceCallingScreen.this, "Settings clicked", Toast.LENGTH_SHORT).show();
                        Intent settings = new Intent(VoiceCallingScreen.this, Settings.class);
                        startActivity(settings);
                        break;
                }
                return true;
            }
        });
    }

    // Back press not need to work
    @Override
    public void onBackPressed() {
        Toast.makeText(VoiceCallingScreen.this,"Connected to Device, Please disconnect call for further activity",Toast.LENGTH_LONG).show();
        return;
    }
}
