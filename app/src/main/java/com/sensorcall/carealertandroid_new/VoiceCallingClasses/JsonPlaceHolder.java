package com.sensorcall.carealertandroid_new.VoiceCallingClasses;

import com.sensorcall.carealertandroid_new.GetLocation;
import com.sensorcall.carealertandroid_new.Post;
import com.sensorcall.carealertandroid_new.VoiceReminder.GetReminder;
import com.google.gson.JsonObject;
import com.sensorcall.carealertandroid_new.model.DeviceRegistrationStatus;
import com.sensorcall.carealertandroid_new.ui.email.Email;
import com.sensorcall.carealertandroid_new.ui.email.ResendEmail;
import com.sensorcall.carealertandroid_new.ui.email.UpdateUserRequest;
import com.sensorcall.carealertandroid_new.ui.lovedones.LovedOne;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface JsonPlaceHolder {

    @GET("api/Device/GetDeviceByLocationId")
    Call<List<GetDevicePojo>> getPojo(@Query("LocationId") int locationId,
                                      @Header("Authorization") String Authtoken);

    @POST("api/Device/Notify")
    Call<GetDevicePojo> getNotify(
            @Header("Authorization") String Authtoken,
            @Body JsonObject Body
    );

    @GET("/api/LovedOnes/GetLocationForLoggedInUser")
    Call<List<GetLocation>> getDefaultLocation(
            @Header("Authorization") String Authtoken
    );

    @GET("/api/LovedOnes/ActivityNotification")
    Call<List<GetLocation>> getActivity(
            @Header("Authorization") String Authtoken,
            @Query("Locationid") int LocationId
    );


    @GET("/api/LovedOnes/GetReminder")
    Call<List<GetReminder>> getReminder(
            @Header("Authorization") String Authtoken,
            @Query("LocationId") int LocationId
    );

    @DELETE("/api/LovedOnes/DeleteReminder")
    Call<GetReminder> deleteReminder(
            @Header("Authorization") String Authtoken,
            @Query("ReminderId") int ReminderId
    );

    @POST("/api/LovedOnes/CreateReminder")
    Call<GetReminder> createReminder(
            @Header("Authorization") String Authtoken,
            @Body JsonObject Body
    );

    @PUT("/api/LovedOnes/UpdateReminder")
    Call<GetReminder> updateReminder(
            @Header("Authorization") String Authtoken,
            @Body JsonObject Body
    );

    @Multipart
    @POST("/api/LovedOnes/audio")
    Call<GetReminder> audio(
            @Header("Authorization") String Authtoken,
            @Query("LocationId") int Locationid,
            @Part MultipartBody.Part file
    );

    @GET("/api/Device/LastActivity")
    Call<GetLocation> GetLastAct(
            @Header("Authorization") String Authtoken,
            @Query("LocationId") int LocationId
    );


    @GET("/api/Device/IsDeviceAssociatedWithUser")
    Call<DeviceRegistrationStatus> getDeviceRegistrationStatus(
            @Query("DeviceNumber") String deviceNumber
    );

    @GET("/api/LovedOnes/GetLovedOnesByLocationId")
    Call<List<LovedOne>> getLovedOnesForLoggedInUser(@Query("LocationId") int locationId
    );

    @PUT("/api/LovedOnes/UpdateLovedOnes")
    Call<ResponseBody> updateLovedOnes(@Body LovedOne lovedOne);


    @GET("api/Account/GetUserEmailIdAndEmailConfirmation")
    Call<Email> getUserEmailIdAndEmailConfirmation();

    @FormUrlEncoded
    @POST("/api/Account/ResendEmailNotification")
    Call<ResendEmail> resendEmailNotification(@Field("email") String email);


    @PUT(" /api/Account/UpdateUser")
    Call<ResponseBody> updateUser(@Body UpdateUserRequest updateUserRequest);

}
