package com.sensorcall.carealertandroid_new.VoiceCallingClasses;

public class GetDevicePojo {


    private int DeviceId;
    private String DeviceNumber;
    private String DeviceRelatedId;
    private String DeviceProfileName;
    private int DeviceStatus;
    private int LocationId;
    private int HealthStatus;
    private boolean IsOn;
    private String devicenumber;

    public GetDevicePojo(String devicenumber, String portnumber) {
        this.devicenumber = devicenumber;
        this.portnumber = portnumber;
    }

    private String portnumber;

    public int getDeviceId() {
        return DeviceId;
    }

    public String getDeviceNumber() {
        return DeviceNumber;
    }

    public String getDeviceRelatedId() {
        return DeviceRelatedId;
    }

    public String getDeviceProfileName() {
        return DeviceProfileName;
    }

    public int getDeviceStatus() {
        return DeviceStatus;
    }

    public int getLocationId() {
        return LocationId;
    }

    public int getHealthStatus() {
        return HealthStatus;
    }

    public boolean isOn() {
        return IsOn;
    }

}