package com.sensorcall.carealertandroid_new.VoiceCallingClasses;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.Dashboard;
import com.sensorcall.carealertandroid_new.DashboardWeekly;
import com.sensorcall.carealertandroid_new.GetLocation;
import com.sensorcall.carealertandroid_new.JsonPlaceHolderApi;
import com.sensorcall.carealertandroid_new.LovedOnes;
import com.sensorcall.carealertandroid_new.Notification;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.Settings;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.core.content.res.ResourcesCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SelectToCall extends AppCompatActivity {

    TextView txt_info_call,LocationName;
    TextView txt_date_time;
    int sizeofButton=0;
    String current_date_time = new SimpleDateFormat("MMM dd, yyyy, hh:mm aa", Locale.getDefault()).format(new Date());
    String care_receiver_location, device_number,loc;
    private List<GetDevicePojo> pojos;
    ProgressDialog loader,progressDialog;
    int locationId;
    //private int device_id;
    String care_receiver_type;
    private JsonPlaceHolder jsonPlaceHolderApi;
    SharedPreferences CareAlert_SharedData;
    Boolean fnccheck[];
    Button DeviceButton[];
    String LastActivityDeviceNumber="";
    Drawable drawable, changeButtonColor,icon;
    boolean DeviceSelected;
    String Auth_Token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selecttocall);

        loader = new ProgressDialog(SelectToCall.this);
        loader=showLoadingDialog(this);

//        loader=new ProgressDialog(this);
//        loader.setMessage("Loading...");
//        loader.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData",0);
        Auth_Token= CareAlert_SharedData.getString("Auth_Token","");
        LocationName = (TextView) findViewById(R.id.location_name);

//        Log.d("abc","care_rec: "+care_receiver_type);
//        Log.d("abc","location: "+locationId);

        //Api calling
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

       // jsonPlaceHolderApi = retrofit.create(JsonPlaceHolder.class);
      //  jsonPlaceHolder= retrofit.create(JsonPlaceHolderApi.class);

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolder.class);

        checkNetConnectivity();
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            getDefaultLocationId();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    // Default location for devices
    private void getDefaultLocationId(){
        loader.show();
        Log.d("default location","into the location id after net connection");
        String Auth_token=CareAlert_SharedData.getString("Auth_Token","");
        Call<List<GetLocation>> call = jsonPlaceHolderApi.getDefaultLocation("Bearer "+Auth_token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if(response.isSuccessful())
                {
                    Log.i("Succees", "onResponse: Yes");
                    List<GetLocation> get_list = response.body();
                    if(get_list.isEmpty()) {
                        Toast.makeText(SelectToCall.this, "Default location is not set for this account", Toast.LENGTH_SHORT).show();
                        loader.dismiss();
                    }
                    else{
                        for(int i=0;i<get_list.size();i++){
                            if(get_list.get(i).isDefaultLocationId()) {
                                locationId = get_list.get(i).getLocationId();
                                loc = get_list.get(i).getLocationName();
                                LocationName.setText(loc);
                                Log.d("Location Name", "onResponse:Select to Call "+loc);
                                List<LovedOnes> Lovedonelist= get_list.get(i).getLovedOne();
                                for (int j=0;j<Lovedonelist.size();j++){
                                    if(Lovedonelist.get(j).isPrimary()){
                                        care_receiver_type=Lovedonelist.get(j).getProfileName();
                                        break;
                                    }
                                }

                                Log.i("Location Mil Ga yi", "Abc " + Integer.toString(locationId));
                                Call<GetLocation> call2= jsonPlaceHolderApi.GetLastAct("Bearer "+Auth_token,locationId);
                                call2.enqueue(new Callback<GetLocation>() {
                                    @Override
                                    public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                                        if(response.isSuccessful()){
                                            GetLocation result= response.body();
                                            LastActivityDeviceNumber= result.getDeviceNumber();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<GetLocation> call, Throwable t) {

                                    }
                                });

                                getPojo();
                            }
                        }
                    }
                }
                else{
                    Toast.makeText(SelectToCall.this, "LocationId Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.d("res error","resuis: "+response.errorBody().toString());
                    loader.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                Toast.makeText(SelectToCall.this, "No Default location set", Toast.LENGTH_SHORT).show();
                Log.d("failure12345","error in fail");
                loader.dismiss();
            }
        });
    }


    //For Retrieving details of devices as per location
    void getPojo(){
        loader.show();
        //   final String auth = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIrOTE4Mjc1MjQ1MTM5IiwianRpIjoiYWIzMjc2OTktNDAzZi00YjBlLWE5NzgtODBlNmIyZTg4YjcxIiwiaWF0IjoxNjAxMjg3OTk3LCJyb2xlIjoiYXBpX2FjY2VzcyIsImlkIjoiMjZmMDVkOGItNDIyYS00Y2Y1LTk2NDUtMDQ0NzJmYzY4NThjIiwibmJmIjoxNjAxMjg3OTk2LCJleHAiOjE2MDE4OTI3OTYsImlzcyI6IndlYkFwaSIsImF1ZCI6Imh0dHA6Ly9jYXJlYWxlcnRkZXYuYXp1cmV3ZWJzaXRlcy5uZXQvIn0.5Dkswm5E78AESwzYrPIxlPvLK7IGU8fJV8-DQqqDMmM";
        String Auth_token=CareAlert_SharedData.getString("Auth_Token","");
        Log.d("Auth_token","Check---: "+Auth_token);
        Call<List<GetDevicePojo>> call = jsonPlaceHolderApi.getPojo(locationId,"Bearer "+Auth_token);
        Log.d("location123","locationgetpojo: "+locationId);
        call.enqueue(new Callback<List<GetDevicePojo>>() {

            @Override  @RequiresApi(api = Build.VERSION_CODES.N)
            public void onResponse(Call<List<GetDevicePojo>> call, Response<List<GetDevicePojo>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(SelectToCall.this, "Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    loader.dismiss();
                    return;
                }
                else {
                    pojos = response.body();
                    pojos.sort(Comparator.comparing(GetDevicePojo::getDeviceProfileName));
                    Log.d("Response_","pojos=="+ pojos.toString());
                    setUpUI();
                }
            }
            @Override
            public void onFailure(Call<List<GetDevicePojo>> call, Throwable t) {
                Toast.makeText(SelectToCall.this, "Failed", Toast.LENGTH_SHORT).show();
                loader.dismiss();
            }
        });
    }

    // Add button to UI basis of API response.
    @SuppressLint("ResourceType")
    private void setUpUI() {
        Resources res = getResources();
//        Drawable drawable= ResourcesCompat.getDrawable(res, R.drawable.add_more, null);
//        Drawable changeButtonColor = ResourcesCompat.getDrawable(res, R.drawable.change_device_button, null);

        drawable= ResourcesCompat.getDrawable(res, R.drawable.add_more, null);
        changeButtonColor = ResourcesCompat.getDrawable(res, R.drawable.change_device_button, null);
        icon= ResourcesCompat.getDrawable(res, R.drawable.ic_presense, null);


        FloatingActionButton floatingActionButton = findViewById(R.id.call_fab);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        TextView txt_call_mom = (TextView) findViewById(R.id.title_call_mom);
        txt_call_mom.setText("Call "+ care_receiver_type);
        txt_info_call = (TextView) findViewById(R.id.text_info_call);
        txt_info_call.setText("Place a Voice call to "+care_receiver_type);
        txt_date_time = (TextView) findViewById(R.id.text_time);
        txt_date_time.setText(current_date_time);


        LinearLayout layout= (LinearLayout) findViewById(R.id.buttons_layout);
        if(pojos.size()>0) {
            for (int i = 0; i < pojos.size(); i++){
                if (pojos.get(i).isOn() == true && pojos.get(i).getDeviceStatus() == 3){
                    sizeofButton +=1;
                }
            }
            DeviceButton= new Button[sizeofButton];
            fnccheck= new Boolean[sizeofButton];
            for(int i=0;i<sizeofButton;i++){
                fnccheck[i]=false;
            }
            if(sizeofButton==0)
            {
                Toast.makeText(SelectToCall.this, "No Devices are Online at this location", Toast.LENGTH_SHORT).show();

            }
            for (int i = 0; i < pojos.size(); i++) {
                if (pojos.get(i).isOn() == true && pojos.get(i).getDeviceStatus() == 3) {
                    DeviceButton[i]= new Button(this);
                    LinearLayout.LayoutParams layoutParams =new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    DeviceButton[i].setLayoutParams(layoutParams);
                    layoutParams.setMargins(150,30,150,5);
                    DeviceButton[i].setText(pojos.get(i).getDeviceProfileName());
                    DeviceButton[i].setTextColor(Color.parseColor("#ffffff"));
                    DeviceButton[i].setBackgroundDrawable(drawable);
                    DeviceButton[i].setTextSize(16);
                    DeviceButton[i].setAllCaps(false);
                    DeviceButton[i].setId(pojos.get(i).getDeviceId());
                    //DeviceButton[i].setCompoundDr awables(null,null,icon,null);
                    //DeviceButton[i].setCompoundDrawablesRelative(null,null,icon,null);
                    if(pojos.get(i).getDeviceNumber().equals(LastActivityDeviceNumber))
                        DeviceButton[i].setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,icon,null);

                    //DeviceButton[i].setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.ic_presense,0);
                    layout.addView(DeviceButton[i]);

                    int Deviceid=pojos.get(i).getDeviceId();
                    Button btn= DeviceButton[i];
                    int currentbutton=i;
                    final String[] deviceName = {pojos.get(i).getDeviceProfileName()};
                    String deviceNumber = pojos.get(i).getDeviceNumber();
                    //    int deviceId = pojos.get(i).getDeviceId();
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            btn_call.setEnabled(true);
                            if (fnccheck[currentbutton]) {
                                DeviceSelected=false;
                                btn.setTextColor(Color.parseColor("#ffffff"));
                                btn.setBackgroundDrawable(drawable);
                                care_receiver_location = deviceName[0];
                                device_number = deviceNumber;
                                fnccheck[currentbutton] = false;
                            } else {

                                DeviceSelected=true;
                                btn.setTextColor(Color.parseColor("#ffffff"));
                                btn.setBackgroundDrawable(changeButtonColor);
                                care_receiver_location = deviceName[0];
                                device_number = deviceNumber;
                                Log.d("DN","devicenumber1234: "+device_number);
                                fnccheck[currentbutton] = true;
                                for(int i=0;i<sizeofButton;i++){
                                    if(i==currentbutton)
                                        continue;
                                    fnccheck[i]=false;
                                }
                                UncheckButton(currentbutton);
                            }
                        }
                    });
                }
            }
        }
        else{
            Toast.makeText(SelectToCall.this, "No Devices are installed at this location", Toast.LENGTH_SHORT).show();

        }

//        LinearLayout layout= (LinearLayout) findViewById(R.id.buttons_layout);
//        if(pojos.size()>0) {
////            fnccheck= new Boolean[pojos.size()];
////            for(int i=0;i<pojos.size();i++){
////                fnccheck[i]=false;
////            }
//            for (int i = 0; i < pojos.size(); i++) {
//                if (pojos.get(i).isOn() == true) {
//                    Button btn = new Button(this);
//                    LinearLayout.LayoutParams layoutParams =new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
//                            LinearLayout.LayoutParams.WRAP_CONTENT);
//                    btn.setLayoutParams(layoutParams);
//                    layoutParams.setMargins(150,30,150,5);
//                    btn.setText(pojos.get(i).getDeviceProfileName());
//                    btn.setTextColor(Color.parseColor("#ffffff"));
//                    btn.setBackgroundDrawable(drawable);
//                    btn.setTextSize(16);
//                    btn.setAllCaps(false);
//                    btn.setId(pojos.get(i).getDeviceId());
//                    btn.setWidth(240);
//                    layout.addView(btn);
//                 //   int currentbutton=i;
//
//                    final boolean[] click = {true};
//                //    int deviceId = pojos.get(i).getDeviceId();
//                    final String[] deviceName = {pojos.get(i).getDeviceProfileName()};
//                    String deviceNumber = pojos.get(i).getDeviceNumber();
//                    btn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            if(click[0]) {
//                            floatingActionButton.setEnabled(true);
//                            care_receiver_location = deviceName[0];
//                            device_number = deviceNumber;
//                            btn.setBackgroundDrawable(changeButtonColor);
//                            click[0] = false;
////                          //  device_id =  deviceId;
//                        }
//                        else {
//                            btn.setBackgroundDrawable(drawable);
//                            click[0] = true;
//                        }
//                        }
//                    });
//                }
//            }
//        }
//        else{
//            Toast.makeText(SelectToCall.this, "Devices has not installed at this location", Toast.LENGTH_SHORT).show();
//        }
//
        loader.dismiss();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DeviceSelected) {
                    floatingActionButton.setEnabled(false);
                    Toast.makeText(SelectToCall.this, "Calling to : " + device_number, Toast.LENGTH_SHORT).show();
                    Intent voice_calling_screen = new Intent(SelectToCall.this, VoiceCallingScreen.class);
                    voice_calling_screen.putExtra("current_location", care_receiver_location);
                    voice_calling_screen.putExtra("txt_call_mom", care_receiver_type);
                    voice_calling_screen.putExtra("device_number", device_number);
                    loader.dismiss();
                    startActivity(voice_calling_screen);
                }
                else{
                    Toast.makeText(SelectToCall.this, "Select a Device To Call", Toast.LENGTH_SHORT).show();
                }
            }
        });

        bottomNavigation();
    }


    public void UncheckButton(int currbtn){
        for(int i=0;i<sizeofButton;i++){
            if(i==currbtn)
                continue;
            DeviceButton[i].setBackground(drawable);
            DeviceButton[i].setTextColor(Color.parseColor("#ffffff"));
        }
    }

    //Bottom Navigation
    public void bottomNavigation(){

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        Menu menu = navigation.getMenu();
        MenuItem menuitem = menu.getItem(2);
        menuitem.setChecked(true);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.navigation_dashboard:
                        //  Toast.makeText(SelectToCall.this, "Dashboard clicked", Toast.LENGTH_SHORT).show();
                        Intent dashBoard = new Intent(SelectToCall.this, Dashboard.class);
                        startActivity(dashBoard);
                        SelectToCall.this.finish();
                        break;

                    case R.id.navigation_Reminders:
                        Intent reminder = new Intent(SelectToCall.this, RemindersViewActivity.class);
                        //  Toast.makeText(SelectToCall.this, "Reminders clicked", Toast.LENGTH_SHORT).show();
                        startActivity(reminder);
                        break;

                    case R.id.navigation_notifications:
                        //  Toast.makeText(SelectToCall.this, "Notifications click", Toast.LENGTH_SHORT).show();
                        Intent notification = new Intent(SelectToCall.this, Notification.class);
                        startActivity(notification);
                        break;

                    case R.id.navigation_settings:
                        //  Toast.makeText(SelectToCall.this, "Settings clicked", Toast.LENGTH_SHORT).show();
                        Intent settings = new Intent(SelectToCall.this, Settings.class);
                        startActivity(settings);
                        break;
                }
                return true;
            }
        });
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    // Back press button
    @Override
    public void onBackPressed() {
        Intent dashBoard = new Intent(SelectToCall.this,Dashboard.class);
        startActivity(dashBoard);
    }
}

