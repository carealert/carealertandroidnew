package com.sensorcall.carealertandroid_new.HelperClass;


import android.content.Context;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import com.sensorcall.carealertandroid_new.HelperClass.AudioStream.AudioStreaming;
import com.sensorcall.carealertandroid_new.HelperClass.NetUtils.IPEndPoint;
import com.sensorcall.carealertandroid_new.STUN.app_client.AppClient;
import com.sensorcall.carealertandroid_new.STUN.app_client.CommuMessage;
import com.sensorcall.carealertandroid_new.STUN.client.STUN_Client;
import com.sensorcall.carealertandroid_new.STUN.client.STUN_NatType;
import com.sensorcall.carealertandroid_new.STUN.client.STUN_Result;

import java.util.Random;

public class LiveChat {
    private static final String TAG = "LiveChat";

    private IPEndPoint master;
    private IPEndPoint target;
    private AudioStreaming audioStreaming;
    private Context ringtoneContext;
    private String STUN_Server;
    private String TURN_Server;
    private int STUN_Port;
    private int[] TURN_Ports = {7000, 8000, 9000, 10000, 11000, 12000, 13000};
    private int TURN_Port;
    private AppClient appClient;
    private String deviceId;
    private String deviceName;
    private String auth2;
    private Uri ringtone;
    private boolean isRinging;

    public LiveChat(String deviceId, String deviceName, Context ringtoneContext,String auth1) {
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.ringtoneContext = ringtoneContext;
        this.auth2 = auth1;
        Log.d("authLiveChat","authhhh: "+auth2);
        Log.d("ID and Number Check", "chatInit: "+deviceId+deviceName);
    }

    public boolean chatInit() {

        STUN_Server = "stun.l.google.com";
        STUN_Port = 19302;
        TURN_Server = "54.165.124.138";
        Random rand = new Random();
        TURN_Port = TURN_Ports[rand.nextInt(7)];
        Log.d("Test1","TurnPort"+TURN_Port);
   //     TURN_Port = 7000;

        try {
            //STUN_Result result = STUN_Client.query(STUN_Server, STUN_Port, User.getSTUN_socket());
            //Log.d(TAG, result.toString());
            appClient = new AppClient(TURN_Server, TURN_Port, User.getVoip_socket());

            //Signaling process through Notify API Call
            isRinging = true;
            //start the ringtone
            ringtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            Ringtone r = RingtoneManager.getRingtone(ringtoneContext, ringtone);
            r.setStreamType(AudioManager.STREAM_MUSIC);
            r.play();
            Log.d("check","ringtone: ");
        //   CommuMessage peerRes = appClient.RequestForConnection(result.getM_NatType(), deviceId, TURN_Server, TURN_Port);
             CommuMessage peerRes = appClient.RequestForConnection(STUN_NatType.Symmetric, deviceId, TURN_Server, TURN_Port,auth2);


       //     Log.d("LiveChat","peerRes--- :"+peerRes);

            //end the ringtone
            r.stop();
            isRinging = false;

            if (peerRes == null) {
                Log.d("in peerRes","peerRes");
                return false;
            }

            master = appClient.getMaster();
            target = peerRes.getEndPoint();
         //   target = appClient.getTarget();
            audioStreaming = new AudioStreaming(User.getVoip_socket(), target);
            audioStreaming.startAudioStream();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void chatTerminate() {
        if (audioStreaming != null) {
            audioStreaming.stopAudioStream();
            Log.d("Audio Streaming", "chatTerminate: ");
        }
        if (appClient != null) {
            appClient.RequestConnectionCancel(isRinging,deviceId);
            Log.d("App Client", "chatTerminate: ");
        }
        isRinging = false;
    }

    //walki-talki implementation
    public void startTalk() {
        if (audioStreaming != null) {
            audioStreaming.talk();
        }
    }

    public void stopTalk() {
        if(audioStreaming != null) {
            audioStreaming.stopTalk();
        }
    }
}

