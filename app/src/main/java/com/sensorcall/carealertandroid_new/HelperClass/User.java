package com.sensorcall.carealertandroid_new.HelperClass;

import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.*;


public class User {
    private static String uid = "";
    private static String userName = "default name";
    private static boolean emailVerified;
    private static String email = "default@sensorscall.com";
    private static String phone = "+1-000-000-0000";
    private static String registerTime = "";
    private static String lastLoginTime = "";
    private static String userType = "Customer";
    private static String currentHomeLocation = null;
    private static String lastestFirmwareVersion = "";
    private static String otaURL = "";

    private static Map<String, List<String>> homeList = new HashMap<>();
    private static List<String> ownedDeviceList = new ArrayList<>();
    private static Set<String> outOfDatedDeviceList = new HashSet<>();

    private static DatagramSocket STUN_socket;
    private static DatagramSocket Voip_socket;

    static {
        try {
            STUN_socket = new DatagramSocket(54320);
            Voip_socket = new DatagramSocket();
            STUN_socket.setReuseAddress(true);
            Voip_socket.setReuseAddress(true);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public static void signout() {
        uid = "";
        userName = "default name";
        emailVerified = false;
        email = "default@sensorscall.com";
        phone = "+1-000-000-0000";
        registerTime = "";
        lastLoginTime = "";
        userType = "Customer";
        currentHomeLocation = null;

        homeList.clear();
        ownedDeviceList.clear();
        outOfDatedDeviceList.clear();
    }

    public static String getUid() {
        return uid;
    }

    public static void setUid(String uid) {
        User.uid = uid;
    }

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        User.userName = userName;
    }

    public static boolean isEmailVerified() {
        return emailVerified;
    }

    public static void setEmailVerified(boolean emailVerified) {
        User.emailVerified = emailVerified;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        User.email = email;
    }

    public static String getPhone() {
        return phone;
    }

    public static void setPhone(String phone) {
        User.phone = phone;
    }

    public static String getRegisterTime() {
        return registerTime;
    }

    public static void setRegisterTime(String registerTime) {
        User.registerTime = registerTime;
    }

    public static String getLastLoginTime() {
        return lastLoginTime;
    }

    public static void setLastLoginTime(String lastLoginTime) {
        User.lastLoginTime = lastLoginTime;
    }

    public static String getUserType() {
        return userType;
    }

    public static void setUserType(String userType) {
        User.userType = userType;
    }

    public static Map<String, List<String>> getHomeList() {
        return homeList;
    }



    public static List<String> getOwnedDeviceList() {
        return ownedDeviceList;
    }

    public static DatagramSocket getSTUN_socket() {
        return STUN_socket;
    }

    public static void setSTUN_socket(DatagramSocket STUN_socket) {
        User.STUN_socket = STUN_socket;
    }

    public static DatagramSocket getVoip_socket() {
        return Voip_socket;
    }

    public static void setVoip_socket(DatagramSocket voip_socket) {
        Voip_socket = voip_socket;
    }


    public static String getCurrentHomeLocation() {
        return currentHomeLocation;
    }

    public static void setCurrentHomeLocation(String currentHomeLocation) {
        User.currentHomeLocation = currentHomeLocation;
    }

    public static String getLastestFirmwareVersion() {
        return lastestFirmwareVersion;
    }

    public static void setLastestFirmwareVersion(String lastestFirmwareVersion) {
        User.lastestFirmwareVersion = lastestFirmwareVersion;
    }

    public static String getOtaURL() {
        return otaURL;
    }

    public static void setOtaURL(String otaURL) {
        User.otaURL = otaURL;
    }

    public static Set<String> getOutOfDatedDeviceList() {
        return outOfDatedDeviceList;
    }

    public static void setOutOfDatedDeviceList(Set<String> outOfDatedDeviceList) {
        User.outOfDatedDeviceList = outOfDatedDeviceList;
    }

    public static String printInfo() {
        return uid + " " + userName + " " + userType + " " + email + " " + emailVerified + " " + phone;
    }
}

