package com.sensorcall.carealertandroid_new.HelperClass.AudioStream;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.NoiseSuppressor;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.HelperClass.AudioCodec.MuLawDecoder;
import com.sensorcall.carealertandroid_new.HelperClass.AudioCodec.MuLawEncoder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceReminder.GetReminder;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersRecordAudioActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AudioReminder {
    private static final String TAG = "AudioReminder";
    private boolean isReceiving = true;
    private boolean isRecording = true;
    private String fileName;
    private String filePath;
    private RandomAccessFile randomAccessWriter;
    private RandomAccessFile randomAccessReader;
    private int payloadSize;
    private long fileSize;
    String url;

    //parameter for audio
    private static final int SAMPLE_RATE = 16000;
    private static final int INPUT_CHANNEL = AudioFormat.CHANNEL_IN_MONO;
    private static final int OUTPUT_CHANNEL = AudioFormat.CHANNEL_OUT_MONO;
    private static final int NUM_OF_CHANNEL = 1;
    private static final int FORMAT = AudioFormat.ENCODING_PCM_16BIT;
    private static final int BITS_PER_SAMPLE = 8; //Even format is 16bit, we are encoding into MuLaw before saving into file, i.e. 8 bit
    private static final int BUFFER_UNIT_LENGTH = 512;
    private static final int INPUT_BUFFER_SIZE =
            (AudioRecord.getMinBufferSize(SAMPLE_RATE, INPUT_CHANNEL, FORMAT) / BUFFER_UNIT_LENGTH + 1) * BUFFER_UNIT_LENGTH;
    private static final int OUTPUT_BUFFER_SIZE =
            (AudioTrack.getMinBufferSize(SAMPLE_RATE, OUTPUT_CHANNEL, FORMAT) / BUFFER_UNIT_LENGTH) * BUFFER_UNIT_LENGTH;

    private AudioRecord audioRecord;
    private AudioTrack audioTrack;

    private byte[][] outputBuffers;
    private int outputBufferNums;
    private int outPutBuffersIndex;

    String auth1;
    int locationid;
    byte[] BinaryAudioFile;

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private JsonPlaceHolder jsonPlaceHolderApi = retrofit.create(JsonPlaceHolder.class);

    public AudioReminder(String fileName1, String audioFile, String auth, int location) {
        this.fileName = fileName1;
        //this.filePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+fileName+".wav";
        this.filePath = audioFile;
        this.auth1 = auth;
        this.locationid = location;
        Log.d("Filepath","Filepath "+filePath);
        //String s1 = String.format("%8s", Integer.toBinaryString(b1 & 0xFF)).replace(' ', '0');
       // this.filePath = Environment.getExternalStorageDirectory().getPath();
        prepareInputAudio();
    }

    private void startRecording() {
        if (audioRecord != null && audioRecord.getState() == AudioRecord.STATE_INITIALIZED) {
            payloadSize = 0;
            int audioSessionId = audioRecord.getAudioSessionId();
            if (AcousticEchoCanceler.isAvailable()) {
                AcousticEchoCanceler.create(audioSessionId);
            }
            if (NoiseSuppressor.isAvailable()) {
                NoiseSuppressor.create(audioSessionId);
            }
            audioRecord.startRecording();
            Log.d("sbc","sbc");
        }
    }

    public void stopRecording() {
        if (audioRecord != null && audioRecord.getState() == AudioRecord.STATE_INITIALIZED) {
            audioRecord.stop();
            isRecording = false;
            try {
                synchronized (this) {
                    this.wait(10);


                    try {
                        Log.d("AudioRecrd State Stop", "run: " + audioRecord.getState());
                        Log.d("StopRecording", "StopRecordring");
                        randomAccessWriter.seek(4); // Write size to RIFF header
                        randomAccessWriter.writeInt(Integer.reverseBytes(36 + payloadSize));
                        randomAccessWriter.seek(40); // Write size to Subchunk2Size field
                        randomAccessWriter.writeInt(Integer.reverseBytes(payloadSize));
                        randomAccessWriter.close();
                        //apiCallAudio();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

     private void prepareFileHeader() {
        try {
            if ((audioRecord.getState() == AudioRecord.STATE_INITIALIZED) && (filePath != null)) {
                // write file header
                (new File(filePath)).delete();
                Log.d("sbc","inFileHeader");
                randomAccessWriter = new RandomAccessFile(filePath, "rw");
                Log.d("abcdefgh","abcdefgh: "+randomAccessWriter);
                randomAccessWriter.setLength(0); // Set file length to 0, to prevent unexpected behavior in case the file already existed
                randomAccessWriter.writeBytes("RIFF");
                randomAccessWriter.writeInt(0); // Final file size not known yet, write 0
                randomAccessWriter.writeBytes("WAVE");
                randomAccessWriter.writeBytes("fmt ");
                randomAccessWriter.writeInt(Integer.reverseBytes(16)); // Sub-chunk size, 16 for PCM
                randomAccessWriter.writeShort(Short.reverseBytes((short) 7)); // AudioFormat, 7 for MuLaw
                randomAccessWriter.writeShort(Short.reverseBytes((short) 1));// Number of channels, 1 for mono, 2 for stereo
                randomAccessWriter.writeInt(Integer.reverseBytes(SAMPLE_RATE)); // Sample rate
                randomAccessWriter.writeInt(Integer.reverseBytes(SAMPLE_RATE * NUM_OF_CHANNEL * BITS_PER_SAMPLE / 8)); // Byte rate, SampleRate*NumberOfChannels*mBitsPersample/8
                randomAccessWriter.writeShort(Short.reverseBytes((short)(NUM_OF_CHANNEL * BITS_PER_SAMPLE / 8))); // Block align, NumberOfChannels*mBitsPersample/8
                randomAccessWriter.writeShort(Short.reverseBytes((short) BITS_PER_SAMPLE)); // Bits per sample
                randomAccessWriter.writeBytes("data");
                randomAccessWriter.writeInt(0); // Data chunk size not known yet, write 0
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    //mic --> File
    private void prepareInputAudio() {
        Thread inputAudioStream = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, INPUT_CHANNEL, FORMAT, INPUT_BUFFER_SIZE);
                    Log.d("AudioRecrd State", "run: "+audioRecord.getState());
                    prepareFileHeader();
                    Log.d("sbc","InPrepareInputAudio");
                    //start to write data packets into file
                    short[] buffer = new short[INPUT_BUFFER_SIZE];
                    startRecording();
                    while (isRecording) {
                        int read = audioRecord.read(buffer, 0, buffer.length);
                        //read ==> number of frames read in
                        for (int i=0; i<buffer.length; i++)
                        {
                            int temp = buffer[i] * 10;
                            if (temp>=32767)
                                buffer[i]=32767;
                            else if (temp<=-32768)
                                buffer[i]=-32768;
                            else
                                buffer[i]=(short)temp;

                            }
                        Log.d("Read Value in Int", "run: "+read);
                        if (read != 0) {
                            byte[] toWrite = MuLawEncoder.MuLawEncode(buffer, read);
                            randomAccessWriter.write(toWrite);
                            payloadSize += toWrite.length;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });
        inputAudioStream.start();
    }

//    public void voiceReminderPlayback(Context context, String FilePath, ImageButton Playbutton, Drawable Imagesrc) {
//        Thread playbackThread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                audioTrack = new AudioTrack(AudioAttributes.CONTENT_TYPE_MUSIC, SAMPLE_RATE, OUTPUT_CHANNEL, FORMAT, OUTPUT_BUFFER_SIZE, AudioTrack.MODE_STREAM);
//                outputBufferNums = 10;
//                outputBuffers = new byte[outputBufferNums][BUFFER_UNIT_LENGTH];
//                outPutBuffersIndex = 0;
//                int readStatus = 0;
//                AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//                ByteArrayOutputStream FileValue=new ByteArrayOutputStream();
//                short[] playbackBuffer;
//                if (audioTrack.getState() == AudioTrack.STATE_INITIALIZED && FilePath != null) {
//                    try {
//                        randomAccessReader = new RandomAccessFile(FilePath, "r");
//                        Log.d("randomAccessReader:","randomAccessReader: "+randomAccessReader);
//                        audioManager.setMode(AudioManager.MODE_NORMAL);
//                        audioManager.setSpeakerphoneOn(true);
//                        audioTrack.play();
//                        while ((readStatus = randomAccessReader.read(outputBuffers[outPutBuffersIndex])) != -1) {
//                            Log.d(TAG, readStatus + "");
//                            playbackBuffer = MuLawDecoder.MuLawDecode(outputBuffers[outPutBuffersIndex]);
//                            FileValue.write(outputBuffers[outPutBuffersIndex]);
//                            audioTrack.write(playbackBuffer, 0, playbackBuffer.length);
//                            outPutBuffersIndex = (outPutBuffersIndex + 1) % outputBufferNums;
//                        }
//                        BinaryAudioFile=FileValue.toByteArray();
//                        Log.d("FileBianryValue", "run: "+BinaryAudioFile.toString());
//                        audioTrack.setVolume((float) 1.0);
//                        audioTrack.stop();
//                        audioTrack.release();
//                        Playbutton.setImageDrawable(Imagesrc);
//                    } catch (Exception f) {
//                        Log.d(TAG, f.getMessage());
//                        f.printStackTrace();
//                    }
//                }
//            }
//        });
//        playbackThread.start();
//    }

//    void apiCallAudio(){
//        Log.d("APICallAudio","inApiCallAudio");
//        Log.d("APICallAudio","inApiCallAudio: "+auth1);
//        Log.d("APICallAudio","inApiCallAudio: "+locationid);
//        Log.d("APICallAudio","inApiCallAudio: "+filePath);
//        File file = new File(filePath);
//        RequestBody requestFile = RequestBody.create(MediaType.parse("audio/wav"), file);
//        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
//        //Uri file = Uri.fromFile(new File(filePath));
//        Call<GetReminder> call = jsonPlaceHolderApi.audio("Bearer "+auth1,locationid,body);
//        call.enqueue(new Callback<GetReminder>() {
//            @Override
//            public void onResponse(Call<GetReminder> call, Response<GetReminder> response) {
//                if(response.isSuccessful()){
//                    Log.d("successfulApiCall","successAPICALL"+response.code()+" "+response.body().toString());
//                    url = response.body().getUrl();
//                    Log.d("url","urlExample"+url);
//                    RemindersRecordAudioActivity.getFilePath(url);
//                }
//                else{
//                    try {
//                        Log.d("notsuccessful","FailAPICALL"+response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GetReminder> call, Throwable t) {
//                Log.d("OnFailure","Failure12APICALL");
//            }
//        });
//    }

    public void release() {
        try {
            randomAccessWriter.close(); // Remove prepared file
        } catch (IOException e) {
            e.printStackTrace();
        }
       new File(filePath).delete(); //delete the temp file after uploading to firebase storage
        if (audioRecord != null) {
            audioRecord.release();
            Log.d("deleteFile","deleteFile");
        }
    }
}

