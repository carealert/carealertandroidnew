package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.ActivityResult;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.installations.remote.FirebaseInstallationServiceClient;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import java.util.concurrent.TimeUnit;


public class MainActivity extends AppCompatActivity {
    private Button signup_button;
    private boolean signupProcess;
    SharedPreferences CareAlert_StoredData;
    SharedPreferences.Editor CareAlert_StoredData_Editor;
    FirebaseInstanceId fcm;
    private String FCM_token;
    int MY_REQUEST_CODE=100;
    boolean check = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //openTestPage();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CareAlert_StoredData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_StoredData_Editor=CareAlert_StoredData.edit();
        signupProcess=CareAlert_StoredData.getBoolean("SignupProcess",false);
        CareAlert_StoredData_Editor.putBoolean("Reset",false);


//        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(MainActivity.this);
//        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
//        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
//            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
//                    // For a flexible update, use AppUpdateType.FLEXIBLE
//                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
//                // Request the update.
//                try {
//                    appUpdateManager.startUpdateFlowForResult(
//                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
//                            appUpdateInfo,
//                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
//                            AppUpdateType.IMMEDIATE,
//                            // The current activity making the update request.
//                            this,
//                            // Include a request code to later monitor this update request.
//                            MY_REQUEST_CODE);
//                } catch (IntentSender.SendIntentException e) {
//                    e.printStackTrace();
//                }
//
//            }
//            else{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    // Create channel to show notifications.
                    String channelId  = "fcm_default_channel";
                    String channelName = "CareAlert";
                    NotificationManager notificationManager =
                            getSystemService(NotificationManager.class);
                    notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                            channelName, NotificationManager.IMPORTANCE_LOW));
                }



//        new AsyncTask<Object, Object, Object>() {
//            @Override
//            protected Object doInBackground(Object... params) {
//                try {
//                    FirebaseMessaging.getInstance().getToken()
//                            .addOnCompleteListener(new OnCompleteListener<String>() {
//                                @Override
//                                public void onComplete(@NonNull Task<String> task) {
//                                    if (!task.isSuccessful()) {
//                                        Log.w("HI", "Fetching FCM registration token failed", task.getException());
//                                        return;
//                                    }
//
//                                    // Get new FCM registration token
//                                    String token = task.getResult();
//
//                                    // Log and toast
//                                    String msg = token.toString();
//                                    Log.d("Tiken is", msg);
//                                    Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
//                                }
//
//
//                            });
//                    TimeUnit.SECONDS.sleep(1);
//
//                } catch (Exception e) {
//                    Toast.makeText(MainActivity.this, "Failed to Register", Toast.LENGTH_SHORT).show();
//                    return e;
//                }
//                return null;
//            }
//
//            protected void onPostExecute(Object result) {
//                Toast.makeText(MainActivity.this, "Signed in and Registered", Toast.LENGTH_SHORT).show();
//                Log.d("FCM Token is:", "onPostExecute: "+FCM_token);
//            }
//        }.execute(null, null, null);

                boolean returnfromlp=CareAlert_StoredData.getBoolean("return",false);
                if(signupProcess && !returnfromlp){
                    //   openSigninPage();
                    check = true;
                    checkNetConnectivity();
                }
                CareAlert_StoredData_Editor.putBoolean("return",false);
                CareAlert_StoredData_Editor.commit();
                signup_button= (Button) findViewById(R.id.get_started);
                TextView sign_in_link = (TextView) findViewById(R.id.sign_in);
                signup_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //openEnterPhonePage();
                        checkNetConnectivity();
                    }
                });

                String sign_in_msg="Already have account? Sign in";
                SpannableString sign_in_string= new SpannableString(sign_in_msg);
                ClickableSpan clickspan= new ClickableSpan() {
                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }

                    @Override
                    public void onClick(@NonNull View view) {
                        //   openSigninPage();
                        check = true;
                        checkNetConnectivity();
                    }
                };
                sign_in_string.setSpan(clickspan,22,29, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                sign_in_link.setText(sign_in_string);
                sign_in_link.setMovementMethod(LinkMovementMethod.getInstance());

            }
//        });

        //FireBase Registration
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Update Canceled, Please Update App to use it.", Toast.LENGTH_SHORT).show();
            } else if (resultCode == ActivityResult.RESULT_IN_APP_UPDATE_FAILED) {
                Toast.makeText(this, "Failed to update App", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "App Updated Successfully, Please Restart the App", Toast.LENGTH_SHORT).show();
            }
            finishAffinity();
            System.exit(0);

        }
    }

    public void openEnterPhonePage(){
        CareAlert_StoredData_Editor.putBoolean("Reset",false);
        CareAlert_StoredData_Editor.putBoolean("AddedviaSignupProcess",true);
        CareAlert_StoredData_Editor.putBoolean("ReconfigureBT",false);
        CareAlert_StoredData_Editor.commit();
        Intent EnterPhonePage =new Intent(this, TermsandConditions.class);
        startActivity(EnterPhonePage);
    }
    public void openSigninPage(){
        Intent Signin_Page =new Intent(this, LoginScreen.class);
        startActivity(Signin_Page);
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet", "devic123");
            if (check == true) {
                //Intent Test= new Intent(this,Deviceaddsteps.class);
                //startActivity(Test);
                AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(this);

                // Returns an intent object that you use to check for an update.
                Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

                // Checks that the platform will allow the specified type of update.
                appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
                    if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                            // For a flexible update, use AppUpdateType.FLEXIBLE
                            && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                        // Request the update.

                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("An Update is available for CareAlert, Please Download the update to continue using CareAler.")
                                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        try {
                                            appUpdateManager.startUpdateFlowForResult(appUpdateInfo,AppUpdateType.IMMEDIATE,MainActivity.this,MY_REQUEST_CODE);
                                        } catch (IntentSender.SendIntentException e) {
                                            Toast.makeText(MainActivity.this, "Failed to Update App, Try using Play Store.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                                )
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finishAffinity();
                                        System.exit(0);
                                    }
                                });
                        // Create the AlertDialog object and return it
                        builder.create();
                        builder.show();
                    }
                });

                openSigninPage();
            } else {
                openEnterPhonePage();
            }
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void onBackPressed() {
        Toast.makeText(this, "Back Button Not Supported on Main Page", Toast.LENGTH_SHORT).show();
        return;
    }
}