package com.sensorcall.carealertandroid_new;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.ConnectionDetector;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.CustomBottomNavigationView1;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sensorcall.carealertandroid_new.ui.email.AccountSettingsActivity;
import com.sensorcall.carealertandroid_new.ui.lovedones.EditLovedOnesActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Settings extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private NetworkStateReceiver networkStateReceiver;      // Receiver that detects network state changes
    LinearLayout btn_settings, manage_devices, manage_term, manage_notification, support, loved_one_setings, account_settings;
    String sensorcall = "https://sensorscall.com/";
    LinearLayout location;
    TextView Logout;
    JsonPlaceHolderApi get_api;
    TextView loc_name, loc_add;
    boolean bool = false;
    ProgressDialog loader;
    List<GetLocation> get_list;
    ProgressDialog progressDialog;
    AlertDialog.Builder builder;
    private ConnectionDetector detector;
    int id = -1;
    int finalId = -1;
    private ConnectivityManager mManager;
    private ConnectivityManager.NetworkCallback mCallback;
    SharedPreferences sharedpreferences, CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Location_Name = "LocationnameKey";
    public static final String Location_Address = "LocationaddKey";
    private int loc_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        loader = new ProgressDialog(Settings.this);
        loader = showLoadingDialog(this);
        loader.getWindow().setGravity(Gravity.CENTER);

        startNetworkBroadcastReceiver(this);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
        CareAlert_SharedData_Editor = CareAlert_SharedData.edit();
        btn_settings = (LinearLayout) findViewById(R.id.rectangle_BG_2);
        manage_devices = (LinearLayout) findViewById(R.id.phonedata1);
        manage_notification = (LinearLayout) findViewById(R.id.phonedata2);
        loved_one_setings = (LinearLayout) findViewById(R.id.rectangle_BG_loved_one);
        account_settings = (LinearLayout) findViewById(R.id.ll_account_details);
        manage_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent push = new Intent(Settings.this, PushNotificationSettings.class);
                startActivity(push);
            }
        });

        manage_term = (LinearLayout) findViewById(R.id.terms_condition);
        manage_term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CareAlert_SharedData_Editor.putBoolean("viaSettings", true);
                CareAlert_SharedData_Editor.commit();
                Intent intent = new Intent(Settings.this, Disclaimer.class);
                startActivity(intent);
            }
        });

        support = (LinearLayout) findViewById(R.id.support);
        support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent supportTab = new Intent(Settings.this, Support.class);
                startActivity(supportTab);
            }
        });

        location = (LinearLayout) findViewById(R.id.rectangle_BG_1);
        loc_name = (TextView) findViewById(R.id.Location_name);
        loc_add = (TextView) findViewById(R.id.loc_add);
        Logout = (TextView) findViewById(R.id.logout);
        FloatingActionButton floatingActionButton = findViewById(R.id.call_fab);

        CustomBottomNavigationView1 curvedBottomNavigationView = findViewById(R.id.bottom_navigation);

        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CareAlert_SharedData_Editor.putBoolean("Rememberme", false);
                CareAlert_SharedData_Editor.commit();
                Intent selectToCall = new Intent(Settings.this, LoginScreen.class);
                startActivity(selectToCall);
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent selectToCall = new Intent(Settings.this, SelectToCall.class);
                startActivity(selectToCall);
            }
        });

        btn_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int x = getConnectionType(getApplicationContext());
                if (x == 0) {
                    alert();
                } else {
                    Intent in = new Intent(Settings.this, Emergencylightsetup.class);

                    //  in.putExtra("LocationId", get_list.get(id).LocationId );
                    startActivity(in);
                }
            }
        });

        manage_devices.setOnClickListener(new View.OnClickListener() {
            int id1 = id;

            @Override
            public void onClick(View view) {
                int x = getConnectionType(getApplicationContext());
                if (x == 0) {
                    alert();
                } else {
                    Intent in = new Intent(Settings.this, ManageDevices.class);
                    Log.d("Manage Device", "onClick: " + finalId);
                    if (finalId == -1)
                        in.putExtra("DefaultLocationId", 0);
                    else
                        in.putExtra("DefaultLocationId", get_list.get(finalId).getLocationId());
                    startActivity(in);
                }
            }
        });


        loved_one_setings.setOnClickListener(view -> {
            Intent intent = new Intent(Settings.this, EditLovedOnesActivity.class);
            intent.putExtra(Constants.DEFAULT_LOCATION_ID, loc_id);
            startActivity(intent);
        });


        account_settings.setOnClickListener(view -> {
            Intent intent = new Intent(Settings.this, AccountSettingsActivity.class);
            startActivity(intent);
        });

        Menu menu = curvedBottomNavigationView.getMenu();
        MenuItem menuitem = menu.getItem(4);
        menuitem.setChecked(true);
        curvedBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.navigation_settings:
                        //    Toast.makeText(MainActivity.this, "Settings clicked", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(Settings.this, Settings.class);
                        startActivity(in);
                        Settings.this.finish();
                        break;
                    case R.id.navigation_notifications:
                        // Toast.makeText(MainActivity.this, "Notification clicked", Toast.LENGTH_SHORT).show();
                        Intent not = new Intent(Settings.this, Notification.class);
                        startActivity(not);
                        Settings.this.finish();
                        break;

                    case R.id.navigation_dashboard:
                        // Toast.makeText(MainActivity.this, "Dashboard clicked", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(Settings.this, Dashboard.class);
                        startActivity(i);
                        Settings.this.finish();
                        break;

                    case R.id.navigation_Reminders:
                        // Toast.makeText(MainActivity.this, "Reminders clicked", Toast.LENGTH_SHORT).show();
                        Intent rem = new Intent(Settings.this, RemindersViewActivity.class);
                        startActivity(rem);
                        Settings.this.finish();
                        break;
                }
                return true;
            }
        });
    }


    public void alert() {
        builder = new AlertDialog.Builder(this);
        builder.setMessage("Cannot load the contents. Make sure your phone has an internet connection " +
                "and try again.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void alert1() {
        builder = new AlertDialog.Builder(this);
        builder.setMessage("Cannot load the contents. Make sure your phone has an internet connection " +
                "and try again.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }


    @IntRange(from = 0, to = 3)
    public static int getConnectionType(Context context) {
        int result = 0; // Returns connection type. 0: none; 1: mobile data; 2: wifi
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm != null) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = 2;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = 1;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)) {
                        result = 3;
                    }
                }
            }
        } else {
            if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null) {
                    // connected to the internet
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                        result = 2;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        result = 1;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_VPN) {
                        result = 3;
                    }
                }
            }
        }
        return result;
    }


    @Override
    protected void onPause() {
        /***/
        unregisterNetworkBroadcastReceiver(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        /***/
        registerNetworkBroadcastReceiver(this);
        super.onResume();
    }

    @Override
    public void networkAvailable() {
        Log.i("Availability", "networkAvailable()");
        //Proceed with online actions in activity (e.g. hide offline UI from user, start services, etc...)
        loader.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        get_api = retrofit.create(JsonPlaceHolderApi.class);

        final String Auth = CareAlert_SharedData.getString("Auth_Token", "");
        Call<List<GetLocation>> call = get_api.getLocation("Bearer " + Auth);

        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if (response == null) {
                    Log.d("Location Api", " Location API data is null");
                } else {

                    Log.d("Location Api", " Location API data fetched");

                }

                if (response.isSuccessful()) {
                    get_list = response.body();
                    loader.dismiss();
                    if (get_list.size() == 0) {
                        Toast.makeText(Settings.this, "No Location Available for the Account", Toast.LENGTH_SHORT).show();
                        String locname = "";
                        String locadd = "";
                        loc_name.setText("");
                        loc_add.setText("");
                        location.setOnClickListener(new View.OnClickListener() {

                            int id1 = finalId;


                            @Override
                            public void onClick(View view) {
                                int x = getConnectionType(getApplicationContext());
                                if (x == 0) {
                                    alert();
                                } else {
                                    Intent i = new Intent(Settings.this, AddLocation.class);
                                    i.putExtra("locname", "");
                                    i.putExtra("locadd", "");
                                    i.putExtra("DefaultLocationId", 0);
                                    //  i.putExtra("DefaultLocationId", get_list.get(id1).getLocationId());
                                    //Toast.makeText(getApplicationContext(), "DefaultId" + get_list.get(id1).getLocationId(), Toast.LENGTH_SHORT).show();
                                    startActivity(i);
                                    Settings.this.finish();
                                    Intent i1 = getIntent();
                                    bool = i1.getBooleanExtra("boolval", true);
                                }
                            }
                        });
                    } else {
                        if (bool == false) {
                            for (int i = 0; i < get_list.size(); i++) {
                                if (get_list.get(i).IsDefaultLocationId == true) {
                                    String locname = get_list.get(i).getLocationName();
                                    String locadd = get_list.get(i).getLocationAddress();
                                    loc_name.setText(get_list.get(i).getLocationName());
                                    loc_add.setText(get_list.get(i).getLocationAddress());
                                    loc_id = get_list.get(i).getLocationId();
                                    id = i;

                                    break;
                                }
                            }


                        } else {
                            Intent i = getIntent();
                            String a = i.getStringExtra("locname");
                            String b = i.getStringExtra("locAdd");

                            loc_name.setText(a);
                            loc_add.setText(b);
                        }

                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Location_Name, get_list.get(id).getLocationName());
                        editor.putString(Location_Address, get_list.get(id).getLocationAddress());
                        editor.apply();

                        finalId = id;
                        location.setOnClickListener(new View.OnClickListener() {

                            int id1 = finalId;


                            @Override
                            public void onClick(View view) {
                                int x = getConnectionType(getApplicationContext());
                                if (x == 0) {
                                    alert();
                                } else {
                                    Intent i = new Intent(Settings.this, AddLocation.class);
                                    i.putExtra("locname", get_list.get(id1).getLocationName());
                                    i.putExtra("locadd", get_list.get(id1).getLocationAddress());
                                    i.putExtra("DefaultLocationId", get_list.get(id1).getLocationId());
                                    //  i.putExtra("DefaultLocationId", get_list.get(id1).getLocationId());
                                    //Toast.makeText(getApplicationContext(), "DefaultId" + get_list.get(id1).getLocationId(), Toast.LENGTH_SHORT).show();
                                    startActivity(i);
                                    Settings.this.finish();
                                    Intent i1 = getIntent();
                                    bool = i1.getBooleanExtra("boolval", true);
                                }
                            }
                        });
                    }
                }
            }


            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                progressDialog.dismiss();
                sharedpreferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String savedlocname = sharedpreferences.getString("LocationnameKey", "");
                String savedlocadd = sharedpreferences.getString("LocationaddKey", "");
                loc_name.setText(savedlocname);
                loc_add.setText(savedlocadd);
                // alert();

                //alert1();
                Log.d("Fails", "true");
                // Toast.makeText(MainActivity.this,"Failure in Settings", Toast.LENGTH_SHORT ).show();
            }
        });


        Log.d("Connected", "true");
//        Toast.makeText(getApplicationContext(), "Connected",Toast.LENGTH_LONG).show();

    }

    @Override
    public void networkUnavailable() {
        Log.i("unavailability", "networkUnavailable()");
        //Proceed with offline actions in activity (e.g. sInform user they are offline, stop services, etc...)
        //
        loader.dismiss();
        Log.d("No internet", "No Internet");
        sharedpreferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        String savedlocname = sharedpreferences.getString("LocationnameKey", "");
        String savedlocadd = sharedpreferences.getString("LocationaddKey", "");
        loc_name.setText(savedlocname);
        loc_add.setText(savedlocadd);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int x = getConnectionType(getApplicationContext());
                if (x == 0) {
                    alert();
                }
            }
        });

        Log.d("COnnected", "False");
//        Toast.makeText(getApplicationContext(), "Not Connected",
//                Toast.LENGTH_LONG).show();
    }

    public void startNetworkBroadcastReceiver(Context currentContext) {
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener((NetworkStateReceiver.NetworkStateReceiverListener) currentContext);
        registerNetworkBroadcastReceiver(currentContext);
    }

    /**
     * Register the NetworkStateReceiver with your activity
     *
     * @param currentContext
     */
    public void registerNetworkBroadcastReceiver(Context currentContext) {
        currentContext.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     * Unregister the NetworkStateReceiver with your activity
     *
     * @param currentContext
     */
    public void unregisterNetworkBroadcastReceiver(Context currentContext) {
        currentContext.unregisterReceiver(networkStateReceiver);
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}
