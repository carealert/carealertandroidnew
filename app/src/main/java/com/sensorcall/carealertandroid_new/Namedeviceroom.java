package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.gson.JsonArray;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.sensorcall.carealertandroid_new.Bluetoothdevice.REQUEST_ACCESS_COARSE_LOCATION;

public class Namedeviceroom extends AppCompatActivity {
    Button continue_button;
    SharedPreferences CareAlert_ShareData;
    boolean AddedviaSignupProcess;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    Spinner roomname_input;
    TextView Title;
    ProgressDialog loaddialog,progressDialog;
    Retrofit retrofit;
    Boolean Selected;
    EditText nickname;
    String RoomType;
    String Auth;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    AlertDialog materialAlertDialogBuilder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_namedeviceroom);
        CareAlert_ShareData=getSharedPreferences("CareAlert_SharedData",0);
        AddedviaSignupProcess=CareAlert_ShareData.getBoolean("AddedviaSignupProcess",true);
        CareAlert_SharedData_Editor=CareAlert_ShareData.edit();
        Auth= CareAlert_ShareData.getString("Auth_Token","");
        nickname=findViewById(R.id.devnick);
        continue_button= (Button) findViewById(R.id.deviceroom_continue);
        Title= (TextView) findViewById(R.id.title);
        if(!AddedviaSignupProcess) {
            Title.setText("Which Room did you plug in the Device?");
        }
        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        loaddialog.show();
        retrofit=new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        roomname_input =(Spinner) findViewById(R.id.room_input);
        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);

        checkNetConnectivity();

        /*
        Call<JsonArray> call= jsonPlaceHolderApi.GetRoomType("Bearer "+Auth);
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if(response.isSuccessful()){
                    JsonArray Body= response.body();
                    ArrayList<String> RoomTypes= new ArrayList<>();
                    for(int i=0;i<Body.size();i++){
                        RoomTypes.add(Body.get(i).toString().replace("\"",""));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Namedeviceroom.this, android.R.layout.simple_list_item_1);
                    adapter.add("");
                    adapter.addAll(RoomTypes);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    roomname_input.setAdapter(adapter);
                    loaddialog.dismiss();
                }
                else{
                    Toast.makeText(Namedeviceroom.this, "Failed to Fetch Room Types Retrying-> not success", Toast.LENGTH_SHORT).show();
                    //Intent Refresh= new Intent(Namedeviceroom.this,Namedeviceroom.class);
                    //startActivity(Refresh);
                    loaddialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Toast.makeText(Namedeviceroom.this, "Failed to Fetch Room Types Retrying", Toast.LENGTH_SHORT).show();
                //Intent Refresh= new Intent(Namedeviceroom.this,Namedeviceroom.class);
                //startActivity(Refresh);
                loaddialog.dismiss();
            }
        });



        roomname_input.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                RoomType=roomname_input.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                //String room= roomname_input.getText().toString();
                if(RoomType.equals("") || nickname.getText().toString().length()==0) {
                    Toast.makeText(Namedeviceroom.this, "Please Select a Room Type, Enter the Device Nickname", Toast.LENGTH_SHORT).show();
                    loaddialog.dismiss();
                    continue_button.setEnabled(true);
                }
                else {
                    CareAlert_SharedData_Editor.putString("Device_Room", RoomType);
                    CareAlert_SharedData_Editor.putString("Device_Name",nickname.getText().toString());
                    CareAlert_SharedData_Editor.commit();
                    openbluetoothpage();
                }
            }
        });
        */
    }
    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            apiCall();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void apiCall(){
        Call<JsonArray> call= jsonPlaceHolderApi.GetRoomType("Bearer "+Auth);
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if(response.isSuccessful()){
                    JsonArray Body= response.body();
                    ArrayList<String> RoomTypes= new ArrayList<>();
                    for(int i=0;i<Body.size();i++){
                        RoomTypes.add(Body.get(i).toString().replace("\"",""));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Namedeviceroom.this, android.R.layout.simple_list_item_1);
                    adapter.add("");
                    adapter.addAll(RoomTypes);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    roomname_input.setAdapter(adapter);
                    loaddialog.dismiss();
                }
                else{
                    Toast.makeText(Namedeviceroom.this, "Failed to Fetch Room Types Retrying-> not success", Toast.LENGTH_SHORT).show();
                    //Intent Refresh= new Intent(Namedeviceroom.this,Namedeviceroom.class);
                    //startActivity(Refresh);
                    loaddialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Toast.makeText(Namedeviceroom.this, "Failed to Fetch Room Types Retrying", Toast.LENGTH_SHORT).show();
                //Intent Refresh= new Intent(Namedeviceroom.this,Namedeviceroom.class);
                //startActivity(Refresh);
                loaddialog.dismiss();
            }
        });



        roomname_input.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                RoomType=roomname_input.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });





        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                loaddialog.show();
//                continue_button.setEnabled(false);
                //String room= roomname_input.getText().toString();
                if(RoomType.equals("") || nickname.getText().toString().length()==0) {
                    Toast.makeText(Namedeviceroom.this, "Please Select a Room Type, Enter the Device Nickname", Toast.LENGTH_SHORT).show();
                    loaddialog.dismiss();
                    continue_button.setEnabled(true);
                }
                else {
                    CareAlert_SharedData_Editor.putString("Device_Room", RoomType);
                    CareAlert_SharedData_Editor.putString("Device_Name",nickname.getText().toString());
                    CareAlert_SharedData_Editor.commit();
                    if (ContextCompat.checkSelfPermission(Namedeviceroom.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Namedeviceroom.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_ACCESS_COARSE_LOCATION );
                    }  else {
                        openbluetoothpage();
                    }

                }
            }
        });
    }

    public void openbluetoothpage(){
        Intent bluetoothpage= new Intent(this,Bluetoothdevice.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(bluetoothpage);
    }

    public void opendevicepluginpage(){
        Intent deviceplugin= new Intent(this,Devicesetuppluginvideo.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(deviceplugin);

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @Override
    public void onBackPressed() {
        if(!AddedviaSignupProcess){
            CareAlert_SharedData_Editor.putBoolean("AddedviaSignupProcess",true);
            CareAlert_SharedData_Editor.commit();
            Intent manageDevice = new Intent(Namedeviceroom.this, ManageDevices.class);
            manageDevice.putExtra("DefaultLocationId",CareAlert_ShareData.getInt("LocationId",-1));
            startActivity(manageDevice);
        }
        else{
            Intent namedevice = new Intent(Namedeviceroom.this, Devicesetuppluginvideo.class);
            startActivity(namedevice);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (REQUEST_ACCESS_COARSE_LOCATION == 1) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                openbluetoothpage();

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                materialAlertDialogBuilder = new AlertDialog.Builder(Namedeviceroom.this)
                        .setTitle(R.string.permission_time)
                        .setMessage(R.string.permission_error)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                materialAlertDialogBuilder.dismiss();
                            }
                        })
                        .show();
            }
            return;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
