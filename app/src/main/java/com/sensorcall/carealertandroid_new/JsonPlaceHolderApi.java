package com.sensorcall.carealertandroid_new;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {
    @FormUrlEncoded
    @POST ("/api/Account/CreateUser")
    Call<Post> createpost(
            @Field("PhoneNumber") String phone_number
    );

    @FormUrlEncoded
    @POST ("/api/Account/VerifyOtp")
    Call<Post> createpostotp(
            @Field("PhoneNumber") String phone_number,
            @Field("Otp") Integer Otp,
            @Field("Reset") Boolean Reset
    );

    @FormUrlEncoded
    @POST("/api/Account/GenerateResetPasswordOTP")
    Call<Post> createpostresetpasswordreq(
            @Field("PhoneNumber") String phone_number
    );

    @FormUrlEncoded
    @POST("/api/Account/ResetPassword")
    Call<Post> createpostresetpass(
            @Field("PhoneNumber") String phone_number,
            @Field("Otp") Integer Otp,
            @Field("Password") String Password
    );

    @POST("/api/Account/login")
    Call<Post> loginrequest(
            @Body JsonObject Body
    );

    @GET("/api/Account/GetUserById")
    Call<Post> getuserbyid(
            @Header("Authorization") String Authtoken
    );


    @PUT("/api/Account/UpdateUser")
    Call<Post> updateuser(
            @Header("Authorization") String Authtoken,
            @Body JsonObject Body
    );

    @Headers({"Content-Type: application/json-patch+json","accept: */*"})
    @POST("/api/LovedOnes/CreateLocation")
    Call<Post> createlocation(
            @Header("Authorization") String Authtoken,
            @Body JsonObject Body
    );

    @FormUrlEncoded
    @POST("/api/Account/UploadFile")
    Call<Post> uploadimage(
            @Header("Authorization") String Authtoken,
            @Field("Name") String Name,
            @Field("Image") String Image
    );

    @POST("/api/LovedOnes/CreateLovedOnes")
    Call<Post> createlovedones(
            @Header("Authorization") String Authtoken,
            @Body JsonObject Body
    );

    @GET("api/Device/GetDeviceByLocationId")
    Call<List<GetDeviceByLocationId>> getDevice(
            @Query("LocationId") int LocationId,
            @Header("Authorization") String Authtoken

    );

    @GET("api/LovedOnes/GetLocationForLoggedInUser")
    Call<List<GetLocation>> getLocation(@Header("Authorization") String Authtoken);


    @POST("api/LovedOnes/SetDefaultLocation")
    Call<List<GetLocation>> setDefaultLocation(
            @Query("LocationId") int LocationId,
            @Header("Authorization") String Authtoken
    );


    @PUT("/api/LovedOnes/UpdateLocation")
    Call<GetLocation> updateLocation(
            @Header("Authorization") String Authtoken,
            @Body JsonObject body);


    @PUT("api/Device/UpdateDevice")
    Call<GetDeviceByLocationId> updateDevice(
            @Header("Authorization") String Authtoken,
            @Body JsonObject body);

    @GET("/api/Device/FirmwareUpdateByLocationId")
    Call<Void> updatedeviceByLocationId(
            @Query("LocationId") int LocationId,
            @Header("Authorization") String Authtoken
    );

    @POST("api/Device/CreateDevice")
    Call<Post> createdevice(
            @Header("Authorization") String Authtoken,
            @Body JsonObject Body);

    @GET("/api/LovedOnes/DashboardTrend")
    Call<List<Post>> getdashboardtrends(
            @Header("Authorization") String Authtoken,
            @Query("LocationId") Integer LocationId,
            @Query("DeviceId") Integer DeviceId,
            @Query("StartTime") Integer StartTime,
            @Query("EndTime") Integer EndTime
    );

    @DELETE("/api/Device/DeleteDevice")
    Call<Post> deleteDevice(

            @Header("Authorization") String Authtoken,
            @Query("DeviceId") int Deviceid
    );

    @GET("/api/LovedOnes/Questoniare")
    Call<List<GetLocation>> GetQuestions(
            @Header("Authorization") String Authtoken
    );

    @POST("/api/LovedOnes/Questoniare")
    Call<JsonObject> AnswerQuestions(
            @Header("Authorization") String Authtoken,
            @Body JsonObject Body
            );

    @GET("/api/Device/GetRoomType")
    Call<JsonArray> GetRoomType(
            @Header("Authorization") String Authtoken
    );

    @GET("/api/Account/DisclaimerandConditions")
    Call<GetLocation> GetDisandcon();

    @POST("/api/Account/RegisterforPushNotifications")
    Call<GetLocation> PushNotificationRegistration(
        @Header("Authorization") String Authtoken,
        @Body JsonObject Body
    );

    @GET("/api/Device/GetPushNotificationStatus")
    Call<NotificationStatus> GetNotstatus(
            @Header("Authorization") String Authtoken,
            @Query("LocationId") int LocationId
    );

    @POST("/api/Device/UpdatePushNotificationStatus")
    Call<NotificationStatus> UpdateNotstatus(
            @Header("Authorization") String Authtoken,
            @Body JsonObject Body
    );

    @GET("/api/Device/isFirmwareUpdateRunning")
    Call<GetLocation> Firmwarerunning(
            @Header("Authorization") String Authtoken,
            @Query("LocationId") int LocationId
    );







}

