package com.sensorcall.carealertandroid_new.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceRegistrationStatus {

    @SerializedName("Message")
    @Expose
    public String message;
    @SerializedName("IsDeviceAvailable")
    @Expose
    public boolean isDeviceAvailable;

    public DeviceRegistrationStatus(){

    }

    public String getMessage() {
        return message;
    }

    public boolean getDeviceAvailable() {
        return isDeviceAvailable;
    }
}
