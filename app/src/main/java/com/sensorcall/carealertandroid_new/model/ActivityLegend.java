package com.sensorcall.carealertandroid_new.model;

public class ActivityLegend {
    private String legendDesc;
    private int legenRes;

    public ActivityLegend(String legendDesc, int legenRes) {
        this.legendDesc = legendDesc;
        this.legenRes = legenRes;
    }

    public String getLegendDesc() {
        return legendDesc;
    }

    public void setLegendDesc(String legendDesc) {
        this.legendDesc = legendDesc;
    }

    public int getLegenRes() {
        return legenRes;
    }

    public void setLegenRes(int legenRes) {
        this.legenRes = legenRes;
    }
}
