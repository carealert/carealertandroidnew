package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginScreen extends AppCompatActivity {
    SharedPreferences CareAlert_StoredData;
    SharedPreferences.Editor CareAlert_StoredData_Editor;
    EditText username_input, password_input;
    String username="",password="";
    Button loginbutton;
    String Country_code="+1";
    CountryCodePicker ccp;
    Retrofit retrofit;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    ProgressDialog loaddialog,progressDialog;
    Boolean rememberme_value=false;
    TextView forgot_pass;
    TextView sign_up;
    Boolean fingerprint=false;
    int TrialCount = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        CareAlert_StoredData=getSharedPreferences("CareAlert_SharedData",0);
        rememberme_value=CareAlert_StoredData.getBoolean("Rememberme",false);
        fingerprint=CareAlert_StoredData.getBoolean("Fingerprint",false);
        super.onCreate(savedInstanceState);

        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        retrofit=new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        if(rememberme_value && fingerprint){
            Biometriccheck();
        }
        CareAlert_StoredData_Editor =CareAlert_StoredData.edit();
        setContentView(R.layout.activity_login_screen);
        forgot_pass= (TextView)findViewById(R.id.forgot_password);
        sign_up= (TextView) findViewById(R.id.sign_up);
        username_input= (EditText) findViewById(R.id.phone_input);
        password_input= (EditText) findViewById(R.id.password_input1);
        loginbutton = (Button) findViewById(R.id.login_button);
        ccp= (CountryCodePicker) findViewById(R.id.ccp);


        checkNetConnectivity();
/*
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                Country_code=ccp.getSelectedCountryCodeWithPlus();
            }
        });


        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginbutton.setEnabled(false);
                loaddialog.show();
                username=username_input.getText().toString();
                password=password_input.getText().toString();
                LoginFunction(username, password);
            }
        });

        String forgot_password_msg="Forgot Password?";
        SpannableString forgotpassword_string= new SpannableString(forgot_password_msg);
        ClickableSpan clickspan= new ClickableSpan() {
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }

            @Override
            public void onClick(@NonNull View view) {
                loginbutton.setEnabled(false);
                loaddialog.show();
                openEnterPhonePage();
            }
        };
        forgotpassword_string.setSpan(clickspan,0,16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        forgot_pass.setText(forgotpassword_string);
        forgot_pass.setMovementMethod(LinkMovementMethod.getInstance());


        String signup_msg="Need a account? Sign up";
        SpannableString signup_string= new SpannableString(signup_msg);
        ClickableSpan clickspan1= new ClickableSpan() {
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }

            @Override
            public void onClick(@NonNull View view) {
                loginbutton.setEnabled(false);
                loaddialog.show();
                openSignupPage();
            }
        };
        signup_string.setSpan(clickspan1,16,23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sign_up.setText(signup_string);
        sign_up.setMovementMethod(LinkMovementMethod.getInstance());
 */

    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            signInButton();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    public void signInButton(){
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                Country_code=ccp.getSelectedCountryCodeWithPlus();
            }
        });


        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginbutton.setEnabled(false);
                loaddialog.show();
                username=username_input.getText().toString();
                password=password_input.getText().toString();
                LoginFunction(username, password);
            }
        });

        String forgot_password_msg="Forgot Password?";
        SpannableString forgotpassword_string= new SpannableString(forgot_password_msg);
        ClickableSpan clickspan= new ClickableSpan() {
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }

            @Override
            public void onClick(@NonNull View view) {
                loginbutton.setEnabled(false);
                loaddialog.show();
                openEnterPhonePage();
            }
        };
        forgotpassword_string.setSpan(clickspan,0,16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        forgot_pass.setText(forgotpassword_string);
        forgot_pass.setMovementMethod(LinkMovementMethod.getInstance());


        String signup_msg="Need an account? Sign up";
        SpannableString signup_string= new SpannableString(signup_msg);
        ClickableSpan clickspan1= new ClickableSpan() {
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }

            @Override
            public void onClick(@NonNull View view) {
                loginbutton.setEnabled(false);
                loaddialog.show();
                openSignupPage();
            }
        };
        signup_string.setSpan(clickspan1,17,24, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sign_up.setText(signup_string);
        sign_up.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void openEnterPhonePage(){

        CareAlert_StoredData_Editor.putBoolean("Reset",true);
        CareAlert_StoredData_Editor.commit();
        Intent EnterPhonePage =new Intent(this, EnterPhone.class);
        loginbutton.setEnabled(true);
        loaddialog.dismiss();

        startActivity(EnterPhonePage);
    }
    public void openSignupPage(){
        Intent Signup_Page =new Intent(this, MainActivity.class);
        loginbutton.setEnabled(true);
        loaddialog.dismiss();

        CareAlert_StoredData_Editor.putBoolean("return",true);
        CareAlert_StoredData_Editor.commit();

        startActivity(Signup_Page);
    }

    public void LoginFunction(final String username, String password){
        JsonObject apiBody= new JsonObject();
        apiBody.addProperty("PhoneNumber",Country_code+username);
        apiBody.addProperty("Password",password);
        Call<Post> call=jsonPlaceHolderApi.loginrequest(apiBody);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(response.isSuccessful()){
                    fingerprint=response.body().isFingerPrint();
                    String Auth_Token=response.body().getAuthToken();
                    String PreviousAuthToken= CareAlert_StoredData.getString("Auth_Token","");
                    if(!PreviousAuthToken.equals(Auth_Token)){
                        FirebaseMessaging.getInstance().getToken()
                                .addOnCompleteListener(new OnCompleteListener<String>() {
                                    @Override
                                    public void onComplete(@NonNull Task<String> task) {
                                        if (!task.isSuccessful()) {
                                            Log.w("HI", "Fetching FCM registration token failed", task.getException());
                                            return;
                                        }


                                        CareAlert_StoredData_Editor.putString(Constants.PHONE_NUMBER,username);
                                        CareAlert_StoredData_Editor.putInt(Constants.COUNTRY_CODE,ccp.getSelectedCountryCodeAsInt());
                                        CareAlert_StoredData_Editor.commit();

                                        // Get new FCM registration token
                                        String token = task.getResult();

                                        // Log and toast
                                        String msg = token.toString();
                                        Log.d("Tiken is",msg);
                                        JsonObject Body=new JsonObject();
                                        Log.d("HandleKey", "onResponse: "+msg);
                                        Body.addProperty("CMSHandle",msg);
                                        Body.addProperty("Platform","fcm");
                                        Call<GetLocation> callnot=jsonPlaceHolderApi.PushNotificationRegistration("Bearer "+Auth_Token,Body);
                                        callnot.enqueue(new Callback<GetLocation>() {
                                            @Override
                                            public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                                                if(response.isSuccessful()) {
                                                    Log.d("Notificationreg", "onResponse: " + true);
                                                }
                                                else{
                                                    try {
                                                        Log.d("Notificationreg", "onResponse: "+response.code()+response.errorBody().toString());
                                                        Log.d("Notificationreg", "onResponse: " +response.errorBody().string());
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<GetLocation> call, Throwable t) {
                                                Log.d("Notificationreg", "onResponse: Failed" + false);

                                            }
                                        });

                                    }


                                });

                    }
                    rememberme_value=false;
                    CareAlert_StoredData_Editor.putString("Auth_Token",Auth_Token);
                    CareAlert_StoredData_Editor.putBoolean("Rememberme",true);
                    CareAlert_StoredData_Editor.putBoolean("SignupProcess",true);
                    CareAlert_StoredData_Editor.putBoolean("Fingerprint",fingerprint);
                    CareAlert_StoredData_Editor.commit();
                  //  openDashboard();
                    openNotification();
                }
                else{
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String Error = jObjError.getString("message");
                        Toast.makeText(LoginScreen.this, Error, Toast.LENGTH_SHORT).show();
                        loginbutton.setEnabled(true);
                        loaddialog.dismiss();
                       // progressDialog.dismiss();
                    } catch (Exception e) {
                        Toast.makeText(LoginScreen.this, "Unknown Error", Toast.LENGTH_SHORT).show();
                        username_input.setText("");
                        password_input.setText("");
                        loginbutton.setEnabled(true);
                        loaddialog.dismiss();
                      //  progressDialog.dismiss();
                    }

                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Toast.makeText(LoginScreen.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                username_input.setText("");
                password_input.setText("");
                loginbutton.setEnabled(true);
                loaddialog.dismiss();
               // progressDialog.dismiss();
            }
        });

    }
    /*public void openDashboard(){
        Intent openDashboardpage= new Intent(this,Dashboard.class);
        loginbutton.setEnabled(true);
        loaddialog.dismiss();
        //progressDialog.dismiss();
        startActivity(openDashboardpage);
    }*/

    public void openNotification(){
        Intent openNotificationpage= new Intent(this,Notification.class);
        loginbutton.setEnabled(true);
        loaddialog.dismiss();
        //progressDialog.dismiss();
        startActivity(openNotificationpage);
    }

    private void Biometriccheck(){
        Log.i("Check", "Biometriccheck: Entered");
        BiometricPrompt.PromptInfo auth_prompt;
        Executor executor= ContextCompat.getMainExecutor(this);
        BiometricPrompt authentication= new BiometricPrompt(this, executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
                setMobileAndCountryCode();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                loaddialog.show();
                checkValidAuthToken();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }

        });

        auth_prompt = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Biometric login")
                .setSubtitle("Log in using your biometric credential")
                .setNegativeButtonText("Cancel")
                .build();

        authentication.authenticate(auth_prompt);



    }

    private void setMobileAndCountryCode() {
        ccp.setCountryForPhoneCode(CareAlert_StoredData.getInt(Constants.COUNTRY_CODE,1));
        username_input.setText(CareAlert_StoredData.getString(Constants.PHONE_NUMBER,""));
    }

    public void checkValidAuthToken(){
        final String Auth_Token =CareAlert_StoredData.getString("Auth_Token","");
        Call<Post> call =jsonPlaceHolderApi.getuserbyid("Bearer "+Auth_Token);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(response.isSuccessful()){
                 //   openDashboard();
                    openNotification();
                }
                else{
                    Toast.makeText(LoginScreen.this, "Login Session Expired", Toast.LENGTH_SHORT).show();
                    loginbutton.setEnabled(true);
                    loaddialog.dismiss();
                   // progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

                Toast.makeText(LoginScreen.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                loginbutton.setEnabled(true);
                loaddialog.dismiss();
              //  progressDialog.dismiss();
            }
        });

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
    public void onBackPressed() {
        openSignupPage();
        return;
    }
}