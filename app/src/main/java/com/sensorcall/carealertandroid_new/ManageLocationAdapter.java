package com.sensorcall.carealertandroid_new;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ManageLocationAdapter  extends ArrayAdapter<GetLocation>{
    private Context mcontext;
    private List<GetLocation> values;
    boolean healthD=false,ConnectionD=false;
    JsonPlaceHolderApi getdevice_api;
    String Auth;
    Drawable healthy,unhealty,connected,disconnected;
    List<GetDeviceByLocationId> getDevice;


    private static class ViewHolder {
        TextView LocationName;
        TextView LocationAddress;
        ImageView hea,conn;

    }
    public ManageLocationAdapter(List<GetLocation> values, Context context,Drawable healthy,Drawable unhealty,Drawable connected,Drawable disconnected,String AuthToken) {
        super(context, R.layout.manage_loc_listview, values);
        this.mcontext = context;
        this.values = values;
        this.Auth= AuthToken;
        this.healthy=healthy;
        this.unhealty=unhealty;
        this.connected=connected;
        this.disconnected= disconnected;

    }


    @Override
    @NonNull
    public View getView(int position, View convertView, ViewGroup parent)
    {

        GetLocation location= getItem(position);
//        String deviceProfileName = getItem(position).getDeviceProfileName();
//        int healthStatus = getItem(position).getHealthStatus();
//        String deviceNumber = getItem(position).getDeviceNumber();
//        String deviceRelatedId = getItem(position).getDeviceRelatedId();

            boolean wifi = getItem(position).isOnline();


//        int locId= getItem(position).getLocationId();
//        int deviceId=getItem(position).getDeviceId();
            final View result;

            ViewHolder viewHolder; // view lookup cache stored in tag


            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.manage_loc_listview, parent, false);
                viewHolder.LocationName = (TextView) convertView.findViewById(R.id.txt1);
                viewHolder.LocationAddress = (TextView) convertView.findViewById(R.id.txt2);
                viewHolder.hea= (ImageView) convertView.findViewById(R.id.health);
                viewHolder.conn=(ImageView) convertView.findViewById(R.id.connection);

                result = convertView;

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                result = convertView;
            }

            viewHolder.LocationName.setText(location.getLocationName());
            viewHolder.LocationAddress.setText(location.getLocationAddress());
            int locid=location.getLocationId();

            Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        getdevice_api = retrofit.create(JsonPlaceHolderApi.class);
        Call<List<GetDeviceByLocationId>> call = getdevice_api.getDevice(locid,"Bearer "+Auth);

        call.enqueue(new Callback<List<GetDeviceByLocationId>>() {
            @Override
            public void onResponse(Call<List<GetDeviceByLocationId>> call, Response<List<GetDeviceByLocationId>> response) {
                if(response.isSuccessful()){
                    getDevice=response.body();
                    if(getDevice.size()!=0){
                    for(int i=0;i<getDevice.size();i++){
                        GetDeviceByLocationId DeviceInfo= getDevice.get(i);
                        if(DeviceInfo.DeviceStatus!=3){
                            ConnectionD=true;
                        }
                        if(DeviceInfo.HealthStatus!=2){
                            healthD=true;
                        }
                    }

                    if(healthD){
                        viewHolder.hea.setImageDrawable(unhealty);
                    }
                    else{
                        viewHolder.hea.setImageDrawable(healthy);
                    }
                    if(ConnectionD) {
                        viewHolder.conn.setImageDrawable(disconnected);
                    }
                    else{
                        viewHolder.conn.setImageDrawable(connected);
                    }
                    }
                    else{
                        viewHolder.conn.setImageDrawable(disconnected);
                        viewHolder.hea.setImageDrawable(unhealty);

                    }
                }
                else{
                    viewHolder.conn.setImageDrawable(disconnected);
                    viewHolder.hea.setImageDrawable(unhealty);

                }
            }

            @Override
            public void onFailure(Call<List<GetDeviceByLocationId>> call, Throwable t) {
                viewHolder.conn.setImageDrawable(disconnected);
                viewHolder.hea.setImageDrawable(unhealty);

            }
        });





//        if(check==1)  viewHolder.Check.setVisibility(View.VISIBLE);
//        else
//            viewHolder.Check.setVisibility(View.INVISIBLE);



            return convertView;

    }


}
