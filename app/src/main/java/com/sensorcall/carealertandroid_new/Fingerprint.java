package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Fingerprint extends AppCompatActivity {
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    Button continue_button;
    TextView not_now;
    ProgressDialog loaddialog,progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint);
        CareAlert_SharedData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor=CareAlert_SharedData.edit();
        continue_button=(Button) findViewById(R.id.continue_button);
        not_now= (TextView) findViewById(R.id.skip);

        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();

        checkNetConnectivity();

        /*
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateuser("true");
            }
        });
        String not_now_msg="Not Now";
        SpannableString not_now_string = new SpannableString(not_now_msg);
        ClickableSpan clickspan= new ClickableSpan() {
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }

            @Override
            public void onClick(@NonNull View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                updateuser("false");

            }
        };
        not_now_string.setSpan(clickspan,0,7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        not_now.setText(not_now_string);
        not_now.setMovementMethod(LinkMovementMethod.getInstance());

        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                updateuser("true");
            }
        });
        */
    }
    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            checkNetContinue();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void checkNetContinue(){
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateuser("true");
            }
        });
        String not_now_msg="Not Now";
        SpannableString not_now_string = new SpannableString(not_now_msg);
        ClickableSpan clickspan= new ClickableSpan() {
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }

            @Override
            public void onClick(@NonNull View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                updateuser("false");

            }
        };
        not_now_string.setSpan(clickspan,0,7, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        not_now.setText(not_now_string);
        not_now.setMovementMethod(LinkMovementMethod.getInstance());

        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                updateuser("true");
            }
        });
    }

    void openSelectionPage(){
        Intent openservicesetup=new Intent(this,Servicesetupsteps.class);
        loaddialog.dismiss();
        startActivity(openservicesetup);
    }

    void updateuser(String fingerprint){
        Log.i("UpdateUser", "Entered Update User ");
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        String password=CareAlert_SharedData.getString("Password","");
        String email=CareAlert_SharedData.getString("Email","");
        String authToken = CareAlert_SharedData.getString("Auth_Token","");
        Log.i("Email", email);
        Log.i("Password", password);
        Log.i("Token", authToken);
        JsonObject body= new JsonObject();
        body.addProperty("Email",email);
        body.addProperty("Password",password);
        body.addProperty("FingerPrint",fingerprint);

        Call<Post> call = jsonPlaceHolderApi.updateuser("Bearer "+authToken,body);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(response.isSuccessful()){

                    Log.i("UpdateUser", "Call Sucess");
                    CareAlert_SharedData_Editor.putBoolean("SignupProcess",true);
                    CareAlert_SharedData_Editor.commit();
                    openSelectionPage();

                }
                else{

                    Log.i("UpdateUser", "CAll Fail ");
                    Toast.makeText(Fingerprint.this, "User Update Failed", Toast.LENGTH_SHORT).show();
                    loaddialog.dismiss();
                    continue_button.setEnabled(true);


                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Toast.makeText(Fingerprint.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                Log.i("UpdateUser", "Not made Call ");
                loaddialog.dismiss();
                continue_button.setEnabled(true);
            }
        });

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

}