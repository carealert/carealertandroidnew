package com.sensorcall.carealertandroid_new;

public class Constants {
    public static final String DEVICEID = "device_id";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String COUNTRY_CODE = "CountryCode";
    public static final String LOVED_ONE = "loved_one";
    public static final String EMAIL = "email";
    public static final String DEFAULT_LOCATION_ID = "default_location_id";
    private static String PROD = "https://app-ca-webapi.azurewebsites.net/";
    private static String DEV = "https://carealertdev.azurewebsites.net/";
    public static String BASE_URL = BuildConfig.BASE_URL;

}
