package com.sensorcall.carealertandroid_new;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PushNotificationSettings extends AppCompatActivity {
    Switch notification_switch;
    ProgressDialog loader, progressDialog;
    private ConnectionDetector detector;
    AlertDialog.Builder builder;
    SharedPreferences CareAlert_SharedData;
    TextView loc_name;
    String loc;
    Retrofit retrofit;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    JsonPlaceHolder jsonplaceholder;
    String Auth_Token;
    int locid = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_setting);

        notification_switch = findViewById(R.id.notification_switch);
        loc_name = (TextView) findViewById(R.id.location_name);

        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
//        loader=new ProgressDialog(this);
//        loader.setMessage("Loading...");
//        loader.setCanceledOnTouchOutside(false);
//        loader.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        loader.setContentView(R.layout.progress_dialog);

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Auth_Token = CareAlert_SharedData.getString("Auth_Token", "");
        jsonplaceholder = retrofit.create(JsonPlaceHolder.class);

        loader = new ProgressDialog(this);
        loader = showLoadingDialog(this);

        checkNetConnectivity();
        notification_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean prevvalue=notification_switch.isChecked();
                loader.show();
                JsonObject body =new JsonObject();
                body.addProperty("UserNotifications",prevvalue);
                body.addProperty("LocationId",locid);
                Call<NotificationStatus> Update = jsonPlaceHolderApi.UpdateNotstatus("Bearer "+Auth_Token,body);
                Update.enqueue(new Callback<NotificationStatus>() {
                    @Override
                    public void onResponse(Call<NotificationStatus> call, Response<NotificationStatus> response) {
                        if(!response.isSuccessful()){
                            Toast.makeText(PushNotificationSettings.this, "Failed to update Notification settings.", Toast.LENGTH_SHORT).show();
                            notification_switch.setChecked(!prevvalue);
                            loader.dismiss();
                        }
                        else{
                            Toast.makeText(PushNotificationSettings.this, "Updated Notifications settings.", Toast.LENGTH_SHORT).show();
                            loader.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationStatus> call, Throwable t) {
                        Toast.makeText(PushNotificationSettings.this, "Failed to update Notification settings.", Toast.LENGTH_SHORT).show();
                        notification_switch.setChecked(!prevvalue);
                        loader.dismiss();
                    }
                });
            }
        });
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            getDefaultLocationId();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    // Default location for devices
    private void getDefaultLocationId() {
        loader.show();
        Log.d("default location", "into the location id after net connection");
        Call<List<GetLocation>> call = jsonplaceholder.getDefaultLocation("Bearer " + Auth_Token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if (response.isSuccessful()) {
                    Log.i("Succees", "onResponse: Yes");
                    List<GetLocation> get_list = response.body();
                    Log.d("Location Count", "onResponse: " + get_list.size());
                    if (get_list.isEmpty()) {
                        Toast.makeText(PushNotificationSettings.this, "Default location is not set for this account", Toast.LENGTH_SHORT).show();
                        loader.dismiss();
                    } else {
                        for (int i = 0; i < get_list.size(); i++) {
                            if (get_list.get(i).isDefaultLocationId()) {
                                loc = get_list.get(i).getLocationName();
                                loc_name.setText(loc);
                                Log.d("Location Name", "onResponse: " + loc);
                                locid=get_list.get(i).getLocationId();
                                Call<NotificationStatus> call1 = jsonPlaceHolderApi.GetNotstatus("Bearer " + Auth_Token,get_list.get(i).getLocationId());
                                call1.enqueue(new Callback<NotificationStatus>() {
                                    @Override
                                    public void onResponse(Call<NotificationStatus> call, Response<NotificationStatus> response) {
                                        if(response.isSuccessful()){
                                            NotificationStatus result= response.body();
                                            if(result.UserNotifications)
                                                notification_switch.setChecked(true);
                                            else
                                                notification_switch.setChecked(false);
                                            loader.dismiss();
                                        }else{
                                            try {
                                                Log.d("Not", "onResponse: "+response.code()+response.errorBody().string());
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            Toast.makeText(PushNotificationSettings.this, "User is Not Registered for Notifications", Toast.LENGTH_SHORT).show();
                                            Intent back= new Intent(PushNotificationSettings.this,Settings.class);
                                            startActivity(back);
                                            loader.dismiss();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<NotificationStatus> call, Throwable t) {
                                        Toast.makeText(PushNotificationSettings.this, "Failed to get previous Notification Settings", Toast.LENGTH_SHORT).show();
                                        loader.dismiss();
                                    }
                                });
                               break;
                            }

                            //Log.i("Location Mil Gayi", "Abc " + Integer.toString(locationId));

                        }
                    }
                } else {
                    Toast.makeText(PushNotificationSettings.this, "LocationId Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.d("res error", "resuis: " + response.errorBody().toString());

                    loader.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                Toast.makeText(PushNotificationSettings.this, "No location set", Toast.LENGTH_SHORT).show();
                Log.d("failure12345", "error in fail");
                loader.dismiss();
            }
        });
    }


    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }


}
