package com.sensorcall.carealertandroid_new;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Wifipage extends AppCompatActivity {
    int selected_pos=-1;
    ArrayList<String> mWifiList= new ArrayList<String>();
    JSONObject WifiData,Response,SendData;
    Button continue_button;
    TextView txtAddDeviceManually;
    MyAdapter arrayAdapter;
    BluetoothDevice device;
    ProgressDialog loaddialog,progressDialog;
    ListView listView;
    JSONArray Dataarray;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifipage);
        listView= findViewById(R.id.list);
        continue_button=(Button) findViewById(R.id.bluetooth_continue);
        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        txtAddDeviceManually = findViewById(R.id.btn_add_manually);

        Intent prevpage=getIntent();
        device=prevpage.getExtras().getParcelable("CareAlert_Device");
        String WifiDatastr=prevpage.getStringExtra("WifiData");
        Log.i("DATA str is", "onCreate: "+WifiDatastr);
        try {
            WifiData= new JSONObject(WifiDatastr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("DATA object is", "onCreate: "+WifiData);
        try {
            Dataarray = WifiData.getJSONArray("ap");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("DATA ARray is", "onCreate: "+Dataarray);
        if (Dataarray!=null){
            for(int i=0;i<Dataarray.length();i++){
                try {
                    mWifiList.add(Dataarray.get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        arrayAdapter = new MyAdapter(mWifiList);
        listView.setAdapter(arrayAdapter);


        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selected_pos<0){
                    Toast.makeText(Wifipage.this, "Select a Wifi Connection", Toast.LENGTH_SHORT).show();
                }else {
                    loaddialog.show();
                    continue_button.setEnabled(false);
                    //openwifipasswordpage();
                    checkNetConnectivity();
                }
            }
        });

        txtAddDeviceManually.setOnClickListener(v -> {
            showAddDeviceBottomSheet();
        });


    }

    private void showAddDeviceBottomSheet() {
       BottomSheetDialog bottomsheet = new BottomSheetDialog(Wifipage.this, R.style.BottomSheetDialogTheme);

        View bottomSheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.bottomsheet_manual_device_add,
                null);

        EditText etwifiname = bottomSheetView.findViewById(R.id.edit_wifi_name);
        Button btnContinue = bottomSheetView.findViewById(R.id.wifipass_continue);

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wifiName = etwifiname.getText().toString();
                if(!wifiName.isEmpty()) {
                    openwifipasswordpage(wifiName);
                } else {
                    Toast.makeText(Wifipage.this, R.string.please_enter_wifi_name, Toast.LENGTH_LONG).show();
                }
            }
        });


        bottomsheet.setContentView(bottomSheetView);
        bottomsheet.show();

    }

    class MyAdapter extends BaseAdapter

    {

        private ArrayList< String> arrayList;

        MyAdapter(ArrayList<String> arrayList){
            this.arrayList = arrayList;
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return arrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null){
                view = android.view.View.inflate(Wifipage.this, R.layout.row, null);
            }
            final LinearLayout ll_parent = (LinearLayout) view.findViewById(R.id.ll_parent);
            final RadioButton radioButton = (RadioButton) view.findViewById(R.id.radio);
            radioButton.setText(arrayList.get(position));
            radioButton.setChecked(position == selected_pos);
            radioButton.setTag(position);
            ll_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selected_pos = (Integer) v.findViewById(R.id.radio).getTag();
                    notifyDataSetChanged();
                }
            });

            return view;
        }
    }
    public void openbluetoothpage(){
        Intent bluetoothpage= new Intent(this,Bluetoothdevice.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(bluetoothpage);
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            openwifipasswordpage(mWifiList.get(selected_pos));
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void openwifipasswordpage(String wifiName){
        Intent Wifipasswordpage= new Intent(this,WifiPassword.class);
        Wifipasswordpage.putExtra("CareAlert_Device", (Parcelable) device);
        Wifipasswordpage.putExtra("SSID", wifiName);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(Wifipasswordpage);

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}