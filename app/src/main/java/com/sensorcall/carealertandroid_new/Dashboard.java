package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.CustomBottomNavigationView1;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Dashboard extends AppCompatActivity {
    ProgressBar inscreenloader;
    CustomBottomNavigationView1 curvedBottomNavigationView;
    ProgressDialog loaddialog,progressDialog;
    ProgressBar Activityprogress,Comfortprogess,Environmentprogess;
    Retrofit retrofit;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    JsonPlaceHolder jsonplaceholder;
    SharedPreferences CareAlert_SharedData;
    ImageView ActivityImage,ComfortImage,EnvironmentImage;
    TextView ActivityText,ComfortText,EnvironmentText,DateText,DashboardWeekly,LocationName;
    String Auth_Token;
    TextView Message;
    String care_receiver_type, loc_name;
    int Act,Com,Env;

    SharedPreferences.Editor CareAlert_SharedData_Editor;
    int Locationid=0;
    Drawable restick,reswarn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        loaddialog.show();
        Message= (TextView) findViewById(R.id.wellness_subtitle);
        inscreenloader= (ProgressBar) findViewById(R.id.progressBar1);
        inscreenloader.setVisibility(View.VISIBLE);
        Activityprogress= (ProgressBar) findViewById(R.id.progressbar_activity) ;
        Comfortprogess= (ProgressBar) findViewById(R.id.progressbar_comfort) ;
        Environmentprogess= (ProgressBar) findViewById(R.id.progressbar_environment) ;

        DashboardWeekly= (TextView) findViewById(R.id.daily_trends);
        LocationName = (TextView) findViewById(R.id.location_name);
        ActivityImage=(ImageView) findViewById(R.id.activity_img);
        ComfortImage=(ImageView) findViewById(R.id.icon_circle1);
        EnvironmentImage=(ImageView) findViewById(R.id.icon_circle2);

        ActivityText=(TextView) findViewById(R.id.check);
        ComfortText=(TextView) findViewById(R.id.normal1);
        EnvironmentText=(TextView) findViewById(R.id.normal2);
        DateText=(TextView) findViewById(R.id.wellness_subtitle2);



        CareAlert_SharedData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor=CareAlert_SharedData.edit();
        Act = CareAlert_SharedData.getInt("Activity_Value",0);
        Env = CareAlert_SharedData.getInt("Env_Value",0);
        Com = CareAlert_SharedData.getInt("Com_Value",0);
        DateText.setText(CareAlert_SharedData.getString("DashboardDate","-"));


        String sign_in_msg="View Daily Trends";
        SpannableString sign_in_string= new SpannableString(sign_in_msg);
        ClickableSpan clickspan= new ClickableSpan() {
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }

            @Override
            public void onClick(@NonNull View view) {
                openDashboardweekly();
            }
        };
        sign_in_string.setSpan(clickspan,0,17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        DashboardWeekly.setText(sign_in_string);
        DashboardWeekly.setMovementMethod(LinkMovementMethod.getInstance());

        retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);

        checkNetConnectivity();

  /*      Auth_Token= CareAlert_SharedData.getString("Auth_Token","");
        jsonplaceholder = retrofit.create(JsonPlaceHolder.class);
        String Uritick="@drawable/ic_active";
        int imageResourcetick = getResources().getIdentifier(Uritick, null, getPackageName());
        restick = getResources().getDrawable(imageResourcetick);
        String Uriwarn="@drawable/ic_warning";
        int imageResourcewarn = getResources().getIdentifier(Uriwarn, null, getPackageName());
        reswarn = getResources().getDrawable(imageResourcewarn);
        FloatingActionButton floatingActionButton = findViewById(R.id.call_fab);
        curvedBottomNavigationView = findViewById(R.id.bottom_navigation);
        Activityprogress.setProgress(Act);
        Comfortprogess.setProgress(Com);
        Environmentprogess.setProgress(Env);
        if(Act<50){
            ActivityText.setText("Check");
            ActivityImage.setImageDrawable(reswarn);
        }
        else{
            ActivityText.setText("Normal");
            ActivityImage.setImageDrawable(restick);
        }
        if(Com<50){
            ComfortText.setText("Check");
            ComfortImage.setImageDrawable(reswarn);
        }
        else{
            ComfortText.setText("Normal");
            ComfortImage.setImageDrawable(restick);
        }
        if(Env<50){
            EnvironmentText.setText("Check");
            EnvironmentImage.setImageDrawable(reswarn);
        }
        else{
            EnvironmentText.setText("Normal");
            EnvironmentImage.setImageDrawable(restick);
        }
        getDefaultLocationId();
        //    bottomNavigationView.inflateMenu(R.menu.botton_navigation_menu);
//        call_btn = (Button) findViewById(R.id.button_call);
//
//        call_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent selectToCall = new Intent(Dashboard.this, SelectToCall.class);
//                selectToCall.putExtra("careReceiver",care_receiver);
//                selectToCall.putExtra("locationId",location_id);
//                startActivity(selectToCall);
//            }
//        });


        // Call button
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent selectToCall = new Intent(Dashboard.this,SelectToCall.class);
                startActivity(selectToCall);
            }
        });

        bottomNavigation();
        */
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            setUpUi();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void setUpUi(){
        Auth_Token= CareAlert_SharedData.getString("Auth_Token","");
        jsonplaceholder = retrofit.create(JsonPlaceHolder.class);
        String Uritick="@drawable/ic_active";
        int imageResourcetick = getResources().getIdentifier(Uritick, null, getPackageName());
        restick = getResources().getDrawable(imageResourcetick);
        String Uriwarn="@drawable/ic_warning";
        int imageResourcewarn = getResources().getIdentifier(Uriwarn, null, getPackageName());
        reswarn = getResources().getDrawable(imageResourcewarn);
        FloatingActionButton floatingActionButton = findViewById(R.id.call_fab);
        curvedBottomNavigationView = findViewById(R.id.bottom_navigation);
        Activityprogress.setProgress(Act);
        Comfortprogess.setProgress(Com);
        Environmentprogess.setProgress(Env);
        if(Act<50){
            ActivityText.setText("Check");
            ActivityImage.setImageDrawable(reswarn);
        }
        else{
            ActivityText.setText("Normal");
            ActivityImage.setImageDrawable(restick);
        }
        if(Com<50){
            ComfortText.setText("Check");
            ComfortImage.setImageDrawable(reswarn);
        }
        else{
            ComfortText.setText("Normal");
            ComfortImage.setImageDrawable(restick);
        }
        if(Env<50){
            EnvironmentText.setText("Check");
            EnvironmentImage.setImageDrawable(reswarn);
        }
        else{
            EnvironmentText.setText("Normal");
            EnvironmentImage.setImageDrawable(restick);
        }
        getDefaultLocationId();
        //    bottomNavigationView.inflateMenu(R.menu.botton_navigation_menu);
//        call_btn = (Button) findViewById(R.id.button_call);
//
//        call_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent selectToCall = new Intent(Dashboard.this, SelectToCall.class);
//                selectToCall.putExtra("careReceiver",care_receiver);
//                selectToCall.putExtra("locationId",location_id);
//                startActivity(selectToCall);
//            }
//        });


        // Call button
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent selectToCall = new Intent(Dashboard.this,SelectToCall.class);
                startActivity(selectToCall);
            }
        });
        bottomNavigation();
    }

    public void openDashboardweekly(){
        loaddialog.show();
        Intent opeweeklydash=new Intent(this, DashboardWeekly.class);
        loaddialog.dismiss();
        startActivity(opeweeklydash);
    }

    private void getDefaultLocationId() {
        Log.d("default location", "into the location id after net connection");
        Call<List<GetLocation>> call = jsonplaceholder.getDefaultLocation("Bearer " + Auth_Token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if (response.isSuccessful()) {
                    Log.i("Succees", "onResponse: Yes");
                    List<GetLocation> get_list = response.body();
                    Log.d("Location Count", "onResponse: "+get_list.size());
                    if (get_list.isEmpty()) {
                        Toast.makeText(Dashboard.this, "Default location is not set for this account", Toast.LENGTH_SHORT).show();
                        //loaddialog.dismiss();
                        inscreenloader.setVisibility(View.GONE);
                    } else {
                        for (int i = 0; i < get_list.size(); i++) {
                            if (get_list.get(i).isDefaultLocationId()) {
                                Locationid = get_list.get(i).getLocationId();
                                loc_name = get_list.get(i).getLocationName();
                                LocationName.setText(loc_name);
                                Log.d("Location Name", "onResponse: "+loc_name);
                                Log.d("Default Location Id is:", "onResponse: "+Locationid);
                                List<LovedOnes> Lovedonelist= get_list.get(i).getLovedOne();
                                Log.d("LoveOneList is", "onResponse: "+Lovedonelist.size());
                                for(int j=0;j<Lovedonelist.size();j++) {
                                    if(Lovedonelist.get(j).isPrimary()){
                                    care_receiver_type = Lovedonelist.get(j).getProfileName();
                                    Message.setText("How is " + care_receiver_type + " doing?");
                                    break;
                                    }
                                }
                                break;
                                }

                                //Log.i("Location Mil Gayi", "Abc " + Integer.toString(locationId));

                        }
                    }
                    if(Locationid==0){
                        Toast.makeText(Dashboard.this, "No Default Location Set", Toast.LENGTH_SHORT).show();
                        inscreenloader.setVisibility(View.GONE);
                        loaddialog.dismiss();
                    }
                    else{
                        Log.i("Default Location is:", "onResponse: "+ Locationid);
                        loaddialog.dismiss();
                        getDashboardValues();

                    }

                } else {
                    Toast.makeText(Dashboard.this, "LocationId Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.d("res error", "resuis: " + response.errorBody().toString());
                    inscreenloader.setVisibility(View.GONE);
                    loaddialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                Toast.makeText(Dashboard.this, "No Default location set", Toast.LENGTH_SHORT).show();
                Log.d("failure12345", "error in fail");
                //loaddialog.dismiss();
                inscreenloader.setVisibility(View.GONE);
            }
        });
    }

    public void getDashboardValues(){

        Call<List<Post>> call=jsonPlaceHolderApi.getdashboardtrends("Bearer "+Auth_Token,Locationid,null,null,null);
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    Log.i("Dashboard Response", "onResponse: "+ response.body().toString());
                    SimpleDateFormat sdf=new SimpleDateFormat("MMM dd, yyyy hh:mm a");
                    Log.d("Date is", "onResponse: "+Calendar.getInstance().getTime());
                    String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
                    List<Post> DashboardData=response.body();
                    if(DashboardData.size()==0){
                        Act=0;
                        Com=0;
                        Env=0;
                    }
                    else {
                        Act = (int) (DashboardData.get(0).getActivity());
                        Com = (int) (DashboardData.get(0).getComfort());
                        Env = (int) (DashboardData.get(0).getEnvironment());
                    }
                    DateText.setText(currentDateandTime);
                    Activityprogress.setProgress(Act);
                    Comfortprogess.setProgress(Com);
                    Environmentprogess.setProgress(Env);
                    if(Act<50){
                        ActivityText.setText("Check");
                        ActivityImage.setImageDrawable(reswarn);
                    }
                    else{
                        ActivityText.setText("Normal");
                        ActivityImage.setImageDrawable(restick);
                    }
                    if(Com<50){
                        ComfortText.setText("Check");
                        ComfortImage.setImageDrawable(reswarn);
                    }
                    else{
                        ComfortText.setText("Normal");
                        ComfortImage.setImageDrawable(restick);
                    }
                    if(Env<50){
                        EnvironmentText.setText("Check");
                        EnvironmentImage.setImageDrawable(reswarn);
                    }
                    else{
                        EnvironmentText.setText("Normal");
                        EnvironmentImage.setImageDrawable(restick);
                    }
                    CareAlert_SharedData_Editor.putInt("Activity_Value",Act);
                    CareAlert_SharedData_Editor.putInt("Env_Value",Env);
                    CareAlert_SharedData_Editor.putInt("Com_Value",Com);
                    CareAlert_SharedData_Editor.putString("DashboardDate",currentDateandTime);
                    CareAlert_SharedData_Editor.commit();
                    //loaddialog.dismiss();
                    inscreenloader.setVisibility(View.GONE);
                }
                else{
                    Toast.makeText(Dashboard.this, "Could Not get DashBoard Info", Toast.LENGTH_SHORT).show();
                    //loaddialog.dismiss();
                    inscreenloader.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Toast.makeText(Dashboard.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                //loaddialog.dismiss();
                inscreenloader.setVisibility(View.GONE);
            }
        });
    }
    //Bottom Navigation
    public void bottomNavigation(){

        Menu menu= curvedBottomNavigationView.getMenu();
      //  MenuItem menuitem= menu.getItem(2);
        MenuItem menuitem= menu.getItem(3);
        menuitem.setChecked(true);

        curvedBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.navigation_dashboard:
                        Intent dashBoard = new Intent(Dashboard.this, Dashboard.class);
                        startActivity(dashBoard);
                        Dashboard.this.finish();
                        break;

                    case R.id.navigation_Reminders:
                        Intent reminder = new Intent(Dashboard.this, RemindersViewActivity.class);
                        startActivity(reminder);
                        break;

                    case R.id.navigation_notifications:
                        Intent notification = new Intent(Dashboard.this, Notification.class);
                        startActivity(notification);
                        break;

                    case R.id.navigation_settings:
                        Intent settings = new Intent(Dashboard.this, Settings.class);
                        startActivity(settings);
                        break;
                }
                return true;
            }
        });
    }
    // Back press not need to work
    @Override
    public void onBackPressed() {
        Toast.makeText(Dashboard.this,"Please select from menu",Toast.LENGTH_LONG).show();
        return;
    }
    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

}