package com.sensorcall.carealertandroid_new.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
//import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public  class RetrofitInstance {
    public static JsonPlaceHolder jsonPlaceHolderApi = null;
    
    public static JsonPlaceHolder getRetrofitInstance(Context context){
        if(jsonPlaceHolderApi == null){
            jsonPlaceHolderApi = getRetrofit(context);
            return jsonPlaceHolderApi;
        }
        return jsonPlaceHolderApi;
    }

    private static  JsonPlaceHolder getRetrofit(Context context){
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.MINUTES)
                .connectTimeout(10, TimeUnit.MINUTES)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(new Interceptor() {
                    @NotNull
                    @Override
                    public Response intercept(@NotNull Chain chain) throws IOException {
                        SharedPreferences CareAlert_SharedData = context.getSharedPreferences("CareAlert_SharedData", 0);
                        String auth_token = CareAlert_SharedData.getString("Auth_Token", "");
                        Request request = chain.request()
                                .newBuilder()
                                .addHeader("Authorization", "Bearer "+auth_token)
                                .build();
                        return chain.proceed(request);
                    }
                })
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(JsonPlaceHolder.class);
    }
}
