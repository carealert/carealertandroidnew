package com.sensorcall.carealertandroid_new.util;

import android.bluetooth.BluetoothDevice;

import com.sensorcall.carealertandroid_new.Bluetoothdevice;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.model.ActivityLegend;

import java.util.ArrayList;

public class AppUtil {

    public static BluetoothDevice pairedDevice = null;

    public static ArrayList<ActivityLegend> getLegendLegendArrayList(){
        ArrayList<ActivityLegend> activityLegendArrayList = new ArrayList<>();
        activityLegendArrayList.add(new ActivityLegend("Carealert is highly confident that this activity was identified correctly", R.drawable.ic_active));
        activityLegendArrayList.add(new ActivityLegend("Carealert is not fully confident that this activity identified correctly an", R.drawable.ic_warning));
        activityLegendArrayList.add(new ActivityLegend("Presence was not used to determine the activity", R.drawable.ic_presense_unselecetd));
        activityLegendArrayList.add(new ActivityLegend("Presence was used to determine the activity", R.drawable.ic_presense));
        activityLegendArrayList.add(new ActivityLegend("Motion was not used to determine the activity", R.drawable.ic_motion_unselected));
        activityLegendArrayList.add(new ActivityLegend("Motion was used to determine the activity", R.drawable.ic_motion));
        activityLegendArrayList.add(new ActivityLegend("Sound classification was not used  to determine the activity", R.drawable.ic_sound_up_unslected));
        activityLegendArrayList.add(new ActivityLegend("Sound classification was used to determine the activity", R.drawable.ic_sound_up));
        return activityLegendArrayList;
    }
}
