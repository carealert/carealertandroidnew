package com.sensorcall.carealertandroid_new.VoiceReminder;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.Dashboard;
import com.sensorcall.carealertandroid_new.GetLocation;
import com.sensorcall.carealertandroid_new.HelperClass.AudioStream.AudioReminder;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RemindersRecordAudioActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "RemindersRecordAudioActivity";
    private static final int PERMISSION_REQUEST_CODE = 100;
    private static final int REQUEST_PERMISSION = 125;
    String remType;
    private static String filePath;
    private EditText etReminderName;
    TextView Reminderprev;
    boolean previousFile=false;
    String PreviousFilePath="";
    private RadioGroup radiogroupReminderType;
    private RadioButton reminderType;
    private Button btnNext;
    private Button btnRecord;
    private boolean isRecording = false;
    private String recordPermission = Manifest.permission.RECORD_AUDIO;
    private int PERMISSION_CODE = 7;
    private MediaPlayer mediaPlayer = null;
    private boolean isAudioPlaying = false;
    private ImageButton btnPlayReminderAudio;
    // Seekbar variables
    private SeekBar audioSeekbar;
    private Handler seekbarHandler;
    private Runnable updateSeekbar;
    Boolean AudioRecorded=false;
    private Button btnBackward;
    private Button btnForward;
    private TextView tvCurrentPlaybackTime;
    private TextView tvTotalPlaybackTime;
    Drawable Playbutton,Puasebutton;
    SharedPreferences CareAlert_SharedData;
    private static String AUDIO_FILE_PATH;
    String voiceMessageId;
    AudioReminder audioReminder;
    String Auth_token;
    String reminderName="";
    int reminderid;
    String URLFILEPATH;
    String type;
    String url;
    int locationid,loc_id;
    ArrayList<Integer> specifictime;
    ArrayList<Integer> weekday;
    ArrayList<Integer> weektime;
    ProgressDialog loaddialog,progressDialog;
    String loc_name;
    TextView loc;

    private MediaRecorder mediaRecorder;

    public static void getFilePath(String url) {
        filePath = url;
        Log.d("FileNameCheck","FileNameInRemInder"+filePath);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder);


        Intent remView = getIntent();
        loc_name = remView.getStringExtra("LocationName");
        loc_id = remView.getIntExtra("LocationId",0);

        Intent bottomSheetEdit = getIntent();
        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        reminderName = bottomSheetEdit.getStringExtra("ReminderName");
        reminderid = bottomSheetEdit.getIntExtra("ReminderId",0);
        type = bottomSheetEdit.getStringExtra("RemType");
        url = bottomSheetEdit.getStringExtra("url");
        locationid = bottomSheetEdit.getIntExtra("LocationId",0);
        specifictime = bottomSheetEdit.getIntegerArrayListExtra("SpecificDate");
        weekday = bottomSheetEdit.getIntegerArrayListExtra("WeekDay");
        weektime = bottomSheetEdit.getIntegerArrayListExtra("WeekTime");
        loc_name = bottomSheetEdit.getStringExtra("LocationName");
        Reminderprev=(TextView) findViewById(R.id.tvReminderPreview);
        Reminderprev.setVisibility(View.INVISIBLE);
        Log.d("Previous URL", "onCreate: "+ url);


        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData",0);
        Auth_token = CareAlert_SharedData.getString("Auth_Token","");
        Playbutton=getResources().getDrawable(getResources().getIdentifier("@drawable/play_button", null, getPackageName()));
        Puasebutton=getResources().getDrawable(getResources().getIdentifier("@drawable/ic_playbutton_reminder", null, getPackageName()));

        loc = (TextView) findViewById(R.id.location_name);
        loc.setText(loc_name);

        etReminderName = (EditText) findViewById(R.id.etReminderName);
        radiogroupReminderType = (RadioGroup) findViewById(R.id.radiogroupReminderType);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnRecord = (Button) findViewById(R.id.btnRecord);
        btnPlayReminderAudio = (ImageButton) findViewById(R.id.btnPlay);
        //btnPlayReminderAudio.setEnabled(false);
        btnPlayReminderAudio.setVisibility(View.INVISIBLE);

        audioSeekbar = (SeekBar) findViewById(R.id.seekBar2);
        audioSeekbar.setVisibility(View.INVISIBLE);

        btnBackward = (Button) findViewById(R.id.btnBackward);
        btnBackward.setVisibility(View.INVISIBLE);
        btnForward = (Button) findViewById(R.id.btnForward);
        btnForward.setVisibility(View.INVISIBLE);
        tvCurrentPlaybackTime = (TextView) findViewById(R.id.txtPreview1);
        tvCurrentPlaybackTime.setVisibility(View.INVISIBLE);
        tvTotalPlaybackTime = (TextView) findViewById(R.id.txtPreview2);
        tvTotalPlaybackTime.setVisibility(View.INVISIBLE);

        //AUDIO_FILE_PATH = getExternalFilesDir("/").getAbsolutePath() + "/" + voiceMessageId+".wav";


        if(reminderid != 0){
            try {
                loaddialog.show();
                startDownload(url);
                loaddialog.dismiss();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            etReminderName.setText(reminderName);
            //btnPlayReminderAudio.setEnabled(false);
        }
        else{
            //btnPlayReminderAudio.setVisibility();
            //btnPlayReminderAudio.setEnabled(true);
        }

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO},
                    REQUEST_PERMISSION);
            checkNetConnectivity();
        } else {
            //setupUI();
            checkNetConnectivity();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            setupUI();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setupUI() {
//        btnNext.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                int selectedId = radiogroupReminderType.getCheckedRadioButtonId();
////                Intent reminderAdd1 = new Intent(getApplicationContext(),ReminderAdd1Activity.class);
////                startActivity(reminderAdd1);
//            }
//        });


        btnRecord.setBackgroundResource(R.drawable.mic_new);
        btnRecord.setSoundEffectsEnabled(false);

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isRecording){
                    isRecording=false;
                    btnRecord.setBackgroundResource(R.drawable.mic_new);
                    audioReminder.stopRecording();
                    AudioRecorded=true;
                    //btnPlayReminderAudio.setEnabled(true);
                    btnPlayReminderAudio.setImageDrawable(Playbutton);
                      btnPlayReminderAudio.setVisibility(View.VISIBLE);
                    audioSeekbar.setVisibility(View.VISIBLE);
                    btnBackward.setVisibility(View.VISIBLE);
                    btnForward.setVisibility(View.VISIBLE);
                    tvCurrentPlaybackTime.setVisibility(View.VISIBLE);
                    tvTotalPlaybackTime.setVisibility(View.VISIBLE);
                    Reminderprev.setVisibility(View.VISIBLE);
                    previousFile=true;
                    PreviousFilePath=AUDIO_FILE_PATH;
                }
                else{
                    if(previousFile){
                        File DelFile = new File(PreviousFilePath);
                        DelFile.delete();
                    }
                    voiceMessageId = String.valueOf((System.currentTimeMillis()/1000));
                    AUDIO_FILE_PATH = getExternalFilesDir("/")+voiceMessageId+".wav";
                    btnRecord.setBackgroundResource(R.drawable.stop);
                    isRecording=true;
                    audioReminder = new AudioReminder(voiceMessageId,AUDIO_FILE_PATH,Auth_token,locationid);
                    if(isAudioPlaying){
                        stopAudio();
                    }
                    mediaPlayer=null;
                    if(mediaPlayer==null){
                        Log.d("MediaPlayer", "onTouch: Null Success");
                    }
                }
            }
        });
//        btnRecord.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        btnRecord.setBackgroundResource(R.drawable.ic_record_audio);
//                        audioReminder = new AudioReminder(voiceMessageId,AUDIO_FILE_PATH,Auth_token,locationid);
//                        if(isAudioPlaying){
//                            stopAudio();
//                        }
//                        mediaPlayer=null;
//                        if(mediaPlayer==null){
//                            Log.d("MediaPlayer", "onTouch: Null Success");
//                        }
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        btnRecord.setBackgroundResource(R.drawable.ic_record_stop);
//                        audioReminder.stopRecording();
//                        AudioRecorded=true;
//                        //btnPlayReminderAudio.setEnabled(true);
//                        btnPlayReminderAudio.setImageDrawable(Playbutton);
//                        btnPlayReminderAudio.setVisibility(View.VISIBLE);
//                        break;
//                }
//                return false;
//            }
//        });




        btnPlayReminderAudio.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (isRecording){
                    stopRecording();
                    audioSeekbar.setProgress(0);
                }
                else{
                    if(isAudioPlaying){
                        pauseAudio();
                    }
                    else{
                        if (mediaPlayer == null){
                            if(reminderid!=0){
                                Log.d("Play Audio", "onClick: Entered"+AudioRecorded);
                                if(!AudioRecorded){
                                    Log.d("Audio", "onClick: Will Play Last One");
                                    Log.d("URLFILEPATH is", "onClick: "+URLFILEPATH);
                                    playAudio(URLFILEPATH);
                                    //audioReminder.voiceReminderPlayback(RemindersRecordAudioActivity.this,URLFILEPATH);
                                }
                                else{
                                    Log.d("Audio", "onClick: Will play newone");
                                    btnPlayReminderAudio.setImageDrawable(Puasebutton);
                                    //audioReminder.voiceReminderPlayback(RemindersRecordAudioActivity.this, AUDIO_FILE_PATH,btnPlayReminderAudio,Playbutton);
                                    playAudio(AUDIO_FILE_PATH);
                                }
                            }
                            else {
                                Log.d("File Path->->", "onClick: " + AUDIO_FILE_PATH);
                                //playAudio(AUDIO_FILE_PATH);
                                btnPlayReminderAudio.setImageDrawable(Puasebutton);
                                //audioReminder.voiceReminderPlayback(RemindersRecordAudioActivity.this, AUDIO_FILE_PATH,btnPlayReminderAudio,Playbutton);
                                playAudio(AUDIO_FILE_PATH);


                            }
                        }
                        else{
                            resumeAudio();
                        }
                    }
                }
            }
        });

        btnBackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mediaPlayer != null && isRecording == false){
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 2000);
                    audioSeekbar.setProgress(mediaPlayer.getCurrentPosition() - 2000);
                }
            }
        });

        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mediaPlayer != null && isRecording == false){
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 2000);
                    audioSeekbar.setProgress(mediaPlayer.getCurrentPosition() + 2000);
                }
            }
        });


        audioSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.d("PlayBack TIme is ", "onProgressChanged: "+mediaPlayer.getCurrentPosition());
                int duration= mediaPlayer.getCurrentPosition();
                String time = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(duration),
                        TimeUnit.MILLISECONDS.toSeconds(duration) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
                );
                tvCurrentPlaybackTime.setText(time);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if(isRecording == false && mediaPlayer != null){
                    pauseAudio();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(isRecording == false && mediaPlayer != null){
                    int progress = seekBar.getProgress();
                    mediaPlayer.seekTo(progress);
                    resumeAudio();
                }
            }
        });

//        if(etReminderName.getText().toString().isEmpty()) {
//            Toast.makeText(this,"Please give a specific name to reminder",Toast.LENGTH_SHORT).show();
//        }
//        else{
//            btnNext.setEnabled(true);
//        }

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reminderName = etReminderName.getText().toString();
                int selectedId = radiogroupReminderType.getCheckedRadioButtonId();
                reminderType = (RadioButton) findViewById(selectedId);
                remType = (String) reminderType.getText();
                if (reminderName.length() == 0) {
                    Toast.makeText(RemindersRecordAudioActivity.this, "Reminder Name should contain something...", Toast.LENGTH_SHORT).show();
                }
                else if(!AudioRecorded && reminderid==0 ){
                    Toast.makeText(RemindersRecordAudioActivity.this, "Record an Audio Message for the Reminder", Toast.LENGTH_SHORT).show();

                }
                else {
                    if(reminderid == 0) {
                        loaddialog.show();
                        apiCallAudio(AUDIO_FILE_PATH);
                    }
                    else{
                        //reminderRepeatType.putExtra("url", filePath);
                        if(AudioRecorded){
                            loaddialog.show();
                            apiCallAudio(AUDIO_FILE_PATH);
                            Log.d("New URL Recorded", "onClick: "+filePath);
                            //reminderRepeatType.putExtra("url", filePath);
                        }else {
                            Log.d("New URL Recorded", "onClick: "+url);
                            Intent reminderRepeatType = new Intent(getApplicationContext(), RemindersRepeatTypeActivity.class);
                            reminderRepeatType.putExtra("ReminderName", reminderName);
                            reminderRepeatType.putExtra("url", "");
                            reminderRepeatType.putExtra("reminderType", remType);
                            reminderRepeatType.putExtra("ReminderId",reminderid);
                            reminderRepeatType.putExtra("LocationId",locationid);
                            reminderRepeatType.putIntegerArrayListExtra("WeekDay",weekday);
                            reminderRepeatType.putIntegerArrayListExtra("WeekTime",weektime);
                            reminderRepeatType.putIntegerArrayListExtra("SpecificDate",specifictime);
                            reminderRepeatType.putExtra("LocationName",loc_name);
                            Log.d("UpdateReminder","In ReminderRecord"+reminderid+" // "+reminderName+" // "+url+" // "+remType+" // "+weekday+" // "+locationid+" // "+weektime+" // "+specifictime);
                            startActivity(reminderRepeatType);
                        }
                    }
                }
            }
        });
    }


    void apiCallAudio(String FiletoUpload){
        File file = new File(FiletoUpload);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolder jsonPlaceHolderApi = retrofit.create(JsonPlaceHolder.class);
        RequestBody requestFile = RequestBody.create(MediaType.parse("audio/wav"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        //Uri file = Uri.fromFile(new File(filePath));
        Call<GetReminder> call = jsonPlaceHolderApi.audio("Bearer "+Auth_token,locationid,body);
        call.enqueue(new Callback<GetReminder>() {
            @Override
            public void onResponse(Call<GetReminder> call, Response<GetReminder> response) {
                if(response.isSuccessful()){
                    Log.d("successfulApiCall","successAPICALL"+response.code()+" "+response.body().toString());

                    if(reminderid==0){
                    Intent reminderRepeatType = new Intent(getApplicationContext(), RemindersRepeatTypeActivity.class);
                    reminderRepeatType.putExtra("ReminderName", reminderName);
                    Log.d("reminderName", "reminderNmae: " + reminderName);
                    reminderRepeatType.putExtra("url", response.body().getUrl());
                    Log.d("reminderName", "reminderUrl: " + filePath);
                    reminderRepeatType.putExtra("reminderType", remType);
                    reminderRepeatType.putExtra("LocationId", locationid);
                    Log.d("reminderName", "LocationId ");
                    Log.d("reminderName", "reminderType: " + remType);

                        File Delfile= new File(PreviousFilePath);
                        Delfile.delete();
                        loaddialog.dismiss();
                    startActivity(reminderRepeatType);
                    }
                    else{
                        Intent reminderRepeatType = new Intent(getApplicationContext(), RemindersRepeatTypeActivity.class);
                        reminderRepeatType.putExtra("ReminderName", reminderName);
                        reminderRepeatType.putExtra("url",response.body().getUrl());
                        reminderRepeatType.putExtra("reminderType", remType);
                        reminderRepeatType.putExtra("ReminderId",reminderid);
                        reminderRepeatType.putExtra("LocationId",locationid);
                        reminderRepeatType.putIntegerArrayListExtra("WeekDay",weekday);
                        reminderRepeatType.putIntegerArrayListExtra("WeekTime",weektime);
                        reminderRepeatType.putIntegerArrayListExtra("SpecificDate",specifictime);
                        Log.d("UpdateReminder","In ReminderRecord"+reminderid+" // "+reminderName+" // "+url+" // "+remType+" // "+weekday+" // "+locationid+" // "+weektime+" // "+specifictime);

                        File Delfile= new File(PreviousFilePath);
                        Delfile.delete();

                        loaddialog.dismiss();
                        startActivity(reminderRepeatType);
                    }

                }
                else{
                    if(reminderid==0){
                        Toast.makeText(RemindersRecordAudioActivity.this, "Failed to Upload Audio, Please Try Again", Toast.LENGTH_SHORT).show();
                        Intent Back= new Intent(RemindersRecordAudioActivity.this,RemindersViewActivity.class);

                        File Delfile= new File(PreviousFilePath);
                        Delfile.delete();

                        loaddialog.dismiss();
                        startActivity(Back);
                    }
                    else {
                        Toast.makeText(RemindersRecordAudioActivity.this, "Failed to Upload  Updated Audio , Please Try Again", Toast.LENGTH_SHORT).show();
                        Intent Back= new Intent(RemindersRecordAudioActivity.this,RemindersViewActivity.class);

                        File Delfile= new File(PreviousFilePath);
                        Delfile.delete();

                        loaddialog.dismiss();
                        startActivity(Back);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetReminder> call, Throwable t) {
                if(reminderid==0){
                    Toast.makeText(RemindersRecordAudioActivity.this, "Failed to Upload Audio, Please check your Internet Connection and Try Again", Toast.LENGTH_SHORT).show();
                    Intent Back= new Intent(RemindersRecordAudioActivity.this,RemindersViewActivity.class);

                    File Delfile= new File(PreviousFilePath);
                    Delfile.delete();

                    loaddialog.dismiss();
                    startActivity(Back);
                }
                else {
                    Toast.makeText(RemindersRecordAudioActivity.this, "Failed to Upload  Updated Audio , Please check your Internet Connection and Try Again", Toast.LENGTH_SHORT).show();
                    Intent Back= new Intent(RemindersRecordAudioActivity.this,RemindersViewActivity.class);

                    File Delfile= new File(PreviousFilePath);
                    Delfile.delete();

                    loaddialog.dismiss();
                    startActivity(Back);
                }
            }
        });
    }
    private void pauseAudio(){
        mediaPlayer.pause();
        isAudioPlaying = false;
        seekbarHandler.removeCallbacks(updateSeekbar);
        btnPlayReminderAudio.setImageDrawable(Playbutton);
    }

    private void resumeAudio(){
        mediaPlayer.start();
        isAudioPlaying = true;
        updateRunnable();
        seekbarHandler.postDelayed(updateSeekbar, 0);
        btnPlayReminderAudio.setImageDrawable(Puasebutton);
    }

    private void playAudio(String FilePath ) {
        mediaPlayer = new MediaPlayer();
        btnPlayReminderAudio.setImageDrawable(Puasebutton);

        try {
            mediaPlayer.setDataSource(FilePath);
            Log.d("FileName","FilePathInPlayAudio"+FilePath);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
        isAudioPlaying = true;

//        tvTotalPlaybackTime.setText(mediaPlayer.getDuration());

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){

            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stopAudio();
            }
        });

        audioSeekbar.setMax(mediaPlayer.getDuration());
        Log.d("THe Length of Audio is:", "playAudio: "+mediaPlayer.getDuration());
        int duration = mediaPlayer.getDuration();
        String time = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
        );
        Log.d("THe Length of Audio is:", "playAudio: "+time);
        tvTotalPlaybackTime.setText(time);
        seekbarHandler = new Handler();
        updateRunnable();
        seekbarHandler.postDelayed(updateSeekbar, 0);
    }



    private void updateRunnable() {
        updateSeekbar = new Runnable() {
            @Override
            public void run() {
                audioSeekbar.setProgress(mediaPlayer.getCurrentPosition());
                seekbarHandler.postDelayed(this, 50);
//                tvCurrentPlaybackTime.setText(mediaPlayer.getCurrentPosition());
            }
        };
    }

    private void stopAudio() {
        mediaPlayer.stop();
        mediaPlayer.release();
        isAudioPlaying = false;
        seekbarHandler.removeCallbacks(updateSeekbar);
        btnPlayReminderAudio.setImageDrawable(Playbutton);
        mediaPlayer = null;
    }


    //      Check for RECORD_AUDIO permission. Request permission if not present.
    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkAudioPermission(){
        if(ActivityCompat.checkSelfPermission(getApplicationContext(), recordPermission)  == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        else{
            requestPermissions(new String[]{recordPermission}, PERMISSION_CODE);
            return false;
        }
    }

    private void startRecording(){
        btnRecord.setBackgroundResource(R.drawable.stop);
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setOutputFile(AUDIO_FILE_PATH);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mediaRecorder.start();
        isRecording = true;

        mediaPlayer = null;
        audioSeekbar.setProgress(0);
    }

    private void stopRecording(){
        btnRecord.setBackgroundResource(R.drawable.mic_new);
        mediaRecorder.stop();
        isRecording = false;
        mediaRecorder.release();
        mediaRecorder = null;

        audioSeekbar.setProgress(0);

    }

    @Override
    protected void onStop() {
        super.onStop();

        if(isRecording){
            stopRecording();
        }

        if(isAudioPlaying){
            stopAudio();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnPlay:
                playAudio(AUDIO_FILE_PATH);
                //audioReminder.voiceReminderPlayback(this,AUDIO_FILE_PATH);
                return;
        }
    }





    // Back press button
    @Override
    public void onBackPressed() {
        Intent reminderView = new Intent(RemindersRecordAudioActivity.this, RemindersViewActivity.class);
        startActivity(reminderView);
    }

    private void startDownload(String url) throws ExecutionException, InterruptedException {
        AsyncTask<String, String, String> Filename=new DownloadFileAsync().execute(url);
        Log.d("Returned File Name is:", "startDownload: "+Filename.get());
        URLFILEPATH=Filename.get();
        previousFile=true;
        PreviousFilePath=URLFILEPATH;
        Log.d("FileName", "startDownload: "+URLFILEPATH);
        btnPlayReminderAudio.setImageDrawable(Playbutton);
        btnPlayReminderAudio.setVisibility(View.VISIBLE);
        audioSeekbar.setVisibility(View.VISIBLE);
        btnBackward.setVisibility(View.VISIBLE);
        btnForward.setVisibility(View.VISIBLE);
        tvCurrentPlaybackTime.setVisibility(View.VISIBLE);
        tvTotalPlaybackTime.setVisibility(View.VISIBLE);
        Reminderprev.setVisibility(View.VISIBLE);

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (audioReminder != null) {
            audioReminder.release();
        }
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}