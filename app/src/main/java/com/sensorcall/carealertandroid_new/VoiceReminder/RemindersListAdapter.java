package com.sensorcall.carealertandroid_new.VoiceReminder;



import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.sensorcall.carealertandroid_new.R;
import com.google.gson.JsonArray;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.widget.LinearLayout.HORIZONTAL;

//public class RemindersListAdapter extends RecyclerView.Adapter<RemindersListAdapter.ViewHolder> {
//
//    private List<RemindersListModel> mReminders;
//
//    public RemindersListAdapter(List<RemindersListModel> reminders){
//        mReminders = reminders;
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//        public TextView tvReminderType;
//        public TextView tvReminderDesc;
//
//        public ViewHolder(View itemView) {
//            super(itemView);
//
//            tvReminderType = (TextView) itemView.findViewById(R.id.tvReminderType);
//            tvReminderDesc = (TextView) itemView.findViewById(R.id.tvReminderDesc);
//        }
//    }
//
//    @Override
//    public RemindersListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
//        Context context = parent.getContext();
//        LayoutInflater inflater = LayoutInflater.from(context);
//
//        View reminderView = inflater.inflate(R.layout.recyclerview_reminder_item, parent, false);
//
//        ViewHolder viewHolder = new ViewHolder(reminderView);
//        return viewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(RemindersListAdapter.ViewHolder holder, int position){
//        RemindersListModel reminder = mReminders.get(position);
//
//        TextView tvType = holder.tvReminderType;
//        tvType.setText(reminder.getReminderType());
//        TextView tvDesc = holder.tvReminderDesc;
//        tvDesc.setText(reminder.getReminderDesc());
//    }
//
//    @Override
//    public int getItemCount() {
//        return mReminders.size();
//    }
//}

//public class RemindersListAdapter extends RecyclerView.Adapter<RemindersListAdapter.ViewHolder> {
//
//    private List<GetReminder> mReminders;
//
//    public RemindersListAdapter(List<GetReminder> reminders){
//        mReminders = reminders;
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//        public TextView tvReminderType;
//        public TextView tvReminderDesc;
//
//        public ViewHolder(View itemView) {
//            super(itemView);
//
//            tvReminderType = (TextView) itemView.findViewById(R.id.tvReminderType);
//            tvReminderDesc = (TextView) itemView.findViewById(R.id.tvReminderDesc);
//        }
//    }
//
//    @Override
//    public RemindersListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
//        Context context = parent.getContext();
//        LayoutInflater inflater = LayoutInflater.from(context);
//
//        View reminderView = inflater.inflate(R.layout.recyclerview_reminder_item, parent, false);
//
//        ViewHolder viewHolder = new ViewHolder(reminderView);
//        return viewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(RemindersListAdapter.ViewHolder holder, int position){
//        GetReminder reminder = mReminders.get(position);
//
//        TextView tvType = holder.tvReminderType;
//        tvType.setText(reminder.getType());
//        TextView tvDesc = holder.tvReminderDesc;
//        tvDesc.setText(reminder.getName());
//    }
//
//    @Override
//    public int getItemCount() {
//        return mReminders.size();
//    }
//}


public class RemindersListAdapter extends RecyclerView.Adapter<RemindersListAdapter.ViewHolder> {

    private List<GetReminder> mReminders;
    private RemindersListAdapter.OnItemListener mOnItemListener;
    private  float BaseUTCOffset;

    public RemindersListAdapter(List<GetReminder> reminders, RemindersListAdapter.OnItemListener mOnItemListener,float baseUTCOffset){
        mReminders = reminders;
        this.mOnItemListener = mOnItemListener;
        this.BaseUTCOffset=baseUTCOffset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tvReminderType;
        public TextView tvReminderDesc;
        RemindersListAdapter.OnItemListener onItemListener;

        public ViewHolder(View itemView, RemindersListAdapter.OnItemListener onItemListener) {
            super(itemView);

            tvReminderType = (TextView) itemView.findViewById(R.id.tvReminderType);
            tvReminderDesc = (TextView) itemView.findViewById(R.id.tvReminderDesc);
            this.onItemListener = onItemListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemListener.onItemClick(getAdapterPosition());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View reminderView = inflater.inflate(R.layout.reminders_items, parent, false);

        RemindersListAdapter.ViewHolder viewHolder = new RemindersListAdapter.ViewHolder(reminderView, mOnItemListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        GetReminder reminder = mReminders.get(position);
        String AppendString=" at ";
        TextView tvType = holder.tvReminderType;

        String remTpye = reminder.getType();
        List<Integer> SpecificTime= reminder.getSpecificTime();
        if(SpecificTime.size()>0) {
            Collections.sort(SpecificTime);
            int LatestTime= SpecificTime.get(0);
            long Multiplier=1000;
            long millis = LatestTime*Multiplier;
            Log.d("Epoch Time in Ms is", "upadteReminderTime: "+millis);
            Date date = new Date(millis);
            Log.d("The Date From Epoch is", "upadteReminderTime: "+date);
            SimpleDateFormat sdf = new SimpleDateFormat("h:mm a", Locale.ENGLISH);
            String formattedTime = sdf.format(date);
            AppendString=AppendString+formattedTime;
            Calendar calendar = Calendar.getInstance();
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(date);
            if(calendar.get(Calendar.DAY_OF_YEAR)==calendar1.get(Calendar.DAY_OF_YEAR) && calendar1.get(Calendar.YEAR)==calendar.get(Calendar.YEAR)){
                AppendString = AppendString + ", Today";
            }
            else {

                SimpleDateFormat sdf1 = new SimpleDateFormat("EEE, dd MMM", Locale.ENGLISH);
                String formattedDate = sdf1.format(date);
                AppendString = AppendString + " on " + formattedDate;
            }
        }
        else{
            List<Integer> Days= reminder.getWeekDay();
            List<Integer> WeekTime= reminder.getWeekTime();
            Collections.sort(Days);
            Collections.sort(WeekTime);

            Log.d("Days and Time", "onBindViewHolder: "+Days);
            Log.d("Days and Time", "onBindViewHolder: "+WeekTime);
            boolean DayFound=false;
            int NextDay=0;
            int NextTime;
            int CommingDay=0;

            SimpleDateFormat sdf=new SimpleDateFormat("EEEE");
            Log.d("Date is", "onResponse: "+ Calendar.getInstance().getTime());
            int Today=0;
            String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
            switch (currentDateandTime.toLowerCase()){
                case "sunday":
                    Today=0;
                    break;
                case "monday":
                    Today=1;
                    break;
                case "tuesday":
                    Today=2;
                    break;
                case "wednesday":
                    Today=3;
                    break;
                case "thursday":
                    Today=4;
                    break;
                case "friday":
                    Today=5;
                    break;
                case "saturday":
                    Today=6;
                    break;
            }

            for(int i=0;i<Days.size();i++){
                if(Days.get(i)>=Today){
                    NextDay=Days.get(i);
                    DayFound=true;
                    if(i==Days.size()-1){
                        CommingDay=Days.get(0);
                    }
                    else{
                        CommingDay=Days.get(i+1);
                    }
                    break;

                }

            }
            if(!DayFound)
                NextDay=Days.get(0);
            boolean TimeFound=false;
            if(NextDay==Today){
                NextTime=0;
                Calendar calendar = Calendar.getInstance();
                int hour24hrs = calendar.get(Calendar.HOUR_OF_DAY);
                int minutes = calendar.get(Calendar.MINUTE);
                int TimeEpoch= hour24hrs*3600 + minutes*60;
                for(int i=0;i<WeekTime.size();i++){
                    if(WeekTime.get(i)>TimeEpoch){
                        NextTime=WeekTime.get(i);
                        TimeFound=true;
                        break;
                    }
                }
                if(!TimeFound){
                    NextTime=WeekTime.get(0);
                }

                int hour=NextTime/3600;
                int Min = (NextTime%3600)/60;
                if(hour>12){
                    if(Min<10){
                        AppendString=AppendString+String.valueOf(hour-12)+":0"+String.valueOf(Min)+ " PM";
                    }
                    else{
                        AppendString=AppendString+String.valueOf(hour-12)+":"+String.valueOf(Min)+ " PM";
                    }
                }
                else{
                    if(Min<10){
                        AppendString=AppendString+String.valueOf(hour)+":0"+String.valueOf(Min)+ " AM";
                    }
                    else{
                        AppendString=AppendString+String.valueOf(hour)+":"+String.valueOf(Min)+ " AM";
                    }
                }
                if(!TimeFound){
                    DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM");
                    int diff=0;
                    if(CommingDay<Today){
                        diff= (7-Today)+NextDay;
                    }
                    else if(CommingDay==Today){
                        diff=7;
                    }
                    else{
                        diff= CommingDay-Today;
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, diff);
                    Date todate1 = cal.getTime();
                    String fromdate = dateFormat.format(todate1);
                    AppendString= AppendString+" on "+fromdate;
                }
                else {
                    AppendString = AppendString + ", Today";
                }
            }
            else{
                NextTime=WeekTime.get(0);
                int hour=NextTime/3600;
                int Min = (NextTime%3600)/60;
                if(hour>12){
                    if(Min<10){
                        AppendString=AppendString+String.valueOf(hour-12)+":0"+String.valueOf(Min)+ " PM";
                    }
                    else{
                        AppendString=AppendString+String.valueOf(hour-12)+":"+String.valueOf(Min)+ " PM";
                    }
                }
                else{
                    if(Min<10){
                        AppendString=AppendString+String.valueOf(hour)+":0"+String.valueOf(Min)+ " AM";
                    }
                    else{
                        AppendString=AppendString+String.valueOf(hour)+":"+String.valueOf(Min)+ " AM";
                    }
                }
                DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM");
                int diff=0;
                if(NextDay<Today){
                    diff= (7-Today)+NextDay;
                }
                else{
                    diff= NextDay-Today;
                }
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, diff);
                Date todate1 = cal.getTime();
                String fromdate = dateFormat.format(todate1);
                AppendString= AppendString+" on "+fromdate;
            }

        }

        if(remTpye.toLowerCase().equals("medication")) {
            tvType.setText("Medication Reminder");
            tvType.setTextColor(Color.parseColor("#FF3F35"));
        }
        else{
            tvType.setText("Message Reminder");
            tvType.setTextColor(Color.parseColor("#71BBE2"));
        }
        TextView tvDesc = holder.tvReminderDesc;
        tvDesc.setText(reminder.getName() + AppendString);
    }

    @Override
    public int getItemCount() {
        return mReminders.size();
    }

    public interface OnItemListener{
        void onItemClick(int position);
    }
}
