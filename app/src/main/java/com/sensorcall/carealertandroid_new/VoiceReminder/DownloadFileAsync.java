package com.sensorcall.carealertandroid_new.VoiceReminder;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

class DownloadFileAsync extends AsyncTask<String, String, String> {



    @Override
    protected String doInBackground(String... aurl) {
        int count;
        String FileName="";
        try {
            URL url = new URL(aurl[0]);
            URLConnection conexion = url.openConnection();
            conexion.connect();
            int lenghtOfFile = conexion.getContentLength();
            Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
            InputStream input = new BufferedInputStream(url.openStream());
            FileName= Environment.getExternalStorageDirectory().getAbsolutePath()+"/DownloadedFile.wav";
            Log.d("File Name is", "doInBackground: "+ FileName);
            Log.d("File Location is", "doInBackground: "+FileName);
            OutputStream output = new FileOutputStream(FileName);
            Log.d("OutputStream is", "doInBackground: "+output);
            byte data[] = new byte[1024];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {}
        return FileName;
    }

    @Override
    protected void onPostExecute(String unused) {

    }



}
