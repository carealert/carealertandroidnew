package com.sensorcall.carealertandroid_new.VoiceReminder;


import android.content.SharedPreferences;

import java.util.List;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemindersViewController implements Callback<List<GetReminder>>{

    static final String BASE_URL = Constants.BASE_URL;
    private JsonPlaceHolder jsonPlaceHolderApi;
    //    static final String Auth_Token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIrOTE3MjQwNDU4ODQ1IiwianRpIjoiYzAwNzA0ZjMtOTg5NS00MTk1LWIxOWQtMjRmNDI4ZWRlMjI1IiwiaWF0IjoxNjAwNjg2MzM5LCJyb2xlIjoiYXBpX2FjY2VzcyIsImlkIjoiZjkzNWY2NjktNmI5YS00MzhjLTg4MTgtOTc3NjJiYzFhZWRhIiwibmJmIjoxNjAwNjg2MzM5LCJleHAiOjE2MDEyOTExMzksImlzcyI6IndlYkFwaSIsImF1ZCI6Imh0dHA6Ly9jYXJlYWxlcnRkZXYuYXp1cmV3ZWJzaXRlcy5uZXQvIn0.VMddjEyzi4fiNi2E7OvIkDEctcf19IMzyiMlNDYl7Cg";
    private List<GetReminder> getReminders;
    static final Integer LocationId = 9;
    String Auth_Token;
//    private static GetReminder getReminder;

    public void start() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolder.class);

//        final String Auth_Token = CareAlert_StoredData.getString("Auth_Token","");

        Call<List<GetReminder>> call = jsonPlaceHolderApi.getReminder("Bearer " + Auth_Token,LocationId);
        call.enqueue(this);

    }

    public void Reminders(List<GetReminder> getReminders){

    }

    @Override
    public void onResponse(Call<List<GetReminder>> call, Response<List<GetReminder>> response) {
        if(response.isSuccessful()) {
            getReminders = (List<GetReminder>) response.body();
//            getReminder = response.body();
            Reminders(getReminders);
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<List<GetReminder>> call, Throwable t) {
        t.printStackTrace();
    }
}
