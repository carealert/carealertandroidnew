package com.sensorcall.carealertandroid_new.VoiceReminder;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DividerItemDecoration;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.HelperClass.AudioStream.AudioReminder;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static android.widget.LinearLayout.HORIZONTAL;


public class RemindersSpecificDateActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String TAG = "RemindersSpecificDateActivity";

    int day, month, year, hour, minute;
    int myday, myMonth, myYear, myHour, myMinute;
    DatePicker datePicker;

    private Button btnCreate;
    private Button btnSetDate;
    SharedPreferences CareAlert_SharedData;
    String auth;
    String name;
    int reminderid;
    String type;
    String url;
    int locationid;
    JsonArray specifictime = new JsonArray();
    JsonPlaceHolder jsonPlaceHolder;
    ProgressDialog loader, progressDialog;
    int epoch = 0;
    int epochDiff = 0;
    float BaseUTCOffset;
    float CurrentOffsetTime;
    List<Integer> specificTime;
    AudioReminder audioReminder;
    ArrayList<Integer> specifictime1;
    LinearLayout listDateTime;
    String loc;
    TextView loc_name;
    Calendar c;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_date);

        Intent recordAudio = getIntent();
        reminderid = recordAudio.getIntExtra("ReminderId", 0);
        Log.d("ReminderId", "onCreate:In specific Date " + reminderid);
        locationid = recordAudio.getIntExtra("LocationId", 0);
        name = recordAudio.getStringExtra("ReminderName");
        url = recordAudio.getStringExtra("url");
        type = recordAudio.getStringExtra("reminderType");
        specifictime1 = recordAudio.getIntegerArrayListExtra("SpecificDate");
        loc = recordAudio.getStringExtra("LocationName");
        loc_name = (TextView) findViewById(R.id.location_name);
        loc_name.setText(loc);

        Log.d("recordrepeat", "recordrepeatDate: " + name + " " + url + " " + type);

        btnSetDate = (Button) findViewById(R.id.btnSetDate);
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
        BaseUTCOffset = CareAlert_SharedData.getFloat("BaseUTCOffset", 0);
        Log.d("BaseOffset is", "onCreate: " + BaseUTCOffset);
        String CurrentOffset = new SimpleDateFormat("XXX", Locale.getDefault()).format(System.currentTimeMillis());
        Log.d("Current Offset is", "onCreate: " + CurrentOffset);
        String[] separated = CurrentOffset.split(":");
        double Minutesoffset = (Double.valueOf(separated[1]) / 60) * 10;
        CurrentOffset = separated[0] + ":" + String.valueOf((int) Minutesoffset);
        CurrentOffsetTime = Float.valueOf(CurrentOffset.replace(":", ".")).floatValue();
        loader = new ProgressDialog(RemindersSpecificDateActivity.this);
        loader = showLoadingDialog(this);
        loader.dismiss();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolder = retrofit.create(JsonPlaceHolder.class);

        btnSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (specifictime.size() == 4) {
                    Toast.makeText(RemindersSpecificDateActivity.this, "Maximum 4 Reminder Time can be set", Toast.LENGTH_SHORT).show();

                } else {
                    Calendar calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog datePickerDialog = new DatePickerDialog(RemindersSpecificDateActivity.this, RemindersSpecificDateActivity.this, year, month, day);
                    datePickerDialog.show();
                }
            }
        });
        checkNetConnectivity();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void checkNetConnectivity() {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet", "devic123");
            setUpUI();
        } else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog() {
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    public void setUpUI() {
        if (reminderid == 0) {
            // epochTime();
            btnCreate = (Button) findViewById(R.id.btnNext2);
            btnCreate.setText("Create");
            btnCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (specifictime.size() == 0) {
                        Toast.makeText(RemindersSpecificDateActivity.this, "Please Select Date and Time", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d("CreateReminder", "CreateReminder: ");
                        createReminder();
//                            if (audioReminder != null) {
//                                audioReminder.release();
//                            }

                    }
                }
            });
        } else {
            upadteReminderTime();
            btnCreate = (Button) findViewById(R.id.btnNext2);
            btnCreate.setText("Update");
            btnCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("UpdateReminder", "UpdateReminder: ");
                    updateReminder();

                }
            });
        }
    }


    public void createReminder() {
        loader.show();
        if (specifictime.size() == 0) {
            Toast.makeText(this, "Reminder Time cannot be Empty\n Add Time for Reminder", Toast.LENGTH_SHORT).show();
            loader.dismiss();
        } else {
            JsonObject apiBody = new JsonObject();
            apiBody.addProperty("Name", name);
            apiBody.addProperty("Type", type);
            apiBody.addProperty("Url", url);
            apiBody.addProperty("LocationId", locationid);
            apiBody.add("SpecificTime", specifictime);
            Log.d("TIme", "Specific TIme : " + apiBody.toString());
            auth = CareAlert_SharedData.getString("Auth_Token", "");
            Log.d("", "" + auth);
            Log.d("TIme", "Specific TIme : " + apiBody.toString());
            Call<GetReminder> call = jsonPlaceHolder.createReminder("Bearer " + auth, apiBody);
            call.enqueue(new Callback<GetReminder>() {
                @Override
                public void onResponse(Call<GetReminder> call, Response<GetReminder> response) {
                    if (response.isSuccessful()) {
                        String res = response.body().toString();
                        Log.d("Sucesss", "ResBody: " + res);
                        loader.dismiss();
                        Intent reminderView = new Intent(RemindersSpecificDateActivity.this, RemindersViewActivity.class);
                        startActivity(reminderView);
                    } else {
                        Toast.makeText(RemindersSpecificDateActivity.this, "Failed to Create Reminder, Please Try Again", Toast.LENGTH_SHORT).show();
                        Intent reminderView = new Intent(RemindersSpecificDateActivity.this, RemindersViewActivity.class);
                        startActivity(reminderView);
                        loader.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GetReminder> call, Throwable t) {
                    Log.d("in failure", "fail");
                    loader.dismiss();
                    Toast.makeText(RemindersSpecificDateActivity.this, "Failed to Create Reminder, Please check your Internet Connection and Try Again", Toast.LENGTH_SHORT).show();
                    Intent reminderView = new Intent(RemindersSpecificDateActivity.this, RemindersViewActivity.class);
                    startActivity(reminderView);
                }
            });
        }
    }

    public void updateReminder() {
        loader.show();
        if (specifictime.size() == 0) {
            Toast.makeText(this, "Reminder Time cannot be Empty\n Add Time for Reminder", Toast.LENGTH_SHORT).show();
            loader.dismiss();
        } else {
            Log.d("UpdateReminder", "UpdateReminder: ");
            JsonObject apiBody = new JsonObject();
            apiBody.addProperty("ReminderId", reminderid);
            apiBody.addProperty("Name", name);
            apiBody.addProperty("Type", type);
            if (url != "")
                apiBody.addProperty("Url", url);
            apiBody.addProperty("LocationId", locationid);
            apiBody.add("SpecificTime", (JsonElement) specifictime);
            Log.d("TIme", "Specific TIme : " + apiBody.toString());
            auth = CareAlert_SharedData.getString("Auth_Token", "");
            Log.d("", "" + auth);
            Call<GetReminder> call = jsonPlaceHolder.updateReminder("Bearer " + auth, apiBody);
            call.enqueue(new Callback<GetReminder>() {
                @Override
                public void onResponse(Call<GetReminder> call, Response<GetReminder> response) {
                    if (response.isSuccessful()) {
                        String res = response.body().toString();
                        Log.d("Sucesss", "ResBody: " + res);
                        loader.dismiss();
                        Intent reminderView = new Intent(RemindersSpecificDateActivity.this, RemindersViewActivity.class);
                        startActivity(reminderView);
                    } else {
                        loader.dismiss();
                        Toast.makeText(RemindersSpecificDateActivity.this, "Failed to Update Reminder, Please Try Again", Toast.LENGTH_SHORT).show();
                        Intent reminderView = new Intent(RemindersSpecificDateActivity.this, RemindersViewActivity.class);
                        startActivity(reminderView);
                    }
                }

                @Override
                public void onFailure(Call<GetReminder> call, Throwable t) {
                    Log.d("in failure", "fail");
                    loader.dismiss();
                    Toast.makeText(RemindersSpecificDateActivity.this, "Failed to Update Reminder, Please check your Internet Connection and Try Again", Toast.LENGTH_SHORT).show();
                    Intent reminderView = new Intent(RemindersSpecificDateActivity.this, RemindersViewActivity.class);
                    startActivity(reminderView);
                }
            });
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
        myYear = y;
        myMonth = m + 1;
        myday = d;

        c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR);
        minute = c.get(Calendar.MINUTE);
        // int am = c.get(Calendar.AM_PM);
        TimePickerDialog timePickerDialog = new TimePickerDialog(RemindersSpecificDateActivity.this, RemindersSpecificDateActivity.this, hour, minute, DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int h, int t) {
        myHour = h;
        myMinute = t;

//        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_remove_time, null);
//
//        LinearLayout layout= (LinearLayout) findViewById(R.id.textview_time);
//        TextView dateTime = new TextView(this);
//        LinearLayout.LayoutParams layoutParams =new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        dateTime.setLayoutParams(layoutParams);
//        layoutParams.setMargins(40,10,20,5);
//        dateTime.setText(myday+"/"+myMonth+"/"+myYear+", "+myHour+":"+myMinute);
//        dateTime.setTextSize(16);
//        dateTime.setBackgroundColor(Color.WHITE);
//        layout.addView(dateTime);
//        Button remove = new Button(this);
//        layoutParams.setMargins(30,20,5,5);
//        remove.setBackgroundDrawable(drawable);
//        remove.setWidth(14);
//        remove.setHeight(14);
//        layout.addView(remove);

        listDateTime = (LinearLayout) findViewById(R.id.textview_time);
        View dateTimeList = getLayoutInflater().inflate(R.layout.row_add_specifictime, null, false);

        TextView speciDate = (TextView) dateTimeList.findViewById(R.id.time);
        speciDate.setBackgroundColor(Color.parseColor("#ffffff"));
        String calendar = myMonth + "/" + myday + "/" + myYear + " " + myHour + ":" + myMinute + ":00";
        Date date = new Date(calendar);
        Calendar calendarObj = Calendar.getInstance();
        calendarObj.setTime(date);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy, h:mm a", Locale.ENGLISH);
        String formattedDate = sdf.format(date);

        try {
            epoch = (int) (new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(calendar).getTime() / 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateTime dateTimeInUtc = new DateTime(calendarObj, DateTimeZone.UTC);
        epoch = (int) (dateTimeInUtc.getMillis() / 1000);

        if (calendarObj.getTimeInMillis() > System.currentTimeMillis()) {
            specifictime.add((int) epoch);
            speciDate.setText(formattedDate);
            speciDate.setTextSize(18);
            ImageView removeTime = (ImageView) dateTimeList.findViewById(R.id.remove_time);

            removeTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int i = 0; i < specifictime.size(); i++) {
                        if (specifictime.get(i).getAsInt() == epoch) {
                            specifictime.remove(i);
                            break;
                        }
                    }

                    removeTime(dateTimeList);

                }
            });

            listDateTime.addView(dateTimeList);
        } else
            Toast.makeText(this, "Reminders cannot be set for Past Time", Toast.LENGTH_SHORT).show();
        Log.d("DateFormat", "dateFormat " + date);
        Log.d("DateFormat", "dateFormat " + epoch);
        Log.d("TimeDate", "TimeDate:" + calendar);

    }

    //Predefine reminder Time and Date

    public void upadteReminderTime() {
        for (int i = 0; i < specifictime1.size(); i++) {
            Log.d("specificTime", "upadteReminderTime: " + specifictime1.get(i));
            int simpleDateTime = specifictime1.get(i) + (int) ((CurrentOffsetTime - BaseUTCOffset) * 3600);
            Log.d("specificTime", "upadteReminderTime: " + simpleDateTime);
            specifictime.add(specifictime1.get(i));
            Log.d("Epoch time is", "upadteReminderTime: " + simpleDateTime);
            long Multiplier = 1000;
            long millis = simpleDateTime * Multiplier;
            Log.d("Epoch Time in Ms is", "upadteReminderTime: " + millis);
            Date date = new Date(millis);
            Log.d("The Date From Epoch is", "upadteReminderTime: " + date);
            SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy, h:mm a", Locale.ENGLISH);
            String formattedDate = sdf.format(date);

//            try {
//                Date format = new SimpleDateFormat("MM/dd/yyyy, HH:mm.ss").parse(String.valueOf(simpleDateTime));
//                Log.d("Format Time:", "upadteReminderTime:: "+format);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
            Log.d("Formated Time", "upadteReminderTime: " + formattedDate);

            listDateTime = (LinearLayout) findViewById(R.id.textview_time);
            View dateTimeList = getLayoutInflater().inflate(R.layout.row_add_specifictime, null, false);

            TextView speciDate = (TextView) dateTimeList.findViewById(R.id.time);
            speciDate.setBackgroundColor(Color.parseColor("#ffffff"));
            speciDate.setText(formattedDate);
            speciDate.setTextSize(18);
            ImageView removeTime = (ImageView) dateTimeList.findViewById(R.id.remove_time);

            removeTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeTime(dateTimeList);
                    specifictime.remove((int) epoch);
                }
            });
            listDateTime.addView(dateTimeList);
        }
    }

    private void removeTime(View view) {
        listDateTime.removeView(view);
    }

    public void epochTime() {
        for (int i = 0; i < specifictime1.size(); i++) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.format(new Date());
        }
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    // Back press button
    @Override
    public void onBackPressed() {
        Intent reminderView = new Intent(RemindersSpecificDateActivity.this, RemindersViewActivity.class);
        startActivity(reminderView);
    }

}

