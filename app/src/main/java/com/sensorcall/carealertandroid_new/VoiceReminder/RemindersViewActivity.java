package com.sensorcall.carealertandroid_new.VoiceReminder;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.gesture.GestureOverlayView;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.Dashboard;
import com.sensorcall.carealertandroid_new.GetDeviceByLocationId;
import com.sensorcall.carealertandroid_new.GetLocation;
import com.sensorcall.carealertandroid_new.JsonPlaceHolderApi;
import com.sensorcall.carealertandroid_new.Notification;
import com.sensorcall.carealertandroid_new.Settings;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import static com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewController.Auth_Token;

public class RemindersViewActivity extends AppCompatActivity implements RemindersListAdapter.OnItemListener {

    private static final String TAG = "RemindersViewActivity";

    ArrayList<RemindersListModel> reminders;
    static final String BASE_URL = Constants.BASE_URL;
    private JsonPlaceHolder jsonPlaceHolderApi;
    JsonPlaceHolderApi jsonPlaceHolderApi1;
    private List<GetReminder> mReminders;
    private RecyclerView rvReminders;
    private TextView tvReminderDesc;
    int locationId;
    float UTCOffset;
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    ProgressDialog loader,progressDialog;
    String Auth_token;
    TextView loc_name;
    String loc;
    Retrofit retrofit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_view);

        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor=CareAlert_SharedData.edit();

        loader = new ProgressDialog(RemindersViewActivity.this);
        loader = showLoadingDialog(this);
        Auth_token = CareAlert_SharedData.getString("Auth_Token","");
        loc_name = (TextView) findViewById(R.id.location_name);
        FloatingActionButton addReminders = findViewById(R.id.add_reminders);
        rvReminders = (RecyclerView) findViewById(R.id.rvReminders);
        FloatingActionButton floatingActionButton = findViewById(R.id.call_fab);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvReminders.getContext(), 0);
//        rvReminders.addItemDecoration(dividerItemDecoration);

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolder.class);
        jsonPlaceHolderApi1=retrofit.create(JsonPlaceHolderApi.class);

        checkNetConnectivity();

        addReminders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loader.show();
                Call<List<GetDeviceByLocationId>> call= jsonPlaceHolderApi1.getDevice(locationId,"Bearer "+Auth_token);
                call.enqueue(new Callback<List<GetDeviceByLocationId>>() {
                    @Override
                    public void onResponse(Call<List<GetDeviceByLocationId>> call, Response<List<GetDeviceByLocationId>> response) {
                        if(response.isSuccessful()){
                            List<GetDeviceByLocationId> result=response.body();
                            if(result.size()==0){
                                Toast.makeText(RemindersViewActivity.this, "No Device added to Location, cannot create a reminder.", Toast.LENGTH_SHORT).show();
                                loader.dismiss();
                            }
                            else {
                                loader.dismiss();
                                Intent reminderAdd = new Intent(RemindersViewActivity.this, RemindersRecordAudioActivity.class);
                                reminderAdd.putExtra("LocationId", locationId);
                                reminderAdd.putExtra("LocationName", loc);
                                startActivity(reminderAdd);
                            }
                        }
                        else{
                            Toast.makeText(RemindersViewActivity.this, "Failed to initate Reminder, Please try again.", Toast.LENGTH_SHORT).show();
                            loader.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<GetDeviceByLocationId>> call, Throwable t) {
                        Toast.makeText(RemindersViewActivity.this, "Failed to initate Reminder, Please try again.", Toast.LENGTH_SHORT).show();
                        loader.dismiss();
                    }
                });

            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent selectToCall = new Intent(RemindersViewActivity.this,SelectToCall.class);
                startActivity(selectToCall);
            }
        });
        bottomNavigation();
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            getDefaultLocationId();
            //getReminders();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    private void getDefaultLocationId(){
        loader.show();
        Log.d("default location","into the location id after net connection");
        String Auth_token=CareAlert_SharedData.getString("Auth_Token","");
        Call<List<GetLocation>> call = jsonPlaceHolderApi.getDefaultLocation("Bearer "+Auth_token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if(response.isSuccessful())
                {
                    Log.i("Succees", "onResponse: Yes");
                    List<GetLocation> get_list = response.body();
                    if(get_list.isEmpty()) {
                        Toast.makeText(RemindersViewActivity.this, "Default location is not set for this account", Toast.LENGTH_SHORT).show();
                        loader.dismiss();
                    }
                    else{
                        for(int i=0;i<get_list.size();i++){
                            if(get_list.get(i).isDefaultLocationId()) {
                                locationId = get_list.get(i).getLocationId();
                                UTCOffset=get_list.get(i).getBaseUTCOffsetHour();
                                loc = get_list.get(i).getLocationName();
                                loc_name.setText(loc);
                                CareAlert_SharedData_Editor.putFloat("BaseUTCOffset",UTCOffset);
                                Log.d("BaseOffset is", "onResponse: "+UTCOffset);

                                CareAlert_SharedData_Editor.commit();
                                Log.i("Location Mil Gayi", "Abc " + Integer.toString(locationId));
                                getReminders();
                            }
                        }
                    }
                }
                else{
                    Toast.makeText(RemindersViewActivity.this, "LocationId Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.d("res error","resuis: "+response.errorBody().toString());
                    loader.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                Toast.makeText(RemindersViewActivity.this, "No Default location set", Toast.LENGTH_SHORT).show();
                Log.d("failure12345","error in fail");
                loader.dismiss();
            }
        });
    }

    void getReminders(){
        Auth_token = CareAlert_SharedData.getString("Auth_Token","");
        Log.d("",""+Auth_token);
        Call<List<GetReminder>> call = jsonPlaceHolderApi.getReminder("Bearer "+Auth_token,locationId);
        call.enqueue(new Callback<List<GetReminder>>() {
            @Override
            public void onResponse(Call<List<GetReminder>> call, Response<List<GetReminder>> response) {
                if(response.isSuccessful()) {
                    mReminders = response.body();
                    initRecyclerView(mReminders);  // Initialize Recyclerview
                } else {
                    System.out.println(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<GetReminder>> call, Throwable t) {
                t.printStackTrace();
                loader.dismiss();
            }
        });
    }

    //  Recyclerview initialization method
    public void initRecyclerView(List<GetReminder> mReminders){
        RemindersListAdapter adapter1 = new RemindersListAdapter(mReminders, this,UTCOffset);
        rvReminders.setAdapter(adapter1);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerLine(ContextCompat.getDrawable(this, R.drawable.horizontal_line));
        rvReminders.addItemDecoration(dividerItemDecoration);
        rvReminders.setLayoutManager(new LinearLayoutManager(this));
        loader.dismiss();
    }

    //  Click listener for List items
    @Override
    public void onItemClick(int position) {
//        Log.d(TAG, "onItemClick: clicked" + position);

        RemindersViewBottomSheetFragment bottomSheet;
        bottomSheet = new RemindersViewBottomSheetFragment();
        Bundle bundle = new Bundle();

        String remindername = mReminders.get(position).getName();
        String name = mReminders.get(position).getName();
        int reminderId = mReminders.get(position).getReminderId();
        String type = mReminders.get(position).getType();
        String url = mReminders.get(position).getDownurl();
        int locationid = mReminders.get(position).getLocationId();
        List<Integer> specificTime = mReminders.get(position).getSpecificTime();
        List<Integer> weekday = mReminders.get(position).getWeekDay();
        List<Integer> weektime = mReminders.get(position).getWeekTime();
        String rem_loc = loc;

        bundle.putString("remindername", remindername);
        bottomSheet.setArguments(bundle);
        bottomSheet.getAuthToken(Auth_token, name, reminderId, type, url, locationId, specificTime, weekday, weektime,rem_loc);
        bottomSheet.show(getSupportFragmentManager(),"ModalBottomSheet");


//        bottomsheet = new BottomSheetDialog(RemindersViewActivity.this, R.style.BottomSheetDialogTheme);
//
//        View bottomSheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.bottom_sheet_reminders,
//                findViewById(R.id.bottom_sheet_container));
//
//        bottomsheet.setContentView(bottomSheetView);
//
//        Button btnEdit = bottomSheetView.findViewById(R.id.edit_btn);
//        Button btnDelete = bottomSheetView.findViewById(R.id.delete_btn);
//        Button btnDismiss = bottomSheetView.findViewById(R.id.dismiss_btn);
//
//        TextView tvReminderDesc = bottomSheetView.findViewById(R.id.txtReminderDesc);
//     //   tvReminderDesc.setText(getArguments().getString("remindername"));
//
//        btnEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                Toast.makeText(RemindersViewActivity.this,
//                        "Update", Toast.LENGTH_SHORT)
//                        .show();
//                bottomsheet.dismiss();
//            }
//        });
//
//        btnDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                Toast.makeText(RemindersViewActivity.this,
//                        "Delete", Toast.LENGTH_SHORT)
//                        .show();
//                bottomsheet.dismiss();
//            }
//        });
//        btnDismiss.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                Toast.makeText(RemindersViewActivity.this,
//                        "Dismiss", Toast.LENGTH_SHORT)
//                        .show();
//                bottomsheet.dismiss();
//            }
//        });
//

//        String responseAudioPath = getExternalFilesDir("/").getAbsolutePath() + "/" + "response_reminder_audio.ulaw";
//
//
//        new DownloadFileFromURL().execute(mReminders.get(position).getUrl());
//
//        MediaPlayer mediaPlayer = new MediaPlayer();
//
//        try {
//            mediaPlayer.setDataSource(responseAudioPath);
//            mediaPlayer.prepare();
//            mediaPlayer.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

//    @Override
//    protected Dialog onCreateDialog(int id) {
//        switch (id) {
//            case progress_bar_type: // we set this to 0
//                pDialog = new ProgressDialog(this);
//                pDialog.setMessage("Downloading file. Please wait...");
//                pDialog.setIndeterminate(false);
//                pDialog.setMax(100);
//                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//                pDialog.setCancelable(true);
//                pDialog.show();
//                return pDialog;
//            default:
//                return null;
//        }
//    }
//
//    class DownloadFileFromURL extends AsyncTask<String, String, String> {
//
//        /**
//         * Before starting background thread Show Progress Bar Dialog
//         * */
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            showDialog(progress_bar_type);
//        }
//
//        /**
//         * Downloading file in background thread
//         * */
//        @Override
//        protected String doInBackground(String... f_url) {
//            int count;
//            try {
//                URL url = new URL(f_url[0]);
//                URLConnection connection = url.openConnection();
//                connection.connect();
//
//                // this will be useful so that you can show a tipical 0-100%
//                // progress bar
//                int lenghtOfFile = connection.getContentLength();
//
//                // download the file
//                InputStream input = new BufferedInputStream(url.openStream(),
//                        8192);
//
//                // Output stream
//                OutputStream output = new FileOutputStream(getExternalFilesDir("/").getAbsolutePath() + "/" + "response_reminder_audio.ulaw");
//
//                byte data[] = new byte[1024];
//
//                long total = 0;
//
//                while ((count = input.read(data)) != -1) {
//                    total += count;
//                    // publishing the progress....
//                    // After this onProgressUpdate will be called
//                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
//
//                    // writing data to file
//                    output.write(data, 0, count);
//                }
//
//                // flushing output
//                output.flush();
//
//                // closing streams
//                output.close();
//                input.close();
//
//            } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
//            }
//
//            return null;
//        }
//
//        /**
//         * Updating progress bar
//         * */
//        protected void onProgressUpdate(String... progress) {
//            // setting progress percentage
//            pDialog.setProgress(Integer.parseInt(progress[0]));
//        }
//
//        /**
//         * After completing background task Dismiss the progress dialog
//         * **/
//        @Override
//        protected void onPostExecute(String file_url) {
//            // dismiss the dialog after the file was downloaded
//            dismissDialog(progress_bar_type);
//
//        }
//
//    }

    @Override
    protected void onResume(){
        super.onResume();

//        btnNew.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//             //   Intent reminderAdd1 = new Intent(getApplicationContext(),RemindersRecordAudioActivity.class);
////                intent.putExtra("mreminders", (Serializable) mReminders);
//         //       startActivity(reminderAdd1);
//            }
//        });
    }

    public void bottomNavigation(){

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        Menu menu = navigation.getMenu();
        MenuItem menuitem = menu.getItem(1);
        menuitem.setChecked(true);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.navigation_dashboard:
                        //  Toast.makeText(SelectToCall.this, "Dashboard clicked", Toast.LENGTH_SHORT).show();
                        Intent dashBoard = new Intent(RemindersViewActivity.this, Dashboard.class);
                        startActivity(dashBoard);
                        break;

                    case R.id.navigation_Reminders:
                        Intent reminder = new Intent(RemindersViewActivity.this, RemindersViewActivity.class);
                        //  Toast.makeText(SelectToCall.this, "Reminders clicked", Toast.LENGTH_SHORT).show();
                        startActivity(reminder);
                        RemindersViewActivity.this.finish();
                        break;

                    case R.id.navigation_notifications:
                        //  Toast.makeText(SelectToCall.this, "Notifications click", Toast.LENGTH_SHORT).show();
                        Intent notification = new Intent(RemindersViewActivity.this, Notification.class);
                        startActivity(notification);
                        break;

                    case R.id.navigation_settings:
                        //  Toast.makeText(SelectToCall.this, "Settings clicked", Toast.LENGTH_SHORT).show();
                        Intent settings = new Intent(RemindersViewActivity.this, Settings.class);
                        startActivity(settings);
                        break;
                }
                return true;
            }
        });
    }


    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
    // Back press not need to work
    @Override
    public void onBackPressed() {
        Intent dashBoard = new Intent(RemindersViewActivity.this, Dashboard.class);
        startActivity(dashBoard);
        Toast.makeText(RemindersViewActivity.this,"Please select from menu",Toast.LENGTH_LONG).show();
        return;
    }
}



//}
