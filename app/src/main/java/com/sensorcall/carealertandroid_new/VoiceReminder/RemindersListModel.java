package com.sensorcall.carealertandroid_new.VoiceReminder;



import java.util.ArrayList;

public class RemindersListModel {
//    private String mName;
//    private boolean mOnline;

    private String reminderType;
    private String reminderDesc;

//    public RemindersListModel(String name, boolean online) {
//        mName = name;
//        mOnline = online;
//    }

    public RemindersListModel(String Type, String Desc) {
        reminderType = Type;
        reminderDesc = Desc;
    }

    public String getReminderType() {
        return reminderType;
    }

    public String getReminderDesc() {
        return reminderDesc;
    }

//    public String getName() {
//        return mName;
//    }
//
//    public boolean isOnline() {
//        return mOnline;
//    }
//
//    private static int lastContactId = 0;

    public static ArrayList<RemindersListModel> remindersList(){
        ArrayList<RemindersListModel> reminders = new ArrayList<RemindersListModel>();

        reminders.add(new RemindersListModel("Medication Reminder","Statin - II at 8:00 AM Today"));
        reminders.add(new RemindersListModel("Message Reminder","Good morning Stacy at 8:30 AM Today "));
        reminders.add(new RemindersListModel("Medication Reminder","Asprin 5mg at 5:00 PM Today"));
        reminders.add(new RemindersListModel("Medication Reminder","Remdesivir at 5:00 PM Today"));
        reminders.add(new RemindersListModel("Message Reminder","Go for a walk at 6:00 PM Today"));
        reminders.add(new RemindersListModel("Message Reminder","TV show at 7:00 PM Today"));
        reminders.add(new RemindersListModel("Medication Reminder","Asprin 5mg at 8:00 PM Today"));

        return reminders;
    }

//    public static ArrayList<RemindersListModel> createContactsList(int numContacts) {
//        ArrayList<RemindersListModel> contacts = new ArrayList<RemindersListModel>();
//
//        for (int i = 1; i <= numContacts; i++) {
//            contacts.add(new RemindersListModel("Person " + ++lastContactId, i <= numContacts / 2));
//        }
//
//        return contacts;
//    }
}

