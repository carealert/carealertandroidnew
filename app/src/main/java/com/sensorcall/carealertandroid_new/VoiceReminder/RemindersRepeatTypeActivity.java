package com.sensorcall.carealertandroid_new.VoiceReminder;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.Emergencylightsetup;
import com.sensorcall.carealertandroid_new.GetLocation;
import com.sensorcall.carealertandroid_new.JsonPlaceHolderApi;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemindersRepeatTypeActivity extends AppCompatActivity {

    private static final String TAG = "RemindersRepeatTypeActivity";

    private LinearLayout btnSpecificReminder;
    private LinearLayout btnWeeklyReminder;
    String remindername;
    String reminderUrl;
    String reminderType;
    int locationid;
    int reminderid;
    ArrayList<Integer> specifictime;
    ArrayList<Integer> weekday;
    ArrayList<Integer> weektime;
    SharedPreferences CareAlert_SharedData;
    TextView loc_name;
    String loc;
    Retrofit retrofit;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    JsonPlaceHolder jsonplaceholder;
    String Auth_Token;
    ProgressDialog loaddialog,progressDialog;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_repeat_type);

        Intent recordAudio = getIntent();
        remindername = recordAudio.getStringExtra("ReminderName");
        reminderUrl = recordAudio.getStringExtra("url");
        reminderType = recordAudio.getStringExtra("reminderType");
        locationid = recordAudio.getIntExtra("LocationId",0);
        reminderid = recordAudio.getIntExtra("ReminderId",0);
        specifictime = recordAudio.getIntegerArrayListExtra("SpecificDate");
        weekday = recordAudio.getIntegerArrayListExtra("WeekDay");
        weektime = recordAudio.getIntegerArrayListExtra("WeekTime");
        loc = recordAudio.getStringExtra("LocationName");

        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();

        loc_name = (TextView) findViewById(R.id.location_name);
        loc_name.setText(loc);

        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Auth_Token = CareAlert_SharedData.getString("Auth_Token", "");
        jsonplaceholder = retrofit.create(JsonPlaceHolder.class);

        checkNetConnectivity();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            //setUpUI();
            getDefaultLocationId();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void setUpUI(){
        btnSpecificReminder = (LinearLayout) findViewById(R.id.rectangle_BG_2);
        btnWeeklyReminder = (LinearLayout) findViewById(R.id.rectangle_BG_1);

        btnSpecificReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(reminderid == 0) {
                    Intent reminderSpecificDate = new Intent(RemindersRepeatTypeActivity.this, RemindersSpecificDateActivity.class);
                    reminderSpecificDate.putExtra("ReminderName", remindername);
                    reminderSpecificDate.putExtra("url", reminderUrl);
                    reminderSpecificDate.putExtra("reminderType", reminderType);
                    reminderSpecificDate.putExtra("LocationId",locationid);
                    reminderSpecificDate.putExtra("LocationName",loc);
                    Log.d("recordrepeat", "recordrepeat: " + reminderUrl + " " + reminderType + " " + remindername);
                    startActivity(reminderSpecificDate);
                }
                else{
                    Intent reminderSpecificDate = new Intent(RemindersRepeatTypeActivity.this, RemindersSpecificDateActivity.class);
                    reminderSpecificDate.putExtra("ReminderName", remindername);
                    reminderSpecificDate.putExtra("url", reminderUrl);
                    reminderSpecificDate.putExtra("reminderType", reminderType);
                    reminderSpecificDate.putExtra("ReminderId",reminderid);
                    reminderSpecificDate.putExtra("LocationId",locationid);
                    reminderSpecificDate.putExtra("LocationName",loc);
                    reminderSpecificDate.putIntegerArrayListExtra("SpecificDate",specifictime);

                    Log.d("UpdateReminder","In ReminderRecord"+reminderid+" // "+remindername+" // "+reminderUrl+" // "+reminderType+" // "+locationid+" // "+specifictime);
                    startActivity(reminderSpecificDate);
                }
            }
        });

        btnWeeklyReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(reminderid == 0) {
                    Intent reminderWeek = new Intent(RemindersRepeatTypeActivity.this, RemindersRepeatWeeklyActivity.class);
                    reminderWeek.putExtra("ReminderName", remindername);
                    reminderWeek.putExtra("url", reminderUrl);
                    reminderWeek.putExtra("reminderType", reminderType);
                    reminderWeek.putExtra("LocationId",locationid);
                    reminderWeek.putExtra("LocationName",loc);
                    Log.d("recordrepeat", "recordrepeat: " + reminderUrl + " " + reminderType + " " + remindername);
                    startActivity(reminderWeek);
                }
                else{
                    Intent reminderWeek = new Intent(RemindersRepeatTypeActivity.this, RemindersRepeatWeeklyActivity.class);
                    reminderWeek.putExtra("ReminderName", remindername);
                    reminderWeek.putExtra("url", reminderUrl);
                    reminderWeek.putExtra("reminderType", reminderType);
                    reminderWeek.putExtra("ReminderId",reminderid);
                    reminderWeek.putExtra("LocationId",locationid);
                    reminderWeek.putIntegerArrayListExtra("WeekDay",weekday);
                    reminderWeek.putIntegerArrayListExtra("WeekTime",weektime);
                    reminderWeek.putExtra("LocationName",loc);
                    Log.d("UpdateReminder","In ReminderRecord"+reminderid+" // "+remindername+" // "+reminderUrl+" // "+reminderType+" // "+weekday+" // "+locationid+" // "+weektime);
                    startActivity(reminderWeek);
                }
            }
        });
    }

    private void getDefaultLocationId() {
        loaddialog.show();
        Log.d("default location", "into the location id after net connection");
        Call<List<GetLocation>> call = jsonplaceholder.getDefaultLocation("Bearer " + Auth_Token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if (response.isSuccessful()) {
                    Log.i("Succees", "onResponse: Yes");
                    List<GetLocation> get_list = response.body();
                    Log.d("Location Count", "onResponse: " + get_list.size());
                    if (get_list.isEmpty()) {
                        Toast.makeText(RemindersRepeatTypeActivity.this, "Default location is not set for this account", Toast.LENGTH_SHORT).show();
                        loaddialog.dismiss();
                    } else {
                        for (int i = 0; i < get_list.size(); i++) {
                            if (get_list.get(i).isDefaultLocationId()) {
                                loc = get_list.get(i).getLocationName();
                                loc_name.setText(loc);
                                setUpUI();
                                Log.d("Location Name", "onResponse: " + loc);
                                loaddialog.dismiss();
                            }

                            //Log.i("Location Mil Gayi", "Abc " + Integer.toString(locationId));

                        }
                    }
                } else {
                    Toast.makeText(RemindersRepeatTypeActivity.this, "LocationId Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.d("res error", "resuis: " + response.errorBody().toString());

                    loaddialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                Toast.makeText(RemindersRepeatTypeActivity.this, "No location set", Toast.LENGTH_SHORT).show();
                Log.d("failure12345", "error in fail");
                loaddialog.dismiss();
            }
        });
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    // Back press button
    @Override
    public void onBackPressed() {
        Intent reminderView = new Intent(RemindersRepeatTypeActivity.this, RemindersViewActivity.class);
        startActivity(reminderView);
    }
}