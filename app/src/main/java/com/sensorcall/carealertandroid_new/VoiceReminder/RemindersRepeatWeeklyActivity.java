package com.sensorcall.carealertandroid_new.VoiceReminder;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.HelperClass.AudioStream.AudioReminder;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RemindersRepeatWeeklyActivity extends AppCompatActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    private static final String TAG = "RemindersRepeatWeeklyActivity";

    private ToggleButton btnMonday;
    private ToggleButton btnTuesday;
    private ToggleButton btnWednesday;
    private ToggleButton btnThursday;
    private ToggleButton btnFriday;
    private ToggleButton btnSaturday;
    private ToggleButton btnSunday;

    private TimePicker timePicker1;

    private Button btnCreate;

    Boolean mon=false,tue=false,wen=false,thus=false,fri=false,sat=false,sun=false;

    SharedPreferences CareAlert_SharedData;
    int myHour, myMinute;
    int hour, minute;
    String auth;
    String name;
    int reminderid;
    String type;
    String url;
    int locationid;
    JsonArray weektime = new JsonArray();
    JsonPlaceHolder jsonPlaceHolder;
    ProgressDialog loader,progressDialog;
    ArrayList<Integer> weekDays = new ArrayList<Integer>();
    AudioReminder audioReminder;
    private Button btnSetDate;
    ArrayList<Integer> weekday;
    ArrayList<Integer> weektime1;
    LinearLayout listDateTime;
    int time;
    String loc;
    TextView loc_name;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_repeat_week);

        Intent recordAudio = getIntent();
        reminderid = recordAudio. getIntExtra("ReminderId",0);
        locationid = recordAudio.getIntExtra("LocationId",0);
        name = recordAudio.getStringExtra("ReminderName");
        url = recordAudio.getStringExtra("url");
        type = recordAudio.getStringExtra("reminderType");
        weekday = recordAudio.getIntegerArrayListExtra("WeekDay");
        weektime1 = recordAudio.getIntegerArrayListExtra("WeekTime");
        loc = recordAudio.getStringExtra("LocationName");
        loc_name = (TextView) findViewById(R.id.location_name);
        loc_name.setText(loc);
        Log.d("recordrepeat","recordrepeatWeekly: "+name+" "+url+" "+type);

        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData",0);
        loader = new ProgressDialog(RemindersRepeatWeeklyActivity.this);
        loader = showLoadingDialog(this);
        loader.dismiss();

        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolder = retrofit.create(JsonPlaceHolder.class);

//      Initialization of all the views
        btnMonday = (ToggleButton) findViewById(R.id.btnMonday);
        btnTuesday = (ToggleButton) findViewById(R.id.btnTuesday);
        btnWednesday = (ToggleButton) findViewById(R.id.btnWednesday);
        btnThursday = (ToggleButton) findViewById(R.id.btnThursday);
        btnFriday = (ToggleButton) findViewById(R.id.btnFriday);
        btnSaturday = (ToggleButton) findViewById(R.id.btnSaturday);
        btnSunday = (ToggleButton) findViewById(R.id.btnSunday);

        btnSetDate = (Button) findViewById(R.id.btnSetDate);

        if(reminderid!=0 && weekday.size()>0) {
            for (int t = 0; t < weekday.size(); t++) {
                switch (weekday.get(t)) {
                    case 1:
                        mon = true;
                        break;
                    case 2:
                        tue = true;
                        break;
                    case 3:
                        wen = true;
                        break;
                    case 4:
                        thus = true;
                        break;
                    case 5:
                        fri = true;
                        break;
                    case 6:
                        sat = true;
                        break;
                    case 0:
                        sun = true;
                        break;
                }
            }
        }

        auth = CareAlert_SharedData.getString("Auth_Token", "");

//      OnClick listeners for all the buttons
        btnMonday.setOnClickListener(this);
        btnTuesday.setOnClickListener(this);
        btnWednesday.setOnClickListener(this);
        btnThursday.setOnClickListener(this);
        btnFriday.setOnClickListener(this);
        btnSaturday.setOnClickListener(this);
        btnSunday.setOnClickListener(this);


        btnSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(weektime.size()==4){
                    Toast.makeText(RemindersRepeatWeeklyActivity.this, "Maximum 4 Reminders can be set", Toast.LENGTH_SHORT).show();
                }
                else {
                    Calendar c = Calendar.getInstance();
                    hour = c.get(Calendar.HOUR);
                    minute = c.get(Calendar.MINUTE);
                    //int am = c.get(Calendar.AM_PM);
                    TimePickerDialog timePickerDialog = new TimePickerDialog(RemindersRepeatWeeklyActivity.this, RemindersRepeatWeeklyActivity.this, hour, minute, false);
                    timePickerDialog.show();
                }
            }
        });
        checkNetConnectivity();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            setUpUI();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void setUpUI(){
        if(reminderid == 0) {
            btnCreate = (Button) findViewById(R.id.btnNext);
            btnCreate.setText("Create");
            btnCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(weektime.size() == 0 || weekDays.size() ==0) {
                        Toast.makeText(RemindersRepeatWeeklyActivity.this, "Weekday and Time Both are Required", Toast.LENGTH_SHORT).show();
                    }else {
                        createReminder();
//                            if (audioReminder != null) {
//                                audioReminder.release();
//                            }

                    }
                }
            });
        }
        else{
            weekDays = weekday;
            Log.d("weekday","Weekdays:Update reminder: "+weekDays);
            setWeekTime();
            btnCreate = (Button) findViewById(R.id.btnNext);
            btnCreate.setText("Update");
            btnCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(weektime.size() == 0 || weekDays.size() ==0) {
                        Toast.makeText(RemindersRepeatWeeklyActivity.this, "Weekday and Time Both are Required", Toast.LENGTH_SHORT).show();
                    }else {
                        updateReminder();

                    }
                }
            });
        }
    }
    public void createReminder(){
        if(weektime.size()==0 || weekDays.size()==0){
            Toast.makeText(this, "Weekday and Time Both are Required", Toast.LENGTH_SHORT).show();
        }
        else {
            loader.show();
            JsonObject apiBody = new JsonObject();
            apiBody.addProperty("Name", name);
            apiBody.addProperty("Type", type);
            apiBody.addProperty("Url", url);
            apiBody.addProperty("LocationId", locationid);
            apiBody.add("WeekTime", weektime);
            JsonArray weekDayJson = new JsonArray();
            for (int i = 0; i < weekDays.size(); i++) {
                weekDayJson.add(weekDays.get(i));
            }
            apiBody.add("WeekDay", weekDayJson);
            Log.d("AuthToken is", "" + auth);
            Call<GetReminder> call = jsonPlaceHolder.createReminder("Bearer " + auth, apiBody);
            call.enqueue(new Callback<GetReminder>() {
                @Override
                public void onResponse(Call<GetReminder> call, Response<GetReminder> response) {
                    if (response.isSuccessful()) {
                        String res = response.body().toString();
                        Log.d("Sucesss", "ResBody: " + res);
                        loader.dismiss();
                        Intent reminderView = new Intent(RemindersRepeatWeeklyActivity.this, RemindersViewActivity.class);
                        startActivity(reminderView);
                    } else {
                        Log.d("resUnsucces", "resNOt success");
                        loader.dismiss();
                        Toast.makeText(RemindersRepeatWeeklyActivity.this, "Failed To Create Reminder, Please Try Again", Toast.LENGTH_SHORT).show();
                        Intent reminderView = new Intent(RemindersRepeatWeeklyActivity.this, RemindersViewActivity.class);
                        startActivity(reminderView);
                    }
                }

                @Override
                public void onFailure(Call<GetReminder> call, Throwable t) {
                    Log.d("in failure", "fail");
                    loader.dismiss();
                    Toast.makeText(RemindersRepeatWeeklyActivity.this, "Failed To Create Reminder, Please check you internet connection and Try Again", Toast.LENGTH_SHORT).show();
                    Intent reminderView = new Intent(RemindersRepeatWeeklyActivity.this, RemindersViewActivity.class);
                    startActivity(reminderView);
                }
            });
        }
    }

    public void updateReminder(){
        if(weektime.size()==0 || weekDays.size()==0){
            Toast.makeText(this, "Weekday and Time Both are Required", Toast.LENGTH_SHORT).show();
        }
        else {
            loader.show();
            JsonObject apiBody = new JsonObject();
            apiBody.addProperty("ReminderId",reminderid);
            apiBody.addProperty("Name", name);
            apiBody.addProperty("Type", type);
            apiBody.addProperty("Url", url);
            apiBody.addProperty("LocationId", locationid);
            apiBody.add("WeekTime", weektime);
            JsonArray weekDayJson = new JsonArray();
            for (int i = 0; i < weekDays.size(); i++) {
                weekDayJson.add(weekDays.get(i));
            }
            apiBody.add("WeekDay", weekDayJson);
            Log.d("Auth Token is", "updateReminder: "+auth);
            Log.i("Update Body is", "updateReminder: "+apiBody.toString());
            Call<GetReminder> call = jsonPlaceHolder.updateReminder("Bearer " + auth, apiBody);
            call.enqueue(new Callback<GetReminder>() {
                @Override
                public void onResponse(Call<GetReminder> call, Response<GetReminder> response) {
                    if (response.isSuccessful()) {
                        String res = response.body().toString();
                        Log.d("Update Response", "ResBody:Update Success " + res);
                        loader.dismiss();
                        Intent reminderView = new Intent(RemindersRepeatWeeklyActivity.this, RemindersViewActivity.class);
                        startActivity(reminderView);
                    } else {
                        loader.dismiss();
                        Toast.makeText(RemindersRepeatWeeklyActivity.this, "Failed To Update Reminder, Please Try Again", Toast.LENGTH_SHORT).show();
                        Intent reminderView = new Intent(RemindersRepeatWeeklyActivity.this, RemindersViewActivity.class);
                        startActivity(reminderView);
                    }
                }

                @Override
                public void onFailure(Call<GetReminder> call, Throwable t) {
                    Log.d("Update failure", "fail");
                    loader.dismiss();
                    Toast.makeText(RemindersRepeatWeeklyActivity.this, "Failed To Update Reminder, Please Check you Internet Connection and Try Again ", Toast.LENGTH_SHORT).show();
                    Intent reminderView = new Intent(RemindersRepeatWeeklyActivity.this, RemindersViewActivity.class);
                    startActivity(reminderView);
                }
            });
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnMonday:
                if(!mon){
                    mon = true;
                    weekDays.add(1);
                }
                else
                {
                    int i;
                    for(i=0; i<weekDays.size();i++){
                        if(weekDays.get(i)==1){
                            break;
                        }
                    }
                    mon = false;
                    weekDays.remove(i);
                }
                break;
            case R.id.btnTuesday:
                if(!tue){
                    tue = true;
                    weekDays.add(2);
                }
                else
                {
                    int i;
                    for(i=0; i<weekDays.size();i++){
                        if(weekDays.get(i)==2){
                            break;
                        }
                    }
                    tue = false;
                    weekDays.remove(i);
                }
                break;
            case R.id.btnWednesday:
                if(!wen){
                    wen = true;
                    weekDays.add(3);
                }
                else
                {
                    int i;
                    for(i=0; i<weekDays.size();i++){
                        if(weekDays.get(i)==3){
                            break;
                        }
                    }
                    wen = false;
                    weekDays.remove(i);
                }
                break;
            case R.id.btnThursday:
                if(!thus){
                    thus = true;
                    weekDays.add(4);
                }
                else
                {
                    int i;
                    for(i=0; i<weekDays.size();i++){
                        if(weekDays.get(i)==4){
                            break;
                        }
                    }
                    thus = false;
                    weekDays.remove(i);
                }
                break;
            case R.id.btnFriday:
                if(!fri){
                    fri = true;
                    weekDays.add(5);
                }
                else
                {
                    int i;
                    for(i=0; i<weekDays.size();i++){
                        if(weekDays.get(i)==5){
                            break;
                        }
                    }
                    fri = false;
                    weekDays.remove(i);
                }
                break;
            case R.id.btnSaturday:
                if(!sat){
                    sat = true;
                    weekDays.add(6);
                }
                else
                {
                    int i;
                    for(i=0; i<weekDays.size();i++){
                        if(weekDays.get(i)==6){
                            break;
                        }
                    }
                    sat = false;
                    weekDays.remove(i);
                }
                break;
            case R.id.btnSunday:
                if(!sun){
                    sun = true;
                    weekDays.add(0);
                }
                else
                {
                    int i;
                    for(i=0; i<weekDays.size();i++){
                        if(weekDays.get(i)==0){
                            break;
                        }
                    }
                    sun = false;
                    weekDays.remove(i);
                }
                break;
            case R.id.btnNext:

                break;
            default:
                break;
        }
    }


    public void onTimeSet(TimePicker timePicker, int h, int t) {
        myHour = h;
        myMinute = t;

//        LinearLayout layout= (LinearLayout) findViewById(R.id.textview_time);
//        TextView dateTime = new TextView(this);
//        LinearLayout.LayoutParams layoutParams =new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        dateTime.setLayoutParams(layoutParams);
//        layoutParams.setMargins(40,10,20,5);
//        dateTime.setText(myHour+":"+myMinute);
//        dateTime.setTextSize(16);
//        dateTime.setBackgroundColor(Color.WHITE);
//        layout.addView(dateTime);

        listDateTime =(LinearLayout) findViewById(R.id.textview_time);
        View dateTimeList = getLayoutInflater().inflate(R.layout.row_add_specifictime,null,false);
        String Time="";
        if(myHour>11){
            String HourString=Integer.toString(myHour-12);
            if  (HourString.equals("0")){
                HourString="12";
            }
            if(myMinute<10){
                Time= HourString+":0"+Integer.toString(myMinute)+" PM";
            }
            else
            {
                Time= HourString+":"+Integer.toString(myMinute)+" PM";
            }
        }
        else{
            String HourString=Integer.toString(myHour);
            if  (HourString.equals("0")){
                HourString="12";
            }
            if(myMinute<10){
                Time= HourString+":0"+Integer.toString(myMinute)+" AM";
            }
            else
            {
                Time= HourString+":"+Integer.toString(myMinute)+" AM";
            }

        }
        TextView speciDate = (TextView) dateTimeList.findViewById(R.id.time);
        speciDate.setBackgroundColor(Color.parseColor("#ffffff"));
        speciDate.setText(Time);
        speciDate.setTextSize(18);
        ImageView removeTime = (ImageView) dateTimeList.findViewById(R.id.remove_time);

        removeTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i=0;i<weektime.size();i++){
                    if(weektime.get(i).getAsInt()==time) {
                        weektime.remove(i);
                        break;
                    }
                }
                removeTime(dateTimeList);
            }
        });

        listDateTime.addView(dateTimeList);
        int Hour = myHour*3600;
        int Min = myMinute*60;
        time = Hour+Min;
        weektime.add(time);
        Log.d("WeekTime","WeekTime: "+weektime);
//        Time time = new Time(calendar);
//        Date date = new Date(calendar);
//        try {
//            epoch = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(calendar).getTime() / 1000;
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        specifictime.put(epoch);
//        Log.d("DateFormat","dateFormat "+date);
//        Log.d("DateFormat","dateFormat "+epoch);
//        Log.d("TimeDate","TimeDate:"+calendar);
    }


    //Predefined Week and Time setting
    public void setWeekTime(){
        int week = 7;
        for(int i=0; i<weekday.size(); i++) {
            week = weekday.get(i);
            switch (week) {
                case 1:
                    btnMonday.setChecked(true);
                    break;
                case 2:
                    btnTuesday.setChecked(true);
                    break;
                case 3:
                    btnWednesday.setChecked(true);
                    break;
                case 4:
                    btnThursday.setChecked(true);
                    break;
                case 5:
                    btnFriday.setChecked(true);
                    break;
                case 6:
                    btnSaturday.setChecked(true);
                    break;
                case 0:
                    btnSunday.setChecked(true);
                    break;
                case 7:
                    Toast.makeText(this, "No day has been selected", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
        for(int i=0; i<weektime1.size(); i++){
            int time = weektime1.get(i);
            float hour = time/3600;
            int Hour = (int) hour;
            Log.d("Time->Hour is", "setWeekTime: "+hour);
            Log.d("Hour", "setWeekTime: "+Hour);
            int min = time%3600;
            Log.d("Time->Min are", "setWeekTime: "+min);
            min = min/60;
            String Time="";
            if(Hour>11){
                String HourString=Integer.toString(Hour-12);
                if  (HourString.equals("0")){
                    HourString="12";
                }
                if(min<10){
                    Time= HourString+":0"+Integer.toString(min)+" PM";
                }
                else
                {
                    Time= HourString+":"+Integer.toString(min)+" PM";
                }
            }
            else {
                String HourString = Integer.toString(Hour);
                if (HourString.equals("0")) {
                    HourString = "12";
                }
                if (min < 10) {
                    Time = HourString + ":0" + Integer.toString(min) + " AM";
                } else {
                    Time = HourString + ":" + Integer.toString(min) + " AM";
                }
            }

            int Min = (int) min;
            Log.d("Hour", "setWeekTime: "+Min);
            weektime.add(weektime1.get(i));
            listDateTime =(LinearLayout) findViewById(R.id.textview_time);
            View dateTimeList = getLayoutInflater().inflate(R.layout.row_add_specifictime,null,false);

            TextView speciDate = (TextView) dateTimeList.findViewById(R.id.time);
            speciDate.setBackgroundColor(Color.parseColor("#ffffff"));
            speciDate.setText(Time);
            speciDate.setTextSize(18);
            ImageView removeTime = (ImageView) dateTimeList.findViewById(R.id.remove_time);

            removeTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(int i=0;i<weektime.size();i++){
                        if(weektime.get(i).getAsInt()==time) {
                            weektime.remove(i);
                            break;
                        }
                    }
                    removeTime(dateTimeList);
                }
            });

            listDateTime.addView(dateTimeList);
        }
    }

    private void removeTime(View view){
        listDateTime.removeView(view);
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
    // Back press button
    @Override
    public void onBackPressed() {
        Intent reminderView = new Intent(RemindersRepeatWeeklyActivity.this, RemindersViewActivity.class);
        startActivity(reminderView);
    }
}
