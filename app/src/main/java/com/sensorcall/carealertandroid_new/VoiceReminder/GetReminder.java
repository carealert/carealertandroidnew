package com.sensorcall.carealertandroid_new.VoiceReminder;

import java.util.List;
import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetReminder implements Serializable{
    private String AuthToken;

    public String getAuthToken() {
        return AuthToken;
    }

    @SerializedName("ReminderId")
    @Expose
    private Integer reminderId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("uploadUrl")
    @Expose
    private String url;
    @SerializedName("Url")
    @Expose
    private String downurl;
    @SerializedName("LocationId")
    @Expose
    private Integer locationId;
    @SerializedName("SpecificTime")
    @Expose
    private List<Integer> specificTime = null;
    @SerializedName("WeekDay")
    @Expose
    private List<Integer> weekDay = null;
    @SerializedName("WeekTime")
    @Expose
    private List<Integer> weekTime = null;

    private final static long serialVersionUID = -49692438208095968L;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetReminder() {
    }

    /**
     *
     * @param reminderId
     * @param specificTime
     * @param locationId
     * @param weekDay
     * @param weekTime
     * @param name
     * @param type
     * @param url
     */
    public GetReminder(Integer reminderId, String name, String type, String url,String downUrl, Integer locationId, List<Integer> specificTime, List<Integer> weekDay, List<Integer> weekTime) {
        super();
        this.reminderId = reminderId;
        this.name = name;
        this.type = type;
        this.url = url;
        this.downurl = downUrl;
        this.locationId = locationId;
        this.specificTime = specificTime;
        this.weekDay = weekDay;
        this.weekTime = weekTime;
    }

    public Integer getReminderId() {
        return reminderId;
    }

    public void setReminderId(Integer reminderId) {
        this.reminderId = reminderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDownurl(){
        return downurl;
    }

    public String getUrl() {
        return url;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public List<Integer> getSpecificTime() {
        return specificTime;
    }

    public void setSpecificTime(List<Integer> specificTime) {
        this.specificTime = specificTime;
    }

    public List<Integer> getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(List<Integer> weekDay) {
        this.weekDay = weekDay;
    }

    public List<Integer> getWeekTime() {
        return weekTime;
    }

    public void setWeekTime(List<Integer> weekTime) {
        this.weekTime = weekTime;
    }

}



