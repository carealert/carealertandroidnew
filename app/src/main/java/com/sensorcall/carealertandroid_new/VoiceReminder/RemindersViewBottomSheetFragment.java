package com.sensorcall.carealertandroid_new.VoiceReminder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.GetLocation;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

public class RemindersViewBottomSheetFragment extends BottomSheetDialogFragment{

    private JsonPlaceHolder jsonPlaceHolderApi;
    String auth;
    String name, loc_name;
    int reminderid;
    String type;
    String url;
    int locationid = 9;
    List<Integer> specifictime = new ArrayList<Integer>();
    List<Integer> weekday = new ArrayList<Integer>();
    List<Integer> weektime = new ArrayList<Integer>();
    public RemindersViewActivity activity;
    ProgressDialog loaddialog,progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.bottom_sheet_reminders, container, false);
        loaddialog=new ProgressDialog(getActivity());
        loaddialog=showLoadingDialog(getActivity());
        loaddialog.dismiss();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolder.class);

        Button btnEdit = v.findViewById(R.id.edit_btn);
        Button btnDelete = v.findViewById(R.id.delete_btn);
        Button btnDismiss = v.findViewById(R.id.dismiss_btn);

        TextView tvReminderDesc = v.findViewById(R.id.txtReminderDesc);
        tvReminderDesc.setText(getArguments().getString("remindername"));

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                loaddialog.show();
                Intent updateReminder = new Intent(getActivity(),RemindersRecordAudioActivity.class);
                updateReminder.putExtra("ReminderName",name);
                updateReminder.putExtra("ReminderId",reminderid);
                updateReminder.putExtra("LocationId",locationid);
                updateReminder.putExtra("url",url);
                updateReminder.putExtra("RemType",type);
                updateReminder.putIntegerArrayListExtra("WeekDay",(ArrayList<Integer>) weekday);
                updateReminder.putIntegerArrayListExtra("WeekTime",(ArrayList<Integer>) weektime);
                updateReminder.putIntegerArrayListExtra("SpecificDate",(ArrayList<Integer>) specifictime);
                updateReminder.putExtra("LocationName",loc_name);
                Log.d("UpdateReminder","In update reminder button"+name);
                startActivity(updateReminder);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Context context = getContext();
                Call<GetReminder> call = jsonPlaceHolderApi.deleteReminder("Bearer "+auth,reminderid);
                Activity reminderAbc = getActivity();
                call.enqueue(new Callback<GetReminder>() {
                    @Override
                    public void onResponse(Call<GetReminder> call, Response<GetReminder> response) {
                        if(response.isSuccessful()) {
                            Intent remiderView = new Intent(context, RemindersViewActivity.class);
                            context.startActivity(remiderView);
                            Toast.makeText(reminderAbc,
                                    "Reminder deleted", Toast.LENGTH_SHORT)
                                    .show();
                        }
                        else{
                            Toast.makeText(reminderAbc,
                                    "Fail to delete reminder", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                    @Override
                    public void onFailure(Call<GetReminder> call, Throwable t) {
                        Toast.makeText(getActivity(),
                                "Fail to delete reminder", Toast.LENGTH_SHORT)
                                .show();
                        Log.d("response","responsebody:Failure ");
                    }
                });
                dismiss();
            }
        });
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(getActivity(),
                        "Dismiss", Toast.LENGTH_SHORT)
                        .show();
                dismiss();
            }
        });
        return v;
    }

    public void getAuthToken(String authToken, String naMe, int reminderId, String tyPe, String urL, int locationId, List<Integer> specificTime, List<Integer> weekDay, List<Integer> weekTime, String locationName){

        this.name = naMe;
        Log.d("Name", "ReminderName: "+name);
        this.reminderid = reminderId;
        Log.d("Name", "ReminderName: "+reminderid);
        this.auth = authToken;
        Log.d("Name", "ReminderName: "+auth);
        this.type = tyPe;
        Log.d("Name", "ReminderName: "+type);
        this.url = urL;
        Log.d("Name", "ReminderName: "+url);
        this.locationid = locationId;
        Log.d("Name", "ReminderName: "+locationid);
        this.specifictime = specificTime;
        Log.d("Name", "ReminderName: "+specifictime);
        this.weekday = weekDay;
        Log.d("Name", "ReminderName: "+weekday);
        this.weektime = weekTime;
        Log.d("Name", "ReminderName: "+weektime);
        this.loc_name = locationName;

        Log.d("abc","authToken1234:"+auth);
        Log.d("abc","authToken1234:"+reminderid);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (RemindersViewActivity) activity;
    }
    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}


