package com.sensorcall.carealertandroid_new;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.ui.email.AccountSettingsActivity;
import com.sensorcall.carealertandroid_new.ui.email.Email;
import com.sensorcall.carealertandroid_new.ui.email.UpdateUserRequest;
import com.sensorcall.carealertandroid_new.ui.lovedones.EditLovedOnesActivity;
import com.sensorcall.carealertandroid_new.util.RetrofitInstance;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Enteremail extends AppCompatActivity {
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    private String email_id;
    private EditText email_input;
    ProgressDialog loaddialog, progressDialog;
    Button Continue_email;
    private Email email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enteremail);
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
        CareAlert_SharedData_Editor = CareAlert_SharedData.edit();
        email_input = (EditText) findViewById(R.id.email_input);
        loaddialog = new ProgressDialog(this);
        loaddialog = showLoadingDialog(this);
        loaddialog.dismiss();
        Continue_email = (Button) findViewById(R.id.enteremail_continue);
        checkNetConnectivity();

        email = getIntent().getParcelableExtra(Constants.EMAIL);

        if (email != null) {
            TextView txtHeader = (TextView) findViewById(R.id.enterotp_msg);
            txtHeader.setText(R.string.update_email);
            email_input.setText(email.getUserEmail());
        }
    }

    void checkNetConnectivity() {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet", "devic123");
            setUi();
        } else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog() {
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void setUi() {
        email_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                email_id = email_input.getText().toString();

            }
        });
        Continue_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                Continue_email.setEnabled(false);
                String exp = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
                if (email_id.matches(exp) && email_id.length() > 1) {
                    CareAlert_SharedData_Editor.putString("Email", email_id);
                    CareAlert_SharedData_Editor.commit();
                    if (email != null) {
                        updateEmail();
                    } else {
                        openEnterPasswordPage();
                    }
                } else {
                    email_input.setText("");
                    Toast.makeText(Enteremail.this, "Not a valid Email Id", Toast.LENGTH_SHORT).show();
                    Continue_email.setEnabled(true);
                    loaddialog.dismiss();
                }
            }
        });

    }

    private void updateEmail() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.email = email_id;
        Call<ResponseBody> call = RetrofitInstance.getRetrofitInstance(getApplicationContext()).updateUser(updateUserRequest);

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(Enteremail.this, R.string.update_failed, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    showSuccessDialog();

                }
                loaddialog.dismiss();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Enteremail.this, "Failed", Toast.LENGTH_SHORT).show();
                loaddialog.dismiss();

            }
        });

    }

    private void showSuccessDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.email_updated_success)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    Intent editLovedOnesActivity = new Intent(this, AccountSettingsActivity.class);
                    editLovedOnesActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    ;
                    startActivity(editLovedOnesActivity);
                })
                .show();
    }

    public void openEnterOtpPage() {
        Intent openotppage = new Intent(this, Enterotp.class);
        Continue_email.setEnabled(true);
        loaddialog.dismiss();
        startActivity(openotppage);
    }

    public void openEnterPasswordPage() {
        Intent enterpasswordpage = new Intent(this, Enterpassword.class);
        Continue_email.setEnabled(true);
        loaddialog.dismiss();
        startActivity(enterpasswordpage);

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @Override
    public void onBackPressed() {
        if(email==null) {
            Intent loginpage = new Intent(Enteremail.this, LoginScreen.class);
            loaddialog.dismiss();
            startActivity(loginpage);
        } else {
            finish();
        }
    }
}