package com.sensorcall.carealertandroid_new;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Support extends AppCompatActivity {
    TextView email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        email=(TextView) findViewById(R.id.email_address);
        checkNetConnectivity();
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            setUpUi();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }
    public void setUpUi(){
        email.setText("support@sensorscall.com");
    }

    @Override
    public void onBackPressed() {
        Intent settings = new Intent(Support.this,Settings.class);
        startActivity(settings);
    }
}

