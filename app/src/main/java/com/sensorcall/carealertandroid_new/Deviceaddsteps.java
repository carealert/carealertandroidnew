package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.location.LocationManagerCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class Deviceaddsteps extends AppCompatActivity {

    private static final int RECOVERY_REQUEST = 2;
    private YouTubePlayerFragment mVideoView;

    Button continue_button,turn_on_button,edit_address;
    TextView locationdisplay, getlocation, test;
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;

    public String Location = "Unknown Location";
    ProgressDialog loaddialog, progressDialog;
    LocationManager mLocationManager;
    GpsTracker gpsTracker;
    boolean AddedviaSignupProcess = true;
    public static int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    boolean getLocationbool = false;
    private LocationRequest mLocationRequest;
    public static final int REQUEST_CHECK_SETTING = 1001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deviceaddsteps);
        mVideoView=(YouTubePlayerFragment) getFragmentManager()
                .findFragmentById(R.id.unboxing_video);
        locationdisplay = (TextView) findViewById(R.id.location_disp);
        turn_on_button =(Button)findViewById(R.id.get_location);
        edit_address =(Button)findViewById(R.id.edit_address);
        //getlocation = (TextView) findViewById(R.id.get_location);
        continue_button = (Button) findViewById(R.id.deviceadd_continue);
        gpsTracker = new GpsTracker(Deviceaddsteps.this);
        mLocationRequest = new LocationRequest();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        /*String getlocationstring = "Turn Location Service On";
        SpannableString getlocationspan = new SpannableString(getlocationstring);
        ClickableSpan span = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }

            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };*/
        loaddialog = new ProgressDialog(this);
        loaddialog = showLoadingDialog(this);
        loaddialog.dismiss();
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
        CareAlert_SharedData_Editor = CareAlert_SharedData.edit();
        AddedviaSignupProcess = CareAlert_SharedData.getBoolean("AddedviaSignupProcess", true);

        //CareAlert_SharedData_Editor.putString("Auth_Token","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIrOTE3MjkxOTQ4MDgzIiwianRpIjoiODA1MzRmZDAtYzdhMC00NWZjLWI5ZTMtOTRjNGFiOTYzNWI2IiwiaWF0IjoxNjAyNjE0MTcxLCJyb2xlIjoiYXBpX2FjY2VzcyIsImlkIjoiNjVkNTY1NGMtY2YyNS00ZGMzLWI3ZjItM2RjMTVhZGJlNGQ5IiwibmJmIjoxNjAyNjE0MTcxLCJleHAiOjE2MDMyMTg5NzEsImlzcyI6IndlYkFwaSIsImF1ZCI6Imh0dHA6Ly9jYXJlYWxlcnRkZXYuYXp1cmV3ZWJzaXRlcy5uZXQvIn0.CU_h24Bg-z6sdiuzJcaYXgS-pv4PfRcr6ZG9ly0m7tY");
        //CareAlert_SharedData_Editor.commit();

        /*String getlocationdispstring = "Turn Location Service On";
        SpannableString getlocation_span = new SpannableString(getlocationstring);
        ClickableSpan clickspan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                getLocationbool = true;
                //    fncgetlocation();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        getlocation_span.setSpan(clickspan, 0, 24, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        getlocation.setText(getlocation_span);
        getlocation.setMovementMethod(LinkMovementMethod.getInstance());*/
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        turn_on_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //code
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                getLocationbool = true;
            }
        });

        edit_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //code
                locationdisplay.setCursorVisible(true);
                locationdisplay.setFocusableInTouchMode(true);
                locationdisplay.requestFocus();
                locationdisplay.setEnabled(true);
                locationdisplay.setBackgroundColor(Color.WHITE);
            }
        });
        Log.i("Entering Location fnc", "onCreate: ");
        if (!gpsTracker.canGetLocation()) {
            //gpsTracker.showSettingsAlert();
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        }
        if (!isLocationEnabled(this)) {
            Toast.makeText(Deviceaddsteps.this, "Location Services are Turned OFF, Please Turn them on", Toast.LENGTH_SHORT).show();
        } else {
            turn_on_button.setVisibility(View.INVISIBLE);
          //  getlocation.setVisibility(View.INVISIBLE);
        }
        fncgetlocation();

        checkNetConnectivity();

/*
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Location=="Not Found"){
                    continue_button.setEnabled(false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(Deviceaddsteps.this);
                    builder.setMessage("Unable to Find Location Data, Press Ok to Refresh")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    continue_button.setEnabled(true);
                                    Intent refresh= new Intent(Deviceaddsteps.this,Deviceaddsteps.class);
                                    startActivity(refresh);

                                }
                            });
                    // Create the AlertDialog object and return it
                    builder.create();
                    builder.show();
                }
                else {
                    loaddialog.show();
                    continue_button.setEnabled(false);
                    opennamelocationpage();
                }
            }
        });

        */

    }

    void checkNetConnectivity() {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet", "devic123");
            loadLocation();
        } else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog() {
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final androidx.appcompat.app.AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    public void loadLocation() {
        setupyoutubevideo();
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Location == "Not Found") {
                    continue_button.setEnabled(false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(Deviceaddsteps.this);
                    builder.setMessage("Unable to Find Location Data, Press Ok to Refresh")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    continue_button.setEnabled(true);
                                    Intent refresh = new Intent(Deviceaddsteps.this, Deviceaddsteps.class);
                                    startActivity(refresh);
                                }
                            });
                    // Create the AlertDialog object and return it
                    builder.create();
                    builder.show();
                } else {
                    loaddialog.show();
                    continue_button.setEnabled(false);
                    CareAlert_SharedData_Editor.putString("GeoLocationData", String.valueOf(locationdisplay.getText()));
                    CareAlert_SharedData_Editor.commit();
                    opennamelocationpage();
                }
            }
        });
    }

    public void setupyoutubevideo() {
        mVideoView.initialize("AIzaSyD2eykRREReql8UPSfCAh0GrzqnzowlLVo", new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    youTubePlayer.cueVideo("UMJz4cBLsZ4"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                if (youTubeInitializationResult.isUserRecoverableError()) {
                    youTubeInitializationResult.getErrorDialog(Deviceaddsteps.this, RECOVERY_REQUEST).show();
                } else {
                    String error = String.format(getString(R.string.player_error), youTubeInitializationResult.toString());
                    Toast.makeText(Deviceaddsteps.this, error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        Log.d("DID We Enter?", "onRequestPermissionsResult: Yes" + "requestCode is " + requestCode);
        switch (requestCode) {
            case 101: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent Restart = new Intent(this, Deviceaddsteps.class);
                    startActivity(Restart);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    public void opennamelocationpage() {
        Intent namelocationpage = new Intent(this, Namelocation.class);
        continue_button.setEnabled(true);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(namelocationpage);
    }

    public void openservicesetupstepspage() {
        Intent servicesetuppage = new Intent(this, Servicesetupsteps.class);
        continue_button.setEnabled(true);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(servicesetuppage);
    }

//    BroadcastReceiver LocationListner= new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action= intent.getAction();
//            if(action.equals(LocationRequest.))
//        }
//    };
    public void fncgetlocation(){
        Log.d("Called Count", "fncgetlocation: ");
        double latitude = gpsTracker.getLatitude();
        double longitude = gpsTracker.getLongitude();
        findlocation(latitude,longitude);

    }
    public void findlocation(double latitude_par,double longitude_par) {

        Log.d("Called Count", "fncgetlocation: ");
        double latitude = latitude_par;
        double longitude =longitude_par;
        //locationdisplay.setText(Double.toString(latitude));
        CareAlert_SharedData_Editor.putString("Latitude", Double.toString(latitude));
        CareAlert_SharedData_Editor.putString("Longitude", Double.toString(longitude));
        Log.d("Lat and Long", "fncgetlocation: " + Double.toString(latitude) + Double.toString(longitude));
        CareAlert_SharedData_Editor.commit();
        Geocoder geocoder = new Geocoder(Deviceaddsteps.this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(
                    latitude, longitude, 1);
        } catch (IOException ioException) {
            addresses = null;
        }
        Log.d("Address is", "fncgetlocation: " + addresses);
        if (addresses == null || addresses.size() == 0) {
            Location = "Not Found";
            edit_address.setVisibility(View.INVISIBLE);
        } else {
            Location = addresses.get(0).getAddressLine(0);

        }
        locationdisplay.setText(Location);
        CareAlert_SharedData_Editor.putString("GeoLocationData", String.valueOf(locationdisplay.getText()));
        CareAlert_SharedData_Editor.commit();
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public void onBackPressed() {
        if (!AddedviaSignupProcess) {
            CareAlert_SharedData_Editor.putBoolean("AddedviaSignupProcess", true);
            CareAlert_SharedData_Editor.commit();
            Intent manageDevice = new Intent(Deviceaddsteps.this, AddLocation.class);
            manageDevice.putExtra("DefaultLocationId", CareAlert_SharedData.getInt("LocationId", -1));
            manageDevice.putExtra("locname", CareAlert_SharedData.getString("locname", ""));
            manageDevice.putExtra("locadd", CareAlert_SharedData.getString("locadd", ""));
            startActivity(manageDevice);
        } else {
            Intent namedevice = new Intent(Deviceaddsteps.this, Servicesetupsteps.class);
            startActivity(namedevice);
        }
    }

    private boolean isLocationEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return LocationManagerCompat.isLocationEnabled(locationManager);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (getLocationbool) {
            gpsTracker = new GpsTracker(Deviceaddsteps.this);
            if (gpsTracker.canGetLocation == true) {
                turn_on_button.setVisibility(View.INVISIBLE);
            }
            getLocationbool = false;
            getCurrentLocation();

        }
    }

    public void getCurrentLocation() {
        LocationManager locationManager;
        String context = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) getSystemService(context);
        Criteria crta = new Criteria();
        crta.setAccuracy(Criteria.ACCURACY_FINE);
        crta.setAltitudeRequired(false);
        crta.setBearingRequired(false);
        crta.setCostAllowed(true);
        crta.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(crta, true);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(provider, 1000, 0,
                new LocationListener() {
                    @Override
                    public void onStatusChanged(String provider, int status,
                                                Bundle extras) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                    }

                    @Override
                    public void onLocationChanged(Location location) {
                        if (location != null) {
                            double lat = location.getLatitude();
                            double lng = location.getLongitude();
                            if (lat != 0.0 && lng != 0.0) {
                                findlocation(lat,lng);
                            }
                        }

                    }

                });
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (requestCode == RECOVERY_REQUEST) {
//            // Retry initialization if user performed a recovery action
//            setupyoutubevideo();
//        }
//    }


}


