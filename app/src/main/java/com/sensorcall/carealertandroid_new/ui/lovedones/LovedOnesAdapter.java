package com.sensorcall.carealertandroid_new.ui.lovedones;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.databinding.LovedOneRowBinding;

import java.util.List;

public class LovedOnesAdapter
        extends RecyclerView.Adapter<LovedOnesAdapter.LovedOnesViewHolder> {
    private List<LovedOne> lovedOnes;
    private LovedOnesAdapterListener lovedOnesAdapterListener;

    public LovedOnesAdapter(LovedOnesAdapterListener lovedOnesAdapterListener) {
        this.lovedOnesAdapterListener = lovedOnesAdapterListener;
    }

    interface LovedOnesAdapterListener {
        void onLovedOneMoreClick(LovedOne lovedOne);
    }

    @NonNull
    @Override
    public LovedOnesAdapter.LovedOnesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LovedOneRowBinding lovedOneRowBinding =
                DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.loved_one_row, viewGroup, false);
        return new LovedOnesViewHolder(lovedOneRowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull LovedOnesAdapter.LovedOnesViewHolder lovedOnesViewHolder, int i) {
        LovedOne lovedOne = lovedOnes.get(i);
        lovedOnesViewHolder.lovedOneRowBinding.txtLovedOneProfileName.setText(lovedOne.getProfileName());
        lovedOnesViewHolder.lovedOneRowBinding.getRoot().setOnClickListener(view -> {
            lovedOnesAdapterListener.onLovedOneMoreClick(lovedOnes.get(lovedOnesViewHolder.getAdapterPosition()));
        });


    }

    @Override
    public int getItemCount() {
        if (lovedOnes != null) {
            return lovedOnes.size();
        } else {
            return 0;
        }
    }

    public void setLovedOneList(List<LovedOne> lovedOnes) {
        this.lovedOnes = lovedOnes;
        notifyDataSetChanged();
    }

    class LovedOnesViewHolder extends RecyclerView.ViewHolder {
        private LovedOneRowBinding lovedOneRowBinding;

        public LovedOnesViewHolder(@NonNull LovedOneRowBinding lovedOneRowBinding) {
            super(lovedOneRowBinding.getRoot());
            this.lovedOneRowBinding = lovedOneRowBinding;
        }
    }
}
