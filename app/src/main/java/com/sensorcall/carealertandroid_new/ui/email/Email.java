package com.sensorcall.carealertandroid_new.ui.email;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Email implements Parcelable {

    @SerializedName("userEmail")
    @Expose
    private String userEmail;
    @SerializedName("emailConfirmation")
    @Expose
    private boolean emailConfirmation;

    protected Email(Parcel in) {
        userEmail = in.readString();
        emailConfirmation = in.readByte() != 0;
    }


    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public boolean isEmailConfirmation() {
        return emailConfirmation;
    }

    public void setEmailConfirmation(boolean emailConfirmation) {
        this.emailConfirmation = emailConfirmation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userEmail);
        dest.writeByte((byte) (emailConfirmation ? 1 : 0));
    }

    public static final Creator<Email> CREATOR = new Creator<Email>() {
        @Override
        public Email createFromParcel(Parcel in) {
            return new Email(in);
        }

        @Override
        public Email[] newArray(int size) {
            return new Email[size];
        }
    };
}
