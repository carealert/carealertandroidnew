package com.sensorcall.carealertandroid_new.ui.lovedones;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LovedOne implements Parcelable {
    @SerializedName("LovedOnesId")
    @Expose
    private int lovedOnesId;
    @SerializedName("LovedOneProfilePicture")
    @Expose
    private String lovedOneProfilePicture;
    @SerializedName("Age")
    @Expose
    private int age;
    @SerializedName("ProfileName")
    @Expose
    private String profileName;
    @SerializedName("LocationNickName")
    @Expose
    private Object locationNickName;
    @SerializedName("LocationId")
    @Expose
    private int locationId;
    @SerializedName("IsPrimary")
    @Expose
    private boolean isPrimary;

    protected LovedOne(Parcel in) {
        lovedOnesId = in.readInt();
        lovedOneProfilePicture = in.readString();
        age = in.readInt();
        profileName = in.readString();
        locationId = in.readInt();
        isPrimary = in.readByte() != 0;
    }

    public static final Creator<LovedOne> CREATOR = new Creator<LovedOne>() {
        @Override
        public LovedOne createFromParcel(Parcel in) {
            return new LovedOne(in);
        }

        @Override
        public LovedOne[] newArray(int size) {
            return new LovedOne[size];
        }
    };

    public int getLovedOnesId() {
        return lovedOnesId;
    }

    public void setLovedOnesId(int lovedOnesId) {
        this.lovedOnesId = lovedOnesId;
    }

    public String getLovedOneProfilePicture() {
        return lovedOneProfilePicture;
    }

    public void setLovedOneProfilePicture(String lovedOneProfilePicture) {
        this.lovedOneProfilePicture = lovedOneProfilePicture;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public Object getLocationNickName() {
        return locationNickName;
    }

    public void setLocationNickName(Object locationNickName) {
        this.locationNickName = locationNickName;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(lovedOnesId);
        dest.writeString(lovedOneProfilePicture);
        dest.writeInt(age);
        dest.writeString(profileName);
        dest.writeInt(locationId);
        dest.writeByte((byte) (isPrimary ? 1 : 0));
    }
}