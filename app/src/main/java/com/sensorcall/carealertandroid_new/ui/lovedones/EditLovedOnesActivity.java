package com.sensorcall.carealertandroid_new.ui.lovedones;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.Selectage;
import com.sensorcall.carealertandroid_new.Settings;
import com.sensorcall.carealertandroid_new.Wifipage;
import com.sensorcall.carealertandroid_new.databinding.ActivityEditLovedOnesBinding;
import com.sensorcall.carealertandroid_new.databinding.BottomsheetEditLovedOnesBinding;
import com.sensorcall.carealertandroid_new.ui.email.AccountSettingsActivity;
import com.sensorcall.carealertandroid_new.util.ProgressDialogUtil;
import com.sensorcall.carealertandroid_new.util.RetrofitInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditLovedOnesActivity extends AppCompatActivity {
    private ActivityEditLovedOnesBinding activityEditLovedOnesBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityEditLovedOnesBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_loved_ones);
        ProgressDialogUtil.showLoadingDialog(this);

        activityEditLovedOnesBinding.imgBack.setOnClickListener(view -> {
         onBackPressedAction();
        });

        loadLovedOnes();
    }

    private void loadLovedOnes() {

        Call<List<LovedOne>> call = RetrofitInstance.getRetrofitInstance(getApplicationContext()).getLovedOnesForLoggedInUser(getIntent().getIntExtra(Constants.DEFAULT_LOCATION_ID,0));

        call.enqueue(new Callback<List<LovedOne>>() {

            @Override
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onResponse(Call<List<LovedOne>> call, Response<List<LovedOne>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(EditLovedOnesActivity.this, "Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    LovedOnesAdapter lovedOnesAdapter = new LovedOnesAdapter(lovedOne -> {
                        showEditLovedOneBottomSheet(lovedOne);
                    });

                    activityEditLovedOnesBinding.rvLovedOne.setAdapter(lovedOnesAdapter);
                    lovedOnesAdapter.setLovedOneList(response.body());
                    ProgressDialogUtil.dismissDialog();

                }
            }

            @Override
            public void onFailure(Call<List<LovedOne>> call, Throwable t) {
                Toast.makeText(EditLovedOnesActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                ProgressDialogUtil.dismissDialog();

            }
        });
    }


    private void showEditLovedOneBottomSheet(LovedOne lovedOne) {
        BottomSheetDialog bottomsheet = new BottomSheetDialog(EditLovedOnesActivity.this, R.style.BottomSheetDialogTheme);

        BottomsheetEditLovedOnesBinding bottomsheetEditLovedOnesBinding = BottomsheetEditLovedOnesBinding.inflate(getLayoutInflater());

        bottomsheetEditLovedOnesBinding.editDetails.setOnClickListener(view -> {
            bottomsheet.dismiss();
            Intent intent = new Intent(EditLovedOnesActivity.this, Selectage.class);
            intent.putExtra(Constants.LOVED_ONE, lovedOne);
            startActivity(intent);
        });


        bottomsheetEditLovedOnesBinding.dismissBtn.setOnClickListener(view -> {
           bottomsheet.dismiss();
        });

        bottomsheet.setContentView(bottomsheetEditLovedOnesBinding.getRoot());
        bottomsheet.show();

    }

    private void onBackPressedAction() {
        Intent intent = new Intent(EditLovedOnesActivity.this, Settings.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        onBackPressedAction();
    }
}
