package com.sensorcall.carealertandroid_new.ui.notifications;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.model.ActivityLegend;

import java.util.ArrayList;


public class ActivityLegenedAdapter extends RecyclerView.Adapter<ActivityLegenedAdapter.ActivityLegenedAdapterViewHolder> {

    private Context context;

    ActivityLegenedAdapter(Context context){
        this.context = context;
    }

    private ArrayList<ActivityLegend> legendList;
    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ActivityLegenedAdapterViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView txtActivityDesc;
        private final AppCompatImageView imglegendIcon;
        public ActivityLegenedAdapterViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            txtActivityDesc = (AppCompatTextView) view.findViewById(R.id.txt_activity_description);
            imglegendIcon = (AppCompatImageView) view.findViewById(R.id.img_activity);

        }
    }



    // Create new views (invoked by the layout manager)
    @Override
    public ActivityLegenedAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(context)
                .inflate(R.layout.activity_legend_row, viewGroup, false);

        return new ActivityLegenedAdapterViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ActivityLegenedAdapterViewHolder viewHolder, final int position) {
        ActivityLegend activityLegend = legendList.get(position);
        viewHolder.imglegendIcon.setImageResource(activityLegend.getLegenRes());
        viewHolder.txtActivityDesc.setText(activityLegend.getLegendDesc());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return legendList.size();
    }

    public void setData(ArrayList<ActivityLegend> legendList){
        this.legendList = legendList;
        notifyDataSetChanged();
    }
}
