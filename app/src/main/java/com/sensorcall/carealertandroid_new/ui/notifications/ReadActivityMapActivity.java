package com.sensorcall.carealertandroid_new.ui.notifications;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sensorcall.carealertandroid_new.Dashboard;
import com.sensorcall.carealertandroid_new.GetLocation;
import com.sensorcall.carealertandroid_new.Notification;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.Settings;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.CustomBottomNavigationView1;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.sensorcall.carealertandroid_new.util.AppUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ReadActivityMapActivity extends AppCompatActivity {

    public final static String ARG_LOCATION = "LOCATION";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_read_map);
        ActivityLegenedAdapter activityLegenedAdapter = new ActivityLegenedAdapter(this);
        RecyclerView recyclerView = findViewById(R.id.rv_legends);
        recyclerView.setAdapter(activityLegenedAdapter);
        activityLegenedAdapter.setData(AppUtil.getLegendLegendArrayList());
        findViewById(R.id.img_back).setOnClickListener(v -> finish());

        bottomNavigation();


        GetLocation activity = getIntent().getParcelableExtra(ARG_LOCATION);

        if (activity != null) {
            mapDataIfAvaiable(activity, findViewById(R.id.activity_layout));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    void mapDataIfAvaiable(GetLocation Activity, View HL) {

        TextView TextName = HL.findViewById(R.id.Activity_DeviceName);
        TextName.setText(Notification.textBreak(Activity.getDeviceName()));
        TextName.setAutoSizeTextTypeUniformWithConfiguration(1, 15, 1, TypedValue.COMPLEX_UNIT_DIP);

        TextView ActivityName = HL.findViewById(R.id.Activity_Name);

        int actId = Activity.getActivityId();
        if ((actId >= 0 && actId <= 2) || (actId >= 11 && actId <= 12) || actId == 22) {
            ActivityName.setText("");
            ActivityName.setVisibility(View.INVISIBLE);
        } else {
            ActivityName.setText(Activity.getActivity());
            ActivityName.setAutoSizeTextTypeUniformWithConfiguration(1, 20, 1, TypedValue.COMPLEX_UNIT_DIP);
        }

        ImageView config_image = HL.findViewById(R.id.ActivityConf);
        ImageView motion_image = HL.findViewById(R.id.Activity_Motion);
        ImageView presence_image = HL.findViewById(R.id.Activity_Presence);
        ImageView activity_model = HL.findViewById(R.id.activity_model);

        // confidence check : if confidencevalue is greater than 50, confidence is "STRONG", else week
                           /*Model - When Confidence is more than 50 then speaker should be highlighted (Green)
                            else it do not highlight (greyed)*/
        int Confvalue = Activity.getConfidence();
        if (Confvalue > 7) {
            config_image.setImageResource(R.drawable.ic_active);
        } else {
            config_image.setImageResource(R.drawable.ic_warning);
        }


        if (Confvalue > 50) {
            activity_model.setImageResource(R.drawable.ic_sound_up);
        } else {
            activity_model.setImageResource(R.drawable.ic_sound_up_unslected);
        }

                            /*Motion check : When presence is 1 or ActivityId is 1 to 10 highlight/activate (green)
                             the motion icon else do not highlight it (greyed)*/
        int presenceValue = Activity.getPresence();
        if (presenceValue == 1 || (actId >= 1 && actId <= 10)) {
            motion_image.setImageResource(R.drawable.ic_motion);
        } else {
            motion_image.setImageResource(R.drawable.ic_motion_unselected);
        }

                            /*Presence - When ActivityId is 11, 13, 14, 15, or 16 highlight presence icon (green)
                            else do not highlight it (greyed)*/
        // presence_image.setImageDrawable(presenceImage);
        if (actId == 11 || (actId >= 13 && actId <= 16)) {
            presence_image.setImageResource(R.drawable.ic_presense);
        } else {
            presence_image.setImageResource(R.drawable.ic_presense_unselecetd);
        }


        LocalDateTime datetime1 = LocalDateTime.now();
        String strDateFormat = "E, MMM dd yyyy";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(strDateFormat);
        String formatDateTime = datetime1.format(format);
        LocalDateTime datetime2 = datetime1.minusDays(1);
        String yesterdayDateTime = datetime2.format(format);
        //  String strDateFormat = "E, MMM dd yyyy hh:mm a"; //Date format is Specified
        SimpleDateFormat formatter = new SimpleDateFormat(strDateFormat);
        String strActivityFormat = "E, MMM dd yyyy hh:mm a"; //Date format is Specified
        SimpleDateFormat formatterActivity = new SimpleDateFormat(strActivityFormat);
        Date dt_1 = null;
        Date dt_2 = null;
        Date yesterday = null;
        Date activity_date = null;
        try {
            dt_1 = formatter.parse(formatDateTime);
            dt_2 = formatter.parse(Activity.getTime());
            yesterday = formatter.parse(yesterdayDateTime);
            activity_date = formatterActivity.parse(Activity.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String strDateFormat1 = "h:mm a"; //Date format is Specified
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat1);
        String strDate = objSDF.format(activity_date);
        TextView dateTime = HL.findViewById(R.id.activity_DateTime);
        dateTime.setText(strDate);
    }


    public void bottomNavigation() {
        CustomBottomNavigationView1 curvedBottomNavigationView = findViewById(R.id.bottom_navigation);
        Menu menu = curvedBottomNavigationView.getMenu();
        //   MenuItem menuitem= menu.getItem(3);
        MenuItem menuitem = menu.getItem(0);
        menuitem.setChecked(true);

        curvedBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.navigation_dashboard:
                        Intent dashBoard = new Intent(ReadActivityMapActivity.this, Dashboard.class);
                        startActivity(dashBoard);
                        ReadActivityMapActivity.this.finish();
                        break;

                    case R.id.navigation_Reminders:
                        Intent reminder = new Intent(ReadActivityMapActivity.this, RemindersViewActivity.class);
                        startActivity(reminder);
                        break;

                    case R.id.navigation_notifications:
                        Intent notification = new Intent(ReadActivityMapActivity.this, Notification.class);
                        startActivity(notification);
                        break;

                    case R.id.navigation_settings:
                        Intent settings = new Intent(ReadActivityMapActivity.this, Settings.class);
                        startActivity(settings);
                        break;
                }
                return true;
            }
        });
    }
}