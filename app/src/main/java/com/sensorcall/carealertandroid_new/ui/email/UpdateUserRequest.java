package com.sensorcall.carealertandroid_new.ui.email;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateUserRequest {
    @SerializedName("FirstName")
    @Expose
    public String firstName;
    @SerializedName("LastName")
    @Expose
    public String lastName;
    @SerializedName("Email")
    @Expose
    public String email;
    @SerializedName("Password")
    @Expose
    public String password;
    @SerializedName("ProfilePicture")
    @Expose
    public String profilePicture;
    @SerializedName("FingerPrint")
    @Expose
    public String fingerPrint;
    @SerializedName("Role")
    @Expose
    public String role;
    @SerializedName("CreatedAt")
    @Expose
    public String createdAt;
    @SerializedName("ModifiedAt")
    @Expose
    public String modifiedAt;
    @SerializedName("LastLoggedOn")
    @Expose
    public String lastLoggedOn;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(String fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getLastLoggedOn() {
        return lastLoggedOn;
    }

    public void setLastLoggedOn(String lastLoggedOn) {
        this.lastLoggedOn = lastLoggedOn;
    }
}
