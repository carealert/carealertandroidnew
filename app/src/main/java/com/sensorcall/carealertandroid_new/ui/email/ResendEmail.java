package com.sensorcall.carealertandroid_new.ui.email;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResendEmail {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("isSuccess")
    @Expose
    public boolean isSuccess;

}
