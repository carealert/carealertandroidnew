package com.sensorcall.carealertandroid_new.ui.email;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.Enteremail;
import com.sensorcall.carealertandroid_new.R;
import com.sensorcall.carealertandroid_new.Selectage;
import com.sensorcall.carealertandroid_new.Settings;
import com.sensorcall.carealertandroid_new.databinding.ActivityAccountSettingsBinding;
import com.sensorcall.carealertandroid_new.databinding.BottomSheetEmailBinding;
import com.sensorcall.carealertandroid_new.ui.lovedones.EditLovedOnesActivity;
import com.sensorcall.carealertandroid_new.ui.lovedones.LovedOne;
import com.sensorcall.carealertandroid_new.ui.lovedones.LovedOnesAdapter;
import com.sensorcall.carealertandroid_new.util.ProgressDialogUtil;
import com.sensorcall.carealertandroid_new.util.RetrofitInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountSettingsActivity extends AppCompatActivity {

    private ActivityAccountSettingsBinding activityAccountSettingsBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityAccountSettingsBinding = DataBindingUtil.setContentView(this, R.layout.activity_account_settings);

        activityAccountSettingsBinding.imgBack.setOnClickListener(v->{
            onBackPressedAction();
        });

        loadEmail();
    }

    private void onBackPressedAction() {
        Intent intent = new Intent(AccountSettingsActivity.this, Settings.class);
        startActivity(intent);
        finish();
    }

    private void loadEmail() {
        ProgressDialogUtil.showLoadingDialog(this);
        Call<Email> call = RetrofitInstance.getRetrofitInstance(getApplicationContext()).getUserEmailIdAndEmailConfirmation();

        call.enqueue(new Callback<Email>() {

            @Override
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onResponse(Call<Email> call, Response<Email> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(AccountSettingsActivity.this, "Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Email email = response.body();
                    activityAccountSettingsBinding.txtEmail.setText(email.getUserEmail());

                    if (email.isEmailConfirmation()) {
                        activityAccountSettingsBinding.txtVerified.setText(R.string.verified);
                    } else {
                        activityAccountSettingsBinding.txtVerified.setText(R.string.unverified);
                    }

                    activityAccountSettingsBinding.imgMoreOptions.setOnClickListener(v -> {
                        showEmailBottomSheet(email);
                    });


                    ProgressDialogUtil.dismissDialog();

                }
            }

            @Override
            public void onFailure(Call<Email> call, Throwable t) {
                Toast.makeText(AccountSettingsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                ProgressDialogUtil.dismissDialog();

            }
        });
    }


    private void showEmailBottomSheet(Email email) {
        BottomSheetDialog bottomsheet = new BottomSheetDialog(AccountSettingsActivity.this, R.style.BottomSheetDialogTheme);

        BottomSheetEmailBinding bottomSheetEmailBinding = BottomSheetEmailBinding.inflate(getLayoutInflater());

        bottomSheetEmailBinding.editEmail.setOnClickListener(view -> {
            bottomsheet.dismiss();
            Intent intent = new Intent(AccountSettingsActivity.this, Enteremail.class);
            intent.putExtra(Constants.EMAIL, email);
            startActivity(intent);
        });

        if(email.isEmailConfirmation()){
            bottomSheetEmailBinding.resendVerification.setEnabled(false);
            bottomSheetEmailBinding.resendVerification.setBackground(ContextCompat.getDrawable(this,R.drawable.ic_rectangle_gray));
        }

        bottomSheetEmailBinding.resendVerification.setOnClickListener(view -> {
            bottomsheet.dismiss();
            resendEmail(email);

        });

        bottomSheetEmailBinding.dismissBtn.setOnClickListener(view -> {
            bottomsheet.dismiss();
        });

        bottomsheet.setContentView(bottomSheetEmailBinding.getRoot());
        bottomsheet.show();

    }

    private void resendEmail(Email email) {
        ProgressDialogUtil.showLoadingDialog(this);
        Call<ResendEmail> call = RetrofitInstance.getRetrofitInstance(getApplicationContext()).resendEmailNotification(email.getUserEmail());

        call.enqueue(new Callback<ResendEmail>() {

            @Override
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onResponse(Call<ResendEmail> call, Response<ResendEmail> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(AccountSettingsActivity.this, "Response is unsuccessful", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Toast.makeText(AccountSettingsActivity.this, response.body().message, Toast.LENGTH_SHORT).show();

                    ProgressDialogUtil.dismissDialog();

                }
            }

            @Override
            public void onFailure(Call<ResendEmail> call, Throwable t) {
                Toast.makeText(AccountSettingsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                ProgressDialogUtil.dismissDialog();

            }
        });
    }

    @Override
    public void onBackPressed() {
        onBackPressedAction();
    }
}