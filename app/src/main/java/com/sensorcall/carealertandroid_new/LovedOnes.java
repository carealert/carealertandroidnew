package com.sensorcall.carealertandroid_new;

public class LovedOnes {

    int LovedOnesId;
    String ProfileName;
    boolean IsPrimary;

    public int getLovedOnesId() {
        return LovedOnesId;
    }

    public String getProfileName() {
        return ProfileName;
    }

    public boolean isPrimary() {
        return IsPrimary;
    }
}
