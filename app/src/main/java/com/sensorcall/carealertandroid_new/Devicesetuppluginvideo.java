package com.sensorcall.carealertandroid_new;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;

public class Devicesetuppluginvideo extends AppCompatActivity {

    Button continue_button;
    ProgressDialog loaddialog,progressDialog;
    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerFragment mVideoView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devicesetuppluginvideo);
        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        mVideoView = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.plugin_video);
        continue_button= (Button) findViewById(R.id.deviceplugin_continue);

        checkNetConnectivity();
        /*

        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                opennamedeviceroompage();
            }
        });

         */


    }


    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            stepUpNet();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    public void stepUpNet(){
        setupyoutubevideo();
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                opennamedeviceroompage();
            }
        });
    }

    public void setupyoutubevideo(){
        mVideoView.initialize("AIzaSyD2eykRREReql8UPSfCAh0GrzqnzowlLVo", new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    youTubePlayer.cueVideo("HaCfcPhKS7k"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                if (youTubeInitializationResult.isUserRecoverableError()) {
                    youTubeInitializationResult.getErrorDialog(Devicesetuppluginvideo.this, RECOVERY_REQUEST).show();
                } else {
                    String error = String.format(getString(R.string.player_error), youTubeInitializationResult.toString());
                    Toast.makeText(Devicesetuppluginvideo.this, error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void opennamedeviceroompage(){
        Intent namedeviceroom= new Intent(this,Namedeviceroom.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(namedeviceroom);
    }

    public void openslectagepage(){
        Intent selectage= new Intent(this,Selectage.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(selectage);
    }






    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @Override
    public void onBackPressed() {
            Intent loginpage= new Intent(Devicesetuppluginvideo.this,LoginScreen.class);
            loaddialog.dismiss();
            startActivity(loginpage);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            setupyoutubevideo();
        }
    }
}