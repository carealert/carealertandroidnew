package com.sensorcall.carealertandroid_new.STUN.client;

public enum STUN_NatType {
    FullCone,
    RestrictedCone,
    PortRestrictedCone,
    Symmetric,
    UdpBlocked,
    OpenInternet,
    SymmetricUdpFirewall;
}
