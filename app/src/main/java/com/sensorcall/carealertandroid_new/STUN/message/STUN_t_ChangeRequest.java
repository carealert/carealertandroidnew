package com.sensorcall.carealertandroid_new.STUN.message;

public class STUN_t_ChangeRequest {
    private boolean m_ChangeIP = true;
    private boolean m_ChangePort = true;

    public STUN_t_ChangeRequest(boolean changeIP, boolean changePort) {
        m_ChangeIP = changeIP;
        m_ChangePort = changePort;
    }

    public boolean isM_ChangeIP() {
        return m_ChangeIP;
    }

    public void setM_ChangeIP(boolean m_ChangeIP) {
        this.m_ChangeIP = m_ChangeIP;
    }

    public boolean isM_ChangePort() {
        return m_ChangePort;
    }

    public void setM_ChangePort(boolean m_ChangePort) {
        this.m_ChangePort = m_ChangePort;
    }
}
