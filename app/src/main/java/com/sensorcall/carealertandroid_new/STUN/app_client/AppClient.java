package com.sensorcall.carealertandroid_new.STUN.app_client;


import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.Constants;
import com.sensorcall.carealertandroid_new.HelperClass.NetUtils.IPEndPoint;
import com.sensorcall.carealertandroid_new.STUN.client.STUN_NatType;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.GetDevicePojo;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.google.gson.JsonObject;
import com.microsoft.azure.sdk.iot.service.IotHubServiceClientProtocol;
import com.microsoft.azure.sdk.iot.service.Message;
import com.microsoft.azure.sdk.iot.service.ServiceClient;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppClient{
    private final static String ConnectionString="HostName=CareAlert-IOTHub.azure-devices.net;SharedAccessKeyName=WEBAPP;SharedAccessKey=h2EoXgjzC7Ix6Qk8KzXehpX7ZbkH7NPYIJEQYVu7JrM=";
    private final static String TAG = "AppClient";
    private String masterIp = "127.0.0.1";
    private int masterPort = 7000;
    private IPEndPoint master = null;
    private String pool = "";
    private DatagramSocket socket = null;
    private IPEndPoint target;
    private STUN_NatType peerNatType = STUN_NatType.OpenInternet;
    private String devicenumber, portnumber;
    //private bool periodicRunning = false;
    private String[] NATTYPE = {"FullCone", "RestrictedCone", "PortRestrictedCone", "Symmetric", "UnKnown"};

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private JsonPlaceHolder jsonPlaceHolderApi = retrofit.create(JsonPlaceHolder.class);


    public AppClient(String masterIp, int masterPort, DatagramSocket socket)
    {
        this.socket = socket;
        this.masterIp = masterIp;
        this.masterPort = masterPort;
      //  Log.d("pool","masterPort"+masterPort);
        try {
            master = new IPEndPoint(InetAddress.getByName(masterIp), masterPort);
            } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    private CommuMessage Byte2Address(byte[] data) throws UnknownHostException
    {
        if (data.length != 8)
        {
            Log.d(TAG, "Invalid bytes!");
            return null;
        }
        int offset = 0;
        //Address
        byte[] ip = new byte[4];
        ip[0] = data[offset++];
        ip[1] = data[offset++];
        ip[2] = data[offset++];
        ip[3] = data[offset++];

        //Port
        int firstDigit = byte2int(data[offset++]);
        int secondDigit = byte2int(data[offset++]);
        int port = (firstDigit | secondDigit << 8);

        IPEndPoint endPoint = new IPEndPoint(InetAddress.getByAddress(ip), port);  // Not getting this upto - check with debuger.

        //NatType Id
        char natTypeId = (char)(data[offset++] | data[offset++] << 8);
        STUN_NatType natType = STUN_NatType.values()[3];
        return new CommuMessage(endPoint, natType);
    }

    private int byte2int(byte input) {
        int res = 0;
        for (int i = 0; i < 8; i++) {
            int mask = 1 << i;
            res |= input & mask;
        }
        return res;
    }

    public CommuMessage RequestForConnection(STUN_NatType natType, String pool, String TURN_ip, int TURN_port,String auth1)
    {
        this.pool = pool;
//        Log.d("pool","natType "+natType);   // Changes by Kamlesh
          Log.d("pool","poool "+pool);
//        Log.d("pool","ip "+TURN_ip);
//        Log.d("pool","port "+TURN_port);

        devicenumber = pool;
        portnumber = String.valueOf(TURN_port);
        DatagramPacket sendPacket;
        DatagramPacket receivePacket;
        try
        {
            // Request to pool in AWS server
            byte[] requestBytes = (pool + " " + natType.ordinal() + " " + "2").getBytes();
            sendPacket = new DatagramPacket(requestBytes, requestBytes.length, master.getIpAddress(), master.getPort());
            socket.send(sendPacket);

            Log.d("sendPacket","sendPacket--: "+sendPacket);

            System.out.println(TAG + " Request sent, waiting for partner in pool " + pool);

            // Request peer IP address
            byte[] receiveBuffer = new byte[8];
            receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
            socket.setSoTimeout(15000); //15s timeout for waiting the response from AWS server
            socket.receive(receivePacket);

            Log.d("pool","receiceBuffer:-- "+receivePacket);

            if (new String(receiveBuffer).equals("goodtogo"))
            {
               //  Log.d("into the receive buffer","receiceBuffer:-- "+receiveBuffer.toString());
                 apiCall(devicenumber,portnumber,auth1);                            //signal the device through Notify API call
            } else if (new String(receiveBuffer).equals("occupied")) {
                Log.d(TAG, "device occupied");
                return null;
            } else if (new String(receiveBuffer).equals("cancel!!")) {
                Log.d(TAG, "Request cancelled");
                return null;
            }
            receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
            socket.setSoTimeout(15000);                 //15s timeout for waiting the response from ESP_32
            socket.receive(receivePacket);
//            Log.d("Test","receivePacket:-- "+receivePacket);
//            Log.d("pool1","receiveBuffer:-- "+receivePacket);

            if (new String(receiveBuffer).equals("cancel!!"))
            {
                Log.d(TAG, "Request cancelled");
                return null;
            }
            // Parse message
            CommuMessage commuMessage = Byte2Address(receiveBuffer);
            this.target = commuMessage.getEndPoint();
            this.peerNatType = commuMessage.getNatType();
            Log.d("Test","receivePacket:-- "+receivePacket);
            System.out.println(TAG + String.format(" Connected to %s, it's NAT type is %s", target, NATTYPE[peerNatType.ordinal()]));
            return commuMessage;
        } catch (SocketTimeoutException t) {
            Log.d(TAG, "Socket Timeout in Peer Address Request!");
            t.printStackTrace();
            return null;
        } catch (Exception e) {
            Log.d(TAG, "Socket Error in Peer Address Request!");
            e.printStackTrace();
            return null;
        }
    }

    private void apiCall(String deviceNumber, String portNumber,String auth1){
        JsonObject apiBody= new JsonObject();
        apiBody.addProperty("DeviceNumber",deviceNumber);
        apiBody.addProperty("TurnPort",portNumber);
        Log.d("care_receiver_type","check dev " + deviceNumber);
        Log.d("care_receiver_type","check port " + portNumber);
    //    final String auth = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIrOTE4Mjc1MjQ1MTM5IiwianRpIjoiZjM5YjU2MzYtNjlkMS00MmU1LWJhZTMtNGQ4YWFkNzQzMDU1IiwiaWF0IjoxNjAxODkzMDMyLCJyb2xlIjoiYXBpX2FjY2VzcyIsImlkIjoiMjZmMDVkOGItNDIyYS00Y2Y1LTk2NDUtMDQ0NzJmYzY4NThjIiwibmJmIjoxNjAxODkzMDMyLCJleHAiOjE2MDI0OTc4MzIsImlzcyI6IndlYkFwaSIsImF1ZCI6Imh0dHA6Ly9jYXJlYWxlcnRkZXYuYXp1cmV3ZWJzaXRlcy5uZXQvIn0.rkK1tyopeODl8IlSvwjBnN9zMC30z9NutbNkdLZ29vk";
        final String auth = auth1;
        Call<GetDevicePojo> call = jsonPlaceHolderApi.getNotify("Bearer "+auth,apiBody);
        Log.d("authtoken123","authhhhhhh "+auth);
        call.enqueue(new Callback<GetDevicePojo>() {
            @Override
            public void onResponse(Call<GetDevicePojo> call, Response<GetDevicePojo> response) {
                if(!response.isSuccessful()){
                    Log.d("Test123 ", "Fail");
                    Log.e("errormsg",response.errorBody().toString());
                }
                else {
                    Log.d("testtest", response.body().toString());
                    GetDevicePojo post= response.body();
                    String content ="";
                    content += response.code();
                    Log.d("success :","Check code : "+content);
                }
            }

            @Override
            public void onFailure(Call<GetDevicePojo> call, Throwable t) {
                //Toast.makeText(AppClient.this, "Failed", Toast.LENGTH_SHORT).show();
                Log.d("onFailure","Fail");
            }
        });
    }


    public void RequestConnectionCancel(boolean isRinging, String DeviceNumber) {
        DatagramPacket sendPacket;
        DatagramPacket receivePacket;
        try {
            byte[] requestBytes;
            byte[]receiveBuffer;
            // if !isRinging, means voice call initiated, need to hang up
            if (!isRinging) {
                Log.d("TO IOT Hub", "RequestConnectionCancel: "+"Start");
                ServiceClient serviceClient =ServiceClient.createFromConnectionString(ConnectionString, IotHubServiceClientProtocol.AMQPS);
                if (serviceClient != null) {
                    serviceClient.open();
                    Map<String,String> Command= new HashMap<>();
                    Command.put("LC","Stop");
                    Log.d("Command is", "RequestConnectionCancel: "+Command);
                    Message messageToSend = new Message(" ");
                    messageToSend.setProperties(Command);
                    Log.d("Message to send", "RequestConnectionCancel: "+messageToSend);
                    serviceClient.send(DeviceNumber, messageToSend);
                    Log.d("TO IOT Hub", "RequestConnectionCancel: "+"End");
                }
                    requestBytes = ("LC Stop\0").getBytes();
                    sendPacket = new DatagramPacket(requestBytes, requestBytes.length, target.getIpAddress(), target.getPort());
                    socket.send(sendPacket);
                    try {
                        receiveBuffer = new byte[512];
                        socket.setSoTimeout(1000);
                        receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                        socket.receive(receivePacket);
                        while (true) {
                            receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                            socket.receive(receivePacket);
                            //Log.d(TAG, new String(receiveBuffer));
                        }
                    } catch (SocketTimeoutException t) {
                        Log.d(TAG, "Socket Timeout");
                    }
                socket.disconnect();
                // if isRinging, means during the signaling process, need to cancel;
            } else {

                requestBytes = ("del " + pool).getBytes();
                sendPacket = new DatagramPacket(requestBytes, requestBytes.length, master.getIpAddress(), master.getPort());
                socket.send(sendPacket);

   /*             ServiceClient serviceClient =ServiceClient.createFromConnectionString(ConnectionString, IotHubServiceClientProtocol.AMQPS);
                if (serviceClient != null) {
                    serviceClient.open();
                    Map<String,String> Command= new HashMap<>();
                    Command.put("LC","Stop");
                    Log.d("Command is", "RequestConnectionCancel: "+Command);
                    Message messageToSend = new Message(" ");
                    messageToSend.setProperties(Command);
                    Log.d("Message to send", "RequestConnectionCancel: "+messageToSend);
                    serviceClient.send(DeviceNumber, messageToSend);
                    Log.d("TO IOT Hub", "RequestConnectionCancel: "+"End");
                }
                requestBytes = ("del " + pool).getBytes();
                sendPacket = new DatagramPacket(requestBytes, requestBytes.length, master.getIpAddress(), master.getPort());
                socket.send(sendPacket);
                socket.disconnect();  */
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public IPEndPoint getMaster() {
        return master;
    }

    public IPEndPoint getTarget() {
        return target;
    }
}
