package com.sensorcall.carealertandroid_new.STUN.client;

import com.sensorcall.carealertandroid_new.HelperClass.NetUtils.IPEndPoint;

public class STUN_Result {
    private STUN_NatType m_NatType = STUN_NatType.OpenInternet;
    private IPEndPoint m_pPublicEndPoint = null;

    public STUN_Result(STUN_NatType natType, IPEndPoint publicEndPoint) {
        m_NatType = natType;
        m_pPublicEndPoint = publicEndPoint;
    }

    public STUN_NatType getM_NatType() {
        return m_NatType;
    }

    public IPEndPoint getM_pPublicEndPoint() {
        return m_pPublicEndPoint;
    }

    public String toString() {
        return m_pPublicEndPoint.getIpAddress() + " : " + m_pPublicEndPoint.getPort() + " " + m_NatType.toString();
    }
}

