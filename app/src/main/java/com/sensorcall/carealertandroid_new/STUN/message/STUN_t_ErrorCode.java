package com.sensorcall.carealertandroid_new.STUN.message;


public class STUN_t_ErrorCode {
    private int m_code = 0;
    private String m_ReasonText = "";

    public STUN_t_ErrorCode(int code, String reasonText) {
        m_code = code;
        m_ReasonText = reasonText;
    }

    public int getM_code() {
        return m_code;
    }

    public void setM_code(int m_code) {
        this.m_code = m_code;
    }

    public String getM_ReasonText() {
        return m_ReasonText;
    }

    public void setM_ReasonText(String m_ReasonText) {
        this.m_ReasonText = m_ReasonText;
    }
}

