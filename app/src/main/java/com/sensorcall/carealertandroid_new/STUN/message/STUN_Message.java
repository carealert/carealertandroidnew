package com.sensorcall.carealertandroid_new.STUN.message;

import com.sensorcall.carealertandroid_new.HelperClass.NetUtils.IPEndPoint;

import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

public class STUN_Message {
    private static final String TAG = "STUN_Message";

    private enum AttributeType {
        MappedAddress(0x0001),
        ResponseAddress(0x0002),
        ChangeRequest(0x0003),
        SourceAddress(0x0004),
        ChangedAddress(0x0005),
        Username(0x0006),
        Password(0x0007),
        MessageIntegrity(0x0008),
        ErrorCode(0x0009),
        UnknownAttribute(0x000A),
        ReflectedFrom(0x000B),
        XorMappedAddress(0x8020),
        XorOnly(0x0021),
        ServerName(0x8022);

        private final int value;
        AttributeType(int value) {
            this.value = value;
        }

        public static AttributeType fromInteger(int value) {
            switch (value) {
                case 0x0001:
                    return MappedAddress;
                case 0x0002:
                    return ResponseAddress;
                case 0x0003:
                    return ChangeRequest;
                case 0x0004:
                    return SourceAddress;
                case 0x0005:
                    return ChangedAddress;
                case 0x0006:
                    return Username;
                case 0x0007:
                    return Password;
                case 0x0008:
                    return MessageIntegrity;
                case 0x0009:
                    return ErrorCode;
                case 0x000A:
                    return UnknownAttribute;
                case 0x000B:
                    return ReflectedFrom;
                case 0x8020:
                    return XorMappedAddress;
                case 0x8021:
                    return XorOnly;
                case 0x8022:
                    return ServerName;
            }
            return null;
        }

        public int getValue() {
            return value;
        }
    }

    private enum IPFamily {
        IPv4(0x01),
        IPv6(0x02);

        private final int value;
        IPFamily(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private STUN_MessageType m_Type = STUN_MessageType.BindingRequest;
    private UUID m_pTransactionID = null;
    private IPEndPoint m_pMappedAddress = null;
    private IPEndPoint m_pResponseAddress = null;
    private STUN_t_ChangeRequest m_pChangeRequest = null;
    private IPEndPoint m_pSourceAddress = null;
    private IPEndPoint m_pChangedAddress = null;
    private String m_UserName = null;
    private String m_PassWord = null;
    private STUN_t_ErrorCode m_pErrorCode = null;
    private IPEndPoint m_pReflectedFrom = null;
    private String m_ServerName = null;

    public STUN_Message() {
        m_pTransactionID = UUID.randomUUID();
    }

    //parse STUN message from raw data packet
    public void parse(byte[] data) {
        /* RFC 3489 11.1.
                All STUN messages consist of a 20 byte header:

                0                   1                   2                   3
                0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
               |      STUN Message Type        |         Message Length        |
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
               |
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                                        Transaction ID
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                                                                               |
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

               The message length is the count, in bytes, of the size of the
               message, not including the 20 byte header.
            */

        if (data.length < 20) {
            Log.d(TAG, "Invalid STUN message value");
            return;
        }

        int[] offset = {0};
        //---message header -----------------------------------------
        // STUN Message Type
        int messageType = (data[offset[0]++] << 8 | data[offset[0]++]);
        if (messageType == STUN_MessageType.BindingErrorResponse.getValue())
        {
            m_Type = STUN_MessageType.BindingErrorResponse;
        }
        else if (messageType == STUN_MessageType.BindingRequest.getValue())
        {
            m_Type = STUN_MessageType.BindingRequest;
        }
        else if (messageType == STUN_MessageType.BindingResponse.getValue())
        {
            m_Type = STUN_MessageType.BindingResponse;
        }
        else if (messageType == STUN_MessageType.SharedSecretErrorResponse.getValue())
        {
            m_Type = STUN_MessageType.SharedSecretErrorResponse;
        }
        else if (messageType == STUN_MessageType.SharedSecretRequest.getValue())
        {
            m_Type = STUN_MessageType.SharedSecretRequest;
        }
        else if (messageType == STUN_MessageType.SharedSecretResponse.getValue())
        {
            m_Type = STUN_MessageType.SharedSecretResponse;
        }
        else
        {
            Log.d(TAG, "Invalid STUN message type value!");
            return;
        }

        //Message Length
        int messageLength = (data[offset[0]++] << 8 | data[offset[0]++]);

        //Transaction ID
        byte[] guid = Arrays.copyOfRange(data, offset[0], offset[0] + 16);
        ByteBuffer bb = ByteBuffer.wrap(guid);
        long high = bb.getLong();
        long low = bb.getLong();
        m_pTransactionID = new UUID(high, low);

        offset[0] += 16;

        //--- Message Attributes --------------------------------
        while ((offset[0] - 20) < messageLength)
        {
            try {
                ParseAttribute(data, offset);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    private void ParseAttribute(byte[] data, int[] offset) throws UnknownHostException
    {
            /* RFC 3489 11.2.
                Each attribute is TLV encoded, with a 16 bit type, 16 bit length, and variable value:

                0                   1                   2                   3
                0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
               |         Type                  |            Length             |
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
               |                             Value                             ....
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
            */

        // Type
        AttributeType type = AttributeType.fromInteger(data[offset[0]++] << 8 | data[offset[0]++]);

        // Length
        int length = (data[offset[0]++] << 8 | data[offset[0]++]);
        // MAPPED-ADDRESS
        if (type == AttributeType.MappedAddress)
        {
            m_pMappedAddress = ParseEndPoint(data, offset);
        }
        // RESPONSE-ADDRESS
        else if (type == AttributeType.ResponseAddress)
        {
            m_pResponseAddress = ParseEndPoint(data, offset);
        }
        // CHANGE-REQUEST
        else if (type == AttributeType.ChangeRequest)
        {
                /*
                    The CHANGE-REQUEST attribute is used by the client to request that
                    the server use a different address and/or port when sending the
                    response.  The attribute is 32 bits long, although only two bits (A
                    and B) are used:

                     0                   1                   2                   3
                     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    |0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 A B 0|
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

                    The meaning of the flags is:

                    A: This is the "change IP" flag.  If true, it requests the server
                       to send the Binding Response with a different IP address than the
                       one the Binding Request was received on.

                    B: This is the "change port" flag.  If true, it requests the
                       server to send the Binding Response with a different port than the
                       one the Binding Request was received on.
                */

            // Skip 3 bytes
            offset[0] += 3;

            m_pChangeRequest = new STUN_t_ChangeRequest((data[offset[0]] & 4) != 0, (data[offset[0]] & 2) != 0);
            offset[0]++;
        }
        //SOURCE-ADDRESS
        else if (type == AttributeType.SourceAddress)
        {
            m_pSourceAddress = ParseEndPoint(data, offset);
        }
        //CHANGED-ADDRESS
        else if (type == AttributeType.ChangedAddress)
        {
            m_pChangedAddress = ParseEndPoint(data, offset);
        }
        //UESRNAME
        else if (type == AttributeType.Username)
        {
            m_UserName = new String(data, offset[0], length);
        }
        //PASSWORD
        else if (type == AttributeType.Password)
        {
            m_PassWord = new String(data, offset[0], length);
        }
        //MEESAGE-INTEGRITY
        else if (type == AttributeType.MessageIntegrity)
        {
            offset[0] += length;
        }
        //ERROR-CODE
        else if (type == AttributeType.ErrorCode)
        {
                /* 3489 11.2.9.
                    0                   1                   2                   3
                    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    |                   0                     |Class|     Number    |
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    |      Reason Phrase (variable)                                ..
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                */

            int errorCode = (data[offset[0] + 2] & 0x7) * 100 + (data[offset[0] + 3] & 0xFF);
            m_pErrorCode = new STUN_t_ErrorCode(errorCode, new String(data, offset[0] + 4, length - 4));

        }
        //UNKNOW-ATTRIBUTES
        else if (type == AttributeType.UnknownAttribute)
        {
            offset[0] += length;
        }
        //REFLECTED-FROM
        else if (type == AttributeType.ReflectedFrom)
        {
            m_pReflectedFrom = ParseEndPoint(data, offset);
        }
        //XorMappedAddress
        //XorOnly
        //ServerName
        else if (type == AttributeType.ServerName)
        {
            m_ServerName =new String(data, offset[0], length);
            offset[0] += length;
        }
        //Unkown
        else
        {
            offset[0] += length;
        }
    }

    private IPEndPoint ParseEndPoint(byte[] data, int[] offset) throws UnknownHostException
    {
            /*
                It consists of an eight bit address family, and a sixteen bit
                port, followed by a fixed length value representing the IP address.

                0                   1                   2                   3
                0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
                +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                |x x x x x x x x|    Family     |           Port                |
                +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                |                             Address                           |
                +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
            */
        // Skip family
        offset[0]++;
        offset[0]++;

        //Port Java byte type is not unsigned, it's different from C#, be careful here!
        int firstDigit = byte2int(data[offset[0]++]);
        int secondDigit = byte2int(data[offset[0]++]);
        int port = (firstDigit << 8| secondDigit);

        //Address
        byte[] ip = new byte[4];
        ip[0] = data[offset[0]++];
        ip[1] = data[offset[0]++];
        ip[2] = data[offset[0]++];
        ip[3] = data[offset[0]++];
        return new IPEndPoint(InetAddress.getByAddress(ip), port);
    }

    private int byte2int(byte input) {
        int res = 0;
        for (int i = 0; i < 8; i++) {
            int mask = 1 << i;
            res |= input & mask;
        }
        return res;
    }

    public byte[] ToByteData()
    {
            /* RFC 3489 11.1.
                All STUN messages consist of a 20 byte header:

                0                   1                   2                   3
                0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
               |      STUN Message Type        |         Message Length        |
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
               |
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                                        Transaction ID
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                                                                               |
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

               The message length is the count, in bytes, of the size of the
               message, not including the 20 byte header.

            */

        // Allocate 512 for header, more than enough
        byte[] msg = new byte[512];

        int offset = 0;

        //--- message header ----------------------------------------

        //STUN Message Type (2 bytes)
        msg[offset++] = (byte)(this.m_Type.getValue() >> 8);
        msg[offset++] = (byte)(this.m_Type.getValue() & 0xFF);

        //Message Length (2 bytes) will be assigned at last
        msg[offset++] = 0;
        msg[offset++] = 0;

        //Transaction ID (16 bytes)
        ByteBuffer uuid_byte = ByteBuffer.wrap(new byte[16]);
        uuid_byte.putLong(m_pTransactionID.getMostSignificantBits());
        uuid_byte.putLong(m_pTransactionID.getLeastSignificantBits());

        for (int i = 0; i < 16; i++) {
            msg[offset + i] = uuid_byte.array()[i];
        }

        offset += 16;

        //--- Message attributes -------------------------------------
            /* RFC 3489 11.2.
                After the header are 0 or more attributes.  Each attribute is TLV
                encoded, with a 16 bit type, 16 bit length, and variable value:

                0                   1                   2                   3
                0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
               |         Type                  |            Length             |
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
               |                             Value                             ....
               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
            */

        if (this.getM_pMappedAddress() != null)
        {
            StoreEndPoint(AttributeType.MappedAddress, this.getM_pChangedAddress(), msg, offset);
        }
        else if (this.getM_pResponseAddress() != null)
        {
            StoreEndPoint(AttributeType.ResponseAddress, this.getM_pResponseAddress(), msg, offset);
        }
        else if (this.getM_pChangeRequest() != null)
        {
                /*
                    The CHANGE-REQUEST attribute is used by the client to request that
                    the server use a different address and/or port when sending the
                    response.  The attribute is 32 bits long, although only two bits (A
                    and B) are used:

                     0                   1                   2                   3
                     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    |0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 A B 0|
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

                    The meaning of the flags is:

                    A: This is the "change IP" flag.  If true, it requests the server
                       to send the Binding Response with a different IP address than the
                       one the Binding Request was received on.

                    B: This is the "change port" flag.  If true, it requests the
                       server to send the Binding Response with a different port than the
                       one the Binding Request was received on.
                */

            // Attribute header
            msg[offset++] = (byte)(AttributeType.ChangeRequest.getValue() >> 8);
            msg[offset++] = (byte)(AttributeType.ChangeRequest.getValue() & 0xFF);
            msg[offset++] = 0;
            msg[offset++] = 4;

            msg[offset++] = 0;
            msg[offset++] = 0;
            msg[offset++] = 0;
            msg[offset++] = (byte)((this.getM_pChangeRequest().isM_ChangeIP() ? 1 : 0) << 2 | (this.getM_pChangeRequest().isM_ChangePort() ? 1 : 0) << 1);
        }
        else if (this.getM_pSourceAddress() != null)
        {
            StoreEndPoint(AttributeType.SourceAddress, this.getM_pSourceAddress(), msg, offset);
        }
        else if (this.getM_pResponseAddress() != null)
        {
            StoreEndPoint(AttributeType.ChangedAddress, this.getM_pChangedAddress(), msg, offset);
        }
        else if (this.getM_UserName() != null)
        {
            byte[] userBytes = this.getM_UserName().getBytes();
            // Attribute header
            msg[offset++] = (byte)(AttributeType.Username.getValue() >> 8);
            msg[offset++] = (byte)(AttributeType.Username.getValue() & 0xFF);
            msg[offset++] = (byte)(userBytes.length >> 8);
            msg[offset++] = (byte)(userBytes.length & 0xFF);

            for (int i = 0; i < userBytes.length; i++) {
                msg[offset + i] = userBytes[i];
            }

            offset += userBytes.length;
        }
        else if (this.getM_PassWord() != null)
        {
            byte[] userBytes = this.getM_UserName().getBytes();

            // Attribute header
            msg[offset++] = (byte)(AttributeType.Password.getValue() >> 8);
            msg[offset++] = (byte)(AttributeType.Password.getValue() & 0xFF);
            msg[offset++] = (byte)(userBytes.length >> 8);
            msg[offset++] = (byte)(userBytes.length & 0xFF);

            for (int i = 0; i < userBytes.length; i++) {
                msg[offset + i] = userBytes[i];
            }

            offset += userBytes.length;
        }
        else if (this.getM_pErrorCode() != null)
        {
                /* 3489 11.2.9.
                    0                   1                   2                   3
                    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    |                   0                     |Class|     Number    |
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    |      Reason Phrase (variable)                                ..
                    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                */

            byte[] reasonBytes = this.getM_pErrorCode().getM_ReasonText().getBytes();

            // Header
            msg[offset++] = 0;
            msg[offset++] = (byte) AttributeType.ErrorCode.getValue();
            msg[offset++] = 0;
            msg[offset++] = (byte)(4 + reasonBytes.length);

            // Empty
            msg[offset++] = 0;
            msg[offset++] = 0;
            // Class
            msg[offset++] = (byte)Math.floor((double)(this.getM_pErrorCode().getM_code() / 100));
            // Number
            msg[offset++] = (byte)(this.getM_pErrorCode().getM_code() & 0xFF);
            // ReasonPhrase
            for (int i = 0; i < reasonBytes.length; i++) {
                msg[offset + i] = reasonBytes[i];
            }

            offset += reasonBytes.length;
        }
        else if (this.getM_pReflectedFrom() != null)
        {
            StoreEndPoint(AttributeType.ReflectedFrom, this.getM_pReflectedFrom(), msg, offset);
        }

        // Update Message Length; NOTE: 20 bytes header not included
        msg[2] = (byte)((offset - 20) >> 8);
        msg[3] = (byte)((offset - 20) & 0xFF);

        //Make retVal (return value) with actual size
        byte[] retVal = new byte[offset];
        for (int i = 0; i < retVal.length; i++) {
            retVal[i] = msg[i];
        }

        return retVal;
    }

    private void StoreEndPoint(AttributeType type, IPEndPoint endPoint, byte[] message, int offset)
    {
        return;
    }

    public STUN_MessageType getM_Type() {
        return m_Type;
    }

    public void setM_Type(STUN_MessageType m_Type) {
        this.m_Type = m_Type;
    }

    public UUID getM_pTransactionID() {
        return m_pTransactionID;
    }

    public IPEndPoint getM_pMappedAddress() {
        return m_pMappedAddress;
    }

    public void setM_pMappedAddress(IPEndPoint m_pMappedAddress) {
        this.m_pMappedAddress = m_pMappedAddress;
    }

    public IPEndPoint getM_pResponseAddress() {
        return m_pResponseAddress;
    }

    public void setM_pResponseAddress(IPEndPoint m_pResponseAddress) {
        this.m_pResponseAddress = m_pResponseAddress;
    }

    public STUN_t_ChangeRequest getM_pChangeRequest() {
        return m_pChangeRequest;
    }

    public void setM_pChangeRequest(STUN_t_ChangeRequest m_pChangeRequest) {
        this.m_pChangeRequest = m_pChangeRequest;
    }

    public IPEndPoint getM_pSourceAddress() {
        return m_pSourceAddress;
    }

    public void setM_pSourceAddress(IPEndPoint m_pSourceAddress) {
        this.m_pSourceAddress = m_pSourceAddress;
    }

    public IPEndPoint getM_pChangedAddress() {
        return m_pChangedAddress;
    }

    public void setM_pChangedAddress(IPEndPoint m_pChangedAddress) {
        this.m_pChangedAddress = m_pChangedAddress;
    }

    public String getM_UserName() {
        return m_UserName;
    }

    public void setM_UserName(String m_UserName) {
        this.m_UserName = m_UserName;
    }

    public String getM_PassWord() {
        return m_PassWord;
    }

    public void setM_PassWord(String m_PassWord) {
        this.m_PassWord = m_PassWord;
    }

    public STUN_t_ErrorCode getM_pErrorCode() {
        return m_pErrorCode;
    }

    public void setM_pErrorCode(STUN_t_ErrorCode m_pErrorCode) {
        this.m_pErrorCode = m_pErrorCode;
    }

    public IPEndPoint getM_pReflectedFrom() {
        return m_pReflectedFrom;
    }

    public void setM_pReflectedFrom(IPEndPoint m_pReflectedFrom) {
        this.m_pReflectedFrom = m_pReflectedFrom;
    }

    public String getM_ServerName() {
        return m_ServerName;
    }

    public void setM_ServerName(String m_ServerName) {
        this.m_ServerName = m_ServerName;
    }
}
