package com.sensorcall.carealertandroid_new.STUN.message;


public enum STUN_MessageType {
    BindingRequest(0x0001),
    BindingResponse(0x0101),
    BindingErrorResponse(0x0111),
    SharedSecretRequest(0x0002),
    SharedSecretResponse(0x0102),
    SharedSecretErrorResponse(0x0112);

    private final int value;
    STUN_MessageType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

