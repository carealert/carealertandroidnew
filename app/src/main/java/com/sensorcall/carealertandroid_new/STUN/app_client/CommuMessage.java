package com.sensorcall.carealertandroid_new.STUN.app_client;

import com.sensorcall.carealertandroid_new.HelperClass.NetUtils.IPEndPoint;
import com.sensorcall.carealertandroid_new.STUN.client.STUN_NatType;

public class CommuMessage {
    private IPEndPoint endPoint;
    private STUN_NatType natType;

    public CommuMessage(IPEndPoint endPoint, STUN_NatType natType) {
        this.endPoint = endPoint;
        this.natType = natType;
    }

    public IPEndPoint getEndPoint() {
        return endPoint;
    }

    public STUN_NatType getNatType() {
        return natType;
    }
}

