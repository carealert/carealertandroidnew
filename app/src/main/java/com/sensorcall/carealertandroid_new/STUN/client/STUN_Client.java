package com.sensorcall.carealertandroid_new.STUN.client;


import android.util.Log;


import com.sensorcall.carealertandroid_new.HelperClass.NetUtils.IPEndPoint;
import com.sensorcall.carealertandroid_new.STUN.message.STUN_Message;
import com.sensorcall.carealertandroid_new.STUN.message.STUN_MessageType;
import com.sensorcall.carealertandroid_new.STUN.message.STUN_t_ChangeRequest;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;

public class STUN_Client {
    private static final String TAG = "STUN_Client";

    public static STUN_Result query(String host, int port, DatagramSocket socket) throws UnknownHostException {
        if (host == null) {
            Log.d(TAG, "Invalid host");
            return null;
        }
        if (socket == null) {
            Log.d(TAG, "Invalid socket");
            return null;
        }
        if (port < 1) {
            Log.d(TAG, "Port value must be >= 1");
            return null;
        }

        IPEndPoint remoteEndPoint = new IPEndPoint(InetAddress.getByName(host), port);

         /*
                In test I, the client sends a STUN Binding Request to a server, without any flags set in the
                CHANGE-REQUEST attribute, and without the RESPONSE-ADDRESS attribute. This causes the server
                to send the response back to the address and port that the request came from.

                In test II, the client sends a Binding Request with both the "change IP" and "change port" flags
                from the CHANGE-REQUEST attribute set.

                In test III, the client sends a Binding Request with only the "change port" flag set.

                                    +--------+
                                    |  Test  |
                                    |   I    |
                                    +--------+
                                         |
                                         |
                                         V
                                        /\              /\
                                     N /  \ Y          /  \ Y             +--------+
                      UDP     <-------/Resp\--------->/ IP \------------->|  Test  |
                      Blocked         \ ?  /          \Same/              |   II   |
                                       \  /            \? /               +--------+
                                        \/              \/                    |
                                                         | N                  |
                                                         |                    V
                                                         V                    /\
                                                     +--------+  Sym.      N /  \
                                                     |  Test  |  UDP    <---/Resp\
                                                     |   II   |  Firewall   \ ?  /
                                                     +--------+              \  /
                                                         |                    \/
                                                         V                     |Y
                              /\                         /\                    |
               Symmetric  N  /  \       +--------+   N  /  \                   V
                  NAT  <--- / IP \<-----|  Test  |<--- /Resp\               Open
                            \Same/      |   I    |     \ ?  /               Internet
                             \? /       +--------+      \  /
                              \/                         \/
                              |                           |Y
                              |                           |
                              |                           V
                              |                           Full
                              |                           Cone
                              V              /\
                          +--------+        /  \ Y
                          |  Test  |------>/Resp\---->Restricted
                          |   III  |       \ ?  /
                          +--------+        \  /
                                             \/
                                              |N
                                              |       Port
                                              +------>Restricted

            */

        // Test I
        STUN_Message test1 = new STUN_Message();
        test1.setM_Type(STUN_MessageType.BindingRequest);
        STUN_Message test1response = DoTransaction(test1, socket, remoteEndPoint);

        // UDP blocked
        if (test1response == null) {
            return new STUN_Result(STUN_NatType.UdpBlocked, null);
        }
        else {
            // Test II
            STUN_Message test2 = new STUN_Message();
            test2.setM_Type(STUN_MessageType.BindingRequest);
            test2.setM_pChangeRequest(new STUN_t_ChangeRequest(true, true));

            // No NAT
            if ((new IPEndPoint(socket.getLocalAddress(), socket.getLocalPort())).equals(test1response.getM_pMappedAddress())) {
                STUN_Message test2response = DoTransaction(test2, socket, remoteEndPoint);
                //Open Internet
                if (test2response != null) {
                    return new STUN_Result(STUN_NatType.OpenInternet, test1response.getM_pMappedAddress());
                }
                // Symmetric UDP firewall
                else {
                    return new STUN_Result(STUN_NatType.SymmetricUdpFirewall, test1response.getM_pMappedAddress());
                }
            }
            // NAT
            else {
                STUN_Message test2response = DoTransaction(test2, socket, remoteEndPoint);
                // Full Cone NAT
                if (test2response != null) {
                    return new STUN_Result(STUN_NatType.FullCone, test1response.getM_pMappedAddress());
                }
                else {
                        /*
                            If no response is received, it performs test I again, but this time, does so to
                            the address and port from the CHANGED-ADDRESS attribute from the response to test I.
                        */

                    // Test I(II)
                    STUN_Message test12 = new STUN_Message();
                    test12.setM_Type(STUN_MessageType.BindingRequest);

                    STUN_Message test12response = DoTransaction(test12, socket, test1.getM_pChangedAddress());
                    if (test12response == null) {
                        Log.d(TAG, "STUN test I(II) didn't get response!");
                        return null;
                    } else {
                        // Symmetric NAT
                        if (!test12response.getM_pMappedAddress().equals(test1response.getM_pMappedAddress())) {
                            return new STUN_Result(STUN_NatType.Symmetric, test1response.getM_pMappedAddress());
                        }
                        else {
                            // TEST III
                            STUN_Message test3 = new STUN_Message();
                            test3.setM_Type(STUN_MessageType.BindingRequest);
                            test3.setM_pChangeRequest(new STUN_t_ChangeRequest(false, true));

                            STUN_Message test3response = DoTransaction(test3, socket, test1response.getM_pChangedAddress());
                            // Restricted
                            if (test3response != null) {
                                return new STUN_Result(STUN_NatType.RestrictedCone, test1response.getM_pMappedAddress());
                            }
                            // Port restricted
                            else {
                                return new STUN_Result(STUN_NatType.PortRestrictedCone, test1response.getM_pMappedAddress());
                            }
                        }
                    }
                }
            }
        }
    }

    private static STUN_Message DoTransaction(STUN_Message request, DatagramSocket socket, IPEndPoint remoteEndPoint) {
        byte[] requestBytes = request.ToByteData();
        Calendar calendar = Calendar.getInstance();
        Date startTime = calendar.getTime();

        startTime.setTime(startTime.getTime() + 2000);
        while (startTime.getTime() > calendar.getTime().getTime()) {
            try {
                DatagramPacket sendPacket = new DatagramPacket(requestBytes, requestBytes.length, remoteEndPoint.getIpAddress(), remoteEndPoint.getPort());
                socket.send(sendPacket);

                byte[] receiveBuffer = new byte[512];
                DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                socket.receive(receivePacket);

                // Parse message
                STUN_Message response = new STUN_Message();
                response.parse(receiveBuffer);

                if (request.getM_pTransactionID().equals(response.getM_pTransactionID())) {
                    return response;
                }
                startTime.setTime(startTime.getTime() + 2000);
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, e.toString());
                return null;
            }
        }
        return null;
    }
}
