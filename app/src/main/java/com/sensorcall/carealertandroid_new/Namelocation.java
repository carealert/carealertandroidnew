package com.sensorcall.carealertandroid_new;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Namelocation extends AppCompatActivity {
    String location;
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    TextView location_display;
    EditText location_name;
    Button continue_button;
    ProgressDialog loaddialog,progressDialog;
    Retrofit retrofit;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    String location_nickname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CareAlert_SharedData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor=CareAlert_SharedData.edit();
        location= CareAlert_SharedData.getString("GeoLocationData","Empty found");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_namelocation);
        location_display=(TextView) findViewById(R.id.location_disp);
        continue_button= (Button) findViewById(R.id.namelocation_continue);
        location_name= (EditText) findViewById(R.id.nickname_input);
        location_display.setText(location);

        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);

        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                location_nickname = location_name.getText().toString();
                if(location_nickname.length()>0)
                    checkNetConnectivity();
                else
                    Toast.makeText(Namelocation.this, "Please enter a name for the Location.", Toast.LENGTH_SHORT).show();
               // createlocation(location_nickname);

            }
        });
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            CareAlert_SharedData_Editor.putString("LocationNickName",location_nickname);
            CareAlert_SharedData_Editor.commit();
            openSelectwho();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    public void openSelectwho(){
        Intent openselectwhopage= new Intent(this,Selectwho.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(openselectwhopage);
    }

    public  void openDeviceaddstepspage(){
        Intent openDeviceaddsteps= new Intent(this,Deviceaddsteps.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(openDeviceaddsteps);

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}