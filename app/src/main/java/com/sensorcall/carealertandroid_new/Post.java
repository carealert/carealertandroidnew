package com.sensorcall.carealertandroid_new;


public class Post {
    private int statusCode;
    private String statusDescription;
    private boolean success;
    private String message;
    private String AuthToken;
    private int LocationId;
    private float Activity;
    private float Comfort;
    private float Environment;

    public String getDate() {
        return Date;
    }

    private String Date;


    public float getActivity() {
        return Activity;
    }

    public float getComfort() {
        return Comfort;
    }

    public float getEnvironment() {
        return Environment;
    }

    private boolean FingerPrint;

    public boolean isFingerPrint() {
        return FingerPrint;
    }

    public String getAuthToken() {
        return AuthToken;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getLocationId() {
        return LocationId;
    }

    public String getMessage() {
        return message;
    }

}
