package com.sensorcall.carealertandroid_new;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Wificonnectsuccess extends AppCompatActivity {
    Button continue_button;
    ProgressDialog loaddialog,progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wificonnectsuccess);
        loaddialog=new ProgressDialog(this);
        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();

        continue_button=(Button) findViewById(R.id.wifisucess_continue);
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                opennotification();
            }
        });
    }

    public void  opennotification(){
        Intent notification= new Intent(this,Notificationsettings.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(notification);
    }
    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @Override
    public void onBackPressed() {
            Intent loginpage= new Intent(Wificonnectsuccess.this,LoginScreen.class);
            loaddialog.dismiss();
            startActivity(loginpage);
    }
}