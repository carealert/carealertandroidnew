package com.sensorcall.carealertandroid_new;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TermsandConditions extends AppCompatActivity {
    WebView Terms;
    Button Continue;

    Retrofit retrofit;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    ProgressDialog loaddialog,progressDialog;

    SharedPreferences CareAlert_StoredData;
    SharedPreferences.Editor CareAlert_StoredData_Editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termsand_conditions);
        CareAlert_StoredData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_StoredData_Editor =CareAlert_StoredData.edit();
        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        loaddialog.show();
        Terms= (WebView) findViewById(R.id.list_loc);
        retrofit=new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);

        checkNetConnectivity();

 /*       Call<GetLocation> call = jsonPlaceHolderApi.GetDisandcon();
        call.enqueue(new Callback<GetLocation>() {
            @Override
            public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                if(response.isSuccessful()){
                    GetLocation result= response.body();
                    Terms.setText(Html.fromHtml(result.getDisclaimer()));
                    CareAlert_StoredData_Editor.putString("Termandcod",result.getTermsandConditions());
                    CareAlert_StoredData_Editor.commit();
                    loaddialog.dismiss();
                }
                else{
                    Toast.makeText(TermsandConditions.this, "Failed to Fetch Terms and Conditions, Please try again.", Toast.LENGTH_SHORT).show();
                    loaddialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetLocation> call, Throwable t) {
                Toast.makeText(TermsandConditions.this, "Failed to Fetch Terms and Conditions, Please try again.", Toast.LENGTH_SHORT).show();
                loaddialog.dismiss();
            }
        });
        Continue= (Button) findViewById(R.id.enteremail_continue);
        Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent disclaimer= new Intent(TermsandConditions.this,Disclaimer.class);
                    startActivity(disclaimer);

            }
        });
*/
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            apiCall();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void apiCall(){
        Call<GetLocation> call = jsonPlaceHolderApi.GetDisandcon();
        call.enqueue(new Callback<GetLocation>() {
            @Override
            public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {
                if(response.isSuccessful()){
                    GetLocation result= response.body();
                    Terms.loadDataWithBaseURL(null,result.getDisclaimer(),"text/html","utf8",null);
                    CareAlert_StoredData_Editor.putString("Termandcod",result.getTermsandConditions());
                    CareAlert_StoredData_Editor.commit();
                    loaddialog.dismiss();
                }
                else{
                    Toast.makeText(TermsandConditions.this, "Failed to Fetch Terms and Conditions, Please try again.", Toast.LENGTH_SHORT).show();
                    loaddialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetLocation> call, Throwable t) {
                Toast.makeText(TermsandConditions.this, "Failed to Fetch Terms and Conditions, Please try again.", Toast.LENGTH_SHORT).show();
                loaddialog.dismiss();
            }
        });
        Continue= (Button) findViewById(R.id.enteremail_continue);
        Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent disclaimer= new Intent(TermsandConditions.this,Disclaimer.class);
                startActivity(disclaimer);
            }
        });

    }
    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @Override
    public void onBackPressed() {

        Intent Dis = new Intent(TermsandConditions.this, MainActivity.class);
        startActivity(Dis);
    }

}