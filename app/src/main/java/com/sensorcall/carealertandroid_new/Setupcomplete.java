package com.sensorcall.carealertandroid_new;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;

public class Setupcomplete extends AppCompatActivity {
    Button continue_button;
    ProgressDialog loaddialog,progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setupcomplete);
        loaddialog=new ProgressDialog(this);
        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();
        continue_button=(Button) findViewById(R.id.get_started);
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                continue_button.setEnabled(false);
                checkNetConnectivity();
                // opendashboard();
            }
        });
    }


    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            opendashboard();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }


    public void opendashboard(){
        Intent dashboard =new Intent(this,Dashboard.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(dashboard);
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

}