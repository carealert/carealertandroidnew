package com.sensorcall.carealertandroid_new;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersRepeatWeeklyActivity;
import com.google.android.gms.common.api.BooleanResult;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class questionnaire extends AppCompatActivity{
    //Add the Location Id and Check for The Intents while adding

    int epoch;
    String Auth_Token;
    List<GetLocation> QuestionList;
    SharedPreferences CareAlert_SharesData;
    Retrofit retrofit;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    int hour,min,myHour,myMinute,NumberofQuestions;
    JsonObject body = new JsonObject();
    Button Next;
    ProgressDialog loader,progressDialog;
    int Location_Id=0;
    LinearLayout NotificationVL;
    Boolean[] Answered;
    int i;
    String Timetoset;
    int prevepoch;
    boolean Timeset=false;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);
        loader = new ProgressDialog(questionnaire.this);
        loader=showLoadingDialog(this);
        loader.getWindow().setGravity(Gravity.CENTER);
        loader.show();
        NotificationVL= findViewById(R.id.list_loc);
        Next= (Button) findViewById(R.id.enteremail_continue);
        CareAlert_SharesData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor = CareAlert_SharesData.edit();
        Auth_Token= CareAlert_SharesData.getString("Auth_Token","");
       /* if(CareAlert_SharesData.getBoolean("AddedviaSignupProcess",true)) {
            Location_Id = CareAlert_SharesData.getInt("LocationId", 0);
        }else{
            Location_Id = CareAlert_SharesData.getInt("SecondaryLocation", 0);
        }*/
        retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);

        checkNetConnectivity();

        /*
        Call<List<GetLocation>> call = jsonPlaceHolderApi.GetQuestions("Bearer " + Auth_Token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if(response.isSuccessful()){
                    QuestionList=response.body();
                    Log.d("Questions List:", "onResponse: "+QuestionList.toString());
                    NumberofQuestions=QuestionList.size();
                    loader.dismiss();
                    Answered = new Boolean[QuestionList.size()];
                    Arrays.fill(Answered, Boolean.FALSE);
                    setupQuestions();
                }
                else{
                    loader.dismiss();
                    Toast.makeText(questionnaire.this, "Failed To Get the Questions, Try Again", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                loader.dismiss();
                Toast.makeText(questionnaire.this, "Failed To Get the Questions, Try Again", Toast.LENGTH_SHORT).show();

            }
        });


        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loader.show();
                Boolean AllAns= true;
                for(int j=0;j<Answered.length;j++){
                    if(!Answered[j]){
                        AllAns=false;
                        break;
                    }
                }

                if(AllAns){
                    sendData();
                }
                else{
                    Toast.makeText(questionnaire.this, "Please Answer all the Questions to Proceed", Toast.LENGTH_SHORT).show();
                    loader.dismiss();
                }


            }
        });
        */
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            apiCall();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void apiCall(){
        Call<List<GetLocation>> call = jsonPlaceHolderApi.GetQuestions("Bearer " + Auth_Token);
        call.enqueue(new Callback<List<GetLocation>>() {
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if(response.isSuccessful()){
                    QuestionList=response.body();
                    Log.d("Questions List:", "onResponse: "+QuestionList.toString());
                    NumberofQuestions=QuestionList.size();
                    loader.dismiss();
                    Answered = new Boolean[QuestionList.size()];
                    Arrays.fill(Answered, Boolean.FALSE);
                    setupQuestions();
                }
                else{
                    loader.dismiss();
                    Toast.makeText(questionnaire.this, "Failed To Get the Questions, Try Again", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                loader.dismiss();
                Toast.makeText(questionnaire.this, "Failed To Get the Questions, Try Again", Toast.LENGTH_SHORT).show();

            }
        });



        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loader.show();
                Boolean AllAns= true;
                for(int j=0;j<Answered.length;j++){
                    if(!Answered[j]){
                        AllAns=false;
                        break;
                    }
                }

                if(AllAns){
                    //sendData();
                    createlocation();
                }
                else{
                    Toast.makeText(questionnaire.this, "Please Answer all the Questions to Proceed", Toast.LENGTH_SHORT).show();
                    loader.dismiss();
                }


            }
        });
    }


    public void setupQuestions(){
        Log.d("Number of Ques", "setupQuestions: "+QuestionList.size());
        for (i = 0; i < QuestionList.size(); i++) {
            Log.d("Question No:", "setupQuestions: "+i);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            View HL = LayoutInflater.from(getApplicationContext()).inflate(R.layout.questionsll,
                    findViewById(R.id.Question_LL));
            //layoutParams.setMargins(30, 5, 30, 5);
            //HL.setLayoutParams(layoutParams);
            HL.setId(i);
            TextView Questions = HL.findViewById(R.id.question);
            EditText Answers = HL.findViewById(R.id.ans);
            String QuestionDef=((QuestionList.get(i)).getQuestion()).toString().replace("\"","");
            Log.d("Question is", "setupQuestions: "+QuestionDef);
            Questions.setText(QuestionDef);
            NotificationVL.addView(HL);
            Log.d("Type", "setupQuestions: "+((QuestionList.get(i)).getDataType()).toString().replace("\"",""));
            if(((QuestionList.get(i)).getDataType()).toString().replace("\"","").equals("int")){
                Answers.setInputType(InputType.TYPE_CLASS_NUMBER);
                Answers.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
                Answers.setShowSoftInputOnFocus(true);

                Answers.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if(Answers.getText().length()>0){
                            Answered[HL.getId()]=true;
                            body.addProperty(QuestionList.get(HL.getId()).getKey().toString().replace("\"",""),Integer.parseInt(Answers.getText().toString()));
                        }
                        else{
                            Answered[HL.getId()]=false;
                        }
                    }
                });
            }
            if(((QuestionList.get(i)).getDataType()).toString().replace("\"","").equals("Time")){

                Answers.setShowSoftInputOnFocus(false);

//                Answers.addTextChangedListener(new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//
//                    @Override
//                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable editable) {
//                        if(Answers.getText().length()>0){
//                            Answered[HL.getId()]=true;
//                            body.addProperty(QuestionList.get(HL.getId()).getKey().toString().replace("\"",""),epoch);
//                        }
//                        else{
//                            Answered[HL.getId()]=false;
//                            body.addProperty(QuestionList.get(HL.getId()).getKey().toString().replace("\"",""),epoch);
//
//                        }
//                    }
//                });

                Answers.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if(b){
                            Timetoset=Answers.getText().toString();
                            Calendar c = Calendar.getInstance();
                            hour = c.get(Calendar.HOUR);
                            min = c.get(Calendar.MINUTE);
                            //int am = c.get(Calendar.AM_PM);
                            TimePickerDialog timePickerDialog = new TimePickerDialog(questionnaire.this, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int h, int t) {
                                    myHour = h;
                                    myMinute = t;

                                    String Time="";
                                    if(myHour>11){
                                        String HourString=Integer.toString(myHour-12);
                                        if  (HourString.equals("0")){
                                            HourString="12";
                                        }
                                        if(myMinute<10){
                                            Time= HourString+":0"+Integer.toString(myMinute)+" PM";
                                        }
                                        else
                                        {
                                            Time= HourString+":"+Integer.toString(myMinute)+" PM";
                                        }
                                    }
                                    else{
                                        String HourString=Integer.toString(myHour);
                                        if  (HourString.equals("0")){
                                            HourString="12";
                                        }
                                        if(myMinute<10){
                                            Time= HourString+":0"+Integer.toString(myMinute)+" AM";
                                        }
                                        else
                                        {
                                            Time= HourString+":"+Integer.toString(myMinute)+" AM";
                                        }

                                    }
                                    Answers.setText(Time);
                                    epoch=(myHour*3600)+(myMinute*60);
                                    if(Answers.getText().length()>0){
                                        Answered[HL.getId()]=true;
                                        body.addProperty(QuestionList.get(HL.getId()).getKey().toString().replace("\"",""),epoch);
                                    }
                                    else{
                                        Answered[HL.getId()]=false;
                                        body.addProperty(QuestionList.get(HL.getId()).getKey().toString().replace("\"",""),epoch);

                                    }

                                }
                            }, hour, min, false);

                            timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {

                                }
                            });

                            timePickerDialog.show();
                        }
                    }
                });
                Answers.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN) {
                            Timetoset=Answers.getText().toString();
                            Calendar c = Calendar.getInstance();
                            hour = c.get(Calendar.HOUR);
                            min = c.get(Calendar.MINUTE);
                            //int am = c.get(Calendar.AM_PM);
                            TimePickerDialog timePickerDialog = new TimePickerDialog(questionnaire.this, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int h, int t) {
                                    myHour = h;
                                    myMinute = t;

                                    String Time="";
                                    if(myHour>11){
                                        String HourString=Integer.toString(myHour-12);
                                        if  (HourString.equals("0")){
                                            HourString="12";
                                        }
                                        if(myMinute<10){
                                            Time= HourString+":0"+Integer.toString(myMinute)+" PM";
                                        }
                                        else
                                        {
                                            Time= HourString+":"+Integer.toString(myMinute)+" PM";
                                        }
                                    }
                                    else{
                                        String HourString=Integer.toString(myHour);
                                        if  (HourString.equals("0")){
                                            HourString="12";
                                        }
                                        if(myMinute<10){
                                            Time= HourString+":0"+Integer.toString(myMinute)+" AM";
                                        }
                                        else
                                        {
                                            Time= HourString+":"+Integer.toString(myMinute)+" AM";
                                        }

                                    }
                                    Answers.setText(Time);
                                    epoch=(myHour*3600)+(myMinute*60);
                                    if(Answers.getText().length()>0){
                                        Answered[HL.getId()]=true;
                                        body.addProperty(QuestionList.get(HL.getId()).getKey().toString().replace("\"",""),epoch);
                                    }
                                    else{
                                        Answered[HL.getId()]=false;
                                        body.addProperty(QuestionList.get(HL.getId()).getKey().toString().replace("\"",""),epoch);

                                    }

                                }
                            }, hour, min, false);

                            timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {

                                }
                            });

                            timePickerDialog.show();

                        }
                        return true;
                    }
                });
            }
        }
    }

    public void opendevicesetup() {
        Intent devivesetup = new Intent(this, Devicesetuppluginvideo.class);
        loader.dismiss();
        startActivity(devivesetup);
    }


    public void sendData(){
        body.addProperty("LocationId",Location_Id);
        Log.d("Body to send", "sendData: "+body.toString());
        Call<JsonObject> call = jsonPlaceHolderApi.AnswerQuestions("Bearer " + Auth_Token,body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()){
                    loader.dismiss();
                    if(CareAlert_SharesData.getBoolean("AddedviaSignupProcess",true)) {
                        opendevicesetup();
                    }
                    else{
                        Intent addlocation= new Intent(questionnaire.this,AddLocation.class);
                        addlocation.putExtra("DefaultLocationId",CareAlert_SharesData.getInt("LocationId",0));
                        addlocation.putExtra("locname",CareAlert_SharesData.getString("locname",""));
                        addlocation.putExtra("locadd",CareAlert_SharesData.getString("locadd",""));
                        loader.dismiss();
                        startActivity(addlocation);
                    }

                }
                else{
                    loader.dismiss();
                    Toast.makeText(questionnaire.this, "Failed To Update the Responses, Try Again", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                loader.dismiss();
                Toast.makeText(questionnaire.this, "Failed To Update the Responses, Try Again", Toast.LENGTH_SHORT).show();
            }
        });

    }

//    @Override
//    public void onTimeSet(TimePicker timePicker, int h, int t) {
//        myHour = h;
//        myMinute = t;
//
//        String Time="";
//        if(myHour>11){
//            String HourString=Integer.toString(myHour-12);
//            if  (HourString.equals("0")){
//                HourString="12";
//            }
//            if(myMinute<10){
//                Time= HourString+":0"+Integer.toString(myMinute)+" PM";
//            }
//            else
//            {
//                Time= HourString+":"+Integer.toString(myMinute)+" PM";
//            }
//        }
//        else{
//            String HourString=Integer.toString(myHour);
//            if  (HourString.equals("0")){
//                HourString="12";
//            }
//            if(myMinute<10){
//                Time= HourString+":0"+Integer.toString(myMinute)+" AM";
//            }
//            else
//            {
//                Time= HourString+":"+Integer.toString(myMinute)+" AM";
//            }
//
//        }
//
//        Timetoset=Time;
//        epoch=(myHour*3600)+(myMinute*60);
//    }




    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }


    @Override
    public void onBackPressed() {

        if(CareAlert_SharesData.getBoolean("AddedviaSignupProcess",true)) {
            Intent loginpage= new Intent(questionnaire.this,LoginScreen.class);
            loader.dismiss();
            startActivity(loginpage);
        }
        else {
            //Check from where the App came had to be added
            Toast.makeText(questionnaire.this, "Address and other entered will be deleted", Toast.LENGTH_SHORT).show();
            Intent addlocation = new Intent(questionnaire.this, AddLocation.class);
            addlocation.putExtra("DefaultLocationId", CareAlert_SharesData.getInt("LocationId", 0));
            addlocation.putExtra("locname", CareAlert_SharesData.getString("locname", ""));
            addlocation.putExtra("locadd", CareAlert_SharesData.getString("locadd", ""));
            loader.dismiss();
            startActivity(addlocation);
        }
    }
    public void createlocation(){
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        JsonObject body=new JsonObject();
        String LocationName= CareAlert_SharesData.getString("LocationNickName","");
        String location= CareAlert_SharesData.getString("GeoLocationData","Empty found");
        String Name=CareAlert_SharesData.getString("Name","");
        String ImageName= CareAlert_SharesData.getString("Img_Name","");
        int age = CareAlert_SharesData.getInt("age",0);
        body.addProperty("LocationName",LocationName);
        body.addProperty("LocationAddress",location);
        body.addProperty("GeoLocation",CareAlert_SharesData.getString("Latitude","")+","+CareAlert_SharesData.getString("Longitude",""));
        if(!CareAlert_SharesData.getBoolean("AddedviaSignupProcess",true))
            body.addProperty("IsDefaultLocationId",false);
        Call<Post> call= jsonPlaceHolderApi.createlocation("Bearer "+Auth_Token,body);
        call.enqueue(new Callback<Post>() {

            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(!CareAlert_SharesData.getBoolean("AddedviaSignupProcess",true)){
                    CareAlert_SharedData_Editor.putInt("SecondaryLocation",response.body().getLocationId());
                    CareAlert_SharedData_Editor.commit();

                }
                else {
                    int Locationid = response.body().getLocationId();
                    CareAlert_SharedData_Editor.putInt("LocationId", Locationid);
                    CareAlert_SharedData_Editor.commit();

                }
                Location_Id = response.body().getLocationId();
                JsonObject LovedonesBody= new JsonObject();
                LovedonesBody.addProperty("LovedOneProfilePicture",ImageName);
                LovedonesBody.addProperty("Age",age);
                LovedonesBody.addProperty("ProfileName",Name);
                LovedonesBody.addProperty("LocationId",response.body().getLocationId());
                Call LovedOneCreate= jsonPlaceHolderApi.createlovedones("Bearer "+Auth_Token,LovedonesBody);
                LovedOneCreate.enqueue(new Callback() {

                    @Override
                    public void onResponse(Call call, Response response) {
                        if(response.isSuccessful()){
                            sendData();
                        }else{
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                String Error = jObjError.getString("message");
                                loader.dismiss();
                                Toast.makeText(questionnaire.this, Error, Toast.LENGTH_SHORT).show();

                            } catch (Exception e) {
                                loader.dismiss();
                                Toast.makeText(questionnaire.this, "Unknown Error", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        loader.dismiss();
                        Toast.makeText(questionnaire.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                loader.dismiss();
                Toast.makeText(questionnaire.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}