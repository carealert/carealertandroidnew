package com.sensorcall.carealertandroid_new;

import java.util.ArrayList;

public class NotificationStatus {

    public class Devicenotification{
        int DeviceId;
        boolean AQINotifications;
        boolean TempratureNotifications;
        boolean HumidityNotifications;

        public int getDeviceId() {
            return DeviceId;
        }

        public boolean isAQINotifications() {
            return AQINotifications;
        }

        public boolean isTempratureNotifications() {
            return TempratureNotifications;
        }

        public boolean isHumidityNotifications() {
            return HumidityNotifications;
        }

        public boolean isActNotifications() {
            return ActNotifications;
        }

        boolean ActNotifications;
    }

    public boolean isUserNotifications() {
        return UserNotifications;
    }

    public int getLocationId() {
        return LocationId;
    }

    public ArrayList<Devicenotification> getDeviceNotifiactions() {
        return DeviceNotifiactions;
    }

    public boolean UserNotifications;
    public int LocationId;
    ArrayList<Devicenotification> DeviceNotifiactions;

}


