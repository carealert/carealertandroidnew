package com.sensorcall.carealertandroid_new;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.CustomBottomNavigationView1;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.JsonPlaceHolder;
import com.sensorcall.carealertandroid_new.VoiceCallingClasses.SelectToCall;
import com.sensorcall.carealertandroid_new.VoiceReminder.RemindersViewActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddLocation extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private NetworkStateReceiver networkStateReceiver;
    Button add_loc;
    ImageView Heal, Conn;
    JsonPlaceHolderApi get_api;
    ListView list;
    Drawable GoodHealth,BadHealth,Connected,Disconnected;
    LinearLayout DefLoc;
    SharedPreferences CareAlert_SharedData;
    TextView txt1, def_loc_name, def_loc_add;
    ProgressDialog loader, progressDialog;
    AlertDialog.Builder builder;
    JsonPlaceHolderApi getdevice_api;
    List<GetDeviceByLocationId> getDevice;
    String Auth;
    boolean ConnectionD=false,healthD=false;
    String str1,str2;
    int default_id_initial = -1;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    CustomBottomNavigationView1 curvedBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);
        startNetworkBroadcastReceiver(this);

        DefLoc=(LinearLayout) findViewById(R.id.rectangle_BG_1);
        GoodHealth=getResources().getDrawable(getResources().getIdentifier("@drawable/ic_active", null, getPackageName()));
        BadHealth=getResources().getDrawable(getResources().getIdentifier("@drawable/ic_warning", null, getPackageName()));
        Connected=getResources().getDrawable(getResources().getIdentifier("@drawable/ic_wifi", null, getPackageName()));
        Disconnected=getResources().getDrawable(getResources().getIdentifier("@drawable/ic_wifi_not", null, getPackageName()));

        loader = new ProgressDialog(this);
        loader=showLoadingDialog(this);
        CareAlert_SharedData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor=CareAlert_SharedData.edit();
        Auth=CareAlert_SharedData.getString("Auth_Token","");
        add_loc = (Button) findViewById(R.id.mainbutton);
        def_loc_name = (TextView) findViewById(R.id.default_loc_name);
        def_loc_add = (TextView) findViewById(R.id.default_loc_add);
        Heal = (ImageView) findViewById(R.id.Health);
        Conn = (ImageView) findViewById(R.id.Conn);

        list = findViewById(R.id.list_loc);

        Intent i = getIntent();
        str1 = i.getStringExtra("locname");
        str2 = i.getStringExtra("locadd");
        default_id_initial = i.getIntExtra("DefaultLocationId", 0);
        // Toast.makeText(getApplicationContext(), "Default" + default_id_initial, Toast.LENGTH_SHORT).show();
        def_loc_add.setText(str2);
        def_loc_name.setText(str1);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getdevice_api = retrofit.create(JsonPlaceHolderApi.class);
        Call<List<GetDeviceByLocationId>> call = getdevice_api.getDevice(default_id_initial,"Bearer "+Auth);

        call.enqueue(new Callback<List<GetDeviceByLocationId>>() {
            @Override
            public void onResponse(Call<List<GetDeviceByLocationId>> call, Response<List<GetDeviceByLocationId>> response) {
                if(response.isSuccessful()){
                    getDevice=response.body();
                    if(getDevice.size()!=0){
                        for(int i=0;i<getDevice.size();i++){
                            GetDeviceByLocationId DeviceInfo= getDevice.get(i);
                            if(DeviceInfo.DeviceStatus!=3){
                                ConnectionD=true;
                            }
                            if(DeviceInfo.HealthStatus!=2){
                                healthD=true;
                            }
                        }

                        if(healthD){
                            Heal.setImageDrawable(BadHealth);
                        }
                        else{
                            Heal.setImageDrawable(GoodHealth);
                        }
                        if(ConnectionD) {
                            Conn.setImageDrawable(Disconnected);
                        }
                        else{
                            Conn.setImageDrawable(Connected);
                        }
                    }
                    else{
                        Conn.setImageDrawable(Disconnected);
                        Heal.setImageDrawable(BadHealth);
                    }
                }
                else{
                    Conn.setImageDrawable(Disconnected);
                    Heal.setImageDrawable(BadHealth);
                }
            }

            @Override
            public void onFailure(Call<List<GetDeviceByLocationId>> call, Throwable t) {
                Conn.setImageDrawable(Disconnected);
                Heal.setImageDrawable(BadHealth);

            }
        });
        add_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loader.show();
                CareAlert_SharedData_Editor.putBoolean("AddedviaSignupProcess",false);
                CareAlert_SharedData_Editor.putInt("LocationId",default_id_initial);
                CareAlert_SharedData_Editor.putString("locname",str1);
                CareAlert_SharedData_Editor.putString("locadd",str2);
                CareAlert_SharedData_Editor.commit();
                Intent openNameDeviceRoom= new Intent(AddLocation.this,Deviceaddsteps.class);
                loader.dismiss();
                startActivity(openNameDeviceRoom);

            }
        });
    }


    @IntRange(from = 0, to = 3)
    public static int getConnectionType(Context context) {
        int result = 0; // Returns connection type. 0: none; 1: mobile data; 2: wifi
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm != null) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = 2;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = 1;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)) {
                        result = 3;
                    }
                }
            }
        } else {
            if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null) {
                    // connected to the internet
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                        result = 2;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        result = 1;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_VPN) {
                        result = 3;
                    }
                }
            }
        }
        return result;
    }

    public void alert() {
        builder = new AlertDialog.Builder(this);
        builder.setMessage("Cannot load the contents. Make sure your phone has an internet connection " +
                "and try again.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }

    public void alert1() {
        builder = new AlertDialog.Builder(this);
        builder.setMessage("Cannot load the contents. Make sure your phone has an internet connection " +
                "and try again.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }


    @Override
    protected void onPause() {
        /***/
        unregisterNetworkBroadcastReceiver(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        /***/
        registerNetworkBroadcastReceiver(this);
        super.onResume();
    }

    public void startNetworkBroadcastReceiver(Context currentContext) {
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener((NetworkStateReceiver.NetworkStateReceiverListener) currentContext);
        registerNetworkBroadcastReceiver(currentContext);
    }

    /**
     * Register the NetworkStateReceiver with your activity
     * @param currentContext
     */
    public void registerNetworkBroadcastReceiver(Context currentContext) {
        currentContext.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     Unregister the NetworkStateReceiver with your activity
     * @param currentContext
     */
    public void unregisterNetworkBroadcastReceiver(Context currentContext) {
        currentContext.unregisterReceiver(networkStateReceiver);
    }


    @Override
    public void networkAvailable()
    {
        Log.i("Availability", "networkAvailable()");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        get_api = retrofit.create(JsonPlaceHolderApi.class);
        //Proceed with online actions in activity (e.g. hide offline UI from user, start services, etc...)
        loader.show();

        Call<List<GetLocation>> call = get_api.getLocation("Bearer " + Auth);
        call.enqueue(new Callback<List<GetLocation>>() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                if (response == null) {
                    Log.d("Null ", " API Response is null");
                } else {

                    Log.d("Response", " Location API data fetched");

                }

                if (response.isSuccessful()) {

                    List<GetLocation> get_list = response.body();
                    for (int i = 0; i < get_list.size(); i++) {
                        if (get_list.get(i).getLocationId() == default_id_initial) {
                            get_list.remove(i);

                        }
                    }


                    //loader.setContentView(R.layout.progress_dialog);
                    //   loader.getWindow().setBackgroundDrawableResource(android.R.color.black);

                    ManageLocationAdapter adapter = new ManageLocationAdapter(get_list, getApplicationContext(),GoodHealth,BadHealth,Connected,Disconnected,Auth);
                    list.setAdapter(adapter);
                    loader.dismiss();

                    list.setOnTouchListener(new ListView.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            int action = motionEvent.getAction();
                            switch (action) {
                                case MotionEvent.ACTION_DOWN:
                                    // Disallow ScrollView to intercept touch events.
                                    view.getParent().requestDisallowInterceptTouchEvent(true);
                                    break;

                                case MotionEvent.ACTION_UP:
                                    // Allow ScrollView to intercept touch events.
                                    view.getParent().requestDisallowInterceptTouchEvent(false);
                                    break;
                            }
                            // Handle ListView touch events.
                            view.onTouchEvent(motionEvent);
                            return true;
                        }
                    });

                    DefLoc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            BottomSheetDialog bottomsheet = new BottomSheetDialog(AddLocation.this, R.style.BottomSheetDialogTheme);

                            View bottomSheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.bottomsheet_mnage_list_loc,
                                    findViewById(R.id.bottom_sheet_container));
                            bottomsheet.setContentView(bottomSheetView);
                            bottomsheet.setCanceledOnTouchOutside(false);

                            TextView txt1 = bottomsheet.findViewById(R.id.LocRoom);
                            TextView txt2 = bottomsheet.findViewById(R.id.LocRoomaddr);
                            txt1.setVisibility(View.VISIBLE);
                            txt2.setVisibility(View.VISIBLE);
                            if (str1 != null)
                                txt1.setText(str1);
                            if (str2 != null)
                                txt2.setText(str2);

                            bottomsheet.show();

                            Button edit_loc = bottomsheet.findViewById(R.id.edit_loc__btn);
                            Button dismiss = bottomsheet.findViewById(R.id.dismiss_btn);
                            Button default_set = bottomsheet.findViewById(R.id.set_as_btn);



                            edit_loc.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    bottomsheet.setContentView(R.layout.bottomsheet_edit_location);
                                    int locid = default_id_initial;
                                    EditText edit_locname = bottomsheet.findViewById(R.id.edit_locname);
                                    edit_locname.setText(str1);
                                    EditText edit_locadd = bottomsheet.findViewById(R.id.edit_locadd);
                                    edit_locadd.setText(str2);

                                    bottomsheet.setCanceledOnTouchOutside(false);
                                    Button continue_edit = bottomsheet.findViewById(R.id.conti_btn);

                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl(Constants.BASE_URL)
                                            .addConverterFactory(GsonConverterFactory.create())
                                            .build();

                                    get_api = retrofit.create(JsonPlaceHolderApi.class);


                                    continue_edit.setOnClickListener(new View.OnClickListener() {


                                        @Override
                                        public void onClick(View view) {
                                            if (TextUtils.isEmpty(edit_locadd.getText()))
                                                Toast.makeText(AddLocation.this, "Address can not Empty", Toast.LENGTH_SHORT).show();
                                            else if (TextUtils.isEmpty(edit_locname.getText()))
                                                Toast.makeText(AddLocation.this, "Location Name can not be Empty", Toast.LENGTH_SHORT).show();
                                            else {
                                                loader.setMessage("Loading...");
                                                loader.show();
                                                String name = edit_locname.getText().toString();
                                                String add = edit_locadd.getText().toString();

                                                edit_locname.setText(name);
                                                edit_locadd.setText(add);


                                                JsonObject body = new JsonObject();
                                                body.addProperty("LocationId", locid);
                                                body.addProperty("LocationName", name);
                                                body.addProperty("LocationAddress", add);
                                                Call<GetLocation> call1 = get_api.updateLocation("Bearer " + Auth, body);
                                                bottomsheet.dismiss();
                                                call1.enqueue(new Callback<GetLocation>() {
                                                    @Override
                                                    public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {

                                                        if (response.isSuccessful()) {
                                                            Toast.makeText(AddLocation.this, "Updated Address ", Toast.LENGTH_SHORT).show();
                                                            startNetworkBroadcastReceiver(AddLocation.this);
                                                            def_loc_add.setText(add);
                                                            def_loc_name.setText(name);
                                                            //  Call<List<GetLocation>> call2 = get_api.getLocation("Bearer " + Auth);
                                                        } else {
                                                            try {
                                                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                                                loader.dismiss();
                                                                String s = jObjError.getJSONObject("error").getString("message");
                                                                Log.d("Error", s);
                                                            } catch (Exception e) {
                                                                // Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                                Log.d("Error Handler", e.getMessage());
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<GetLocation> call, Throwable t) {
                                                        loader.dismiss();
                                                        alert();
                                                        Log.d("Failure", "Fails to Fetched Location");

                                                        //Toast.makeText(AddLocation.this, "Fails to update location", Toast.LENGTH_SHORT).show();
                                                    }


                                                });
                                            }
                                        }

                                    });
                                }
                            });


                            dismiss.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    bottomsheet.dismiss();
                                }
                            });


                            default_set.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Toast.makeText(AddLocation.this, "Alerady Set as Default Location", Toast.LENGTH_SHORT).show();

                                }
                            });

                        }
                    });


                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        BottomSheetDialog bottomsheet;

                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            bottomsheet = new BottomSheetDialog(AddLocation.this, R.style.BottomSheetDialogTheme);

                            View bottomSheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.bottomsheet_mnage_list_loc,
                                    findViewById(R.id.bottom_sheet_container));
                            bottomsheet.setContentView(bottomSheetView);
                            bottomsheet.setCanceledOnTouchOutside(false);

                            TextView txt1 = bottomsheet.findViewById(R.id.LocRoom);
                            TextView txt2 = bottomsheet.findViewById(R.id.LocRoomaddr);
                            txt1.setVisibility(View.VISIBLE);
                            txt2.setVisibility(View.VISIBLE);
                            String str1 = get_list.get(i).getLocationName();
                            String str2 = get_list.get(i).getLocationAddress();
                            if (str1 != null)
                                txt1.setText(get_list.get(i).getLocationName());
                            if (str2 != null)
                                txt2.setText(get_list.get(i).getLocationAddress());

                            bottomsheet.show();

                            Button edit_loc = bottomsheet.findViewById(R.id.edit_loc__btn);
                            Button dismiss = bottomsheet.findViewById(R.id.dismiss_btn);
                            Button default_set = bottomsheet.findViewById(R.id.set_as_btn);


                            edit_loc.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    bottomsheet.setContentView(R.layout.bottomsheet_edit_location);
                                    int locid = get_list.get(i).getLocationId();
                                    EditText edit_locname = bottomsheet.findViewById(R.id.edit_locname);
                                    edit_locname.setText(get_list.get(i).getLocationName());
                                    EditText edit_locadd = bottomsheet.findViewById(R.id.edit_locadd);
                                    edit_locadd.setText(get_list.get(i).getLocationAddress());

                                    bottomsheet.setCanceledOnTouchOutside(false);
                                    Button continue_edit = bottomsheet.findViewById(R.id.conti_btn);

                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl(Constants.BASE_URL)
                                            .addConverterFactory(GsonConverterFactory.create())
                                            .build();

                                    get_api = retrofit.create(JsonPlaceHolderApi.class);


                                    continue_edit.setOnClickListener(new View.OnClickListener() {


                                        @Override
                                        public void onClick(View view) {
                                            if (TextUtils.isEmpty(edit_locadd.getText()))
                                                Toast.makeText(AddLocation.this, "Address can not Empty", Toast.LENGTH_SHORT).show();
                                            else if (TextUtils.isEmpty(edit_locname.getText()))
                                                Toast.makeText(AddLocation.this, "Location Name can not be Empty", Toast.LENGTH_SHORT).show();
                                            else {
                                                loader.setMessage("Loading...");
                                                loader.show();
                                                String name = edit_locname.getText().toString();
                                                String add = edit_locadd.getText().toString();

                                                edit_locname.setText(name);
                                                edit_locadd.setText(add);


                                                get_list.get(i).setLocationName(name);
                                                get_list.get(i).setLocationAddress(add);

                                                JsonObject body = new JsonObject();
                                                body.addProperty("LocationId", locid);
                                                body.addProperty("LocationName", name);
                                                body.addProperty("LocationAddress", add);
                                                Call<GetLocation> call1 = get_api.updateLocation("Bearer " + Auth, body);
                                                bottomsheet.dismiss();
                                                call1.enqueue(new Callback<GetLocation>() {
                                                    @Override
                                                    public void onResponse(Call<GetLocation> call, Response<GetLocation> response) {

                                                        if (response.isSuccessful()) {
                                                            Toast.makeText(AddLocation.this, "Updated Address ", Toast.LENGTH_SHORT).show();
                                                            startNetworkBroadcastReceiver(AddLocation.this);
                                                            //  Call<List<GetLocation>> call2 = get_api.getLocation("Bearer " + Auth);
                                                            loader.dismiss();
                                                        } else {
                                                            try {
                                                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                                                loader.dismiss();
                                                                String s = jObjError.getJSONObject("error").getString("message");
                                                                Log.d("Error", s);
                                                            } catch (Exception e) {
                                                                // Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                                Log.d("Error Handler", e.getMessage());
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<GetLocation> call, Throwable t) {
                                                        loader.dismiss();
                                                        alert();
                                                        Log.d("Failure", "Fails to Fetched Location");

                                                        //Toast.makeText(AddLocation.this, "Fails to update location", Toast.LENGTH_SHORT).show();
                                                    }


                                                });
                                            }
                                        }

                                    });
                                }
                            });


                            dismiss.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    bottomsheet.dismiss();
                                }
                            });


                            default_set.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    loader.setMessage("Loading...");
                                    loader.show();
                                    boolean default_loc_id = false;
                                    int new_loc = get_list.get(i).getLocationId();
                                    Call<List<GetLocation>> call2 = get_api.setDefaultLocation(new_loc, "Bearer " + Auth);
                                    call2.enqueue(new Callback<List<GetLocation>>() {
                                        @Override
                                        public void onResponse(Call<List<GetLocation>> call, Response<List<GetLocation>> response) {
                                            if (response.isSuccessful()) {
                                                loader.dismiss();
                                                bottomsheet.dismiss();
                                                Toast.makeText(getApplicationContext(), "Default location Updated" , Toast.LENGTH_SHORT).show();

                                                Intent refresh= new Intent(AddLocation.this,AddLocation.class);
                                                refresh.putExtra("locname",get_list.get(i).getLocationName());
                                                refresh.putExtra("locadd",get_list.get(i).getLocationAddress());
                                                refresh.putExtra("DefaultLocationId",new_loc);
                                                loader.dismiss();
                                                startActivity(refresh);
                                                //  Toast.makeText(AddLocation.this, "Updated new DefaultLocation ", Toast.LENGTH_SHORT).show();
                                              } else {
                                                try {
                                                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                                                    loader.dismiss();
                                                    String s=jObjError.getJSONObject("error").getString("message");
                                                    Log.d("Error", s);
                                                } catch (Exception e) {
                                                    // Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                    Log.d("Error Handler", e.getMessage());
                                                }


                                                bottomsheet.dismiss();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                                            loader.dismiss();
                                            bottomsheet.dismiss();
                                            alert();
                                            // Toast.makeText(AddLocation.this, "Fails to update new default  location ", Toast.LENGTH_LONG).show();
                                            Log.d("Default_Loc_Failure","Fails to update default Location");

                                        }

                                    });

                                }
                            });

                        }


                    });
                }
            }

            @Override
            public void onFailure(Call<List<GetLocation>> call, Throwable t) {
                loader.dismiss();
                // alert1();
                Log.d("Failure","Fails inflating the list");

                //Toast.makeText(AddLocation.this, "Fails to update new default location ", Toast.LENGTH_LONG).show();

            }

        });

    }

    @Override
    public void networkUnavailable() {

        Log.i("unavailability", "networkUnavailable()");
        //Proceed with offline actions in activity (e.g. sInform user they are offline, stop services, etc...)
        Log.d("Connected","False");
//        Toast.makeText(getApplicationContext(), "Not Connected",
//                Toast.LENGTH_LONG).show();
    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @Override
    public void onBackPressed() {
        //Check from where the App came had to be added

        Intent opensettings = new Intent(AddLocation.this, Settings.class);
        startActivity(opensettings);
        return;
    }
}
