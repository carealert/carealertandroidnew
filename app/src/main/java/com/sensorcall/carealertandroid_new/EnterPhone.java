package com.sensorcall.carealertandroid_new;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.hbb20.CountryCodePicker;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class EnterPhone extends AppCompatActivity {

    private Button continue_button;
    private EditText phone_input;
    String Country_code="+1";
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    CountryCodePicker ccp;
    private String phone_number;
    boolean reset;
    Retrofit retrofit;
    SharedPreferences CareAlert_StoredData;
    SharedPreferences.Editor CareAlert_StoredData_Editor;
    ProgressDialog loaddialog,progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_phone);
        //
        CareAlert_StoredData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_StoredData_Editor =CareAlert_StoredData.edit();
        reset =CareAlert_StoredData.getBoolean("Reset",false);
        continue_button= (Button) findViewById(R.id.enterphone_continue);
        phone_input=(EditText) findViewById(R.id.phone_input);
        ccp= (CountryCodePicker) findViewById(R.id.ccp);
        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();

        retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                Country_code=ccp.getSelectedCountryCodeWithPlus();

            }
        });

        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkNetConnectivity();
            }
            });
/*
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phone_number=phone_input.getText().toString();
                if (phone_number.length()>10){
                    Toast.makeText(EnterPhone.this, "Phone Number cannot be greater than 10 digits", Toast.LENGTH_SHORT).show();
                    phone_input.setText("");
                }
                else if (phone_number.length()<1){
                    Toast.makeText(EnterPhone.this, "Phone Number cannot be Blank", Toast.LENGTH_SHORT).show();
                }
                else {
                    loaddialog.show();
                    continue_button.setEnabled(false);
                    if (reset) {
                        sendresetotp();
                    } else {
                        sendfirsttimeotp();
                    }
                }

            }
        });
  */
    }

    void checkNetConnectivity()
    {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet","devic123");
            continueToNextpage();
        }
        else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void showNoInternetConnectionDialog(){
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
        //  Log.e("Testing net Connection", "Showed NoIntenetConnectionDialog");
    }

    public void continueToNextpage(){
                phone_number=phone_input.getText().toString();
                if (phone_number.length()>10){
                    Toast.makeText(EnterPhone.this, "Phone Number cannot be greater than 10 digits", Toast.LENGTH_SHORT).show();
                    phone_input.setText("");
                }
                else if (phone_number.length()<1){
                    Toast.makeText(EnterPhone.this, "Phone Number cannot be Blank", Toast.LENGTH_SHORT).show();
                }
                else {
                    loaddialog.show();
                    continue_button.setEnabled(false);
                    if (reset) {
                        sendresetotp();
                    } else {
                        sendfirsttimeotp();
                    }
                }
            }

    public void openEnterOtpPage(){
        CareAlert_StoredData_Editor.putString("Phone_Number",phone_number);
        CareAlert_StoredData_Editor.commit();
        Intent EnterOtpPage=new Intent(this,Enterotp.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(EnterOtpPage);
    }
    public void openHomePage() {
        Intent HomePage =new Intent(this, MainActivity.class);
        continue_button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(HomePage);
    }

    public void  openLoginPage(){
        Intent LoginPage= new Intent(this,LoginScreen.class);
        startActivity(LoginPage);
    }

    public void sendfirsttimeotp(){

        phone_number = Country_code + phone_number;
        Call<Post> call = jsonPlaceHolderApi.createpost(phone_number);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                if (response.isSuccessful()) {
                    openEnterOtpPage();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String Error = jObjError.getString("message");
                        Toast.makeText(EnterPhone.this, Error, Toast.LENGTH_SHORT).show();
                        phone_input.setText("");
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                    } catch (Exception e) {
                        Toast.makeText(EnterPhone.this, "Unknown Error", Toast.LENGTH_SHORT).show();
                        phone_input.setText("");
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                    }
                }


            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                phone_input.setText("");
                Toast.makeText(EnterPhone.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                continue_button.setEnabled(true);
                loaddialog.dismiss();
            }
        });
    }

    public void sendresetotp(){
        phone_number=Country_code+phone_input.getText().toString();
        Call<Post> call= jsonPlaceHolderApi.createpostresetpasswordreq(phone_number);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                if (response.isSuccessful()) {
                    openEnterOtpPage();
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String Error = jObjError.getString("message");
                        Toast.makeText(EnterPhone.this,Error, Toast.LENGTH_SHORT).show();
                        phone_input.setText("");
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                    } catch (Exception e) {
                        Toast.makeText(EnterPhone.this, "Unknown Error Occured", Toast.LENGTH_SHORT).show();
                        phone_input.setText("");
                        continue_button.setEnabled(true);
                        loaddialog.dismiss();
                    }
                }


            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                phone_input.setText("");
                Toast.makeText(EnterPhone.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                continue_button.setEnabled(true);
                loaddialog.dismiss();
            }
        });

    }

    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }



}
