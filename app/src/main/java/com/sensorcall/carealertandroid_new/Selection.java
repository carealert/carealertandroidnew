package com.sensorcall.carealertandroid_new;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Selection extends AppCompatActivity {
    private static final int GALLERY_REQUEST_CODE = 2;
    Button Continue_Button;
    TextView getFromGallery;
    ImageView selection_image;
    String  ImageName;
    Bitmap bitmap;
    Boolean imageupload=false;
    ProgressDialog loaddialog,progressDialog;
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
        Continue_Button = (Button) findViewById(R.id.selection_continue);
        getFromGallery= (TextView) findViewById(R.id.get_image);

        loaddialog=new ProgressDialog(this);
        loaddialog=showLoadingDialog(this);
        loaddialog.dismiss();

        selection_image= (ImageView) findViewById(R.id.selection_image);
        CareAlert_SharedData=getSharedPreferences("CareAlert_SharedData",0);
        CareAlert_SharedData_Editor=CareAlert_SharedData.edit();

        String customstring="Add Custom Photo";
        SpannableString customstringspan= new SpannableString(customstring);
        ClickableSpan span= new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                getimagefromgallery();
            }
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        customstringspan.setSpan(span,0,16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        getFromGallery.setText(customstringspan);
        getFromGallery.setMovementMethod(LinkMovementMethod.getInstance());

        Continue_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddialog.show();
                Continue_Button.setEnabled(false);
                if (imageupload){
                    String Name= CareAlert_SharedData.getString("Name","");
                    int randomnumber= (int)(Math.random()*10000000);
                    ImageName=Name+Integer.toString(randomnumber);
                    uploadimage(ImageName);
                }
                else{

                    CareAlert_SharedData_Editor.putString("Img_Name",ImageName);
                    CareAlert_SharedData_Editor.commit();
                    openselectagepage();
                }

            }
        });

    }
    public void getimagefromgallery(){
        final CharSequence[] options = {"Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(Selection.this);
        builder.setTitle("Add Photo of Loved Ones");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent Loadimage= new Intent();
                    Loadimage.setType("image/*");
                    Loadimage.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(Loadimage,"Pick an Image"), GALLERY_REQUEST_CODE);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extras = data.getExtras();
                bitmap = (Bitmap) extras.get("data");

                RoundedBitmapDrawable RBD = RoundedBitmapDrawableFactory.create(getResources(),bitmap);
                RBD.setCircular(true);
                RBD.setAntiAlias(true);
                //RBD.setBounds(80,80,80,80);
                selection_image.setImageDrawable(RBD);
                imageupload = true;
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
                    Uri imageData = data.getData();

                    selection_image.setImageURI(imageData);
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageData);

                        RoundedBitmapDrawable RBD = RoundedBitmapDrawableFactory.create(getResources(),bitmap);
                        RBD.setCircular(true);
                        RBD.setAntiAlias(true);
                        //RBD.setBounds(80,80,80,80);
                        selection_image.setImageDrawable(RBD);
                        imageupload = true;
                    } catch (IOException e) {
                        Toast.makeText(this, "Cannot Parse Image", Toast.LENGTH_SHORT).show();
                        imageupload = false;
                    }
                }

            }
        }
    }

    public void uploadimage(final String ImageName){
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonPlaceHolderApi jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);
        String Image=convertToString();
        String Auth_token=CareAlert_SharedData.getString("Auth_Token","");
        Call<Post> call= jsonPlaceHolderApi.uploadimage("Bearer "+Auth_token,ImageName,Image);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(response.isSuccessful()){
                    CareAlert_SharedData_Editor.putString("Img_Name",ImageName);
                    CareAlert_SharedData_Editor.commit();
                    openselectagepage();
                }
                else{
                    Continue_Button.setEnabled(true);
                    loaddialog.dismiss();
                    Toast.makeText(Selection.this, "Failed to Upload Image", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Continue_Button.setEnabled(true);
                loaddialog.dismiss();
                Toast.makeText(Selection.this, "Failed to Upload Image", Toast.LENGTH_SHORT).show();

            }
        });

    }
    private String convertToString()
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgByte = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgByte,Base64.DEFAULT);
    }

    public void openselectagepage(){
        Intent selectagepage= new Intent(this,Selectage.class);
        Continue_Button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(selectagepage);
    }
    public void openselectwhopage(){
        Intent selectwho= new Intent(this,Selectwho.class);
        Continue_Button.setEnabled(true);
        loaddialog.dismiss();
        startActivity(selectwho);
    }
    public ProgressDialog showLoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }
}