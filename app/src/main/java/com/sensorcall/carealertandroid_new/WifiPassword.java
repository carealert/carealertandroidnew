package com.sensorcall.carealertandroid_new;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.BoringLayout;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sensorcall.carealertandroid_new.VoiceCallingClasses.NetConnection;
import com.google.gson.JsonObject;
import com.sensorcall.carealertandroid_new.util.AppUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.bluetooth.BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE;
import static android.bluetooth.BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE;

public class WifiPassword extends AppCompatActivity {
    BluetoothDevice Device;
    TextView ssidmsg;
    EditText wifipass;
    String ssid;
    Button continue_button;
    int Locationid;
    String Connected = "Connected";
    private Handler mHandler;
    //ProgressDialog loaddialog, progressDialog;
    String Password;
    byte[] Databyte;
    SharedPreferences CareAlert_SharedData;
    SharedPreferences.Editor CareAlert_SharedData_Editor;
    boolean ReconfigureBT = false;
    String wifiData;
    int Connection_Failed = 0, ConnectionSuccess = 1, WrongPassword = 2, DialogProgress =3;
    JsonObject WifiDatajson = new JsonObject();
    JsonObject FinalDataj = new JsonObject();
    private OutputStream outputStream;
    private InputStream inputStream;
    private static final UUID MY_UUID = UUID.fromString("DCFED827-7AD0-4D01-8A38-4F432E2DF258"), MY_UUID2 = UUID.fromString("DCFED825-7AD0-4D01-8A38-4F432E2DF258");
    boolean AddedviaSignupProcess;
    private List<BluetoothGattService> mServices;
    private Boolean isIpAddressPresent = false;
    private String deviceId = "";
    private long maxConnectionTimeout = 0;
    private long maxResponseTimeout = 0;
    private BluetoothGattCharacteristic readChar;
    private BluetoothGattDescriptor desc;
    private Dialog checkListDailog;
    private BluetoothGatt gatt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_password);
        Intent prevpage = getIntent();
        ssid = prevpage.getStringExtra("SSID");
        Device = prevpage.getExtras().getParcelable("CareAlert_Device");
        continue_button = (Button) findViewById(R.id.wifipass_continue);
//        loaddialog = new ProgressDialog(this);
//        loaddialog = showLoadingDialog(this);
//        loaddialog.dismiss();
        CareAlert_SharedData = getSharedPreferences("CareAlert_SharedData", 0);
        CareAlert_SharedData_Editor = CareAlert_SharedData.edit();
        ReconfigureBT = CareAlert_SharedData.getBoolean("ReconfigureBT", false);
        Locationid = CareAlert_SharedData.getInt("LocationId", 0);
        //loaddialog.setMessage("Loading ..");
        ssidmsg = (TextView) findViewById(R.id.enterpassword_msg);
        wifipass = (EditText) findViewById(R.id.password_input1);
        wifipass.setText(CareAlert_SharedData.getString(ssid,null));
        ssidmsg.setText("Enter the Password for '" + ssid + "' WiFi");

        findViewById(R.id.img_back).setOnClickListener(v -> {
            finish();
        });

        checkNetConnectivity();

    }

    void checkNetConnectivity() {
        if (NetConnection.checkConnection(this) == true) {
            Log.d("Checking internet", "devic123");
            setupContinue();
        } else {
            showNoInternetConnectionDialog();
            //Toast.makeText(this, "Connect Your Network", Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoInternetConnectionDialog() {
        Log.e("Testing net Connection", "Entering showNoInternetConnectionDialog Method");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Whoops! Its seems you don't have internet connection, please try again later!")
                .setTitle("No Internet Connection")
                .setCancelable(false)
                .setNeutralButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        checkNetConnectivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    public void setupContinue() {
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                if (message.what == Connection_Failed) {
                    String readMessage = null;
                    readMessage = (String) message.obj;
                    Toast.makeText(WifiPassword.this, readMessage, Toast.LENGTH_LONG).show();
                    Log.d("Returned Mes", "handleMessage: " + readMessage);
                    openbluetoothpage();
                }

                if (message.what == ConnectionSuccess) {
                    String readMessage = null;
                    readMessage = (String) message.obj;
                    Toast.makeText(WifiPassword.this, readMessage, Toast.LENGTH_LONG).show();
                    openManageDevicePage();
                }


                if (message.what == WrongPassword) {
                    String readMessage = null;
                    readMessage = (String) message.obj;
                    //loaddialog.dismiss();
                    checkListDailog.dismiss();
                    Toast.makeText(WifiPassword.this, readMessage, Toast.LENGTH_LONG).show();
                }


                if (message.what == DialogProgress) {
                    String readMessage = null;
                    readMessage = (String) message.obj;
                    checkProgressValues(message.arg1);
                }

            }
        };
        continue_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Calendar calendar = (Calendar) Calendar.getInstance().clone();
                calendar.add(Calendar.SECOND, 30);
                maxConnectionTimeout = calendar.getTimeInMillis();


                Calendar calendar1 = (Calendar) Calendar.getInstance().clone();
                calendar1.add(Calendar.SECOND, 70);
                maxResponseTimeout = calendar1.getTimeInMillis();


                Password = wifipass.getText().toString();
                WifiDatajson.addProperty("ssid", ssid);
                WifiDatajson.addProperty("pass", Password);
                WifiDatajson.addProperty("sender_id", "xyz");
                FinalDataj.add("wifi_creden", WifiDatajson);
                wifiData = FinalDataj.toString();
                //Log.i("WifiDatacred is", "onClick: "+wifiData);
                try {
                    Databyte = wifiData.getBytes("UTF-16");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                if (Password.length() > 0) {
                    //loaddialog.show();
                    continue_button.setEnabled(false);
                    gatt = Device.connectGatt(WifiPassword.this, false, gattCallback);
                    showloadingDailog();

                } else {
                    Toast.makeText(WifiPassword.this, "Password cannot be Blank", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.i("onConnectionStateChange", "Status: " + status);
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i("gattCallback", "STATE_CONNECTED");
                gatt.requestMtu(512);
            }

            if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Toast.makeText(WifiPassword.this, R.string.device_discconneted, Toast.LENGTH_SHORT).show();
                //loaddialog.dismiss();
                checkListDailog.dismiss();

            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            mServices = gatt.getServices();
            Log.i("onServicesDiscovered", mServices.toString());
            BluetoothGattCharacteristic Char = mServices.get(2).getCharacteristic(MY_UUID);
            Log.i("DATA String is ", "onServicesDiscovered: " + wifiData);


            Char.setValue(wifiData);

            gatt.writeCharacteristic(Char);

        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic
                                                 characteristic, int status) {

            Log.i("onCharacteristicRead", characteristic.toString());

            final byte[] data = characteristic.getValue();


            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data) {
                    stringBuilder.append(String.format("%02X ", byteChar));
                }
                String PassData = new String(data);
                JSONObject PassJson = new JSONObject();
                try {
                    PassJson = new JSONObject(PassData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isIpAddressPresent) {
                    readForIotChar(PassJson, gatt);
                } else {
                    readDeviceStatus(PassJson, gatt);
                }

            } else {
                mHandler.obtainMessage(Connection_Failed, 1, -1, "Failed To Connect the device please reset the device")
                        .sendToTarget();
            }


        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == 0) {
                mServices = gatt.getServices();
                BluetoothGattCharacteristic readChar = mServices.get(2).getCharacteristic(MY_UUID2);
                gatt.readCharacteristic(readChar);

            } else {
                mHandler.obtainMessage(Connection_Failed, 1, -1, "Failed To Connect the device please reset the device")
                        .sendToTarget();
                gatt.disconnect();
            }

        }


        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            //super.onMtuChanged(gatt, mtu, status);
            if (status == 0) {
                gatt.discoverServices();
            } else {
                mHandler.obtainMessage(Connection_Failed, 1, -1, "Connection to Device Failed")
                        .sendToTarget();
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.e("dsd", "pulse: " + characteristic.getStringValue(0));
            final byte[] data = characteristic.getValue();


            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data) {
                    stringBuilder.append(String.format("%02X ", byteChar));
                }
                String PassData = new String(data);
                JSONObject PassJson = new JSONObject();
                try {
                    PassJson = new JSONObject(PassData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isIpAddressPresent) {
                    readForIotChar(PassJson, gatt);
                } else {
                    readDeviceStatus(PassJson, gatt);
                }


            }


        }
    };

    private void readForIotChar(JSONObject PassJson, BluetoothGatt gatt) {

        try {
            int iotStat = PassJson.getInt("iot_stat");
            int iotReas = PassJson.getInt("iot_reas");

            if (iotStat == 0 && iotReas == 6) {
                mHandler.obtainMessage(DialogProgress, 2, -1, "").sendToTarget();
                gatt.disconnect();
                if (WifiPassword.this.ReconfigureBT) {
                    mHandler.obtainMessage(ConnectionSuccess, 1, -1, "Device Reconfigured Successfully")
                            .sendToTarget();
                } else {
                    createdevice(deviceId);

                }
            } else {
                if (System.currentTimeMillis() > maxResponseTimeout) {
                    mHandler.obtainMessage(Connection_Failed, 1, -1, "Device failed to connect the server. Please reset the device and retry")
                            .sendToTarget();
                    gatt.disconnect();
                } else {
                    gatt.readCharacteristic(mServices.get(2).getCharacteristics().get(3));
                }
            }

        } catch (JSONException e) {
            mHandler.obtainMessage(WrongPassword, 1, -1, e.toString())
                    .sendToTarget();
            e.printStackTrace();
        }
    }


    public void openbluetoothpage() {
        Intent Bluetoothpage = new Intent(this, Bluetoothdevice.class);
        //loaddialog.dismiss();
        checkListDailog.dismiss();
        continue_button.setEnabled(true);
        startActivity(Bluetoothpage);
        finish();
    }

    public void createdevice(String Device) {
        Log.d("Entered Create Device", "createdevice: ");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        String Auth_Token = CareAlert_SharedData.getString("Auth_Token", "");
        AddedviaSignupProcess = CareAlert_SharedData.getBoolean("AddedviaSignupProcess", true);
        String DeviceNickName = CareAlert_SharedData.getString("Device_Name", "");
        String Deviceroom = CareAlert_SharedData.getString("Device_Room", "");
        JsonObject Body = new JsonObject();
        Body.addProperty("DeviceNumber", Device);
        Body.addProperty("LocationId", Locationid);
        Body.addProperty("IsOn", true);
        Log.d("Location ID passed is ", "createdevice: " + Locationid);
        Body.addProperty("DeviceProfileName", DeviceNickName);
        Body.addProperty("DeviceRelatedId", Deviceroom);
        Body.addProperty("IsOn", true);

        Call<Post> call = jsonPlaceHolderApi.createdevice("Bearer " + Auth_Token, Body);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (response.isSuccessful()) {
                    cacheDevice();
                    checkProgressValues(3);
                    checkListDailog.dismiss();
                    CareAlert_SharedData_Editor.putString(ssid,wifipass.getText().toString());
                    CareAlert_SharedData_Editor.commit();
                    if (AddedviaSignupProcess) {
                        openwificonnectsuccess();
                    } else {
                        openManageDevicePage();
                    }
                } else {
                    checkListDailog.dismiss();
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String Error = jObjError.getString("message");
                        continue_button.setEnabled(true);
                        //loaddialog.dismiss();
                        gatt.disconnect();
                        gatt.close();
                        openbluetoothpage();
                        Toast.makeText(WifiPassword.this, Error, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        continue_button.setEnabled(true);
                        //loaddialog.dismiss();

                        openbluetoothpage();
                        Toast.makeText(WifiPassword.this, "Unknown Error", Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });
    }

    private void cacheDevice() {
        // store the device for session so that it will not show in the bluethooth list again
        AppUtil.pairedDevice = this.Device;
    }


    public void openManageDevicePage() {
        CareAlert_SharedData_Editor.putBoolean("ReconfigureBT", false);
        Intent ManageDevicePage = new Intent(this, ManageDevices.class);
        ManageDevicePage.putExtra("DefaultLocationId", Locationid);
        continue_button.setEnabled(true);
       // loaddialog.dismiss();
        checkListDailog.dismiss();
        startActivity(ManageDevicePage);
    }

    public void openwificonnectsuccess() {
        Intent wificonnectsuccess = new Intent(this, Wificonnectsuccess.class);
        continue_button.setEnabled(true);
        //loaddialog.dismiss();
        checkListDailog.dismiss();
        startActivity(wificonnectsuccess);
    }

//    public ProgressDialog showLoadingDialog(Context context) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.show();
//        if (progressDialog.getWindow() != null) {
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        }
//        progressDialog.setContentView(R.layout.progress_dialog);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setCancelable(false);
//        progressDialog.setCanceledOnTouchOutside(false);
//        return progressDialog;
//    }


    private void readDeviceStatus(JSONObject PassJson, BluetoothGatt gatt) {

        Log.d("THe Device Status is", "onCharacteristicRead: " + PassJson.toString());
        String Status = "";
        try {
            Status = PassJson.getString("status");
            deviceId = PassJson.getString("deviceId");

            Log.d("Status is ", "onCharacteristicRead: " + Status);


            if (Status.equals(Connected)) {
                isIpAddressPresent = PassJson.has("ip_addr");
                mHandler.obtainMessage(DialogProgress, 1, -1, "")
                        .sendToTarget();
                if (isIpAddressPresent) {
                    gatt.readCharacteristic(mServices.get(2).getCharacteristics().get(3));
                } else {
                    mHandler.obtainMessage(DialogProgress, 2, -1, "").sendToTarget();
                    if (ReconfigureBT) {
                        mHandler.obtainMessage(ConnectionSuccess, 1, -1, "Device Reconfigured Successfully")
                                .sendToTarget();
                    } else {

                        gatt.disconnect();
                        createdevice(deviceId);
                    }
                }
            } else {
                if (System.currentTimeMillis() > maxConnectionTimeout) {
                    mHandler.obtainMessage(WrongPassword, 1, -1, "Invalid Credentials couldn't connect to Wifi")
                            .sendToTarget();
                    gatt.disconnect();
                    continue_button.setEnabled(true);
                } else {
                    {
                        mServices = gatt.getServices();
                        BluetoothGattCharacteristic readChar = mServices.get(2).getCharacteristic(MY_UUID2);
                        gatt.readCharacteristic(readChar);
                    }
                }
            }
        } catch (JSONException e) {
            continue_button.setEnabled(true);
            mHandler.obtainMessage(WrongPassword, 1, -1, e.toString())
                    .sendToTarget();
            gatt.disconnect();

        }
    }


    private void showloadingDailog(){
        checkListDailog = new Dialog(this);
        checkListDailog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        checkListDailog.setCancelable(false);
        checkListDailog.setContentView(R.layout.dialog_bluethhoth_progress);

        if(this.ReconfigureBT) {
            checkListDailog.findViewById(R.id.check_adding_device_to_account).setVisibility(View.GONE);
            checkListDailog.findViewById(R.id.txt_adding_device_to_account).setVisibility(View.GONE);
            checkListDailog.findViewById(R.id.progress_adding_device_to_account).setVisibility(View.GONE);

        }

        checkListDailog.show();
    }

    private void checkProgressValues(int value){
        switch (value){
            case 1:  AppCompatImageView credintialCheck = (AppCompatImageView) checkListDailog.findViewById(R.id.check_sucess_device_password);
                ProgressBar credintialCheckProgress = (ProgressBar) checkListDailog.findViewById(R.id.progress_create_device_password);
                credintialCheck.setVisibility(View.VISIBLE);
                credintialCheckProgress.setVisibility(View.INVISIBLE);
                break;

            case 2: AppCompatImageView connectingDeviceCheck = (AppCompatImageView) checkListDailog.findViewById(R.id.check_connecting_device_to_server);
                ProgressBar connectingDeviceProgress = (ProgressBar) checkListDailog.findViewById(R.id.progress_connecting_device_to_server);
                connectingDeviceCheck.setVisibility(View.VISIBLE);
                connectingDeviceProgress.setVisibility(View.INVISIBLE);
                break;


            case 3:  AppCompatImageView addingDeviceCheck = (AppCompatImageView) checkListDailog.findViewById(R.id.check_adding_device_to_account);
                ProgressBar addingDeviceProgress = (ProgressBar) checkListDailog.findViewById(R.id.progress_adding_device_to_account);
                addingDeviceCheck.setVisibility(View.VISIBLE);
                addingDeviceProgress.setVisibility(View.INVISIBLE);
                break;
        }

    }


}

