package com.sensorcall.carealertandroid_new;

import android.app.Activity;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class GetLocation implements Parcelable {


        int LocationId;
        String Key;
        String TermsandConditions;
        String Disclaimer;
        String DeviceNumber;

    protected GetLocation(Parcel in) {
        LocationId = in.readInt();
        Key = in.readString();
        TermsandConditions = in.readString();
        Disclaimer = in.readString();
        DeviceNumber = in.readString();
        DataType = in.readString();
        Question = in.readString();
        LocationName = in.readString();
        LocationAddress = in.readString();
        GeoLocation = in.readString();
        IsDefaultLocationId = in.readByte() != 0;
        IsEmergencyLightEnabled = in.readByte() != 0;
        IsNightLightEnabled = in.readByte() != 0;
        OnMotionDetected = in.readByte() != 0;
        IsOnline = in.readByte() != 0;
        HealthStatus = in.readInt();
        Activity = in.readString();
        Confidence = in.readInt();
        DeviceName = in.readString();
        Time = in.readString();
        BaseUTCOffsetHour = in.readFloat();
        ActivityId = in.readInt();
        Presence = in.readInt();
    }

    public static final Creator<GetLocation> CREATOR = new Creator<GetLocation>() {
        @Override
        public GetLocation createFromParcel(Parcel in) {
            return new GetLocation(in);
        }

        @Override
        public GetLocation[] newArray(int size) {
            return new GetLocation[size];
        }
    };

    public String getTermsandConditions() {
        return TermsandConditions;
    }

    public String getDisclaimer() {
        return Disclaimer;
    }

    public String getDeviceNumber() {
        return DeviceNumber;
    }

    public String getKey() {
        return Key;
    }

    public String getDataType() {
        return DataType;
    }

    public String getQuestion() {
        return Question;
    }

    String DataType;
        String Question;
        String LocationName;
        String LocationAddress, GeoLocation;
        boolean IsDefaultLocationId, IsEmergencyLightEnabled, IsNightLightEnabled,
                OnMotionDetected, IsOnline;
        int HealthStatus;
        String Activity;
        int Confidence;
        String DeviceName;

    public int getActivityId() {
        return ActivityId;
    }

    String Time;
        float BaseUTCOffsetHour;
        int ActivityId;
        int Presence;

    public int getPresence() {
        return Presence;
    }

    public String getActivity() {
        return Activity;
    }

    public int getConfidence() {
        return Confidence;
    }

    public String getDeviceName() {
        return DeviceName;
    }

    public String getTime() {
        return Time;
    }

    public List<LovedOnes> getLovedOne() {
        return LovedOne;
    }

    ;
        List<LovedOnes> LovedOne;


//        public GetLocation(String locationName, String locationAddress, int LocationId, String Actname, int conf, int Actno, String DateTime, String Device) {
//            LocationName = locationName;
//            LocationAddress = locationAddress;
//            LocationId= LocationId;
//            ActivityId=Actno;
//            Activity= Actname;
//            Confidence=conf;
//            Time=DateTime;
//            DeviceName=Device;
//
//
//        }


        public boolean isDefaultLocationId() {
            return IsDefaultLocationId;
        }

        public boolean isEmergencyLightEnabled() {
            return IsEmergencyLightEnabled;
        }

        public boolean isNightLightEnabled() {
            return IsNightLightEnabled;
        }

        public boolean isOnMotionDetected() {
            return OnMotionDetected;
        }

        public int getLocationId() {
            return LocationId;
        }

    public float getBaseUTCOffsetHour() {
        return BaseUTCOffsetHour;
    }

    public String getLocationName() {
            return LocationName;
        }

        public String getLocationAddress() {
            return LocationAddress;
        }

        public String getGeoLocation() {
            return GeoLocation;
        }

        public boolean isOnline() {
            return IsOnline;
        }

        public void setLocationAddress(String locationAddress) {
            LocationAddress = locationAddress;
        }

        public void setLocationName(String locationName) {
            LocationName = locationName;
        }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(LocationId);
        dest.writeString(Key);
        dest.writeString(TermsandConditions);
        dest.writeString(Disclaimer);
        dest.writeString(DeviceNumber);
        dest.writeString(DataType);
        dest.writeString(Question);
        dest.writeString(LocationName);
        dest.writeString(LocationAddress);
        dest.writeString(GeoLocation);
        dest.writeByte((byte) (IsDefaultLocationId ? 1 : 0));
        dest.writeByte((byte) (IsEmergencyLightEnabled ? 1 : 0));
        dest.writeByte((byte) (IsNightLightEnabled ? 1 : 0));
        dest.writeByte((byte) (OnMotionDetected ? 1 : 0));
        dest.writeByte((byte) (IsOnline ? 1 : 0));
        dest.writeInt(HealthStatus);
        dest.writeString(Activity);
        dest.writeInt(Confidence);
        dest.writeString(DeviceName);
        dest.writeString(Time);
        dest.writeFloat(BaseUTCOffsetHour);
        dest.writeInt(ActivityId);
        dest.writeInt(Presence);
    }
}

